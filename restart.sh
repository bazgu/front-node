#!/bin/bash
cd `dirname $BASH_SOURCE`

echo 'INFO Testing config'
node config.js || exit 1

./stop.sh
./rotate.sh

echo 'INFO Starting'
node forever.js >> log/index.out 2>> log/index.err &
echo $! > index.pid
sleep 0.2
cat log/index.out log/index.err
