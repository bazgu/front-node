var fs = require('fs'),
    uglifyJs = require('uglify-js')

module.exports = source => {

    fs.writeFileSync('combined.js', source)

    var result = uglifyJs.minify('combined.js', {
        outSourceMap: 'compressed.js.map',
        output: { max_line_len: 1024 },
    })

    fs.writeFileSync('compressed.js', result.code)
    fs.writeFileSync('compressed.js.map', result.map)

}
