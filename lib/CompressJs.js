const fs = require('fs'),
    uglifyJs = require('uglify-js')

module.exports = source => {

    fs.writeFileSync('combined.js', source)

    const result = uglifyJs.minify({
        'combined.js': source,
    }, {
        sourceMap: { url: 'compressed.js.map' },
        output: { max_line_len: 1024 },
    })

    fs.writeFileSync('compressed.js', result.code)
    fs.writeFileSync('compressed.js.map', result.map)

}
