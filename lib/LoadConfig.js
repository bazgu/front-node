var http = require('http')

var Log = require('./Log.js'),
    ReadText = require('./ReadText.js')

var configNode = require('../config.js').configNode

var host = configNode.host,
    port = configNode.port

var logPrefix = 'config-node-client: ' + host + ':' + port + ': get: '

module.exports = callback => {

    function errorListener (err) {
        Log.fatal(logPrefix + err.code)
    }

    var proxyReq = http.get({
        host: host,
        port: port,
        path: '/get',
    }, proxyRes => {

        proxyReq.removeListener('error', errorListener)

        var statusCode = proxyRes.statusCode
        if (statusCode !== 200) {
            Log.fatal(logPrefix + 'HTTP status code ' + statusCode)
        }

        ReadText(proxyRes, responseText => {

            try {
                var response = JSON.parse(responseText)
            } catch (e) {
                Log.fatal(logPrefix + 'Invalid JSON document: ' + JSON.stringify(responseText))
            }

            callback(response)

        })

    })
    proxyReq.on('error', errorListener)

}
