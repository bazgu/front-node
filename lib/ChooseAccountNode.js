var crypto = require('crypto')

module.exports = loadedConfig => {

    var accountNodes = loadedConfig.accountNodes.map(accountNode => {

        var host = accountNode.host,
            port = accountNode.port

        return {
            host: host,
            port: port,
            logPrefix: 'account-node-client: ' + host + ':' + port + ': ',
        }

    })

    return username => {
        if (username === undefined) return
        username = username.toLowerCase()
        var digest = crypto.createHash('md5').update(username).digest()
        return accountNodes[digest.readUInt32LE(0) % accountNodes.length]
    }

}
