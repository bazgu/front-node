const crypto = require('crypto')

module.exports = loadedConfig => {

    const accountNodes = loadedConfig.accountNodes.map(accountNode => {

        const host = accountNode.host,
            port = accountNode.port

        return {
            host, port,
            logPrefix: 'account-node: ' + host + ':' + port + ': ',
        }

    })

    return username => {
        if (username === undefined) return
        username = username.toLowerCase()
        const digest = crypto.createHash('md5').update(username).digest()
        return accountNodes[digest.readUInt32LE(0) % accountNodes.length]
    }

}
