module.exports = app => request => {
    app.EchoText(request, {
        type: 'text/html; charset=UTF-8',
        content:
            '<!DOCTYPE html>' +
            '<html>' +
                '<head>' +
                    '<title>Bazgu &middot; The Free Web Messenger</title>' +
                    '<meta name="viewport" content="width=device-width, user-scalable=no" />' +
                    '<meta charset="UTF-8" />' +
                    '<link rel="icon" href="img/icon/16.png" sizes="16x16" />' +
                    '<link rel="icon" href="img/icon/32.png" sizes="32x32" />' +
                    '<link rel="icon" href="img/icon/48.png" sizes="48x48" />' +
                    '<link rel="icon" href="img/icon/64.png" sizes="64x64" />' +
                    '<link rel="icon" href="img/icon/128.png" sizes="128x128" />' +
                    '<link rel="icon" href="img/icon/256.png" sizes="256x256" />' +
                    '<link rel="icon" href="img/icon/512.png" sizes="512x512" />' +
                    app.NoScriptHead() +
                '</head>' +
                '<body>' +
                    '<script src="compressed.js?' + app.EchoHtml.Revisions['compressed.js'] + '">' +
                    '</script>' +
                    app.NoScriptBody() +
                '</body>' +
            '</html>',
    })
}
