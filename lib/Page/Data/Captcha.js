var http = require('http')

var Error500Page = require('../../Error500Page.js'),
    Log = require('../../Log.js')

module.exports = loadedConfig => {

    var captchaNodes = loadedConfig.captchaNodes.map(captchaNode => {

        var host = captchaNode.host,
            port = captchaNode.port

        return {
            host: host,
            port: port,
            logPrefix: 'captcha-node-client: ' + host + ':' + port + ': frontNode/get: ',
        }

    })

    return (req, res) => {

        function closeListener () {
            proxyReq.removeListener('error', errorListener)
            proxyReq.on('error', () => {})
            proxyReq.abort()
        }

        function errorListener (err) {
            Log.error(logPrefix + err.code)
            Error500Page(res)
        }

        var index = Math.floor(Math.random() * captchaNodes.length)
        var captchaNode = captchaNodes[index]

        var host = captchaNode.host,
            port = captchaNode.port

        var logPrefix = captchaNode.logPrefix

        var proxyReq = http.get({
            host: host,
            port: port,
            path: '/frontNode/get?prefix=' + index,
        }, proxyRes => {

            proxyReq.removeListener('error', errorListener)
            proxyReq.on('error', () => {})

            req.removeListener('close', closeListener)
            req.on('close', () => {
                proxyReq.abort()
            })

            var statusCode = proxyRes.statusCode
            if (statusCode !== 200) {
                Log.error(logPrefix + 'HTTP status code ' + statusCode)
                Error500Page(res)
                return
            }

            res.setHeader('Content-Type', 'application/json')
            proxyRes.pipe(res)
            proxyRes.on('error', err => {
                Log.error(logPrefix + 'Transfer error: ' + err.code)
            })

        })
        proxyReq.on('error', errorListener)

        req.on('close', closeListener)

    }

}
