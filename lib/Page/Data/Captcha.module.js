const http = require('http')

module.exports = app => loadedConfig => {

    const nodes = loadedConfig.captchaNodes.map(node => ({
        ...node,
        log: app.log.sublog('captcha-node: ' + node.host + ':' + node.port + ': frontNode/get:'),
    }))

    return request => {

        function proxy_req_error (err) {
            log.error('Failed: ' + err.code)
            app.Error500Page(request)
        }

        const index = Math.floor(Math.random() * nodes.length)
        const node = nodes[index]
        const log = node.log

        const proxy_req = http.get({
            host: node.host,
            port: node.port,
            path: '/frontNode/get?' + app.BuildQuery({ prefix: index }),
        }, proxy_res => {

            const code = proxy_res.statusCode
            if (code !== 200) {
                log.error('HTTP status code ' + code)
                app.Error500Page(request)
                return
            }

            const res = request.res
            res.setHeader('Content-Type', 'application/json')

            proxy_req.removeListener('error', proxy_req_error)
            proxy_req.on('error', () => {})

            proxy_res.pipe(res)

        })
        proxy_req.on('error', proxy_req_error)

    }

}
