const http = require('http')

module.exports = app => chooseFileNode => request => {

    const { req, res } = request

    const token = request.parsed_url.query.token
    const fileNode = chooseFileNode(token)
    if (fileNode === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    ;(() => {

        function errorListener (err) {
            app.log.error(logPrefix + err.code)
            app.Error500Page(request)
        }

        function req_error () {
            proxyReq.removeListener('error', errorListener)
            proxyReq.on('error', () => {})
            proxyReq.abort()
        }

        const logPrefix = fileNode.logPrefix + 'frontNode/feed: '

        const proxyReq = http.request({
            host: fileNode.host,
            port: fileNode.port,
            path: '/frontNode/feed?token=' + encodeURIComponent(token),
            method: 'post',
            headers: { Expect: '100-continue' },
        }, proxyRes => {

            proxyReq.removeListener('error', errorListener)
            proxyReq.on('error', () => {})

            req.removeListener('error', req_error)
            req.on('error', () => {
                proxyReq.abort()
            })

            const statusCode = proxyRes.statusCode
            if (statusCode !== 200) {
                app.log.error(logPrefix + 'HTTP status code ' + statusCode)
                app.Error500Page(request)
                return
            }

            proxyRes.pipe(res)
            proxyRes.on('error', err => {
                app.log.error(logPrefix + 'Transfer error: ' + err.code)
            })

        })
        proxyReq.on('continue', () => {
            req.pipe(proxyReq)
        })
        proxyReq.on('error', errorListener)

        req.on('error', req_error)

    })()

}
