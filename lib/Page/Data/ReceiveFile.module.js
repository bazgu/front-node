const http = require('http')

module.exports = app => chooseFileNode => request => {

    const { req, res } = request

    const token = request.parsed_url.query.token
    const fileNode = chooseFileNode(token)
    if (fileNode === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    ;(() => {

        function closeListener () {
            proxyReq.removeListener('error', errorListener)
            proxyReq.on('error', () => {})
            proxyReq.abort()
        }

        function errorListener (err) {
            app.log.error(logPrefix + err.code)
            app.Error500Page(request)
        }

        const logPrefix = fileNode.logPrefix + 'frontNode/receive: '

        const proxyReq = http.get({
            host: fileNode.host,
            port: fileNode.port,
            path: '/frontNode/receive?token=' + encodeURIComponent(token),
        }, proxyRes => {

            proxyReq.removeListener('error', errorListener)
            proxyReq.on('error', () => {})

            req.removeListener('close', closeListener)
            req.on('close', () => {
                proxyReq.abort()
            })

            const statusCode = proxyRes.statusCode
            if (statusCode !== 200) {
                app.log.error(logPrefix + 'HTTP status code ' + statusCode)
                app.Error500Page(request)
                return
            }

            const headers = proxyRes.headers
            res.setHeader('Content-Type', headers['content-type'])

            const contentDisposition = headers['content-disposition']
            if (contentDisposition !== undefined) {
                res.setHeader('Content-Disposition', contentDisposition)
            }

            const contentLength = headers['content-length']
            if (contentLength !== undefined) {
                res.setHeader('Content-Length', contentLength)
            }

            proxyRes.pipe(res)
            proxyRes.on('error', err => {
                app.log.error(logPrefix + 'Transfer error: ' + err.code)
            })

        })
        proxyReq.on('error', errorListener)

        req.on('close', closeListener)

    })()

}
