var http = require('http')

var Error500Page = require('../../Error500Page.js'),
    Log = require('../../Log.js')

module.exports = chooseFileNode => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var token = parsedUrl.query.token
        var fileNode = chooseFileNode(token)
        if (fileNode === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        ;(() => {

            function closeListener () {
                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})
                proxyReq.abort()
            }

            function errorListener (err) {
                Log.error(logPrefix + err.code)
                Error500Page(res)
            }

            var logPrefix = fileNode.logPrefix + 'frontNode/receive: '

            var proxyReq = http.get({
                host: fileNode.host,
                port: fileNode.port,
                path: '/frontNode/receive?token=' + encodeURIComponent(token),
            }, proxyRes => {

                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})

                req.removeListener('close', closeListener)
                req.on('close', () => {
                    proxyReq.abort()
                })

                var statusCode = proxyRes.statusCode
                if (statusCode !== 200) {
                    Log.error(logPrefix + 'HTTP status code ' + statusCode)
                    Error500Page(res)
                    return
                }

                var headers = proxyRes.headers
                res.setHeader('Content-Type', headers['content-type'])

                var contentDisposition = headers['content-disposition']
                if (contentDisposition !== undefined) {
                    res.setHeader('Content-Disposition', contentDisposition)
                }

                var contentLength = headers['content-length']
                if (contentLength !== undefined) {
                    res.setHeader('Content-Length', contentLength)
                }

                proxyRes.pipe(res)
                proxyRes.on('error', err => {
                    Log.error(logPrefix + 'Transfer error: ' + err.code)
                })

            })
            proxyReq.on('error', errorListener)

            req.on('close', closeListener)

        })()

    }
}
