var http = require('http')

var Error500Page = require('../../Error500Page.js'),
    Log = require('../../Log.js')

module.exports = chooseFileNode => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var token = parsedUrl.query.token
        var fileNode = chooseFileNode(token)
        if (fileNode === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        ;(() => {

            function closeListener () {
                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})
                proxyReq.abort()
            }

            function errorListener (err) {
                Log.error(logPrefix + err.code)
                Error500Page(res)
            }

            var logPrefix = fileNode.logPrefix + 'frontNode/feed: '

            var proxyReq = http.request({
                host: fileNode.host,
                port: fileNode.port,
                path: '/frontNode/feed?token=' + encodeURIComponent(token),
                method: 'post',
                headers: { Expect: '100-continue' },
            }, proxyRes => {

                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})

                req.removeListener('close', closeListener)
                req.on('close', () => {
                    proxyReq.abort()
                })

                var statusCode = proxyRes.statusCode
                if (statusCode !== 200) {
                    Log.error(logPrefix + 'HTTP status code ' + statusCode)
                    Error500Page(res)
                    return
                }

                proxyRes.pipe(res)
                proxyRes.on('error', err => {
                    Log.error(logPrefix + 'Transfer error: ' + err.code)
                })

            })
            proxyReq.on('continue', () => {
                req.pipe(proxyReq)
            })
            proxyReq.on('error', errorListener)

            req.on('close', closeListener)

        })()

    }
}
