var http = require('http'),
    querystring = require('querystring')

var Error500Page = require('../../../Error500Page.js'),
    Log = require('../../../Log.js')

module.exports = methodSuffix => {
    var method = 'frontNode/' + methodSuffix
    var pathPrefix = '/' + method + '?'
    return chooseSessionNode => {
        return (req, res, parsedUrl) => {

            res.setHeader('Content-Type', 'application/json')

            var query = parsedUrl.query

            var sessionNode = chooseSessionNode(query.token)
            if (sessionNode === undefined) {
                res.end('"INVALID_TOKEN"')
                return
            }

            ;(() => {

                function closeListener () {
                    proxyReq.removeListener('error', errorListener)
                    proxyReq.on('error', () => {})
                    proxyReq.abort()
                }

                function errorListener (err) {
                    Log.error(logPrefix + err.code)
                    Error500Page(res)
                }

                var logPrefix = sessionNode.logPrefix + method + ': '

                var proxyReq = http.get({
                    host: sessionNode.host,
                    port: sessionNode.port,
                    path: pathPrefix + querystring.stringify(query),
                }, proxyRes => {

                    proxyReq.removeListener('error', errorListener)
                    proxyReq.on('error', () => {})

                    req.removeListener('close', closeListener)
                    req.on('close', () => {
                        proxyReq.abort()
                    })

                    var statusCode = proxyRes.statusCode
                    if (statusCode !== 200) {
                        Log.error(logPrefix + 'HTTP status code ' + statusCode)
                        Error500Page(res)
                        return
                    }

                    proxyRes.pipe(res)
                    proxyRes.on('error', err => {
                        Log.error(logPrefix + 'Transfer error: ' + err.code)
                    })

                })
                proxyReq.on('error', errorListener)

                req.on('close', closeListener)

            })()

        }
    }
}
