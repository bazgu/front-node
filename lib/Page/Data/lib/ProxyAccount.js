var http = require('http'),
    querystring = require('querystring')

var Error500Page = require('../../../Error500Page.js'),
    Log = require('../../../Log.js')

module.exports = methodSuffix => {
    var method = 'frontNode/' + methodSuffix
    var pathPrefix = '/' + method + '?'
    return chooseAccountNode => {
        return (req, res, parsedUrl) => {

            res.setHeader('Content-Type', 'application/json')

            var query = parsedUrl.query

            var accountNode = chooseAccountNode(query.username)
            if (accountNode === undefined) {
                res.end('"INVALID_USERNAME"')
                return
            }

            ;(() => {

                function closeListener () {
                    proxyReq.removeListener('error', errorListener)
                    proxyReq.on('error', () => {})
                    proxyReq.abort()
                }

                function errorListener (err) {
                    Log.error(logPrefix + err.code)
                    Error500Page(res)
                }

                var logPrefix = accountNode.logPrefix + method + ': '

                var proxyReq = http.get({
                    host: accountNode.host,
                    port: accountNode.port,
                    path: pathPrefix + querystring.stringify(query),
                }, proxyRes => {

                    proxyReq.removeListener('error', errorListener)
                    proxyReq.on('error', () => {})

                    req.removeListener('close', closeListener)
                    req.on('close', () => {
                        proxyReq.abort()
                    })

                    var statusCode = proxyRes.statusCode
                    if (statusCode !== 200) {
                        Log.error(logPrefix + 'HTTP status code ' + statusCode)
                        Error500Page(res)
                        return
                    }

                    proxyRes.pipe(res)
                    proxyRes.on('error', err => {
                        Log.error(logPrefix + 'Transfer error: ' + err.code)
                    })

                })
                proxyReq.on('error', errorListener)

                req.on('close', closeListener)

            })()

        }
    }
}
