var GzipPage = require('../GzipPage.js'),
    IndexPageCssFiles = require('../IndexPageCssFiles.js'),
    IndexPageJsFiles = require('../IndexPageJsFiles.js'),
    NoScriptBody = require('../NoScriptBody.js'),
    NoScriptHead = require('../NoScriptHead.js')

var content =
    '<!DOCTYPE html>' +
    '<html>' +
        '<head>' +
            '<title>Bazgu &middot; The Free Web Messenger</title>' +
            '<meta name="viewport" content="width=device-width, user-scalable=no" />' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
            '<link rel="icon" type="image/png" href="img/icon/16.png" sizes="16x16" />' +
            '<link rel="icon" type="image/png" href="img/icon/32.png" sizes="32x32" />' +
            '<link rel="icon" type="image/png" href="img/icon/48.png" sizes="48x48" />' +
            '<link rel="icon" type="image/png" href="img/icon/64.png" sizes="64x64" />' +
            '<link rel="icon" type="image/png" href="img/icon/128.png" sizes="128x128" />' +
            '<link rel="icon" type="image/png" href="img/icon/256.png" sizes="256x256" />' +
            '<link rel="icon" type="image/png" href="img/icon/512.png" sizes="512x512" />' +
            IndexPageCssFiles.map(file => {
                return '<link rel="stylesheet" type="text/css" href="css/' + file + '.css" />'
            }).join('') +
        '</head>' +
        '<body>' +
            '<script type="text/javascript">' +
                'function getResourceUrl (url) {' +
                    'return url' +
                '}' +
            '</script>' +
            IndexPageJsFiles.map(file => {
                return '<script type="text/javascript" src="js/' + file + '.js"></script>'
            }).join('') +
            NoScriptBody() +
        '</body>' +
    '</html>'

module.exports = GzipPage(content, 'no-cache', 'text/html; charset=UTF-8')
