var GzipPage = require('../GzipPage.js')

var content = JSON.stringify({
    name: 'Bazgu',
    version: '3.0',
    description: 'The free web messenger.',
    launch_path: '/',
    developer: {
        name: 'Bazgu Team',
        url: 'https://bazgu.com/',
    },
    icons: {
        '16': '/img/icon/16.png',
        '32': '/img/icon/32.png',
        '48': '/img/icon/48.png',
        '64': '/img/icon/64.png',
        '90': '/img/icon/90.png',
        '120': '/img/icon/120.png',
        '128': '/img/icon/128.png',
        '256': '/img/icon/256.png',
        '512': '/img/icon/512.png',
    },
})

module.exports = GzipPage(content, 'no-cache', 'application/x-web-app-manifest+json')
