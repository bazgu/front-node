module.exports = version => {

    var content = JSON.stringify({
        software: 'front-node',
        version: version,
    })

    return (req, res) => {
        res.setHeader('Content-Type', 'application/json')
        res.end(content)
    }

}
