var GzipPage = require('../GzipPage.js'),
    NoScriptBody = require('../NoScriptBody.js'),
    NoScriptHead = require('../NoScriptHead.js'),
    Revisions = require('../Revisions.js')

var content =
    '<!DOCTYPE html>' +
    '<html>' +
        '<head>' +
            '<title>Bazgu &middot; The Free Web Messenger</title>' +
            '<meta name="viewport" content="width=device-width, user-scalable=no" />' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
            '<link rel="icon" type="image/png" href="img/icon/16.png" sizes="16x16" />' +
            '<link rel="icon" type="image/png" href="img/icon/32.png" sizes="32x32" />' +
            '<link rel="icon" type="image/png" href="img/icon/48.png" sizes="48x48" />' +
            '<link rel="icon" type="image/png" href="img/icon/64.png" sizes="64x64" />' +
            '<link rel="icon" type="image/png" href="img/icon/128.png" sizes="128x128" />' +
            '<link rel="icon" type="image/png" href="img/icon/256.png" sizes="256x256" />' +
            '<link rel="icon" type="image/png" href="img/icon/512.png" sizes="512x512" />' +
            NoScriptHead() +
        '</head>' +
        '<body>' +
            '<script type="text/javascript"' +
            ' src="etc/compressed.js?' + Revisions['etc/compressed.js'] + '">' +
            '</script>' +
            NoScriptBody() +
        '</body>' +
    '</html>'

module.exports = GzipPage(content, 'no-cache', 'text/html; charset=UTF-8')
