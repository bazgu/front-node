const http = require('http'),
    querystring = require('querystring')

module.exports = app => methodSuffix => {
    const method = 'frontNode/' + methodSuffix
    const pathPrefix = '/' + method + '?'
    return chooseAccountNode => request => {

        const { req, res } = request

        const query = request.parsed_url.query

        const accountNode = chooseAccountNode(query.username)
        if (accountNode === undefined) {
            request.respond('INVALID_USERNAME')
            return
        }

        ;(() => {

            function closeListener () {
                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})
                proxyReq.abort()
            }

            function errorListener (err) {
                app.log.error(logPrefix + err.code)
                app.Error500Page(request)
            }

            const logPrefix = accountNode.logPrefix + method + ': '

            const proxyReq = http.get({
                host: accountNode.host,
                port: accountNode.port,
                path: pathPrefix + querystring.stringify(query),
            }, proxyRes => {

                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})

                req.removeListener('close', closeListener)
                req.on('close', () => {
                    proxyReq.abort()
                })

                const statusCode = proxyRes.statusCode
                if (statusCode !== 200) {
                    app.log.error(logPrefix + 'HTTP status code ' + statusCode)
                    app.Error500Page(request)
                    return
                }

                proxyRes.pipe(res)
                proxyRes.on('error', err => {
                    app.log.error(logPrefix + 'Transfer error: ' + err.code)
                })

            })
            proxyReq.on('error', errorListener)

            req.on('close', closeListener)

        })()

    }
}
