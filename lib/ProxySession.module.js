const http = require('http'),
    querystring = require('querystring')

module.exports = app => methodSuffix => {
    const method = 'frontNode/' + methodSuffix
    const pathPrefix = '/' + method + '?'
    return chooseSessionNode => request => {

        const { req, res } = request

        const query = request.parsed_url.query

        const sessionNode = chooseSessionNode(query.token)
        if (sessionNode === undefined) {
            request.respond('INVALID_TOKEN')
            return
        }

        ;(() => {

            function closeListener () {
                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})
                proxyReq.abort()
            }

            function errorListener (err) {
                app.log.error(logPrefix + err.code)
                app.Error500Page(request)
            }

            const logPrefix = sessionNode.logPrefix + method + ': '

            const proxyReq = http.get({
                host: sessionNode.host,
                port: sessionNode.port,
                path: pathPrefix + querystring.stringify(query),
            }, proxyRes => {

                proxyReq.removeListener('error', errorListener)
                proxyReq.on('error', () => {})

                req.removeListener('close', closeListener)
                req.on('close', () => {
                    proxyReq.abort()
                })

                const statusCode = proxyRes.statusCode
                if (statusCode !== 200) {
                    app.log.error(logPrefix + 'HTTP status code ' + statusCode)
                    app.Error500Page(request)
                    return
                }

                proxyRes.pipe(res)
                proxyRes.on('error', err => {
                    app.log.error(logPrefix + 'Transfer error: ' + err.code)
                })

            })
            proxyReq.on('error', errorListener)

            req.on('close', closeListener)

        })()

    }
}
