module.exports = () => {
    return '<noscript>' +
            '<link rel="stylesheet" type="text/css" href="css/Main.css">' +
            '<link rel="stylesheet" type="text/css" href="css/NoScript.css">' +
        '</noscript>'
}
