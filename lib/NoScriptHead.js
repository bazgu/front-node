module.exports = () => (
    '<noscript>' +
        '<link rel="stylesheet" href="css/Main.css">' +
        '<link rel="stylesheet" href="css/NoScript.css">' +
    '</noscript>'
)
