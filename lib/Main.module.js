module.exports = app => () => {

    app.LoadConfig(loadedConfig => {

        const url = require('url')

        const chooseAccountNode = app.ChooseAccountNode(loadedConfig),
            chooseFileNode = app.ChooseFileNode(loadedConfig),
            chooseSessionNode = app.ChooseSessionNode(loadedConfig)

        const pages = Object.create(null)
        pages['/'] = app.Page.Index
        pages['/debug'] = app.Page.Debug
        pages['/data/addContact'] = app.ProxySession('addContact')(chooseSessionNode)
        pages['/data/captcha'] = app.Page.Data.Captcha(loadedConfig)
        pages['/data/changePassword'] = app.ProxySession('changePassword')(chooseSessionNode)
        pages['/data/editProfile'] = app.ProxySession('editProfile')(chooseSessionNode)
        pages['/data/feedFile'] = app.Page.Data.FeedFile(chooseFileNode)
        pages['/data/ignoreRequest'] = app.ProxySession('ignoreRequest')(chooseFileNode)
        pages['/data/overrideContactProfile'] = app.ProxySession('overrideContactProfile')(chooseSessionNode)
        pages['/data/pullMessages'] = app.ProxySession('pullMessages')(chooseSessionNode)
        pages['/data/receiveFile'] = app.Page.Data.ReceiveFile(chooseFileNode)
        pages['/data/removeContact'] = app.ProxySession('removeContact')(chooseSessionNode)
        pages['/data/removeRequest'] = app.ProxySession('removeRequest')(chooseSessionNode)
        pages['/data/restoreSession'] = app.ProxyAccount('restoreSession')(chooseAccountNode)
        pages['/data/sendFileMessage'] = app.ProxySession('sendFileMessage')(chooseSessionNode)
        pages['/data/sendTextMessage'] = app.ProxySession('sendTextMessage')(chooseSessionNode)
        pages['/data/signIn'] = app.ProxyAccount('signIn')(chooseAccountNode)
        pages['/data/signOut'] = app.ProxySession('signOut')(chooseSessionNode)
        pages['/data/signUp'] = app.ProxyAccount('signUp')(chooseAccountNode)
        pages['/data/unwatchPublicProfile'] = app.ProxySession('unwatchPublicProfile')(chooseSessionNode)
        pages['/data/watchPublicProfile'] = app.ProxySession('watchPublicProfile')(chooseSessionNode)
        app.Static(pages)

        require('http').createServer((req, res) => {

            app.log.info('Http ' + req.method + ' ' + req.url)

            const parsed_url = url.parse(req.url, true)
            const request = {
                parsed_url, req, res,
                respond (response) {
                    app.EchoText(request, {
                        type: 'application/json',
                        content: JSON.stringify(response),
                    })
                },
            }

            const page = (() => {
                const page = pages[parsed_url.pathname]
                if (page !== undefined) return page
                return app.Error404Page
            })()

            page(request)

        }).listen(listen.port, listen.host)

    })

    app.Watch(['lib', 'static'])

    const listen = app.config.listen

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
