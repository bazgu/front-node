module.exports = app => text => {
    app.log.error(text)
    process.exit(1)
}
