module.exports = loadedConfig => {

    var sessionNodes = loadedConfig.sessionNodes.map(sessionNode => {

        var host = sessionNode.host,
            port = sessionNode.port

        return {
            host: host,
            port: port,
            logPrefix: 'session-node-client: ' + host + ':' + port + ': ',
        }

    })

    return token => {
        if (token === undefined) return
        var index = parseInt(token.split('_', 1)[0], 10)
        if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
        return sessionNodes[index]
    }

}
