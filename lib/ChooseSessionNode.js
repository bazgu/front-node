module.exports = loadedConfig => {

    const sessionNodes = loadedConfig.sessionNodes.map(sessionNode => {

        const host = sessionNode.host,
            port = sessionNode.port

        return {
            host, port,
            logPrefix: 'session-node: ' + host + ':' + port + ': ',
        }

    })

    return token => {
        if (token === undefined) return
        const index = parseInt(token.split('_', 1)[0], 10)
        if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
        return sessionNodes[index]
    }

}
