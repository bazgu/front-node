module.exports = (res, username) => {
    res.statusCode = 302
    res.setHeader('Location', './#' + username)
    res.end()
}
