var content =
    '<!DOCTYPE html>' +
    '<html style="height: 100%">' +
        '<head>' +
            '<title>404 Not Found</title>' +
            '<meta name="viewport" content="width=device-width, user-scalable=no" />' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
            '<link rel="icon" type="image/png" href="/img/icon/16.png" sizes="16x16" />' +
            '<link rel="icon" type="image/png" href="/img/icon/32.png" sizes="32x32" />' +
            '<link rel="icon" type="image/png" href="/img/icon/48.png" sizes="48x48" />' +
            '<link rel="icon" type="image/png" href="/img/icon/64.png" sizes="64x64" />' +
            '<link rel="icon" type="image/png" href="/img/icon/128.png" sizes="128x128" />' +
            '<link rel="icon" type="image/png" href="/img/icon/256.png" sizes="256x256" />' +
            '<link rel="icon" type="image/png" href="/img/icon/512.png" sizes="512x512" />' +
        '</head>' +
        '<body style="margin: 0; text-align: center; height: 100%; background-color: hsl(95, 70%, 85%); font-size: 15px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; white-space: nowrap">' +
            '<div style="display: inline-block; vertical-align: middle; height: 100%">' +
            '</div>' +
            '<div style="display: inline-block; vertical-align: middle; padding: 20px; background-color: white; color: hsl(0, 0%, 30%)">' +
                '404 Not Found' +
            '</div>' +
        '</body>' +
    '</html>'

module.exports = (req, res) => {
    res.statusCode = 404
    res.setHeader('Content-Type', 'text/html; charset=UTF-8')
    res.end(content)
}
