const querystring = require('querystring')

module.exports = params => querystring.stringify(params)
