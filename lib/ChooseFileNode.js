module.exports = loadedConfig => {

    var fileNodes = loadedConfig.fileNodes.map(fileNode => {

        var host = fileNode.host,
            port = fileNode.port

        return {
            host: host,
            port: port,
            logPrefix: 'file-node-client: ' + host + ':' + port + ': ',
        }

    })

    return token => {
        if (token === undefined) return
        var index = parseInt(token.split('_', 1)[0], 10)
        if (!isFinite(index) || index < 0 || index > fileNodes.length - 1) return
        return fileNodes[index]
    }

}
