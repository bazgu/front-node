module.exports = loadedConfig => {

    const fileNodes = loadedConfig.fileNodes.map(fileNode => {

        const host = fileNode.host,
            port = fileNode.port

        return {
            host, port,
            logPrefix: 'file-node: ' + host + ':' + port + ': ',
        }

    })

    return token => {
        if (token === undefined) return
        const index = parseInt(token.split('_', 1)[0], 10)
        if (!isFinite(index) || index < 0 || index > fileNodes.length - 1) return
        return fileNodes[index]
    }

}
