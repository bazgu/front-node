var fs = require('fs'),
    zlib = require('zlib')

module.exports = (plainContent, cacheControl, contentType) => {

    function plainPage (req, res) {
        res.setHeader('Cache-Control', cacheControl)
        res.setHeader('Content-Type', contentType)
        res.setHeader('Content-Length', plainContent.length)
        res.end(plainContent)
    }

    var gzipContent = zlib.gzipSync(plainContent)
    if (gzipContent.length >= plainContent.length) return plainPage

    return (req, res) => {

        var acceptEncoding = req.headers['accept-encoding']
        if (acceptEncoding === undefined) {
            plainPage(req, res)
            return
        }

        var codings = acceptEncoding.split(/,\s*/)
        for (var i = 0; i < codings.length; i++) {
            var coding = codings[i].split(';', 1)[0]
            if (coding === 'gzip') {
                res.setHeader('Cache-Control', cacheControl)
                res.setHeader('Content-Type', contentType)
                res.setHeader('Content-Length', gzipContent.length)
                res.setHeader('Content-Encoding', 'gzip')
                res.end(gzipContent)
                return
            }
        }

        plainPage(req, res)

    }

}
