var fs = require('fs'),
    path = require('path')

var GzipPage = require('./GzipPage.js')

module.exports = (root, pages) => {

    function scan (dir) {
        fs.readdirSync(root + dir).forEach(file => {
            var filepath = dir + '/' + file
            var stat = fs.statSync(root + filepath)
            if (stat.isDirectory()) scan(filepath)
            else {

                var extension = path.extname(file)

                var contentType
                if (extension === '.css') {
                    contentType = 'text/css'
                } else if (extension === '.js') {
                    contentType = 'application/javascript'
                } else if (extension === '.map') {
                    contentType = 'application/json'
                } else if (extension === '.png') {
                    contentType = 'image/png'
                } else if (extension === '.svg') {
                    contentType = 'image/svg+xml'
                } else if (extension === '.wav') {
                    contentType = 'audio/wav'
                } else {
                    return
                }

                var plainContent = fs.readFileSync(root + filepath)
                pages[filepath] = GzipPage(plainContent, cacheControl, contentType)

            }
        })
    }

    var cacheControl = 'public, max-age=' + (60 * 60 * 24 * 365)
    scan('')

}
