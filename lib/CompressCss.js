var uglifyCss = require('uglifycss')

module.exports = source => {
    return uglifyCss.processString(source, { maxLineLen: 1024 })
}
