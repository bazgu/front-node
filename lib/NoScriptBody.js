module.exports = () => {
    return '<noscript>' +
            '<div class="NoScript">' +
                '<div class="NoScript-aligner"></div>' +
                '<div class="NoScript-content">' +
                    '<b>JavaScript is required to use Bazgu.</b>' +
                '</div>' +
            '</div>' +
        '</noscript>'
}
