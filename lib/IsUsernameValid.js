module.exports = username => {
    return /^[a-z0-9_.-]{6,}$/i.test(username)
}
