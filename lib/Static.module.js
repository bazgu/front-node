const fs = require('fs')

module.exports = app => pages => {

    function scan (uri) {
        const directory = __dirname + '/../static' + uri
        fs.readdirSync(directory).forEach(item => {
            const filename = directory + '/' + item
            const item_uri = uri + '/' + item
            if (fs.statSync(filename).isDirectory()) scan(item_uri)
            else pages[item_uri] = app.StaticPage(filename)
        })
    }

    scan('')

}
