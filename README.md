Bazgu Front Node
================

This program **front-node** is one of the nodes in a Bazgu infrastructure.
A front-node is a node that users connect to. It serves static files
as well as proxies other requests to their corresponding nodes.
A front-node is completely stateless.

Workflow
--------

A user connects to a front-node and requests some resources.
If the resource is a static file then its content is returned.
Otherwise the request is routed to another node based on a sharding algorithm.

Configuration
-------------

See `config.js` for details.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
