#!/usr/bin/env node

process.chdir(__dirname)

var fs = require('fs')

var CompressCss = require('../../lib/CompressCss.js'),
    CompressJs = require('../../lib/CompressJs.js')

var cssContent = require('../../lib/IndexPageCssFiles.js').map(file => {
    return fs.readFileSync('../css/' + file + '.css', 'utf8')
}).join('')

var cssLoader =
    '(function () {\n' +
    "    var style = document.createElement('style')\n" +
    "    style.type = 'text/css'\n" +
    '    style.innerHTML = ' + JSON.stringify(CompressCss(cssContent)) + '\n' +
    "    document.head.appendChild(style)\n" +
    '})()\n'

var urls = {}
require('../../lib/IndexPageSvgFiles.js').forEach(file => {
    urls[file] = 'data:image/svg+xml;base64,' + fs.readFileSync('../' + file).toString('base64')
})

var getResourceUrl =
    'var getResourceUrl = (function () {\n' +
    '    var urls = ' + JSON.stringify(urls) + '\n' +
    '    return function (url) {\n' +
    "        return urls[url]\n" +
    '    }\n' +
    '})()\n'

var jsContent = '(function () {\n\n' +
    cssLoader + '\n' +
    getResourceUrl + '\n' +
    require('../../lib/IndexPageJsFiles.js').map(file => {
        return fs.readFileSync('../js/' + file + '.js', 'utf8')
    }).join('\n') + '\n' +
    '})()\n'

var existingContent = fs.readFileSync('compressed.js', 'utf8')
CompressJs(jsContent)
var newContent = fs.readFileSync('compressed.js', 'utf8')
if (newContent !== existingContent) {

    fs.writeFileSync('compressed.js', newContent)

    var Revisions = require('../../lib/Revisions.js')
    Revisions['etc/compressed.js']++

    var content = 'module.exports = {\n'
    for (var i in Revisions) {
        content += "    '" + i + "': " + Revisions[i] + ',\n'
    }
    content += '}\n'
    fs.writeFileSync('../../lib/Revisions.js', content)

}
