(function () {

(function () {
    var style = document.createElement('style')
    style.type = 'text/css'
    style.innerHTML = "*{margin:0;padding:0;border:0;outline:0;border-radius:0;background:transparent;color:inherit;font:inherit;cursor:default;text-align:inherit;text-decoration:none;resize:none;opacity:1;white-space:nowrap;box-sizing:border-box;-moz-box-sizing:border-box;word-wrap:break-word}*::-moz-focus-inner{padding:0;border:0}html,body{background-color:white;color:hsl(0,0%,30%);height:100%;font:normal 15px/30px Helvetica,Arial,sans-serif;overflow:hidden;text-align:left}.AccountReadyPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.AccountReadyPage-aligner{display:inline-block;vertical-align:middle;height:100%}.AccountReadyPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.AccountReadyPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.AccountReadyPage-text{line-height:20px;white-space:normal}\n.AccountReadyPage-button{margin-top:10px;width:100%}.BackButton{position:absolute;top:10px;left:10px;line-height:44px;padding-right:14px;padding-left:14px;cursor:pointer;color:hsl(95,60%,55%)}.BackButton:focus{background-color:hsl(95,70%,95%)}.BackButton:active{background-color:hsl(95,70%,85%)}.CloseButton{position:absolute;top:10px;right:10px;width:44px;height:44px;cursor:pointer;background-position:center;background-repeat:no-repeat}.CloseButton:focus{background-color:hsl(95,70%,95%)}.CloseButton:active{background-color:hsl(95,70%,85%)}.ConnectionErrorPage{overflow:auto;text-align:center}.ConnectionErrorPage-aligner{display:inline-block;vertical-align:middle;height:100%}.ConnectionErrorPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.ConnectionErrorPage-text{padding:20px;margin-bottom:10px;white-space:normal;line-height:20px}.ConnectionErrorPage-button{width:100%}.ContactRequestPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}\n.ContactRequestPage-aligner{display:inline-block;vertical-align:middle;height:100%}.ContactRequestPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.ContactRequestPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px;margin-bottom:10px}.ContactRequestPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.ContactRequestPage-text-username{font-weight:bold}.ContactRequestPage-buttons{margin-top:10px}.ContactRequestPage-addContactButton{width:calc(60% - 5px)}.ContactRequestPage-ignoreButton{width:calc(40% - 5px);margin-left:10px}.CrashPage{overflow:auto;text-align:center}.CrashPage-aligner{display:inline-block;vertical-align:middle;height:100%}.CrashPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.CrashPage-text{padding:20px;margin-bottom:10px;white-space:normal;line-height:20px}\n.CrashPage-button{width:100%}.FormError{background-color:hsl(350,70%,55%);color:white;padding:5px;white-space:normal;text-align:center;padding-top:5px;padding-bottom:5px;line-height:20px;margin-top:10px}.FoundContactPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.FoundContactPage-aligner{display:inline-block;vertical-align:middle;height:100%}.FoundContactPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;text-align:left}.FoundContactPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px;overflow:hidden;text-overflow:ellipsis}.FoundContactPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.FoundContactPage-text-username{font-weight:bold}.FoundContactPage-button{margin-top:10px;width:100%}.FoundSelfPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}\n.FoundSelfPage-aligner{display:inline-block;vertical-align:middle;height:100%}.FoundSelfPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;text-align:left}.FoundSelfPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px;overflow:hidden;text-overflow:ellipsis}.FoundSelfPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.FoundSelfPage-text-username{font-weight:bold}.FoundUserPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.FoundUserPage-aligner{display:inline-block;vertical-align:middle;height:100%}.FoundUserPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;text-align:left}.FoundUserPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px;overflow:hidden;text-overflow:ellipsis}\n.FoundUserPage-button{margin-top:10px;width:100%}.GreenButton{padding-top:5px;padding-bottom:5px;cursor:pointer;color:white;text-align:center;background-color:hsl(95,60%,55%);border:2px solid hsl(95,60%,55%)}.GreenButton:focus{background-color:hsl(95,60%,65%)}.GreenButton:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.GreenButton[disabled]{color:white;background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}.InvitePanel-title{font-size:22px;line-height:44px;text-align:center;color:hsl(95,60%,55%)}.InvitePanel-smsButton{display:inline-block;vertical-align:top;margin-top:10px;width:calc(50% - 5px)}.InvitePanel-emailButton{display:inline-block;vertical-align:top;margin-top:10px;width:calc(50% - 5px);margin-left:10px}.LoadingPage{text-align:center}.LoadingPage-aligner{display:inline-block;vertical-align:middle;height:100%}.LoadingPage-content{display:inline-block;vertical-align:middle;padding:20px;background-color:white}\n.NoSuchUserPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.NoSuchUserPage-aligner{display:inline-block;vertical-align:middle;height:100%}.NoSuchUserPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.NoSuchUserPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.NoSuchUserPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px;color:hsl(350,70%,55%)}.OrangeButton{padding-top:5px;padding-bottom:5px;cursor:pointer;color:white;text-align:center;background-color:hsl(45,75%,55%);border:2px solid hsl(45,75%,55%)}.OrangeButton:focus{background-color:hsl(45,75%,65%)}.OrangeButton:active{color:hsl(45,75%,70%);background-color:hsl(45,75%,35%);border-color:hsl(45,75%,35%)}.OrangeButton[disabled]{color:white;background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}\n.Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsl(95,70%,85%);background-repeat:repeat-x,repeat-x;background-position:center bottom,center 30%}.PrivacyPage{overflow:auto;text-align:center}.PrivacyPage-aligner{display:inline-block;vertical-align:middle;height:100%}.PrivacyPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;text-align:left}.PrivacyPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px}.PrivacyPage-text{line-height:20px;white-space:pre-wrap}.RemoveContactPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.RemoveContactPage-aligner{display:inline-block;vertical-align:middle;height:100%}.RemoveContactPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.RemoveContactPage-text{padding:20px;white-space:normal;line-height:20px}.RemoveContactPage-username{font-weight:bold}\n.RemoveContactPage-buttons{margin-top:10px}.RemoveContactPage-removeButton{width:calc(65% - 5px)}.RemoveContactPage-cancelButton{width:calc(35% - 5px);margin-left:10px}.ServiceErrorPage{overflow:auto;text-align:center}.ServiceErrorPage-aligner{display:inline-block;vertical-align:middle;height:100%}.ServiceErrorPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.ServiceErrorPage-text{padding:20px;margin-bottom:10px;white-space:normal;line-height:20px}.ServiceErrorPage-button{width:100%}.SignOutPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SignOutPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SignOutPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.SignOutPage-text{padding:20px;white-space:normal;line-height:20px;margin-bottom:10px}.SignOutPage-yesButton{width:calc(55% - 5px)}.SignOutPage-noButton{width:calc(45% - 5px);margin-left:10px}\n.SimpleContactPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SimpleContactPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SimpleContactPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.SimpleContactPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SimpleContactPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.SimpleContactPage-text-username{font-weight:bold}.SimpleContactPage-button{margin-top:10px;width:100%}.SimpleSelfPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SimpleSelfPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SimpleSelfPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}\n.SimpleSelfPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SimpleSelfPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.SimpleSelfPage-text-username{font-weight:bold}.SimpleUserPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SimpleUserPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SimpleUserPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.SimpleUserPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SimpleUserPage-button{margin-top:10px;width:100%}.AccountPage_EmailItem{position:relative}.AccountPage_EmailItem-input{background-color:white;padding:5px 10px;cursor:text;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;height:44px;-moz-appearance:none;-webkit-appearance:none}\n.AccountPage_EmailItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_EmailItem-input[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_FullNameItem{position:relative}.AccountPage_FullNameItem-input{background-color:white;padding:5px 10px;cursor:text;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;height:44px;-moz-appearance:none;-webkit-appearance:none}.AccountPage_FullNameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_FullNameItem-input[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_LinkItem-input{background-color:hsl(0,0%,95%);padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.AccountPage_LinkItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}\n.AccountPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.AccountPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.AccountPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.AccountPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.AccountPage_Page-form{border-bottom:1px solid hsl(0,0%,90%);padding-bottom:10px;margin-bottom:10px}.AccountPage_Page-saveChangesButton{margin-top:10px;width:100%}.AccountPage_Page-changePasswordButton{width:100%}.AccountPage_PhoneItem{position:relative}.AccountPage_PhoneItem-input{background-color:white;padding:5px 10px;cursor:text;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;height:44px;-moz-appearance:none;-webkit-appearance:none}\n.AccountPage_PhoneItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_PhoneItem-input[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_PrivacySelect{position:absolute;right:0;bottom:0;width:58px;height:44px;line-height:44px}.AccountPage_PrivacySelect-button{display:inline-block;vertical-align:top;cursor:pointer;width:100%;height:100%;border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-right-width:2px;border-bottom-width:2px;background-repeat:no-repeat,no-repeat;background-position:2px center,calc(100% - 10px) center}.AccountPage_PrivacySelect-button:focus{background-color:hsl(95,70%,95%);border-color:hsl(95,60%,55%)}.AccountPage_PrivacySelect-button:active{background-color:hsl(95,70%,85%);border-color:hsl(95,60%,55%)}.AccountPage_PrivacySelect-button.pressed{color:white;background-color:hsl(95,60%,55%);border-color:hsl(95,60%,55%)}.AccountPage_PrivacySelect-button.pressed:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}\n.AccountPage_PrivacySelect-button[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_PrivacySelect-menu{position:absolute;top:100%;right:0;background-color:white;border:2px solid hsl(95,60%,55%);z-index:1;margin-top:-2px}.AccountPage_PrivacySelectItem{padding-left:44px;padding-right:12px;cursor:pointer;background-position:2px center;background-repeat:no-repeat}.AccountPage_PrivacySelectItem.active,.AccountPage_PrivacySelectItem:active{background-color:hsl(95,70%,85%)}.AccountPage_TimezoneItem{position:relative}.AccountPage_TimezoneItem-select{vertical-align:top;background-color:white;padding:5px 10px;cursor:pointer;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;height:44px;background-repeat:no-repeat;background-position:calc(100% - 10px) center;-moz-appearance:none;-webkit-appearance:none}.AccountPage_TimezoneItem-select:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}\n.AccountPage_TimezoneItem-select[disabled]{background-color:hsl(0,0%,95%)}.AddContactPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.AddContactPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.AddContactPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.AddContactPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.AddContactPage_Page-button{margin-top:10px;width:100%}.AddContactPage_Page-invitePanel{margin-top:10px}.AddContactPage_UsernameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.AddContactPage_UsernameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}\n.AddContactPage_UsernameItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.AddContactPage_UsernameItem-input[disabled]{background-color:hsl(0,0%,95%)}.AddContactPage_UsernameItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ChangePasswordPage_CurrentPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.ChangePasswordPage_CurrentPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.ChangePasswordPage_CurrentPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.ChangePasswordPage_CurrentPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.ChangePasswordPage_CurrentPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}\n.ChangePasswordPage_NewPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.ChangePasswordPage_NewPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.ChangePasswordPage_NewPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.ChangePasswordPage_NewPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.ChangePasswordPage_NewPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ChangePasswordPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.ChangePasswordPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.ChangePasswordPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;text-align:left}\n.ChangePasswordPage_Page-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px}.ChangePasswordPage_Page-button{margin-top:10px;width:100%}.ChangePasswordPage_RepeatNewPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.ChangePasswordPage_RepeatNewPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.ChangePasswordPage_RepeatNewPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.ChangePasswordPage_RepeatNewPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.ChangePasswordPage_RepeatNewPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ContactSelectPage_Item{line-height:25px;margin-top:10px}.ContactSelectPage_Item:first-child{margin-top:0}.ContactSelectPage_Item-click{display:inline-block;vertical-align:top;cursor:pointer;width:100%;overflow:hidden;text-overflow:ellipsis}\n.ContactSelectPage_Item-button{display:inline-block;vertical-align:top;width:25px;height:25px;border:2px solid hsl(0,0%,70%);cursor:inherit;margin-right:5px;line-height:21px;text-align:center;background-position:center;background-repeat:no-repeat}.ContactSelectPage_Item-button:focus{background-color:hsl(95,70%,95%);border-color:hsl(95,60%,55%)}.ContactSelectPage_Item-button:active{background-color:hsl(95,70%,85%)}.ContactSelectPage_Item-button.selected{color:white;background-color:hsl(95,60%,55%);border-color:hsl(95,60%,55%)}.ContactSelectPage_Item-button.selected:active{color:hsl(95,60%,55%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.ContactSelectPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.ContactSelectPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.ContactSelectPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}\n.ContactSelectPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-right:54px;margin-bottom:10px}.ContactSelectPage_Page-button{width:100%;margin-top:10px}.EditContactPage_EmailItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.EditContactPage_EmailItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_EmailItem-input[disabled]{background-color:hsl(0,0%,95%)}.EditContactPage_FullNameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.EditContactPage_FullNameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_FullNameItem-input[disabled]{background-color:hsl(0,0%,95%)}.EditContactPage_LinkItem-input{background-color:hsl(0,0%,95%);padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}\n.EditContactPage_LinkItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.EditContactPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.EditContactPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.EditContactPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.EditContactPage_Page-saveChangesButton{margin-top:10px;width:100%}.EditContactPage_PhoneItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.EditContactPage_PhoneItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_PhoneItem-input[disabled]{background-color:hsl(0,0%,95%)}\n.EditContactPage_TimezoneItem-select{vertical-align:top;background-color:white;padding:5px 10px;cursor:pointer;width:100%;border:2px solid hsl(0,0%,70%);height:44px;background-repeat:no-repeat;background-position:calc(100% - 10px) center;-moz-appearance:none;-webkit-appearance:none}.EditContactPage_TimezoneItem-select:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_TimezoneItem-select[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_AdvancedItem{margin-top:10px;line-height:25px}.SignUpPage_AdvancedItem-button{display:inline-block;vertical-align:top;color:hsl(95,60%,55%);cursor:pointer;padding-right:4px;padding-left:3px;background-color:white}.SignUpPage_AdvancedItem-button:focus{background-color:hsl(95,70%,95%)}.SignUpPage_AdvancedItem-button:active{background-color:hsl(95,70%,85%)}.SignUpPage_AdvancedItem-arrow{display:inline-block;vertical-align:top;width:13px;height:25px;background-position:center;background-repeat:no-repeat;cursor:inherit}\n.SignUpPage_AdvancedItem-line{margin-top:-13px;margin-bottom:12px;border-bottom:1px solid hsl(0,0%,90%)}.SignUpPage_AdvancedPanel{display:none}.SignUpPage_AdvancedPanel.visible{display:block}.SignUpPage_CaptchaItem-image{display:inline-block;vertical-align:top;background-color:hsl(0,0%,95%);width:140px;height:44px;line-height:44px;background-repeat:no-repeat;text-align:center}.SignUpPage_CaptchaItem-image.error{background-color:hsl(350,70%,55%);color:white}.SignUpPage_CaptchaItem-reloadButton{display:inline-block;vertical-align:top;margin-left:10px;width:110px}.SignUpPage_CaptchaItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);margin-top:10px;height:44px;-moz-appearance:none;-webkit-appearance:none}.SignUpPage_CaptchaItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_CaptchaItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}\n.SignUpPage_CaptchaItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_CaptchaItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SignUpPage_Page{overflow:auto;text-align:center}.SignUpPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.SignUpPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;text-align:left}.SignUpPage_Page-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px}.SignUpPage_Page-button{margin-top:10px;width:100%}.SignUpPage_Page-offerUsername{font-weight:bold}.SignUpPage_PasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.SignUpPage_PasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_PasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}\n.SignUpPage_PasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_PasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SignUpPage_RepeatPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.SignUpPage_RepeatPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_RepeatPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.SignUpPage_RepeatPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_RepeatPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SignUpPage_TimezoneItem-select{vertical-align:top;background-color:white;padding:5px 10px;cursor:pointer;width:100%;border:2px solid hsl(0,0%,70%);height:44px;background-repeat:no-repeat;background-position:calc(100% - 10px) center;-moz-appearance:none;-webkit-appearance:none}\n.SignUpPage_TimezoneItem-select:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_TimezoneItem-select[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_UsernameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.SignUpPage_UsernameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_UsernameItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.SignUpPage_UsernameItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_UsernameItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SmileysPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SmileysPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}\n.SmileysPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:260px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.SmileysPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SmileysPage_Page-button{display:inline-block;vertical-align:top;width:44px;height:44px;cursor:pointer;background-repeat:no-repeat;background-position:center}.SmileysPage_Page-button:focus{background-color:hsl(95,70%,95%)}.SmileysPage_Page-button:active{background-color:hsl(95,70%,85%)}.UserInfoPanel_Panel-empty{color:hsl(0,0%,70%);text-align:center}.UserInfoPanel_Panel-empty.hidden{display:none}.UserInfoPanel_Panel-item{overflow:hidden;text-overflow:ellipsis;line-height:20px}.UserInfoPanel_Panel-item.hidden{display:none}.WelcomePage_InstallItem{width:100%;margin-bottom:20px}.WelcomePage_Page{overflow:auto;text-align:center}.WelcomePage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}\n.WelcomePage_Page-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px;text-align:left}.WelcomePage_Page-logoWrapper{padding-top:10px;text-align:center}.WelcomePage_Page-logo{display:inline-block;width:112px;height:154px;vertical-align:top}.WelcomePage_Page-warning{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px;text-align:center}.WelcomePage_Page-signInForm{border-bottom:1px solid hsl(0,0%,90%);padding-bottom:10px}.WelcomePage_Page-signInButton{margin-top:10px;width:100%}.WelcomePage_Page-signUpButton{width:100%}.WelcomePage_PasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}.WelcomePage_PasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.WelcomePage_PasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}\n.WelcomePage_PasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.WelcomePage_PasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.WelcomePage_Privacy{margin-top:10px;margin-bottom:-10px;text-align:center}.WelcomePage_Privacy-privacy{display:inline-block;vertical-align:top;color:hsl(95,60%,55%);cursor:pointer;padding-right:4px;padding-left:4px}.WelcomePage_Privacy-privacy:focus{background-color:hsl(95,70%,95%)}.WelcomePage_Privacy-privacy:active{background-color:hsl(95,70%,85%)}.WelcomePage_PublicLine{text-align:center;position:relative;height:30px;margin-bottom:10px}.WelcomePage_PublicLine-item{position:absolute;top:0;right:0;left:0;opacity:0;transition:opacity .6s ease-in-out}.WelcomePage_PublicLine-item.visible{opacity:1}.WelcomePage_StaySignedInItem{margin-top:10px;line-height:25px}.WelcomePage_StaySignedInItem-click{display:inline-block;vertical-align:top;cursor:pointer}.WelcomePage_StaySignedInItem-click.disabled{cursor:default}\n.WelcomePage_StaySignedInItem-button{display:inline-block;vertical-align:top;width:25px;height:25px;line-height:21px;border:2px solid hsl(0,0%,70%);margin-right:5px;cursor:pointer;text-align:center;background-position:center;background-repeat:no-repeat}.WelcomePage_StaySignedInItem-button:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.WelcomePage_StaySignedInItem-button:active{background-color:hsl(95,70%,85%)}.WelcomePage_StaySignedInItem-button.checked{color:white;border-color:hsl(95,60%,55%);background-color:hsl(95,60%,55%)}.WelcomePage_StaySignedInItem-button.checked:focus{background-color:hsl(95,60%,65%)}.WelcomePage_StaySignedInItem-button.checked:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.WelcomePage_StaySignedInItem-button[disabled]{background-color:hsl(0,0%,95%);cursor:default}.WelcomePage_UnauthenticatedWarning-username{font-weight:bold}.WelcomePage_UsernameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);height:44px;-moz-appearance:none;-webkit-appearance:none}\n.WelcomePage_UsernameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.WelcomePage_UsernameItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.WelcomePage_UsernameItem-input[disabled]{background-color:hsl(0,0%,95%)}.WelcomePage_UsernameItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.WorkPage_Page{overflow-y:auto}.WorkPage_ChatPanel_CloseButton{position:absolute;top:20px;right:20px;width:44px;height:44px;cursor:pointer;background-position:center;background-repeat:no-repeat}.WorkPage_ChatPanel_CloseButton:focus{background-color:hsl(95,70%,95%)}.WorkPage_ChatPanel_CloseButton:active{background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_ContactMenu{position:absolute;top:100%;left:0;background-color:white;border:2px solid hsl(95,60%,55%);z-index:1;min-width:100%;margin-top:-2px}.WorkPage_ChatPanel_ContactMenu-item{line-height:44px;padding-left:12px;padding-right:12px;cursor:pointer}\n.WorkPage_ChatPanel_ContactMenu-item.active,.WorkPage_ChatPanel_ContactMenu-item:active{background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_DaySeparator{color:hsl(0,0%,70%);text-align:center;margin-bottom:-10px}.WorkPage_ChatPanel_DaySeparator:first-child{margin-top:-10px}.WorkPage_ChatPanel_MessagesPanel{position:absolute;top:64px;right:0;bottom:0;left:0;max-width:600px;margin-right:auto;margin-left:auto}.WorkPage_ChatPanel_MessagesPanel-content{position:absolute;top:0;right:20px;left:20px;overflow:auto}.WorkPage_ChatPanel_MessagesPanel-doneMessages{padding-top:10px;padding-bottom:10px}.WorkPage_ChatPanel_MessagesPanel-sendingMessages{padding-bottom:10px}.WorkPage_ChatPanel_MessagesPanel-sendingMessages.hidden{display:none}@media(max-height:300px){.WorkPage_ChatPanel_MessagesPanel.typing{top:0}}@media(orientation:landscape){.WorkPage_ChatPanel_MessagesPanel-content{bottom:64px}}@media(orientation:portrait),(min-height:400px){.WorkPage_ChatPanel_MessagesPanel-content{bottom:118px}}.WorkPage_ChatPanel_MessageText-link{color:hsl(95,60%,55%);cursor:pointer;white-space:pre-wrap}\n.WorkPage_ChatPanel_MessageText-link:hover{text-decoration:underline}.WorkPage_ChatPanel_MessageText-disabledLink{white-space:pre-wrap}.WorkPage_ChatPanel_MoreButton{position:absolute;top:0;width:70px}.WorkPage_ChatPanel_MoreButton-button{padding-top:5px;padding-right:12px;padding-bottom:5px;width:100%;color:white;text-align:center;cursor:pointer;background-color:hsl(45,75%,55%);background-position:calc(100% - 10px) center;background-repeat:no-repeat;border:2px solid hsl(45,75%,55%)}.WorkPage_ChatPanel_MoreButton-button.not_pressed:focus{background-color:hsl(45,75%,65%)}.WorkPage_ChatPanel_MoreButton-button.pressed:active,.WorkPage_ChatPanel_MoreButton-button.not_pressed:active{color:hsl(45,75%,70%);background-color:hsl(45,75%,35%);border-color:hsl(45,75%,35%)}.WorkPage_ChatPanel_MoreButton-button[disabled]{color:white;background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}@media(orientation:landscape){.WorkPage_ChatPanel_MoreButton{right:80px}\n}@media(orientation:landscape) and (max-width:400px){.WorkPage_ChatPanel_MoreButton.typeFocused{display:none}}@media(orientation:portrait),(min-height:400px){.WorkPage_ChatPanel_MoreButton{right:0}}.WorkPage_ChatPanel_MoreMenu{position:absolute;right:0;bottom:100%;background-color:white;border:2px solid hsl(45,75%,55%);z-index:1;min-width:100%;margin-top:-2px}.WorkPage_ChatPanel_MoreMenu-item{position:relative;line-height:44px;padding-left:12px;padding-right:12px;cursor:pointer;overflow:hidden}.WorkPage_ChatPanel_MoreMenu-item.active,.WorkPage_ChatPanel_MoreMenu-item:active{background-color:hsl(45,75%,90%)}.WorkPage_ChatPanel_MoreMenu-fileInput{position:absolute;top:0;left:0;width:100%;height:100%;cursor:inherit;opacity:0}.WorkPage_ChatPanel_MoreMenu-fileInput::-webkit-file-upload-button{display:none}.WorkPage_ChatPanel_Panel{position:absolute;top:0;right:0;bottom:0;left:0;padding:20px;background-color:white;background-position:center bottom;background-repeat:repeat-x}@media(max-height:300px){.WorkPage_ChatPanel_Panel-bar.hidden{display:none}\n}@media(min-width:660px){.WorkPage_ChatPanel_Panel{left:330px;border-left:2px solid hsl(95,70%,85%)}}@media(min-width:1100px){.WorkPage_ChatPanel_Panel{left:400px}}.WorkPage_ChatPanel_ReceivedFileMessage{background-color:hsl(95,70%,85%);padding:12px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_ReceivedFileMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_ReceivedFileMessage-item{position:relative;white-space:pre-wrap}.WorkPage_ChatPanel_ReceivedFileMessage-item.withReceiveLink{padding-left:90px}.WorkPage_ChatPanel_ReceivedFileMessage-item-size{color:hsl(0,0%,70%)}.WorkPage_ChatPanel_ReceivedFileMessage-receiveLink{position:absolute;top:0;left:0;cursor:pointer;width:80px;color:white;background-color:hsl(95,60%,55%);text-align:center;line-height:40px}.WorkPage_ChatPanel_ReceivedFileMessage-receiveLink:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%)}.WorkPage_ChatPanel_ReceivedTextMessage{background-color:hsl(95,70%,85%);padding:12px;margin-top:10px;line-height:20px}\n.WorkPage_ChatPanel_ReceivedTextMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_ReceivedTextMessage-item{white-space:pre-wrap}.WorkPage_ChatPanel_SendingFileMessage{border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;white-space:pre-wrap;line-height:20px;background-color:white;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SendingFileMessage:first-child{margin-top:0}.WorkPage_ChatPanel_SendingFileMessage-size{color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SendingTextMessage{border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;line-height:20px;background-color:white;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SendingTextMessage:first-child{margin-top:0}.WorkPage_ChatPanel_SendingTextMessage-item{white-space:pre-wrap}.WorkPage_ChatPanel_SentFileMessage{background-color:white;border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_SentFileMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}\n.WorkPage_ChatPanel_SentFileMessage-item{position:relative;white-space:pre-wrap}.WorkPage_ChatPanel_SentFileMessage-item.sending{padding-left:90px}.WorkPage_ChatPanel_SentFileMessage-item-size{color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SentFileMessage-item-sending{position:absolute;top:0;left:0;width:80px;text-align:center;line-height:40px;background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_SentFileMessage-item-sending.failed{color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.WorkPage_ChatPanel_SentFileMessage-item-sending-done{position:absolute;top:0;left:0;width:0;overflow:hidden}.WorkPage_ChatPanel_SentFileMessage-item-sending-done-text{width:80px;background-color:hsl(95,60%,55%);color:white}.WorkPage_ChatPanel_SentFileMessage-item-sending-done-text.failed{background-color:hsl(350,70%,55%)}.WorkPage_ChatPanel_SentTextMessage{background-color:white;border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_SentTextMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}\n.WorkPage_ChatPanel_SentTextMessage-item{white-space:pre-wrap}.WorkPage_ChatPanel_Smiley{display:inline-block;vertical-align:top;width:20px;height:20px}.WorkPage_ChatPanel_Title{display:inline-block;vertical-align:top;position:relative}.WorkPage_ChatPanel_Title-button{display:inline-block;vertical-align:top;cursor:pointer;line-height:44px;padding-right:26px;padding-left:14px;background-repeat:no-repeat;background-position:calc(100% - 14px) center}.WorkPage_ChatPanel_Title-button:focus{background-color:hsl(95,70%,95%)}.WorkPage_ChatPanel_Title-button:active{background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_Title-button.pressed{color:white;background-color:hsl(95,60%,55%)}.WorkPage_ChatPanel_Title-button.pressed:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%)}.WorkPage_ChatPanel_Title-buttonText{display:inline-block;vertical-align:top;max-width:132px;overflow:hidden;text-overflow:ellipsis;cursor:inherit;font-weight:bold}@media(min-width:400px){.WorkPage_ChatPanel_Title-buttonText{max-width:182px}\n}@media(min-width:660px){.WorkPage_ChatPanel_Title-buttonText{max-width:144px}}@media(min-width:800px){.WorkPage_ChatPanel_Title-buttonText{max-width:182px}}.WorkPage_ChatPanel_TypePanel{position:absolute;right:20px;bottom:20px;left:20px}.WorkPage_ChatPanel_TypePanel-textarea{padding:10px;display:inline-block;vertical-align:top;height:100%;cursor:text;line-height:20px;border-top:2px solid hsl(0,0%,70%);white-space:pre-wrap;-moz-appearance:none;-webkit-appearance:none}.WorkPage_ChatPanel_TypePanel-textarea:focus{border-color:hsl(95,60%,55%)}.WorkPage_ChatPanel_TypePanel-sendButton{position:absolute;right:0;bottom:0;width:70px}@media(orientation:landscape){.WorkPage_ChatPanel_TypePanel{height:44px}.WorkPage_ChatPanel_TypePanel-textarea{width:calc(100% - 160px)}}@media(orientation:landscape) and (max-width:400px){.WorkPage_ChatPanel_TypePanel-textarea:focus{width:calc(100% - 80px)}}@media(orientation:portrait),(min-height:400px){.WorkPage_ChatPanel_TypePanel{height:98px}.WorkPage_ChatPanel_TypePanel-textarea{width:calc(100% - 80px)}\n}.WorkPage_SidePanel_AccountMenu{position:absolute;top:100%;left:0;background-color:white;border:2px solid hsl(95,60%,55%);z-index:1;min-width:100%;margin-top:-2px}.WorkPage_SidePanel_AccountMenu-item{line-height:44px;padding-left:12px;padding-right:12px;cursor:pointer}.WorkPage_SidePanel_AccountMenu-item.active,.WorkPage_SidePanel_AccountMenu-item:active{background-color:hsl(95,70%,85%)}.WorkPage_SidePanel_Contact{display:block;width:100%;line-height:60px;cursor:pointer;padding-right:18px;padding-left:62px;background-repeat:no-repeat;background-position:left center;overflow:hidden;text-overflow:ellipsis;position:relative;border:2px solid transparent}.WorkPage_SidePanel_Contact.withMessages{padding-right:60px}.WorkPage_SidePanel_Contact.offline{color:hsl(0,0%,70%)}.WorkPage_SidePanel_Contact:focus{background-color:hsl(95,70%,95%)}.WorkPage_SidePanel_Contact:active{background-color:hsl(95,70%,85%)}.WorkPage_SidePanel_Contact.selected{color:white;background-color:hsl(95,60%,55%);border-color:hsl(95,60%,55%)}\n.WorkPage_SidePanel_Contact.selected:focus{background-color:hsl(95,60%,65%)}.WorkPage_SidePanel_Contact.selected:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.WorkPage_SidePanel_Contact.selected:disabled{background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}.WorkPage_SidePanel_Contact-number{position:absolute;top:15px;right:18px;min-width:30px;line-height:30px;background-color:hsl(350,70%,55%);color:white;display:none;text-align:center;padding-right:4px;padding-left:4px}.WorkPage_SidePanel_Contact-number.visible{display:block}@media(min-width:660px){.WorkPage_SidePanel_Contact{width:calc(100% + 20px)}}.WorkPage_SidePanel_ContactList{position:absolute;top:64px;right:0;bottom:0;left:0;overflow-y:auto}.WorkPage_SidePanel_ContactList-content{padding-top:10px;padding-bottom:20px;padding-left:20px;padding-right:20px}.WorkPage_SidePanel_ContactList-empty{color:hsl(0,0%,70%);padding:30px;text-align:center}\n.WorkPage_SidePanel_Panel{position:absolute;top:0;bottom:0;left:0;width:100%;padding:20px;background-color:white;background-position:center bottom;background-repeat:repeat-x}.WorkPage_SidePanel_Panel.chatOpen{display:none}.WorkPage_SidePanel_Panel-addContactButton{position:absolute;top:20px;right:20px;width:118px}@media(min-width:660px){.WorkPage_SidePanel_Panel{width:330px}.WorkPage_SidePanel_Panel.chatOpen{display:block}}@media(min-width:1100px){.WorkPage_SidePanel_Panel{width:400px}}.WorkPage_SidePanel_Title{display:inline-block;vertical-align:top;position:relative}.WorkPage_SidePanel_Title-button{display:inline-block;vertical-align:top;cursor:pointer;line-height:44px;padding-right:26px;padding-left:14px;background-repeat:no-repeat;background-position:calc(100% - 14px) center}.WorkPage_SidePanel_Title-button:focus{background-color:hsl(95,70%,95%)}.WorkPage_SidePanel_Title-button:active{background-color:hsl(95,70%,85%)}.WorkPage_SidePanel_Title-button.pressed{color:white;background-color:hsl(95,60%,55%)}\n.WorkPage_SidePanel_Title-button.pressed:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%)}.WorkPage_SidePanel_Title-buttonText{display:inline-block;vertical-align:top;max-width:102px;overflow:hidden;text-overflow:ellipsis;cursor:inherit;font-weight:bold}@media(min-width:400px){.WorkPage_SidePanel_Title-buttonText{max-width:182px}}@media(min-width:660px){.WorkPage_SidePanel_Title-buttonText{max-width:114px}}@media(min-width:1100px){.WorkPage_SidePanel_Title-buttonText{max-width:182px}}"
    document.head.appendChild(style)
})()

var getResourceUrl = (function () {
    var urls = {"img/arrow-collapsed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjkiIGhlaWdodD0iOSIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDMuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Im0gMSwxMDQzLjg2MjIgMCw4IDcsLTQgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/arrow-expanded.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjkiIGhlaWdodD0iOSIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDMuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Im0gOC41LDEwNDQuMzYyMiAtOCwwIDQsNyB6IiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDI5ODciIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/arrow-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjgiIGhlaWdodD0iOCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDQuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Ik0gMCwxIDgsMSA0LDggeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQ0LjM2MjIpIiBpZD0icGF0aDI5ODciIHN0eWxlPSJmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/arrow-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjgiIGhlaWdodD0iOCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDQuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Ik0gMCwxIDgsMSA0LDggeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQ0LjM2MjIpIiBpZD0icGF0aDI5ODciIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/arrow-up.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjgiIGhlaWdodD0iOCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDQuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Im0gMCwxMDUxLjM2MjIgOCwwIC00LC03IHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMjk4NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/arrow-up-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjgiIGhlaWdodD0iOCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDQuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Im0gMCwxMDUxLjM2MjIgOCwwIC00LC03IHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMjk4NyIgc3R5bGU9ImZpbGw6I2UyYjczNjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/arrow.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjgiIGhlaWdodD0iOCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwNDQuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Ik0gMCwxIDgsMSA0LDggeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQ0LjM2MjIpIiBpZD0icGF0aDI5ODciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/checked/active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjE3IiBoZWlnaHQ9IjE3IiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzNS4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAwLDEwNDMuODYyMiA2LDYgMTEsLTEwIC0yLC0yIC05LDggLTQsLTQgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iY29sb3I6IzAwMDAwMDtmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7bWFya2VyOm5vbmU7dmlzaWJpbGl0eTp2aXNpYmxlO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIvPjwvZz48L3N2Zz4K","img/checked/normal.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjE3IiBoZWlnaHQ9IjE3IiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzNS4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAwLDEwNDMuODYyMiA2LDYgMTEsLTEwIC0yLC0yIC05LDggLTQsLTQgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/close.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjEyIiBoZWlnaHQ9IjEyIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0MC4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAwLDEwIDIsMiA0LC00IDQsNCAyLC0yIEwgOCw2IDEyLDIgMTAsMCA2LDQgMiwwIDAsMiA0LDYgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQwLjM2MjIpIiBpZD0icGF0aDM3NTkiIHN0eWxlPSJmaWxsOiM4MGQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/clouds.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iNzAwIiBoZWlnaHQ9IjMwMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTc1Mi4zNjIxOCkiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgyLjQwMDM4ODEsMCwwLDIuNDAwMzg4MSwtMTg2LjczODE2LDg0MS4wMDI1NykiIGlkPSJwYXRoMzc1OSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCg0LjAwMDY0NzIsMCwwLDQuMDAwNjQ3MiwtMjc4LjA0NTk3LDc0My43NzcwOCkiIGlkPSJwYXRoMzc2MSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgzLjIwMDUxODIsMCwwLDMuMjAwNTE4MiwtOTQuMDE2MjUsNzk3LjcwMjU0KSIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDIuNDAwMzg4MSwwLDAsMi40MDAzODgxLC03OC4wMTM1OSw4NTUuNzk1MTkpIiBpZD0icGF0aDM3NjUiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDExMCw1NSBhIDE1LDE1IDAgMSAxIC0zMCwwIDE1LDE1IDAgMSAxIDMwLDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMi40MDAzODgxLDAsMCwyLjQwMDM4ODEsLTY2LjAxMTY1LDc5NS43ODU0OSkiIGlkPSJwYXRoMzc2NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjYwMDI1ODcsMCwwLDEuNjAwMjU4NywxMTEuNjU0MTYsODg2LjQwNTAyKSIgaWQ9InBhdGgzNzY5IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU0LC0xNDApIiBpZD0iZzM3NjUiPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgyLjQwMDM4ODEsMCwwLDIuNDAwMzg4MSwyMjguNDY2MzcsODQ4Ljk4NDcxKSIgaWQ9InBhdGgzNzcxIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDQuMDAwNjQ3MiwwLDAsNC4wMDA2NDcyLDIzMi4zNTA2Miw3NTMuNDUzMSkiIGlkPSJwYXRoMzc3MyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgzLjIwMDUxODIsMCwwLDMuMjAwNTE4MiwyNjkuNTYxMjQsODE4LjU1NjIyKSIgaWQ9InBhdGgzNzc1IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDIuNDAwMzg4MSwwLDAsMi40MDAzODgxLDMxNS4yNjcyLDgxNi4yNDg2NSkiIGlkPSJwYXRoMzc3NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMTEwLDU1IGEgMTUsMTUgMCAxIDEgLTMwLDAgMTUsMTUgMCAxIDEgMzAsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgzLjAyMjc0MjgsMCwwLDMuMDIyNzQyOCwyMTguMDc0Miw4MjIuMTUxOTIpIiBpZD0icGF0aDM3NzkiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDExMCw1NSBhIDE1LDE1IDAgMSAxIC0zMCwwIDE1LDE1IDAgMSAxIDMwLDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMi4zMzU3Njg3LDAsMCwyLjMzNTc2ODcsNDU1LjcxMTg5LDg1OC43MzgxOCkiIGlkPSJwYXRoMzc4MSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L2c+PC9zdmc+Cg==","img/grass.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjUwMCIgaGVpZ2h0PSIxNTAiIGlkPSJzdmcyIj48ZGVmcyBpZD0iZGVmczQiLz48bWV0YWRhdGEgaWQ9Im1ldGFkYXRhNyI+PHJkZjpSREY+PGNjOldvcmsgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiLz48ZGM6dGl0bGUvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05MDIuMzYyMTgpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC40ODQyMTQ0MiwwLDAsMC40ODQyMTQ0MiwyNy4zOTEzMDUsODgxLjg4MDQ3KSIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNDAzNTEyMDEsMCwwLDAuNDAzNTEyMDEsNzcuODI2MDg2LDg4Ni45NjA3NikiIGlkPSJwYXRoMzc1NyIgc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjUzODUwNzM0LDAsMCwwLjUzODUwNzM0LDIxNS40NjI1OCw4NTYuNzM1MTQpIiBpZD0icGF0aDM3NTkiIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC43MDk0NzEzNSwwLDAsMC43MDk0NzEzNSwzMTIuMDg4MSw4MDguNTI5MzgpIiBpZD0icGF0aDM3NjEiIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC41MDIzMzUxOCwwLDAsMC41MDIzMzUxOCwzODguNDE2MzcsODM4LjQ5NDM3KSIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNDQ4NDM3NTMsMCwwLDAuNDQ4NDM3NTMsNDU1LjM2NzQ2LDg2OC41NjU4NCkiIGlkPSJwYXRoMzc2NSIgc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTAwLC01LjQ2MzQ1NzNlLTYpIiBpZD0idXNlMzc2NyIgc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MSIgeD0iMCIgeT0iMCIgd2lkdGg9IjUwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4NyIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjM3NjYxMTIxLDAsMCwwLjM3NjYxMTIxLDEzMS4zMDQzNSw5MDEuOTg3NTIpIiBpZD0icGF0aDM3NjkiIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC43MDk0NzEzNSwwLDAsMC43MDk0NzEzNSwzOTQuMDg4MSw4NTEuNTI5MzgpIiBpZD0icGF0aDM3NzEiIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC43MDk0NzEzNSwwLDAsMC43MDk0NzEzNSwxMzUuMTMzNzYsODYyLjkyMDIzKSIgaWQ9InBhdGgyOTkyIiBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsMjQ5LjE5OTcyLDg1OC44NTQyNykiIGlkPSJwYXRoMjk5NCIgc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjcwOTQ3MTM1LDAsMCwwLjcwOTQ3MTM1LDQ2OS4xOTk3Miw4NDguODU0MjcpIiBpZD0icGF0aDI5OTYiIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/icon/128.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjEyOCIgaGVpZ2h0PSIxMjgiIGlkPSJzdmcyIj48ZGVmcyBpZD0iZGVmczQiLz48bWV0YWRhdGEgaWQ9Im1ldGFkYXRhNyI+PHJkZjpSREY+PGNjOldvcmsgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiLz48ZGM6dGl0bGUvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05MjQuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0ibWF0cml4KDgsMCwwLDgsMCwtNzM2Ni41MzU0KSIgaWQ9ImczNzYwIj48cGF0aCBkPSJtIDEyLjUsMy41IGEgMywzIDAgMSAxIC02LDAgMywzIDAgMSAxIDYsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjE2NjY2NjcsMCwwLDEuMTY2NjY2NywtMi4wODMzMzM1LDEwMzUuNzc4OSkiIGlkPSJwYXRoMzc1OSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjQ5OTk3MDIpIiBpZD0idXNlMzc2MSIgeD0iMCIgeT0iMCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4bGluazpocmVmPSIjcGF0aDM3NTkiLz48cGF0aCBkPSJtIDE1LjUsOS41IGEgNiw2IDAgMSAxIC0xMiwwIDYsNiAwIDEgMSAxMiwwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNS44NjIyKSIgaWQ9InBhdGgyOTg5IiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZmZmZmZmO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDEyLDMuNSBhIDIuNSwyLjUgMCAxIDEgLTUsMCAyLjUsMi41IDAgMSAxIDUsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzYuMzYyMikiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjUpIiBpZD0idXNlMzc2NSIgeD0iMCIgeT0iMCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4bGluazpocmVmPSIjcGF0aDM3NjMiLz48cGF0aCBkPSJNIDgsOSBBIDEsMSAwIDEgMSA2LDkgMSwxIDAgMSAxIDgsOSB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyLDEwMzQuMzYyMikiIGlkPSJwYXRoMzc2NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIsMikiIGlkPSJ1c2UzNzY5IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc2NyIvPjxyZWN0IHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeD0iMCIgeT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDM2LjM2MjIpIiBpZD0icmVjdDI5OTAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOm5vbmUiLz48L2c+PC9nPjwvc3ZnPgo=","img/icon/16.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzNi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczNzYwIj48cGF0aCBkPSJtIDEyLjUsMy41IGEgMywzIDAgMSAxIC02LDAgMywzIDAgMSAxIDYsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjE2NjY2NjcsMCwwLDEuMTY2NjY2NywtMi4wODMzMzM1LDEwMzUuNzc4OSkiIGlkPSJwYXRoMzc1OSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjQ5OTk3MDIpIiBpZD0idXNlMzc2MSIgeD0iMCIgeT0iMCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4bGluazpocmVmPSIjcGF0aDM3NTkiLz48cGF0aCBkPSJtIDE1LjUsOS41IGEgNiw2IDAgMSAxIC0xMiwwIDYsNiAwIDEgMSAxMiwwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNS44NjIyKSIgaWQ9InBhdGgyOTg5IiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZmZmZmZmO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDEyLDMuNSBhIDIuNSwyLjUgMCAxIDEgLTUsMCAyLjUsMi41IDAgMSAxIDUsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzYuMzYyMikiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjUpIiBpZD0idXNlMzc2NSIgeD0iMCIgeT0iMCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4bGluazpocmVmPSIjcGF0aDM3NjMiLz48cGF0aCBkPSJNIDgsOSBBIDEsMSAwIDEgMSA2LDkgMSwxIDAgMSAxIDgsOSB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyLDEwMzQuMzYyMikiIGlkPSJwYXRoMzc2NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIsMikiIGlkPSJ1c2UzNzY5IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc2NyIvPjxyZWN0IHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeD0iMCIgeT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDM2LjM2MjIpIiBpZD0icmVjdDI5OTAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOm5vbmUiLz48L2c+PC9nPjwvc3ZnPgo=","img/icon/256.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjI1NiIgaGVpZ2h0PSIyNTYiIGlkPSJzdmcyIj48ZGVmcyBpZD0iZGVmczQiLz48bWV0YWRhdGEgaWQ9Im1ldGFkYXRhNyI+PHJkZjpSREY+PGNjOldvcmsgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiLz48ZGM6dGl0bGUvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC03OTYuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0ibWF0cml4KDE2LDAsMCwxNiwwLC0xNTc4NS40MzMpIiBpZD0iZzM3NjAiPjxwYXRoIGQ9Im0gMTIuNSwzLjUgYSAzLDMgMCAxIDEgLTYsMCAzLDMgMCAxIDEgNiwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMTY2NjY2NywwLDAsMS4xNjY2NjY3LC0yLjA4MzMzMzUsMTAzNS43Nzg5KSIgaWQ9InBhdGgzNzU5IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNDk5OTcwMikiIGlkPSJ1c2UzNzYxIiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc1OSIvPjxwYXRoIGQ9Im0gMTUuNSw5LjUgYSA2LDYgMCAxIDEgLTEyLDAgNiw2IDAgMSAxIDEyLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM1Ljg2MjIpIiBpZD0icGF0aDI5ODkiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiNmZmZmZmY7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTIsMy41IGEgMi41LDIuNSAwIDEgMSAtNSwwIDIuNSwyLjUgMCAxIDEgNSwwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNi4zNjIyKSIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNSkiIGlkPSJ1c2UzNzY1IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc2MyIvPjxwYXRoIGQ9Ik0gOCw5IEEgMSwxIDAgMSAxIDYsOSAxLDEgMCAxIDEgOCw5IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIsMTAzNC4zNjIyKSIgaWQ9InBhdGgzNzY3IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwyKSIgaWQ9InVzZTM3NjkiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzY3Ii8+PHJlY3Qgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4PSIwIiB5PSIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzYuMzYyMikiIGlkPSJyZWN0Mjk5MCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZSIvPjwvZz48L2c+PC9zdmc+Cg==","img/icon/32.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAyMC4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgdHJhbnNmb3JtPSJtYXRyaXgoMiwwLDAsMiwwLC0xMDUyLjM2MjIpIiBpZD0iZzM3NjAiPjxwYXRoIGQ9Im0gMTIuNSwzLjUgYSAzLDMgMCAxIDEgLTYsMCAzLDMgMCAxIDEgNiwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMTY2NjY2NywwLDAsMS4xNjY2NjY3LC0yLjA4MzMzMzUsMTAzNS43Nzg5KSIgaWQ9InBhdGgzNzU5IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNDk5OTcwMikiIGlkPSJ1c2UzNzYxIiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc1OSIvPjxwYXRoIGQ9Im0gMTUuNSw5LjUgYSA2LDYgMCAxIDEgLTEyLDAgNiw2IDAgMSAxIDEyLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM1Ljg2MjIpIiBpZD0icGF0aDI5ODkiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiNmZmZmZmY7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTIsMy41IGEgMi41LDIuNSAwIDEgMSAtNSwwIDIuNSwyLjUgMCAxIDEgNSwwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNi4zNjIyKSIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNSkiIGlkPSJ1c2UzNzY1IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc2MyIvPjxwYXRoIGQ9Ik0gOCw5IEEgMSwxIDAgMSAxIDYsOSAxLDEgMCAxIDEgOCw5IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIsMTAzNC4zNjIyKSIgaWQ9InBhdGgzNzY3IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwyKSIgaWQ9InVzZTM3NjkiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzY3Ii8+PHJlY3Qgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4PSIwIiB5PSIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzYuMzYyMikiIGlkPSJyZWN0Mjk5MCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZSIvPjwvZz48L2c+PC9zdmc+Cg==","img/icon/512.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjUxMiIgaGVpZ2h0PSI1MTIiIGlkPSJzdmcyIj48ZGVmcyBpZD0iZGVmczQiLz48bWV0YWRhdGEgaWQ9Im1ldGFkYXRhNyI+PHJkZjpSREY+PGNjOldvcmsgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiLz48ZGM6dGl0bGUvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC01NDAuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0ibWF0cml4KDMyLDAsMCwzMiwwLC0zMjYyMy4yMjgpIiBpZD0iZzM3NjAiPjxwYXRoIGQ9Im0gMTIuNSwzLjUgYSAzLDMgMCAxIDEgLTYsMCAzLDMgMCAxIDEgNiwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMTY2NjY2NywwLDAsMS4xNjY2NjY3LC0yLjA4MzMzMzUsMTAzNS43Nzg5KSIgaWQ9InBhdGgzNzU5IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNDk5OTcwMikiIGlkPSJ1c2UzNzYxIiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc1OSIvPjxwYXRoIGQ9Im0gMTUuNSw5LjUgYSA2LDYgMCAxIDEgLTEyLDAgNiw2IDAgMSAxIDEyLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM1Ljg2MjIpIiBpZD0icGF0aDI5ODkiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiNmZmZmZmY7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTIsMy41IGEgMi41LDIuNSAwIDEgMSAtNSwwIDIuNSwyLjUgMCAxIDEgNSwwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNi4zNjIyKSIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNSkiIGlkPSJ1c2UzNzY1IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHhsaW5rOmhyZWY9IiNwYXRoMzc2MyIvPjxwYXRoIGQ9Ik0gOCw5IEEgMSwxIDAgMSAxIDYsOSAxLDEgMCAxIDEgOCw5IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIsMTAzNC4zNjIyKSIgaWQ9InBhdGgzNzY3IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwyKSIgaWQ9InVzZTM3NjkiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzY3Ii8+PHJlY3Qgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4PSIwIiB5PSIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzYuMzYyMikiIGlkPSJyZWN0Mjk5MCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZSIvPjwvZz48L2c+PC9zdmc+Cg==","img/icon/64.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTg4LjM2MjIpIiBpZD0ibGF5ZXIxIj48ZyB0cmFuc2Zvcm09Im1hdHJpeCg0LDAsMCw0LDAsLTMxNTcuMDg2NikiIGlkPSJnMzc2MCI+PHBhdGggZD0ibSAxMi41LDMuNSBhIDMsMyAwIDEgMSAtNiwwIDMsMyAwIDEgMSA2LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMS4xNjY2NjY3LDAsMCwxLjE2NjY2NjcsLTIuMDgzMzMzNSwxMDM1Ljc3ODkpIiBpZD0icGF0aDM3NTkiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01LjUsNS40OTk5NzAyKSIgaWQ9InVzZTM3NjEiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzU5Ii8+PHBhdGggZD0ibSAxNS41LDkuNSBhIDYsNiAwIDEgMSAtMTIsMCA2LDYgMCAxIDEgMTIsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzUuODYyMikiIGlkPSJwYXRoMjk4OSIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2ZmZmZmZjtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggZD0ibSAxMiwzLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM2LjM2MjIpIiBpZD0icGF0aDM3NjMiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01LjUsNS41KSIgaWQ9InVzZTM3NjUiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzYzIi8+PHBhdGggZD0iTSA4LDkgQSAxLDEgMCAxIDEgNiw5IDEsMSAwIDEgMSA4LDkgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMiwxMDM0LjM2MjIpIiBpZD0icGF0aDM3NjciIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yLDIpIiBpZD0idXNlMzc2OSIgeD0iMCIgeT0iMCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4bGluazpocmVmPSIjcGF0aDM3NjciLz48cmVjdCB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHg9IjAiIHk9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzNi4zNjIyKSIgaWQ9InJlY3QyOTkwIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTpub25lIi8+PC9nPjwvZz48L3N2Zz4K","img/light-grass.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgd2lkdGg9IjUwMCIgaGVpZ2h0PSIxMDAiIGlkPSJzdmcyIj48ZGVmcyBpZD0iZGVmczQiLz48bWV0YWRhdGEgaWQ9Im1ldGFkYXRhNyI+PHJkZjpSREY+PGNjOldvcmsgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiLz48ZGM6dGl0bGUvPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05NTIuMzYyMTgpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC40ODQyMTQ3OSwwLDAsMC40ODQyMTQ3OSwyNy4zOTEzMjUsODg2Ljg4MDMzKSIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNDAzNTEyMDEsMCwwLDAuNDAzNTEyMDEsNzcuODI2MDg2LDg5Ni45NjA3MikiIGlkPSJwYXRoMzc1NyIgc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjQ4NDcwNTc3LDAsMCwwLjQ4NDcwNTc3LDIxNy40MTkxLDg3MS43ODg2MSkiIGlkPSJwYXRoMzc1OSIgc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjUxNjk4NjY5LDAsMCwwLjUxNjk4NjY5LDI5OC4zNzEwOSw4NjguMTQzMzIpIiBpZD0icGF0aDM3NjEiIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC4yODcxMjg3NiwwLDAsMC4yODcxMjg3NiwzOTYuMjQyNDYsOTA4LjcwODQzKSIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNDQ4NDM3NTMsMCwwLDAuNDQ4NDM3NTMsNDY1LjM2NzQ2LDg3OC41NjU4KSIgaWQ9InBhdGgzNzY1IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1MDAsLTQuNTI2MDgzZS02KSIgaWQ9InVzZTM3NjciIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjEiIHg9IjAiIHk9IjAiIHdpZHRoPSI1MDAiIGhlaWdodD0iMzAwIiB4bGluazpocmVmPSIjcGF0aDI5ODciLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC4zNzY2MTEyMSwwLDAsMC4zNzY2MTEyMSwxMzkuMTgzMDMsOTAxLjk4NzQ4KSIgaWQ9InBhdGgzNzY5IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsMzgxLjU0Nzk3LDg2OS43ODgyNykiIGlkPSJwYXRoMzc3MSIgc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjcwOTQ3MTM1LDAsMCwwLjcwOTQ3MTM1LDEzNS4yNDQ1OCw4NTkuNzkzNTcpIiBpZD0icGF0aDI5OTIiIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC43MDk0NzEzNSwwLDAsMC43MDk0NzEzNSwyNTEuMzIxMDQsODY4LjUwMDY4KSIgaWQ9InBhdGgyOTk0IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsNDY5LjE5OTcyLDg1OC44NTQyMykiIGlkPSJwYXRoMjk5NiIgc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBhIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgLTE4NS44NjgwNzQsMCA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIDE4NS44NjgwNzQsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjI4NzEyODc2LDAsMCwwLjI4NzEyODc2LDM1Ni4yNDI0Niw5MjguNzA4NDMpIiBpZD0icGF0aDI5OTUiIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/logo.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIxMTIiIGhlaWdodD0iMTU0IiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtODk4LjM2MjIpIiBpZD0ibGF5ZXIxIj48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOCwtMi42NzdlLTQpIiBpZD0iZzM4NDkiPjxwYXRoIGQ9Im0gMTUuNSw5LjUgYSA2LDYgMCAxIDEgLTEyLDAgNiw2IDAgMSAxIDEyLDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoNy4zMzMzMzM0LDAsMCw3LjMzMzMzMzQsMi4zMzMzMzMyLDg5Mi42OTU4KSIgaWQ9InBhdGgyOTg5IiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAxMiwzLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoOCwwLDAsOCwtNCw4OTAuMzYyMikiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTQ0LDQ0KSIgaWQ9InVzZTM3NjUiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzYzIi8+PHBhdGggZD0iTSA4LDkgQSAxLDEgMCAxIDEgNiw5IDEsMSAwIDEgMSA4LDkgeiIgdHJhbnNmb3JtPSJtYXRyaXgoOCwwLDAsOCwxNiw4NzQuMzYyMikiIGlkPSJwYXRoMzc2NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE2LDE2KSIgaWQ9InVzZTM3NjkiIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeGxpbms6aHJlZj0iI3BhdGgzNzY3Ii8+PC9nPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMiwxOCkiIGlkPSJnMzgyNyIgc3R5bGU9InN0cm9rZTojODFkMTQ3O3N0cm9rZS1vcGFjaXR5OjEiPjxwYXRoIGQ9Im0gMTYsMTQwIDAsMjQiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsODYwLjM2MjIpIiBpZD0icGF0aDM3ODkiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48cGF0aCBkPSJtIDMyLDE1NiBhIDgsOCAwIDEgMSAtMTYsMCA4LDggMCAxIDEgMTYsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDg2MC4zNjIyKSIgaWQ9InBhdGgzNzkxIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwwKSIgaWQ9ImczODEzIiBzdHlsZT0ic3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLW9wYWNpdHk6MSI+PHBhdGggZD0ibSAzMiwxNTYgYSA4LDggMCAxIDEgLTE2LDAgOCw4IDAgMSAxIDE2LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjQsODYwLjM2MjIpIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIGQ9Im0gNTYsMTAwOC4zNjIyIDAsMTYiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzc5NSIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjwvZz48cGF0aCBkPSJtIDYwLDEwMDguMzYyMiAxNiwwIC0xNiwxNiAxNiwwIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNiwwKSIgaWQ9ImczODE3IiBzdHlsZT0ic3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLW9wYWNpdHk6MSI+PHBhdGggZD0ibSAzMiwxNTYgYSA4LDggMCAxIDEgLTE2LDAgOCw4IDAgMSAxIDE2LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNzIsODYwLjM2MjIpIiBpZD0icGF0aDM3OTkiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIGQ9Im0gMTA0LDEwMDguMzYyMiAwLDE2IiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM4MDEiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48cGF0aCBkPSJtIDMyLDE1NiBhIDgsOCAwIDAgMSAtOCw4IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg3Miw4NjguMzYyMikiIGlkPSJwYXRoMzgwMyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNiwwKSIgaWQ9ImczODIyIiBzdHlsZT0ic3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLW9wYWNpdHk6MSI+PHBhdGggZD0ibSAzMiwxNTYgYSA4LDggMCAxIDEgLTE2LDAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDk0LDg2MC4zNjIyKSIgaWQ9InBhdGgzODA1IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjE7c3Ryb2tlLWRhc2hhcnJheTpub25lIi8+PHBhdGggZD0ibSAxMjYsMTAwOC4zNjIyIDAsMTYiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzgwNyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIGQ9Im0gMTEwLDEwMDguMzYyMiAwLDgiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzgwOSIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjwvZz48L2c+PC9nPjwvc3ZnPgo=","img/privacy/contacts-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAyNS45MDYyNSw5LjQwNjI1IGMgLTEuMTU5NTU1LDAgLTIuMTI1OTg1LDAuNzM4Njk5IC0yLjQ2ODc1LDEuNzgxMjUgMC45OTE3MzQsMC45NzM5NjIgMS42MTIxNSwyLjMwMjEgMS42NTYyNSwzLjc4MTI1IDIuMzkzMzYxLDEuNjY5NjUzIDQuMDUzMTYxLDQuMzIwODA5IDQuMjE4NzUsNy40MDYyNSAxLjQyMjg0NywtMS4wNDc0NzYgMi4zNDM3NSwtMi43MjU2NyAyLjM0Mzc1LC00LjYyNSAwLC0yLjI2MTUgLTEuMzE4ODc2LC00LjIxOTY1IC0zLjIxODc1LC01LjE1NjI1IEMgMjguNDgyNzMsMTIuNDAxMTUgMjguNSwxMi4yMDYyIDI4LjUsMTIgMjguNSwxMC41NTgyIDI3LjM0Nzk5OCw5LjQwNjI1IDI1LjkwNjI1LDkuNDA2MjUgeiBtIC02LjMxMjUsMi4xODc1IGMgLTEuOTQzMjI0LDAgLTMuNTMxMjUsMS41ODgwNSAtMy41MzEyNSwzLjUzMTI1IDAsMC4yNzggMC4wNjQwNCwwLjU1MzEgMC4xMjUsMC44MTI1IC0xLjUxNTg5NCwwLjc0ODcgLTIuNzY4NjgyLDEuOTg5OCAtMy41MzEyNSwzLjUgLTAuMjU5MDY2LC0wLjA2MSAtMC41MzQ4OTcsLTAuMDkzNzUgLTAuODEyNSwtMC4wOTM3NSAtMS45NDMyMjQxLDAgLTMuNSwxLjU4ODA1IC0zLjUsMy41MzEyNSAwLDEuOTQzMiAxLjU1Njc3NTksMy41IDMuNSwzLjUgMC4yNzc2MDMsMCAwLjU1MzQzNCwtMC4wMzI3NSAwLjgxMjUsLTAuMDkzNzUgMS4yNjUxMjQsMi41NTQyIDMuODg5NDgzLDQuMzEyNSA2LjkzNzUsNC4zMTI1IDQuMjc1MDkzLDAgNy43NSwtMy40NDM2NSA3Ljc1LC03LjcxODc1IDAsLTMuMDQ2IC0xLjc4NTc5NywtNS42NzQxIC00LjM0Mzc1LC02LjkzNzUgMC4wNjA5NiwtMC4yNTk0IDAuMTI1LC0wLjUzNDUgMC4xMjUsLTAuODEyNSAwLC0xLjk0MzIgLTEuNTg4MDI2LC0zLjUzMTI1IC0zLjUzMTI1LC0zLjUzMTI1IHogbSAtMC4yODEyNSw3LjA2MjUgYyAwLjA5MTYyLC0wLjAxOSAwLjE4NDA4OSwwIDAuMjgxMjUsMCAwLjc3NzI5LDAgMS40MDYyNSwwLjYyODk1IDEuNDA2MjUsMS40MDYyNSAwLDAuNzc3MyAtMC42Mjg5NiwxLjQwNjI1IC0xLjQwNjI1LDEuNDA2MjUgLTAuNzc3MjksMCAtMS40MDYyNSwtMC42Mjg5NSAtMS40MDYyNSwtMS40MDYyNSAwLC0wLjY4MDEgMC40ODM2NjgsLTEuMjc1MDUgMS4xMjUsLTEuNDA2MjUgeiBtIC0yLjUzMTI1LDIuODEyNSBjIDAuNzc3MjksMCAxLjQwNjI1LDAuNjI4OTUgMS40MDYyNSwxLjQwNjI1IDAsMC43NzczIC0wLjYyODk2LDEuNDA2MjUgLTEuNDA2MjUsMS40MDYyNSAtMC43NzcyOSwwIC0xLjQwNjI1LC0wLjYyODk1IC0xLjQwNjI1LC0xLjQwNjI1IDAsLTAuNzc3MyAwLjYyODk2LC0xLjQwNjI1IDEuNDA2MjUsLTEuNDA2MjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDI5ODkiIHN0eWxlPSJmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/contacts-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAyNS45MDYyNSw5LjQwNjI1IGMgLTEuMTU5NTU1LDAgLTIuMTI1OTg1LDAuNzM4Njk5IC0yLjQ2ODc1LDEuNzgxMjUgMC45OTE3MzQsMC45NzM5NjIgMS42MTIxNSwyLjMwMjEgMS42NTYyNSwzLjc4MTI1IDIuMzkzMzYxLDEuNjY5NjUzIDQuMDUzMTYxLDQuMzIwODA5IDQuMjE4NzUsNy40MDYyNSAxLjQyMjg0NywtMS4wNDc0NzYgMi4zNDM3NSwtMi43MjU2NyAyLjM0Mzc1LC00LjYyNSAwLC0yLjI2MTUgLTEuMzE4ODc2LC00LjIxOTY1IC0zLjIxODc1LC01LjE1NjI1IEMgMjguNDgyNzMsMTIuNDAxMTUgMjguNSwxMi4yMDYyIDI4LjUsMTIgMjguNSwxMC41NTgyIDI3LjM0Nzk5OCw5LjQwNjI1IDI1LjkwNjI1LDkuNDA2MjUgeiBtIC02LjMxMjUsMi4xODc1IGMgLTEuOTQzMjI0LDAgLTMuNTMxMjUsMS41ODgwNSAtMy41MzEyNSwzLjUzMTI1IDAsMC4yNzggMC4wNjQwNCwwLjU1MzEgMC4xMjUsMC44MTI1IC0xLjUxNTg5NCwwLjc0ODcgLTIuNzY4NjgyLDEuOTg5OCAtMy41MzEyNSwzLjUgLTAuMjU5MDY2LC0wLjA2MSAtMC41MzQ4OTcsLTAuMDkzNzUgLTAuODEyNSwtMC4wOTM3NSAtMS45NDMyMjQxLDAgLTMuNSwxLjU4ODA1IC0zLjUsMy41MzEyNSAwLDEuOTQzMiAxLjU1Njc3NTksMy41IDMuNSwzLjUgMC4yNzc2MDMsMCAwLjU1MzQzNCwtMC4wMzI3NSAwLjgxMjUsLTAuMDkzNzUgMS4yNjUxMjQsMi41NTQyIDMuODg5NDgzLDQuMzEyNSA2LjkzNzUsNC4zMTI1IDQuMjc1MDkzLDAgNy43NSwtMy40NDM2NSA3Ljc1LC03LjcxODc1IDAsLTMuMDQ2IC0xLjc4NTc5NywtNS42NzQxIC00LjM0Mzc1LC02LjkzNzUgMC4wNjA5NiwtMC4yNTk0IDAuMTI1LC0wLjUzNDUgMC4xMjUsLTAuODEyNSAwLC0xLjk0MzIgLTEuNTg4MDI2LC0zLjUzMTI1IC0zLjUzMTI1LC0zLjUzMTI1IHogbSAtMC4yODEyNSw3LjA2MjUgYyAwLjA5MTYyLC0wLjAxOSAwLjE4NDA4OSwwIDAuMjgxMjUsMCAwLjc3NzI5LDAgMS40MDYyNSwwLjYyODk1IDEuNDA2MjUsMS40MDYyNSAwLDAuNzc3MyAtMC42Mjg5NiwxLjQwNjI1IC0xLjQwNjI1LDEuNDA2MjUgLTAuNzc3MjksMCAtMS40MDYyNSwtMC42Mjg5NSAtMS40MDYyNSwtMS40MDYyNSAwLC0wLjY4MDEgMC40ODM2NjgsLTEuMjc1MDUgMS4xMjUsLTEuNDA2MjUgeiBtIC0yLjUzMTI1LDIuODEyNSBjIDAuNzc3MjksMCAxLjQwNjI1LDAuNjI4OTUgMS40MDYyNSwxLjQwNjI1IDAsMC43NzczIC0wLjYyODk2LDEuNDA2MjUgLTEuNDA2MjUsMS40MDYyNSAtMC43NzcyOSwwIC0xLjQwNjI1LC0wLjYyODk1IC0xLjQwNjI1LC0xLjQwNjI1IDAsLTAuNzc3MyAwLjYyODk2LC0xLjQwNjI1IDEuNDA2MjUsLTEuNDA2MjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDI5ODkiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/contacts.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAyNS45MDYyNSw5LjQwNjI1IGMgLTEuMTU5NTU1LDAgLTIuMTI1OTg1LDAuNzM4Njk5IC0yLjQ2ODc1LDEuNzgxMjUgMC45OTE3MzQsMC45NzM5NjIgMS42MTIxNSwyLjMwMjEgMS42NTYyNSwzLjc4MTI1IDIuMzkzMzYxLDEuNjY5NjUzIDQuMDUzMTYxLDQuMzIwODA5IDQuMjE4NzUsNy40MDYyNSAxLjQyMjg0NywtMS4wNDc0NzYgMi4zNDM3NSwtMi43MjU2NyAyLjM0Mzc1LC00LjYyNSAwLC0yLjI2MTUgLTEuMzE4ODc2LC00LjIxOTY1IC0zLjIxODc1LC01LjE1NjI1IEMgMjguNDgyNzMsMTIuNDAxMTUgMjguNSwxMi4yMDYyIDI4LjUsMTIgMjguNSwxMC41NTgyIDI3LjM0Nzk5OCw5LjQwNjI1IDI1LjkwNjI1LDkuNDA2MjUgeiBtIC02LjMxMjUsMi4xODc1IGMgLTEuOTQzMjI0LDAgLTMuNTMxMjUsMS41ODgwNSAtMy41MzEyNSwzLjUzMTI1IDAsMC4yNzggMC4wNjQwNCwwLjU1MzEgMC4xMjUsMC44MTI1IC0xLjUxNTg5NCwwLjc0ODcgLTIuNzY4NjgyLDEuOTg5OCAtMy41MzEyNSwzLjUgLTAuMjU5MDY2LC0wLjA2MSAtMC41MzQ4OTcsLTAuMDkzNzUgLTAuODEyNSwtMC4wOTM3NSAtMS45NDMyMjQxLDAgLTMuNSwxLjU4ODA1IC0zLjUsMy41MzEyNSAwLDEuOTQzMiAxLjU1Njc3NTksMy41IDMuNSwzLjUgMC4yNzc2MDMsMCAwLjU1MzQzNCwtMC4wMzI3NSAwLjgxMjUsLTAuMDkzNzUgMS4yNjUxMjQsMi41NTQyIDMuODg5NDgzLDQuMzEyNSA2LjkzNzUsNC4zMTI1IDQuMjc1MDkzLDAgNy43NSwtMy40NDM2NSA3Ljc1LC03LjcxODc1IDAsLTMuMDQ2IC0xLjc4NTc5NywtNS42NzQxIC00LjM0Mzc1LC02LjkzNzUgMC4wNjA5NiwtMC4yNTk0IDAuMTI1LC0wLjUzNDUgMC4xMjUsLTAuODEyNSAwLC0xLjk0MzIgLTEuNTg4MDI2LC0zLjUzMTI1IC0zLjUzMTI1LC0zLjUzMTI1IHogbSAtMC4yODEyNSw3LjA2MjUgYyAwLjA5MTYyLC0wLjAxOSAwLjE4NDA4OSwwIDAuMjgxMjUsMCAwLjc3NzI5LDAgMS40MDYyNSwwLjYyODk1IDEuNDA2MjUsMS40MDYyNSAwLDAuNzc3MyAtMC42Mjg5NiwxLjQwNjI1IC0xLjQwNjI1LDEuNDA2MjUgLTAuNzc3MjksMCAtMS40MDYyNSwtMC42Mjg5NSAtMS40MDYyNSwtMS40MDYyNSAwLC0wLjY4MDEgMC40ODM2NjgsLTEuMjc1MDUgMS4xMjUsLTEuNDA2MjUgeiBtIC0yLjUzMTI1LDIuODEyNSBjIDAuNzc3MjksMCAxLjQwNjI1LDAuNjI4OTUgMS40MDYyNSwxLjQwNjI1IDAsMC43NzczIC0wLjYyODk2LDEuNDA2MjUgLTEuNDA2MjUsMS40MDYyNSAtMC43NzcyOSwwIC0xLjQwNjI1LC0wLjYyODk1IC0xLjQwNjI1LC0xLjQwNjI1IDAsLTAuNzc3MyAwLjYyODk2LC0xLjQwNjI1IDEuNDA2MjUsLTEuNDA2MjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDI5ODkiIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/private-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAxNCwxMDI3LjM2MjIgMCwzIC0yLDAgYyAtMC41LDAgLTEsMC41IC0xLDEgbCAwLDEwIGMgMCwwLjUgMC41LDEgMSwxIGwgMTYsMCBjIDAuNSwwIDEsLTAuNSAxLC0xIGwgMCwtMTAgYyAwLC0wLjUgLTAuNSwtMSAtMSwtMSBsIC0yLDAgMCwtMyBjIDAsLTMgLTMsLTYgLTYsLTYgLTMsMCAtNiwzIC02LDYgeiBtIDYsLTQgYyAyLDAgNCwyIDQsNCBsIDAsMyAtOCwwIDAsLTMgYyAwLC0yIDIsLTQgNCwtNCB6IG0gMCwxMCBjIDEsMCAyLDEgMiwyIDAsMSAtMSwxLjA0MDggLTEsMiBsIDAsMiAtMiwwIDAsLTIgYyAwLC0wLjk1OTIgLTEsLTEgLTEsLTIgMCwtMSAxLC0yIDIsLTIgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzMDA2IiBzdHlsZT0iZmlsbDojYTllODdjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/privacy/private-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAxNCwxMDI3LjM2MjIgMCwzIC0yLDAgYyAtMC41LDAgLTEsMC41IC0xLDEgbCAwLDEwIGMgMCwwLjUgMC41LDEgMSwxIGwgMTYsMCBjIDAuNSwwIDEsLTAuNSAxLC0xIGwgMCwtMTAgYyAwLC0wLjUgLTAuNSwtMSAtMSwtMSBsIC0yLDAgMCwtMyBjIDAsLTMgLTMsLTYgLTYsLTYgLTMsMCAtNiwzIC02LDYgeiBtIDYsLTQgYyAyLDAgNCwyIDQsNCBsIDAsMyAtOCwwIDAsLTMgYyAwLC0yIDIsLTQgNCwtNCB6IG0gMCwxMCBjIDEsMCAyLDEgMiwyIDAsMSAtMSwxLjA0MDggLTEsMiBsIDAsMiAtMiwwIDAsLTIgYyAwLC0wLjk1OTIgLTEsLTEgLTEsLTIgMCwtMSAxLC0yIDIsLTIgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzMDA2LTMiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/private.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAxNCwxMDI3LjM2MjIgMCwzIC0yLDAgYyAtMC41LDAgLTEsMC41IC0xLDEgbCAwLDEwIGMgMCwwLjUgMC41LDEgMSwxIGwgMTYsMCBjIDAuNSwwIDEsLTAuNSAxLC0xIGwgMCwtMTAgYyAwLC0wLjUgLTAuNSwtMSAtMSwtMSBsIC0yLDAgMCwtMyBjIDAsLTMgLTMsLTYgLTYsLTYgLTMsMCAtNiwzIC02LDYgeiBtIDYsLTQgYyAyLDAgNCwyIDQsNCBsIDAsMyAtOCwwIDAsLTMgYyAwLC0yIDIsLTQgNCwtNCB6IG0gMCwxMCBjIDEsMCAyLDEgMiwyIDAsMSAtMSwxLjA0MDggLTEsMiBsIDAsMiAtMiwwIDAsLTIgYyAwLC0wLjk1OTIgLTEsLTEgLTEsLTIgMCwtMSAxLC0yIDIsLTIgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzMDA2IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/privacy/public-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAyOS41LDIwIGEgOS41LDkuNSAwIDEgMSAtMTksMCA5LjUsOS41IDAgMSAxIDE5LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDMxMDciIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNhOWU4N2M7c3Ryb2tlLXdpZHRoOjI7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48cGF0aCBkPSJtIDE4LjA2MjUsMTEuMDYyNSAtNS4yNSwyLjM3NSBMIDExLDE5LjUgMTYuMTI1LDE4LjE4NzUgMTYsMTQuOTM3NSBsIDIuNTYyNSwtMiAtMC41LC0xLjg3NSB6IG0gNS4yNSwwLjUgTCAyMiwxNS4zMTI1IDIzLjU2MjUsMTUuMzc1IDIyLjM3NSwxNy44MTI1IDI1LDE2LjYyNSAyOC44MTI1LDE3IGwgLTAuODc1LC0yLjgxMjUgLTQuNjI1LC0yLjYyNSB6IG0gMS4yNSw2LjEyNSAtNC4xODc1LDMuMDYyNSAyLDIuMzEyNSAtMC42MjUsMy41NjI1IDIuMTg3NSwyLjA2MjUgNC42MjUsLTQuMjUgMS4zMTI1LC01LjEyNSAtNS4zMTI1LC0xLjYyNSB6IE0gMTMuNSwxOS41IGwgLTIuMzc1LDEgMC41NjI1LDQuMTg3NSBMIDE2Ljc1LDI4Ljg3NSAxOC4wNjI1LDI2LjY4NzUgMTUuNjI1LDIzLjI1IDE2LjE4NzUsMjAuODEyNSAxMy41LDE5LjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDM4NzciIHN0eWxlPSJmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/public-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAyOS41LDIwIGEgOS41LDkuNSAwIDEgMSAtMTksMCA5LjUsOS41IDAgMSAxIDE5LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDMxMDciIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNmZmZmZmY7c3Ryb2tlLXdpZHRoOjI7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48cGF0aCBkPSJtIDE4LjA2MjUsMTEuMDYyNSAtNS4yNSwyLjM3NSBMIDExLDE5LjUgMTYuMTI1LDE4LjE4NzUgMTYsMTQuOTM3NSBsIDIuNTYyNSwtMiAtMC41LC0xLjg3NSB6IG0gNS4yNSwwLjUgTCAyMiwxNS4zMTI1IDIzLjU2MjUsMTUuMzc1IDIyLjM3NSwxNy44MTI1IDI1LDE2LjYyNSAyOC44MTI1LDE3IGwgLTAuODc1LC0yLjgxMjUgLTQuNjI1LC0yLjYyNSB6IG0gMS4yNSw2LjEyNSAtNC4xODc1LDMuMDYyNSAyLDIuMzEyNSAtMC42MjUsMy41NjI1IDIuMTg3NSwyLjA2MjUgNC42MjUsLTQuMjUgMS4zMTI1LC01LjEyNSAtNS4zMTI1LC0xLjYyNSB6IE0gMTMuNSwxOS41IGwgLTIuMzc1LDEgMC41NjI1LDQuMTg3NSBMIDE2Ljc1LDI4Ljg3NSAxOC4wNjI1LDI2LjY4NzUgMTUuNjI1LDIzLjI1IDE2LjE4NzUsMjAuODEyNSAxMy41LDE5LjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDM4NzciIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/public.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjQwIiBoZWlnaHQ9IjQwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAxMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAyOS41LDIwIGEgOS41LDkuNSAwIDEgMSAtMTksMCA5LjUsOS41IDAgMSAxIDE5LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDMxMDciIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjI7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48cGF0aCBkPSJtIDE4LjA2MjUsMTEuMDYyNSAtNS4yNSwyLjM3NSBMIDExLDE5LjUgMTYuMTI1LDE4LjE4NzUgMTYsMTQuOTM3NSBsIDIuNTYyNSwtMiAtMC41LC0xLjg3NSB6IG0gNS4yNSwwLjUgTCAyMiwxNS4zMTI1IDIzLjU2MjUsMTUuMzc1IDIyLjM3NSwxNy44MTI1IDI1LDE2LjYyNSAyOC44MTI1LDE3IGwgLTAuODc1LC0yLjgxMjUgLTQuNjI1LC0yLjYyNSB6IG0gMS4yNSw2LjEyNSAtNC4xODc1LDMuMDYyNSAyLDIuMzEyNSAtMC42MjUsMy41NjI1IDIuMTg3NSwyLjA2MjUgNC42MjUsLTQuMjUgMS4zMTI1LC01LjEyNSAtNS4zMTI1LC0xLjYyNSB6IE0gMTMuNSwxOS41IGwgLTIuMzc1LDEgMC41NjI1LDQuMTg3NSBMIDE2Ljc1LDI4Ljg3NSAxOC4wNjI1LDI2LjY4NzUgMTUuNjI1LDIzLjI1IDE2LjE4NzUsMjAuODEyNSAxMy41LDE5LjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBpZD0icGF0aDM4NzciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/smiley/face-cat.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48ZyBpZD0iZzMwMjUiPjxwYXRoIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjxwYXRoIGQ9Im0gMiwxMCAwLC05LjIgNiwyIHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMi4zNjIyKSIgaWQ9InBhdGgzODQyIiBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09Im1hdHJpeCgtMSwwLDAsMSwyMCwwLjIpIiBpZD0idXNlMzg0NCIgeD0iMCIgeT0iMCIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiB4bGluazpocmVmPSIjcGF0aDM4NDIiLz48cGF0aCBkPSJtIDYuNSwxMDQ1LjM2MjIgYyAwLjIsMC4yIDAuNywwLjUgMS41LDAuNSAxLjIsMCAyLC0xLjMgMiwtMiAwLDAuNyAwLjgsMiAyLDIgMC44LDAgMS4zLC0wLjMgMS41LC0wLjUiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzg0OCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggZD0ibSAxMC44LDEwLjUgLTEuNiwwIGMgMCwwLjEgMC44LDAuMyAwLjgsMSAwLC0wLjcgMC44LC0wLjkgMC44LC0xIHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMi4zNjIyKSIgaWQ9InBhdGgzODYwIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48ZyBpZD0iZzM4NjYiPjxwYXRoIGQ9Im0gMTQuNSwxMDQzLjM2MjIgMiwwLjUiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzg2MiIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzljN2ExNjtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggZD0ibSAxNC41LDEwNDEuODYyMiAyLDAiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzg2NCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzljN2ExNjtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIi8+PC9nPjx1c2UgdHJhbnNmb3JtPSJtYXRyaXgoLTEsMCwwLDEsMjAsMCkiIGlkPSJ1c2UzODcwIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNnMzg2NiIvPjwvZz48L3N2Zz4K","img/smiley/face-cat-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xKSIgaWQ9ImczMDI1Ij48cGF0aCBkPSJtIDEwLjQsOC40OTk5OTk5IGEgMS41LDEuNSAwIDAgMSAzLDEwZS04IGwgLTEuNSwwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMiwxMDMxLjg2MjIpIiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC00LjA5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIvPjwvZz48cGF0aCBkPSJtIDIsMTAgMCwtOS4yIDYsMiB6IiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzIuMzYyMikiIGlkPSJwYXRoMzg0MiIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJtYXRyaXgoLTEsMCwwLDEsMjAsMC4yKSIgaWQ9InVzZTM4NDQiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzODQyIi8+PHBhdGggZD0ibSA2LjUsMTA0My44NjIyIGMgMC4xLDEgMC42LDIgMS41LDIgMSwwIDIsLTEuNyAyLC0yLjggMCwxLjEgMSwyLjggMiwyLjggMC44LDAgMS40LC0xIDEuNSwtMiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzODQ4IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDEwLjgsMTA0MS44NjIyIC0xLjYsMCBjIDAuMzI1MDY5MiwwLjQwODMgMC44LDAuNyAwLjgsMS4yIDAsLTAuNSAwLjQ5OTgwNSwtMC44NDA4IDAuOCwtMS4yIHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzg2MCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMSkiIGlkPSJnMzg2NiI+PHBhdGggZD0ibSAxNC41LDEwNDMuMzYyMiAyLDAuNSIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzODYyIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojOWM3YTE2O3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDE0LjUsMTA0MS44NjIyIDIsMCIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzODY0IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojOWM3YTE2O3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiLz48L2c+PHVzZSB0cmFuc2Zvcm09Im1hdHJpeCgtMSwwLDAsMSwyMCwwKSIgaWQ9InVzZTM4NzAiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI2czODY2Ii8+PC9nPjwvc3ZnPgo=","img/smiley/face-confused.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA2LjUsMTA0My44NjIyIGMgMSwxLjUgMi41LDEuNSAzLjUsMC41IDEsLTEgMi41LC0xIDMuNSwwLjUiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzc5MyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsMCAxLjUsMS41IDAgMSAxIDMsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMy45OTk5OTk2LDApIiBpZD0idXNlMzgwMyIgeD0iMCIgeT0iMCIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiB4bGluazpocmVmPSIjcGF0aDM3OTciLz48L2c+PC9zdmc+Cg==","img/smiley/face-confused-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA2LjUsMTA0My44NjIyIGMgMSwyLjUgMi41LDIuNSAzLjUsMSAxLC0xLjUgMi41LC0xLjUgMy41LDEiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzc5MyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsLTEwZS04IEwgMTEuOSw4LjUgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45NjU5MjU4MywwLjI1ODgxOTA1LC0wLjI1ODgxOTA1LDAuOTY1OTI1ODMsMi43MDU0NDQ5LDEwMjcuOTcxOSkiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJtYXRyaXgoLTEsMCwwLDEsMTkuOTQ4ODg5LDApIiBpZD0idXNlMzgwMyIgeD0iMCIgeT0iMCIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiB4bGluazpocmVmPSIjcGF0aDM3OTciLz48L2c+PC9zdmc+Cg==","img/smiley/face-frown.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA1LjUsMTA0NC44ODcyIGMgMS41LC0yLjcgNy41LC0yLjcgOSwwIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjwvc3ZnPgo=","img/smiley/face-frown-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsLTEwZS04IEwgMTEuOSw4LjUgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMS4wNjY2NjY2LDAsMCwxLjA2NjY2NjYsLTAuNjkzMzMzMywxMDI5LjI5NTYpIiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIvPjxwYXRoIGQ9Im0gNS41LDEwNDUuODM3MiBjIDEuNDk5OTk5NSwtNS4zIDcuNSwtNS4zIDksMCIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzNzkzIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48L2c+PC9zdmc+Cg==","img/smiley/face-kiss.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA5LjUsMTA0Mi44NjIyIGMgMCwwIDEuNSwwLjUgMS41LDEgMCwwLjUgLTEuNSwxIC0xLjUsMSAwLDAgMS41LDAuNSAxLjUsMSAwLDAuNSAtMS41LDEgLTEuNSwxIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjwvc3ZnPgo=","img/smiley/face-kiss-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA5LjUsMTA0Mi44NjIyIGMgMCwwIDEuNSwwLjUgMS41LDEgMCwwLjUgLTEuNSwxIC0xLjUsMSAwLDAgMS41LDAuNSAxLjUsMSAwLDAuNSAtMS41LDEgLTEuNSwxIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTAuNCw4LjQ5OTk5OTkgYSAxLjUsMS41IDAgMCAxIDMsMTBlLTggbCAtMS41LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMS4wNjY2NjY2LDAsMCwxLC0wLjY5MzMzMjIzLDEwMzAuODYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTQsMCkiIGlkPSJ1c2UzMDU2IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIvPjwvZz48L3N2Zz4K","img/smiley/face-laugh.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA1LjUsMTA0Mi44NjIyIDksMCBjIC0xLjUsNS4zIC03LjUsNS40IC05LDAgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzODMxIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDEzLjQsOC41IGEgMS41LDEuNSAwIDEgMSAtMywwIDEuNSwxLjUgMCAxIDEgMywwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMTAwMDAwMzgsMTAzMS4zNjIyKSIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNCwwKSIgaWQ9InVzZTMwODIiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjwvc3ZnPgo=","img/smiley/face-laugh-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSAxMC40LDguNDk5OTk5OSBhIDEuNSwxLjUgMCAwIDEgMywxMGUtOCBsIC0xLjUsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjYsMCwwLDEsLTAuNjkzMzMzMywxMDMwLjg2MjIpIiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIvPjxwYXRoIGQ9Im0gNS41LDEwNDEuODYyMiA5LDAgYyAtMS41LDYuNyAtNy41LDYuNyAtOSwwIHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzgzMSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PC9nPjwvc3ZnPgo=","img/smiley/face-neutral.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsMCAxLjUsMS41IDAgMSAxIDMsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMy45OTk5OTk2LDApIiBpZD0idXNlMzgwMyIgeD0iMCIgeT0iMCIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiB4bGluazpocmVmPSIjcGF0aDM3OTciLz48cGF0aCBkPSJtIDUuNSwxMDQzLjg2MjIgOSwwIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM4MzEiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjwvZz48L3N2Zz4K","img/smiley/face-neutral-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsLTEwZS04IEwgMTEuOSw4LjUgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzAuODYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PHBhdGggZD0ibSA1LjUsMTA0My44NjIyIDksMCIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzODMxIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48L2c+PC9zdmc+Cg==","img/smiley/face-smile.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PGcgaWQ9ImczMDI1Ij48cGF0aCBkPSJtIDUuNSwxMDQyLjg2MjIgYyAxLjUsMi43IDcuNSwyLjcgOSwwIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjwvZz48L3N2Zz4K","img/smiley/face-smile-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA1LjUsMTA0MS44NjIyIGMgMS41LDUuMyA3LjUsNS4zIDksMCIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzNzkzIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDEwLjQsOC40OTk5OTk5IGEgMS41LDEuNSAwIDAgMSAzLDEwZS04IGwgLTEuNSwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMDY2NjY2NiwwLDAsMSwtMC42OTMzMzMzLDEwMzAuODYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjwvc3ZnPgo=","img/smiley/face-surprised.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEEgMi41LDIuNSAwIDAgMSAyLjUsMTMgMi41LDIuNSAwIDAgMSAwLDEwLjUgMi41LDIuNSAwIDAgMSAyLjUsOCAyLjUsMi41IDAgMCAxIDUsMTAuNSBaIiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGEgOCw4IDAgMCAxIC04LDggOCw4IDAgMCAxIC04LC04IDgsOCAwIDAgMSA4LC04IDgsOCAwIDAgMSA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgZD0iTSAxMy40LDguNSBBIDEuNSwxLjUgMCAwIDEgMTEuOSwxMCAxLjUsMS41IDAgMCAxIDEwLjQsOC41IDEuNSwxLjUgMCAwIDEgMTEuOSw3IDEuNSwxLjUgMCAwIDEgMTMuNCw4LjUgWiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48Y2lyY2xlIHI9IjEuNTAwMDAxOSIgY3k9IjEwNDUuMzYyMiIgY3g9IjEwIiBpZD0icGF0aDQ1NTgiIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiLz48L2c+PC9zdmc+Cg==","img/smiley/face-surprised-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEEgMi41LDIuNSAwIDAgMSAyLjUsMTMgMi41LDIuNSAwIDAgMSAwLDEwLjUgMi41LDIuNSAwIDAgMSAyLjUsOCAyLjUsMi41IDAgMCAxIDUsMTAuNSBaIiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGEgOCw4IDAgMCAxIC04LDggOCw4IDAgMCAxIC04LC04IDgsOCAwIDAgMSA4LC04IDgsOCAwIDAgMSA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMC4zNjIyKSIgZD0iTSAxMy40LDguNSBBIDEuNSwxLjUgMCAwIDEgMTEuOSwxMCAxLjUsMS41IDAgMCAxIDEwLjQsOC41IDEuNSwxLjUgMCAwIDEgMTEuOSw3IDEuNSwxLjUgMCAwIDEgMTMuNCw4LjUgWiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48Y2lyY2xlIHI9IjIuNTAwMDAxOSIgY3k9IjEwNDQuMzYyMiIgY3g9IjEwIiBpZD0icGF0aDQ1NTgiIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiLz48L2c+PC9zdmc+Cg==","img/smiley/face-tongue.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA1LjUsMTA0My44NjIyIDksMCIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgzODMxIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBkPSJtIDgsMTA0NC4zNjIyIDAsMSBjIDAsMSAwLjUsMiAyLDIgMS41LDAgMiwtMSAyLC0yIGwgMCwtMSB6IiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDQ1NzMiIHN0eWxlPSJmaWxsOiNkYjM5NTY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDEzLjQsOC41IGEgMS41LDEuNSAwIDEgMSAtMywwIDEuNSwxLjUgMCAxIDEgMywwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMTAwMDAwMzgsMTAzMS4zNjIyKSIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNCwwKSIgaWQ9InVzZTMwMzciIHg9IjAiIHk9IjAiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgeGxpbms6aHJlZj0iI3BhdGgzNzk3Ii8+PC9nPjwvc3ZnPgo=","img/smiley/face-tongue-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjQ5OTg3NTNlLTYsMCkiIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSAxMC40LDguNDk5OTk5OSBhIDEuNSwxLjUgMCAwIDEgMywxMGUtOCBsIC0xLjUsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjYsMCwwLDEsLTAuNjkzMzMzMywxMDMwLjg2MjIpIiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIvPjxwYXRoIGQ9Im0gNS41LDEwNDEuODYyMiA5LDAiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMzgzMSIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggZD0ibSA4LDEwNDIuMzYyMiAwLDIgYyAwLDEgMC41LDIgMiwyIDEuNSwwIDIsLTEgMiwtMiBsIDAsLTIgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGg0NTczIiBzdHlsZT0iZmlsbDojZGIzOTU2O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/smiley/face-wink.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsMCAxLjUsMS41IDAgMSAxIDMsMCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAxMC40LDguNDk5OTk5OSBhIDEuNSwxLjUgMCAwIDEgMywxMGUtOCBsIC0xLjUsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjcsMCwwLC0xLjA2NjY2NjcsLTQuNjkzMzMyOSwxMDQ4LjQyODgpIiBpZD0icGF0aDMxNTAiIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBkPSJtIDUuNSwxMDQyLjg2MjIgYyAxLjUsMi43IDcuNSwyLjcgOSwwIiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjwvZz48L3N2Zz4K","img/smiley/face-wink-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgaWQ9InN2ZzIiPjxkZWZzIGlkPSJkZWZzNCIvPjxtZXRhZGF0YSBpZD0ibWV0YWRhdGE3Ij48cmRmOlJERj48Y2M6V29yayByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPjxkYzp0aXRsZS8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTEwMzIuMzYyMikiIGlkPSJsYXllcjEiPjxnIGlkPSJnMzg3MSI+PHBhdGggZD0ibSA1LDEwLjUgYSAyLjUsMi41IDAgMSAxIC01LDAgMi41LDIuNSAwIDEgMSA1LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC45MTkyMzgzOCwwLjkxOTIzNDUxLC0wLjkxOTIzODM4LDAuOTE5MjM0NTEsMTIuMTAzOTE0LDEwMjUuMTYyMSkiIGlkPSJwYXRoMjk4OCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIGlkPSJ1c2UzODE4IiB4PSIwIiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHhsaW5rOmhyZWY9IiNwYXRoMjk4OCIvPjxwYXRoIGQ9Ik0gMTgsMTEgQSA4LDggMCAxIDEgMiwxMSA4LDggMCAxIDEgMTgsMTEgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIiBpZD0icGF0aDI5OTQiIHN0eWxlPSJmaWxsOiNmNmNlNTU7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PHBhdGggZD0ibSA1LjUsMTA0MS44NjIyIGMgMC4wMTAwNzMsMi45ODM0IDMuNjAwMzAwNiw0Ljg3MTIgNi4zMTI1LDQuMDc1IiBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiBpZD0icGF0aDM3OTMiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIGQ9Im0gMTAuNCw4LjQ5OTk5OTkgYSAxLjUsMS41IDAgMCAxIDMsMTBlLTggbCAtMS41LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMS4wNjY2NjY3LDAsMCwtMS4wNjY2NjY3LC00LjY5MzMzMjksMTA0OC40Mjg4KSIgaWQ9InBhdGgzMTUwIiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/smiley/heart.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0iTSAxMCwxNyBDIC02LDYgNiwtMSAxMCw1IGMgNCwtNiAxNiwxIDAsMTIgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMyLjM2MjIpIiBpZD0icGF0aDQzODUiIHN0eWxlPSJmaWxsOiNkYjM5NTY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/smiley/heart-broken.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEuMDUzMTAzNCwwLjc4NTEpIiBpZD0iZzMwMDIiPjxwYXRoIGQ9Im0gMTEuNDUzMDQsMTA0OC41NTEgYyAxNi41NTM1MDMsLTEwLjE0NzMgNC45MzY0OTUsLTE3Ljc2NTcgMC42MjgwMjcsLTExLjk4MzMgbCAtMS40MTg5MDIsMS45Mjc5IDIuNzY1Mjc1LDIuMzc2MyAtMy4yODA3ODQsMi42MDM3IDIuNDYyMDEsMi4xMzIgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGg0OTQyIiBzdHlsZT0iZmlsbDojZGIzOTU2O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggZD0ibSAxMC42NTMxNTcsMTA0OC41NzcxIGMgLTE2LjU1MzQ5MDksLTEwLjE0NzMgLTQuOTM2NDgxNSwtMTcuNzY1NyAtMC42MjgwMTUsLTExLjk4MzMgbCAtMS4yMDkxNzc1LDIuMDY1NyAyLjk5ODUxMjUsMi4wNzQyIC0yLjk5MDYzMDksMi45MzI2IDIuNjcwOTE2OSwxLjg2MjcgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGg0OTQ0IiBzdHlsZT0iZmlsbDojZGIzOTU2O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvZz48L3N2Zz4K","img/user-offline-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTkyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDMyLjUsMTAwNC44NjIyIGMgLTMuNDUxNzgsMCAtNi4yNSwyLjc5ODIgLTYuMjUsNi4yNSAwLDAuNTAwOCAwLjA3NjI2LDAuOTcwOCAwLjE4NzUsMS40Mzc1IC0yLjcwNDQ5MSwxLjMzMzYgLTQuOTA1NjI4LDMuNTI2NCAtNi4yNSw2LjIxODggLTAuNDY0MzM3LC0wLjExMDEgLTAuOTM5NTE4LC0wLjE1NjMgLTEuNDM3NSwtMC4xNTYzIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwzLjQ1MTggMi43OTgyMiw2LjI1IDYuMjUsNi4yNSAwLjUwMDc3NiwwIDAuOTcwNzg0LC0wLjA3NiAxLjQzNzUsLTAuMTg3NSAyLjI0MjY5OSw0LjU0NzkgNi44OTg3NDksNy42ODc1IDEyLjMxMjUsNy42ODc1IDcuNTkzOTE1LDAgMTMuNzUsLTYuMTU2MSAxMy43NSwtMTMuNzUgMCwtNS40MTM4IC0zLjEzOTU1NSwtMTAuMDY5OCAtNy42ODc1LC0xMi4zMTI1IDAuMTExMjQzLC0wLjQ2NjcgMC4xODc1LC0wLjkzNjcgMC4xODc1LC0xLjQzNzUgMCwtMy40NTE4IC0yLjc5ODIyLC02LjI1IC02LjI1LC02LjI1IHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMjk4OSIgc3R5bGU9ImZpbGw6I2E5ZTg3YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/user-offline-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTkyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDMyLjUsMTAwNC44NjIyIGMgLTMuNDUxNzgsMCAtNi4yNSwyLjc5ODIgLTYuMjUsNi4yNSAwLDAuNTAwOCAwLjA3NjI2LDAuOTcwOCAwLjE4NzUsMS40Mzc1IC0yLjcwNDQ5MSwxLjMzMzYgLTQuOTA1NjI4LDMuNTI2NCAtNi4yNSw2LjIxODggLTAuNDY0MzM3LC0wLjExMDEgLTAuOTM5NTE4LC0wLjE1NjMgLTEuNDM3NSwtMC4xNTYzIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwzLjQ1MTggMi43OTgyMiw2LjI1IDYuMjUsNi4yNSAwLjUwMDc3NiwwIDAuOTcwNzg0LC0wLjA3NiAxLjQzNzUsLTAuMTg3NSAyLjI0MjY5OSw0LjU0NzkgNi44OTg3NDksNy42ODc1IDEyLjMxMjUsNy42ODc1IDcuNTkzOTE1LDAgMTMuNzUsLTYuMTU2MSAxMy43NSwtMTMuNzUgMCwtNS40MTM4IC0zLjEzOTU1NSwtMTAuMDY5OCAtNy42ODc1LC0xMi4zMTI1IDAuMTExMjQzLC0wLjQ2NjcgMC4xODc1LC0wLjkzNjcgMC4xODc1LC0xLjQzNzUgMCwtMy40NTE4IC0yLjc5ODIyLC02LjI1IC02LjI1LC02LjI1IHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMjk4OSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/user-offline.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTkyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDMyLjUsMTAwNC44NjIyIGMgLTMuNDUxNzgsMCAtNi4yNSwyLjc5ODIgLTYuMjUsNi4yNSAwLDAuNTAwOCAwLjA3NjI2LDAuOTcwOCAwLjE4NzUsMS40Mzc1IC0yLjcwNDQ5MSwxLjMzMzYgLTQuOTA1NjI4LDMuNTI2NCAtNi4yNSw2LjIxODggLTAuNDY0MzM3LC0wLjExMDEgLTAuOTM5NTE4LC0wLjE1NjMgLTEuNDM3NSwtMC4xNTYzIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwzLjQ1MTggMi43OTgyMiw2LjI1IDYuMjUsNi4yNSAwLjUwMDc3NiwwIDAuOTcwNzg0LC0wLjA3NiAxLjQzNzUsLTAuMTg3NSAyLjI0MjY5OSw0LjU0NzkgNi44OTg3NDksNy42ODc1IDEyLjMxMjUsNy42ODc1IDcuNTkzOTE1LDAgMTMuNzUsLTYuMTU2MSAxMy43NSwtMTMuNzUgMCwtNS40MTM4IC0zLjEzOTU1NSwtMTAuMDY5OCAtNy42ODc1LC0xMi4zMTI1IDAuMTExMjQzLC0wLjQ2NjcgMC4xODc1LC0wLjkzNjcgMC4xODc1LC0xLjQzNzUgMCwtMy40NTE4IC0yLjc5ODIyLC02LjI1IC02LjI1LC02LjI1IHoiIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiIGlkPSJwYXRoMjk4OSIgc3R5bGU9ImZpbGw6I2MwYzBjMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/user-online-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTkyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDMyLjUsMTAwNC44NjIyIGMgLTMuNDUxNzgsMCAtNi4yNSwyLjc5ODIgLTYuMjUsNi4yNSAwLDAuNTAwOCAwLjA3NjI2LDAuOTcwOCAwLjE4NzUsMS40Mzc1IC0yLjcwNDQ5MSwxLjMzMzYgLTQuOTA1NjI4LDMuNTI2NCAtNi4yNSw2LjIxODggLTAuNDY0MzM3LC0wLjExMDEgLTAuOTM5NTE4LC0wLjE1NjMgLTEuNDM3NSwtMC4xNTYzIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwzLjQ1MTggMi43OTgyMiw2LjI1IDYuMjUsNi4yNSAwLjUwMDc3NiwwIDAuOTcwNzg0LC0wLjA3NiAxLjQzNzUsLTAuMTg3NSAyLjI0MjY5OSw0LjU0NzkgNi44OTg3NDksNy42ODc1IDEyLjMxMjUsNy42ODc1IDcuNTkzOTE1LDAgMTMuNzUsLTYuMTU2MSAxMy43NSwtMTMuNzUgMCwtNS40MTM4IC0zLjEzOTU1NSwtMTAuMDY5OCAtNy42ODc1LC0xMi4zMTI1IDAuMTExMjQzLC0wLjQ2NjcgMC4xODc1LC0wLjkzNjcgMC4xODc1LC0xLjQzNzUgMCwtMy40NTE4IC0yLjc5ODIyLC02LjI1IC02LjI1LC02LjI1IHogbSAwLDEyLjUgYyAxLjM4MDcxMiwwIDIuNSwxLjExOTMgMi41LDIuNSAwLDEuMzgwNyAtMS4xMTkyODgsMi41IC0yLjUsMi41IC0xLjM4MDcxMiwwIC0yLjUsLTEuMTE5MyAtMi41LC0yLjUgMCwtMS4zODA3IDEuMTE5Mjg4LC0yLjUgMi41LC0yLjUgeiBtIC01LDUgYyAxLjM4MDcxMiwwIDIuNSwxLjExOTMgMi41LDIuNSAwLDEuMzgwNyAtMS4xMTkyODgsMi41IC0yLjUsMi41IC0xLjM4MDcxMiwwIC0yLjUsLTEuMTE5MyAtMi41LC0yLjUgMCwtMS4zODA3IDEuMTE5Mjg4LC0yLjUgMi41LC0yLjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgyOTg5IiBzdHlsZT0iZmlsbDojYTllODdjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/user-online-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTkyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDMyLjUsMTAwNC44NjIyIGMgLTMuNDUxNzgsMCAtNi4yNSwyLjc5ODIgLTYuMjUsNi4yNSAwLDAuNTAwOCAwLjA3NjI2LDAuOTcwOCAwLjE4NzUsMS40Mzc1IC0yLjcwNDQ5MSwxLjMzMzYgLTQuOTA1NjI4LDMuNTI2NCAtNi4yNSw2LjIxODggLTAuNDY0MzM3LC0wLjExMDEgLTAuOTM5NTE4LC0wLjE1NjMgLTEuNDM3NSwtMC4xNTYzIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwzLjQ1MTggMi43OTgyMiw2LjI1IDYuMjUsNi4yNSAwLjUwMDc3NiwwIDAuOTcwNzg0LC0wLjA3NiAxLjQzNzUsLTAuMTg3NSAyLjI0MjY5OSw0LjU0NzkgNi44OTg3NDksNy42ODc1IDEyLjMxMjUsNy42ODc1IDcuNTkzOTE1LDAgMTMuNzUsLTYuMTU2MSAxMy43NSwtMTMuNzUgMCwtNS40MTM4IC0zLjEzOTU1NSwtMTAuMDY5OCAtNy42ODc1LC0xMi4zMTI1IDAuMTExMjQzLC0wLjQ2NjcgMC4xODc1LC0wLjkzNjcgMC4xODc1LC0xLjQzNzUgMCwtMy40NTE4IC0yLjc5ODIyLC02LjI1IC02LjI1LC02LjI1IHogbSAwLDEyLjUgYyAxLjM4MDcxMiwwIDIuNSwxLjExOTMgMi41LDIuNSAwLDEuMzgwNyAtMS4xMTkyODgsMi41IC0yLjUsMi41IC0xLjM4MDcxMiwwIC0yLjUsLTEuMTE5MyAtMi41LC0yLjUgMCwtMS4zODA3IDEuMTE5Mjg4LC0yLjUgMi41LC0yLjUgeiBtIC01LDUgYyAxLjM4MDcxMiwwIDIuNSwxLjExOTMgMi41LDIuNSAwLDEuMzgwNyAtMS4xMTkyODgsMi41IC0yLjUsMi41IC0xLjM4MDcxMiwwIC0yLjUsLTEuMTE5MyAtMi41LC0yLjUgMCwtMS4zODA3IDEuMTE5Mjg4LC0yLjUgMi41LC0yLjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgyOTg5IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/user-online.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHZlcnNpb249IjEuMSIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIiBpZD0ic3ZnMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTciPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlLz48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTkyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBkPSJtIDMyLjUsMTAwNC44NjIyIGMgLTMuNDUxNzgsMCAtNi4yNSwyLjc5ODIgLTYuMjUsNi4yNSAwLDAuNTAwOCAwLjA3NjI2LDAuOTcwOCAwLjE4NzUsMS40Mzc1IC0yLjcwNDQ5MSwxLjMzMzYgLTQuOTA1NjI4LDMuNTI2NCAtNi4yNSw2LjIxODggLTAuNDY0MzM3LC0wLjExMDEgLTAuOTM5NTE4LC0wLjE1NjMgLTEuNDM3NSwtMC4xNTYzIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwzLjQ1MTggMi43OTgyMiw2LjI1IDYuMjUsNi4yNSAwLjUwMDc3NiwwIDAuOTcwNzg0LC0wLjA3NiAxLjQzNzUsLTAuMTg3NSAyLjI0MjY5OSw0LjU0NzkgNi44OTg3NDksNy42ODc1IDEyLjMxMjUsNy42ODc1IDcuNTkzOTE1LDAgMTMuNzUsLTYuMTU2MSAxMy43NSwtMTMuNzUgMCwtNS40MTM4IC0zLjEzOTU1NSwtMTAuMDY5OCAtNy42ODc1LC0xMi4zMTI1IDAuMTExMjQzLC0wLjQ2NjcgMC4xODc1LC0wLjkzNjcgMC4xODc1LC0xLjQzNzUgMCwtMy40NTE4IC0yLjc5ODIyLC02LjI1IC02LjI1LC02LjI1IHogbSAwLDEyLjUgYyAxLjM4MDcxMiwwIDIuNSwxLjExOTMgMi41LDIuNSAwLDEuMzgwNyAtMS4xMTkyODgsMi41IC0yLjUsMi41IC0xLjM4MDcxMiwwIC0yLjUsLTEuMTE5MyAtMi41LC0yLjUgMCwtMS4zODA3IDEuMTE5Mjg4LC0yLjUgMi41LC0yLjUgeiBtIC01LDUgYyAxLjM4MDcxMiwwIDIuNSwxLjExOTMgMi41LDIuNSAwLDEuMzgwNyAtMS4xMTkyODgsMi41IC0yLjUsMi41IC0xLjM4MDcxMiwwIC0yLjUsLTEuMTE5MyAtMi41LC0yLjUgMCwtMS4zODA3IDEuMTE5Mjg4LC0yLjUgMi41LC0yLjUgeiIgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIgaWQ9InBhdGgyOTg5IiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo="}
    return function (url) {
        return urls[url]
    }
})()

function AccountPage_EmailItem (profile, changeListener, closeListener) {

    var classPrefix = 'AccountPage_EmailItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Email'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.email
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var privacySelect = AccountPage_PrivacySelect(
        profile.emailPrivacy, changeListener, closeListener)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)
    element.appendChild(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable: function () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable: function () {
            privacySelect.enable()
            input.disabled = false
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}

function AccountPage_FullNameItem (profile, changeListener, closeListener) {

    var classPrefix = 'AccountPage_FullNameItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Full name'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.fullName
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var privacySelect = AccountPage_PrivacySelect(
        profile.fullNamePrivacy, changeListener, closeListener)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)
    element.appendChild(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable: function () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable: function () {
            privacySelect.enable()
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}

function AccountPage_LinkItem (userLink, closeListener) {

    var classPrefix = 'AccountPage_LinkItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Link'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.readOnly = true
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = userLink
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return { element: element }

}

function AccountPage_Page (timezoneList, userLink, username, session,
    profile, editProfileListener, changePasswordListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === profile.fullName &&
            fullNameItem.getPrivacyValue() === profile.fullNamePrivacy &&
            emailItem.getValue() === profile.email &&
            emailItem.getPrivacyValue() === profile.emailPrivacy &&
            phoneItem.getValue() === profile.phone &&
            phoneItem.getPrivacyValue() === profile.phonePrivacy &&
            timezoneItem.getValue() === profile.timezone &&
            timezoneItem.getPrivacyValue() === profile.timezonePrivacy
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'AccountPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var fullNameItem = AccountPage_FullNameItem(profile, checkChanges, close)

    var emailItem = AccountPage_EmailItem(profile, checkChanges, close)

    var phoneItem = AccountPage_PhoneItem(profile, checkChanges, close)

    var timezoneItem = AccountPage_TimezoneItem(timezoneList, profile, checkChanges, close)

    var linkItem = AccountPage_LinkItem(userLink, close)

    var saveChangesNode = TextNode('Save Changes')

    var saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.appendChild(saveChangesNode)
    saveChangesButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(fullNameItem.element)
    form.appendChild(emailItem.element)
    form.appendChild(phoneItem.element)
    form.appendChild(timezoneItem.element)
    form.appendChild(linkItem.element)
    form.appendChild(saveChangesButton)
    form.addEventListener('submit', function (e) {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var fullName = fullNameItem.getValue(),
            fullNamePrivacy = fullNameItem.getPrivacyValue(),
            email = emailItem.getValue(),
            emailPrivacy = emailItem.getPrivacyValue(),
            phone = phoneItem.getValue(),
            phonePrivacy = phoneItem.getPrivacyValue(),
            timezone = timezoneItem.getValue(),
            timezonePrivacy = timezoneItem.getPrivacyValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        var url = 'data/editProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&fullNamePrivacy=' + encodeURIComponent(fullNamePrivacy) +
            '&email=' + encodeURIComponent(email) +
            '&emailPrivacy=' + encodeURIComponent(emailPrivacy) +
            '&phone=' + encodeURIComponent(phone) +
            '&phonePrivacy=' + encodeURIComponent(phonePrivacy) +
            '&timezone=' + encodeURIComponent(timezone) +
            '&timezonePrivacy=' + encodeURIComponent(timezonePrivacy)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            editProfileListener({
                fullName: fullName,
                fullNamePrivacy: fullNamePrivacy,
                email: email,
                emailPrivacy: emailPrivacy,
                phone: phone,
                phonePrivacy: phonePrivacy,
                timezone: timezone,
                timezonePrivacy: timezonePrivacy,
            })

        }, requestError)

    })

    var changePasswordButton = document.createElement('button')
    changePasswordButton.className = classPrefix + '-changePasswordButton OrangeButton'
    changePasswordButton.appendChild(TextNode('Change Password \u203a'))
    changePasswordButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    changePasswordButton.addEventListener('click', function () {
        destroy()
        changePasswordListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)
    frameElement.appendChild(changePasswordButton)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile: function (newProfile) {
            profile = newProfile
            checkChanges()
        },
        focusChangePassword: function () {
            changePasswordButton.focus()
        },
    }

}

function AccountPage_PhoneItem (profile, changeListener, closeListener) {

    var classPrefix = 'AccountPage_PhoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Phone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.phone
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var privacySelect = AccountPage_PrivacySelect(
        profile.phonePrivacy, changeListener, closeListener)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)
    element.appendChild(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable: function () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable: function () {
            privacySelect.enable()
            input.disabled = false
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}

function AccountPage_PrivacySelect (value, changeListener, closeListener) {

    function add (itemValue, text) {

        function click () {
            if (value !== itemValue) setValue(itemValue)
            collapse()
        }

        var item = AccountPag_PrivacySelectItem(text, itemValue, click)

        menuElement.appendChild(item.element)
        if (value === itemValue) valueIndex = items.length
        items.push({
            click: click,
            value: itemValue,
            deselect: item.deselect,
            select: item.select,
        })

    }

    function collapse () {

        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', expandedKeyDown)
        button.addEventListener('click', expand)
        button.addEventListener('keydown', collapsedKeyDown)

        element.removeChild(menuElement)

        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)

        if (selectedIndex !== null) {
            items[selectedIndex].deselect()
            selectedIndex = null
        }

    }

    function collapsedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        } else if (keyCode === 38) {
            e.preventDefault()
            if (valueIndex === 0) return
            valueIndex--
            setValue(items[valueIndex].value)
        } else if (keyCode === 40) {
            e.preventDefault()
            if (valueIndex === items.length - 1) return
            valueIndex++
            setValue(items[valueIndex].value)
        }
    }

    function expand () {

        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.removeEventListener('keydown', collapsedKeyDown)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', expandedKeyDown)

        element.appendChild(menuElement)

        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)

    }

    function expandedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            if (selectedIndex !== null) {
                items[selectedIndex].click()
            }
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            if (selectedIndex === null) {
                selectedIndex = items.length - 1
                items[selectedIndex].select()
            } else if (selectedIndex === 0) {
                items[0].deselect()
                selectedIndex = items.length - 1
                items[selectedIndex].select()
            } else {
                items[selectedIndex].deselect()
                selectedIndex--
                items[selectedIndex].select()
            }
        } else if (keyCode === 40) {
            e.preventDefault()
            if (selectedIndex === null) {
                selectedIndex = 0
                items[0].select()
            } else if (selectedIndex === items.length - 1) {
                items[selectedIndex].deselect()
                selectedIndex = 0
                items[0].select()
            } else {
                items[selectedIndex].deselect()
                selectedIndex++
                items[selectedIndex].select()
            }
        }
    }

    function setValue (newValue) {
        buttonClassList.remove('privacy_' + value)
        value = newValue
        buttonClassList.add('privacy_' + value)
        changeListener()
    }

    function windowFocus (e) {
        var target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    var items = []
    var valueIndex
    var selectedIndex = null

    var classPrefix = 'AccountPage_PrivacySelect'

    var button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button privacy_' + value
    button.addEventListener('click', expand)
    button.addEventListener('keydown', collapsedKeyDown)

    var buttonClassList = button.classList

    var menuElement = Div(classPrefix + '-menu')
    add('private', 'Me')
    add('contacts', 'Contacts')
    add('public', 'Anyone')

    var element = Div(classPrefix)
    element.appendChild(button)

    return {
        element: element,
        disable: function () {
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        getValue: function () {
            return value
        },
    }

}

function AccountPag_PrivacySelectItem (text, value, clickListener) {

    var element = Div('AccountPage_PrivacySelectItem privacy_' + value)
    element.appendChild(TextNode(text))
    element.addEventListener('click', clickListener)

    var classList = element.classList

    return {
        element: element,
        deselect: function () {
            classList.remove('active')
        },
        select: function () {
            classList.add('active')
        },
    }

}

function AccountPage_PrivacySelectItemStyle (getResourceUrl) {
    return '.AccountPage_PrivacySelectItem.privacy_private {' +
            'background-image: url(' + getResourceUrl('img/privacy/private.svg') + ')' +
        '}' +
        '.AccountPage_PrivacySelectItem.privacy_contacts {' +
            'background-image: url(' + getResourceUrl('img/privacy/contacts.svg') + ')' +
        '}' +
        '.AccountPage_PrivacySelectItem.privacy_public {' +
            'background-image: url(' + getResourceUrl('img/privacy/public.svg') + ')' +
        '}'
}

function AccountPage_PrivacySelectStyle (getResourceUrl) {

    function getUrl (url) {
        return 'url(' + getResourceUrl(url) + ')'
    }

    var arrow = getUrl('img/arrow.svg'),
        arrowPressed = getUrl('img/arrow-pressed.svg'),
        arrowPressedActive = getUrl('img/arrow-pressed-active.svg')

    return '.AccountPage_PrivacySelect-button.privacy_private {' +
            'background-image: ' +
                getUrl('img/privacy/private.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.privacy_contacts {' +
            'background-image: ' +
                getUrl('img/privacy/contacts.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.privacy_public {' +
            'background-image: ' +
                getUrl('img/privacy/public.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_private {' +
            'background-image: ' +
                getUrl('img/privacy/private-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_contacts {' +
            'background-image: ' +
                getUrl('img/privacy/contacts-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_public {' +
            'background-image: ' +
                getUrl('img/privacy/public-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_private:active {' +
            'background-image: ' +
                getUrl('img/privacy/private-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_contacts:active {' +
            'background-image: ' +
                getUrl('img/privacy/contacts-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_public:active {' +
            'background-image: ' +
                getUrl('img/privacy/public-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}'
}

function AccountPage_TimezoneItem (timezoneList,
    profile, changeListener, closeListener) {

    var classPrefix = 'AccountPage_TimezoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Timezone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.addEventListener('change', changeListener)
    timezoneList.forEach(function (timezone) {
        var option = document.createElement('option')
        option.value = timezone.value
        option.appendChild(TextNode(timezone.text))
        select.appendChild(option)
    })
    select.value = profile.timezone
    select.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var privacySelect = AccountPage_PrivacySelect(
        profile.timezonePrivacy, changeListener, closeListener)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(select)
    element.appendChild(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable: function () {
            privacySelect.disable()
            select.disabled = true
            select.blur()
        },
        enable: function () {
            privacySelect.enable()
            select.disabled = false
        },
        getValue: function () {
            return parseInt(select.value, 10)
        },
    }

}

function AccountPage_TimezoneItemStyle (getResourceUrl) {
    return '.AccountPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}

function AccountReadyPage (closeListener) {

    var classPrefix = 'AccountReadyPage'

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Welcome!'))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode(
        'Your account is ready. Add some' +
        ' contacts to start a conversation.'
    ))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: function () {
            closeButton.focus()
        },
    }

}

function AddContactPage_Page (showInvite, session,
    username, checkContactListener, userFoundListener,
    closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        usernameItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Find User'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'AddContactPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Add Contact'))

    var usernameItem = AddContactPage_UsernameItem(close)

    var buttonNode = TextNode('Find User')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var form = document.createElement('form')
    form.appendChild(usernameItem.element)
    form.appendChild(button)
    form.addEventListener('submit', function (e) {

        e.preventDefault()
        usernameItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var username = usernameItem.getValue()
        if (username === null) return

        var lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        usernameItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Finding...'

        var url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                usernameItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Find User'
                usernameItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('There is no such user.'))
                })
                return
            }

            userFoundListener(response.username, response.profile)

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)
    if (showInvite) {

        var invitePanelElement = Div(classPrefix + '-invitePanel')
        invitePanelElement.appendChild(InvitePanel(username, close).element)

        frameElement.appendChild(invitePanelElement)

    }

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: usernameItem.focus,
    }

}

function AddContactPage_UsernameItem (closeListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'AddContactPage_UsernameItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Username'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            var value = CollapseSpaces(input.value)
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Username_IsValid(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The username is invalid.'))
                })
                return null
            }
            return value
        },
    }

}

function BackButton (listener) {

    var element = document.createElement('button')
    element.className = 'BackButton'
    element.appendChild(TextNode('\u2039 Back'))
    element.addEventListener('click', listener)

    return {
        element: element,
        focus: function () {
            element.focus()
        },
    }

}

function ChangePasswordPage_CurrentPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'ChangePasswordPage_CurrentPasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Current password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Password_IsValid(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is invalid.'))
                })
                return null
            }
            return value
        },
    }

}

function ChangePasswordPage_NewPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'ChangePasswordPage_NewPasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Choose a new password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function () {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Password_IsShort(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is too short.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Minimum ' + Password_minLength + ' characters required.'))
                })
                return null
            }
            if (Password_ContainsOnlyDigits(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is too simple.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Use some different symbols.'))
                })
                return null
            }
            return value
        },
    }

}

function ChangePasswordPage_Page (session,
    backListener, closeListener, invalidSessionListener) {

    function back () {
        destroy()
        backListener()
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        currentPasswordItem.enable()
        newPasswordItem.enable()
        repeatNewPasswordItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Save'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'ChangePasswordPage_Page'

    var backButton = BackButton(back)

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Change Password'))

    var currentPasswordItem = ChangePasswordPage_CurrentPasswordItem(back)

    var newPasswordItem = ChangePasswordPage_NewPasswordItem(back)

    var repeatNewPasswordItem = ChangePasswordPage_RepeatNewPasswordItem(back)

    var buttonNode = TextNode('Save')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(currentPasswordItem.element)
    form.appendChild(newPasswordItem.element)
    form.appendChild(repeatNewPasswordItem.element)
    form.appendChild(button)
    form.addEventListener('submit', function (e) {

        e.preventDefault()
        currentPasswordItem.clearError()
        newPasswordItem.clearError()
        repeatNewPasswordItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var currentPassword = currentPasswordItem.getValue()
        if (currentPassword === null) return

        var newPassword = newPasswordItem.getValue()
        if (newPassword === null) return

        var repeatNewPassword = repeatNewPasswordItem.getValue(newPassword)
        if (repeatNewPassword === null) return

        currentPasswordItem.disable()
        newPasswordItem.disable()
        repeatNewPasswordItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Saving...'

        var url = 'data/changePassword' +
            '?token=' + encodeURIComponent(session.token) +
            '&currentPassword=' + encodeURIComponent(currentPassword) +
            '&newPassword=' + encodeURIComponent(newPassword)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_CURRENT_PASSWORD') {
                currentPasswordItem.enable()
                newPasswordItem.enable()
                repeatNewPasswordItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Save'
                currentPasswordItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is incorrect.'))
                })
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            closeListener()

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: currentPasswordItem.focus,
    }

}

function ChangePasswordPage_RepeatNewPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'ChangePasswordPage_RepeatNewPasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Retype the new password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function (password) {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (value !== password) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The passwords doesn\'t match.'))
                })
                return null
            }
            return value
        },
    }

}

function CheckSession (readyListener, signInListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var username, token
    try {
        username = localStorage.username
        token = localStorage.token
    } catch (e) {
    }

    var warningCallback
    var hashUsername = location.hash.substr(1)
    if (Username_IsValid(hashUsername)) {
        warningCallback = WelcomePage_UnauthenticatedWarning(hashUsername)
    }

    if (token === undefined) {
        readyListener(username === undefined ? '' : username, warningCallback)
        return
    }

    var url = 'data/restoreSession?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    var request = GetJson(url, function () {

        if (request.status !== 200) {
            serviceErrorListener()
            return
        }

        var response = request.response
        if (response === null) {
            crashListener()
            return
        }

        if (response === 'INVALID_USERNAME' || response === 'INVALID_TOKEN') {
            try {
                delete localStorage.token
            } catch (e) {
            }
            readyListener(username, warningCallback)
            return
        }

        signInListener(username, response)

    }, connectionErrorListener)

}

function CloseButton (listener) {

    var button = document.createElement('button')
    button.className = 'CloseButton'
    button.title = 'Close'
    button.addEventListener('click', listener)

    return {
        element: button,
        addEventListener: function (name, listener) {
            button.addEventListener(name, listener)
        },
        disable: function () {
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        focus: function () {
            button.focus()
        },
    }

}

function CloseButtonStyle (getResourceUrl) {
    return '.CloseButton {' +
            'background-image: url(' + getResourceUrl('img/close.svg') + ')' +
        '}'
}

function CollapseSpaces (string) {
    return string.replace(/^\s+/, '').replace(/\s+$/, '').replace(/\s{2,}/g, ' ')
}

function ConnectionError () {

    var element = Div('FormError')
    element.appendChild(TextNode('Connection failed.'))
    element.appendChild(document.createElement('br'))
    element.appendChild(TextNode('Please, check your network and try again.'))

    return { element: element }

}

function ConnectionErrorPage (reconnectListener) {

    var classPrefix = 'ConnectionErrorPage'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Connection lost.'))
    textElement.appendChild(document.createElement('br'))
    textElement.appendChild(TextNode('Please, check your network and reconnect.'))

    var buttonNode = TextNode('Reconnect')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('click', function () {
        button.disabled = true
        buttonNode.nodeValue = 'Reconnecting...'
        reconnectListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}

function ContactRequestPage (session, username, profile,
    addContactListener, ignoreListener, closeListener, invalidSessionListener) {

    function clearError () {
        if (error === null) return
        frameElement.removeChild(error.element)
        error = null
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function disableItems () {
        addContactButton.disabled = true
        ignoreButton.disabled = true
    }

    function showAddError (error) {
        showError(error)
        addContactNode.nodeValue = 'Add Contact'
        addContactButton.focus()
    }

    function showError (_error) {
        addContactButton.disabled = false
        ignoreButton.disabled = false
        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
    }

    function showIgnoreError (error) {
        showError(error)
        ignoreNode.nodeValue = 'Ignore'
        ignoreButton.focus()
    }

    var error = null
    var request = null

    var classPrefix = 'ContactRequestPage'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode(
        '" has added you to his/her contacts.' +
        ' Would you like to add him/her to your contacts?'
    ))

    var addContactNode = TextNode('Add Contact')

    var addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.appendChild(addContactNode)
    addContactButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    addContactButton.addEventListener('click', function () {

        clearError()
        disableItems()
        addContactNode.nodeValue = 'Adding...'

        var url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        var timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showAddError(ServiceError())
                return
            }

            if (response === null) {
                showAddError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, function () {
            request = null
            showAddError(ConnectionError())
        })

    })

    var ignoreNode = TextNode('Ignore')

    var ignoreButton = document.createElement('button')
    ignoreButton.className = classPrefix + '-ignoreButton OrangeButton'
    ignoreButton.appendChild(ignoreNode)
    ignoreButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    ignoreButton.addEventListener('click', function () {

        clearError()
        disableItems()
        ignoreNode.nodeValue = 'Ignoring...'

        var url = 'data/ignoreRequest' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showIgnoreError(ServiceError())
                return
            }

            if (response === null) {
                showIgnoreError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            ignoreListener(response)

        }, function () {
            request = null
            showIgnoreError(ConnectionError())
        })

    })

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.appendChild(addContactButton)
    buttonsElement.appendChild(ignoreButton)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)
    frameElement.appendChild(buttonsElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        edit: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}

function ContactSelectPage_Item (contact, selectListener,
    deselectListener, removeListener, closeListener) {

    function deselect () {
        selected = false
        button.classList.remove('selected')
        clickElement.removeEventListener('click', deselect)
        clickElement.addEventListener('click', select)
        deselectListener()
    }

    function select () {
        selected = true
        button.classList.add('selected')
        clickElement.removeEventListener('click', select)
        clickElement.addEventListener('click', deselect)
        selectListener()
    }

    var selected = false

    var classPrefix = 'ContactSelectPage_Item'

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var clickNode = TextNode(contact.getDisplayName())

    var clickElement = Div(classPrefix + '-click')
    clickElement.appendChild(button)
    clickElement.addEventListener('click', select)
    clickElement.appendChild(clickNode)

    var element = Div(classPrefix)
    element.appendChild(clickElement)

    contact.removeEvent.addListener(removeListener)

    return {
        contact: contact,
        element: element,
        destroy: function () {
            contact.removeEvent.removeListener(removeListener)
        },
        focus: function () {
            button.focus()
        },
        isSelected: function () {
            return selected
        },
        update: function () {
            clickNode.nodeValue = contact.getDisplayName()
        },
    }

}

function ContactSelectPage_ItemStyle (getResourceUrl) {
    return '.ContactSelectPage_Item-button.selected {' +
            'background-image: url(' + getResourceUrl('img/checked/normal.svg') + ')' +
        '}' +
        '.ContactSelectPage_Item-button.selected:active {' +
            'background-image: url(' + getResourceUrl('img/checked/active.svg') + ')' +
        '}'
}

function ContactSelectPage_Page (contacts, selectListener, closeListener) {

    function addContact (contact) {

        var username = contact.username

        var item = ContactSelectPage_Item(contact, function () {
            selectedContacts.push(item.contact)
            if (selectedContacts.length === 1) button.disabled = false
        }, function () {
            selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            if (selectedContacts.length === 0) button.disabled = true
        }, function () {

            if (item.isSelected()) {
                selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            }

            delete itemsMap[username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            listElement.removeChild(item.element)

        }, close)

        itemsMap[username] = item
        itemsArray.push(item)
        listElement.appendChild(item.element)

    }

    function close () {
        itemsArray.forEach(function (item) {
            item.destroy()
        })
        closeListener()
    }

    var itemsMap = Object.create(null)
    var itemsArray = []
    var selectedContacts = []

    var classPrefix = 'ContactSelectPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Select Contact'))

    var listElement = Div(classPrefix + '-list')
    contacts.forEach(addContact)

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.disabled = true
    button.appendChild(TextNode('Send'))
    button.addEventListener('click', function () {
        selectListener(selectedContacts)
    })
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(listElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        addContact: addContact,
        element: element,
        focus: function () {
            itemsArray[0].focus()
        },
        reorder: function (contact, index) {

            var item = itemsMap[contact.username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            itemsArray.splice(index, 0, item)

            var nextItem = itemsArray[index + 1]
            if (nextItem === undefined) {
                listElement.appendChild(item.element)
            } else {
                listElement.insertBefore(item.element, nextItem.element)
            }

            item.update()

        },
    }

}

function CrashError () {

    var element = Div('FormError')
    element.appendChild(TextNode('Something went wrong.'))
    element.appendChild(document.createElement('br'))
    element.appendChild(TextNode('Please, try again.'))

    return { element: element }

}

function CrashPage (reloadListener) {

    var classPrefix = 'CrashPage'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Something went wrong.'))
    textElement.appendChild(document.createElement('br'))
    textElement.appendChild(TextNode('Reloading may fix the problem.'))

    var buttonNode = TextNode('Reload')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('click', function () {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}

function Day (time) {
    return Math.floor(time / (1000 * 60 * 60 * 24))
}

function Div (className) {
    var div = document.createElement('div')
    div.className = className
    return div
}

function EditContactPage_EmailItem (profile,
    overrideProfile, changeListener, closeListener) {

    var classPrefix = 'EditContactPage_EmailItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Email'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.email
    input.value = overrideProfile.email
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        disable: function () {
            input.disabled = true
            input.blur()
        },
        editProfile: function (profile) {
            input.placeholder = profile.email
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}

function EditContactPage_FullNameItem (profile,
    overrideProfile, changeListener, closeListener) {

    var classPrefix = 'EditContactPage_FullNameItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Full name'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.fullName
    input.value = overrideProfile.fullName
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        disable: function () {
            input.disabled = true
            input.blur()
        },
        editProfile: function (profile) {
            input.placeholder = profile.fullName
        },
        enable: function () {
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}

function EditContactPage_LinkItem (userLink, closeListener) {

    var classPrefix = 'EditContactPage_LinkItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Link'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.readOnly = true
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = userLink
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return { element: element }

}

function EditContactPage_Page (timezoneList, session, userLink,
    username, profile, overrideProfile, overrideProfileListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === overrideProfile.fullName &&
            emailItem.getValue() === overrideProfile.email &&
            phoneItem.getValue() === overrideProfile.phone &&
            timezoneItem.getValue() === overrideProfile.timezone
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'EditContactPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var fullNameItem = EditContactPage_FullNameItem(profile,
        overrideProfile, checkChanges, close)

    var emailItem = EditContactPage_EmailItem(profile,
        overrideProfile, checkChanges, close)

    var phoneItem = EditContactPage_PhoneItem(profile,
        overrideProfile, checkChanges, close)

    var timezoneItem = EditContactPage_TimezoneItem(timezoneList,
        profile, overrideProfile, checkChanges, close)

    var linkItem = EditContactPage_LinkItem(userLink, close)

    var saveChangesNode = TextNode('Save Changes')

    var saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.appendChild(saveChangesNode)
    saveChangesButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(fullNameItem.element)
    form.appendChild(emailItem.element)
    form.appendChild(phoneItem.element)
    form.appendChild(timezoneItem.element)
    form.appendChild(linkItem.element)
    form.appendChild(saveChangesButton)
    form.addEventListener('submit', function (e) {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var fullName = fullNameItem.getValue()
        var email = emailItem.getValue()
        var phone = phoneItem.getValue()
        var timezone = timezoneItem.getValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        var url = 'data/overrideContactProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&email=' + encodeURIComponent(email) +
            '&phone=' + encodeURIComponent(phone)
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            overrideProfileListener({
                fullName: fullName,
                email: email,
                phone: phone,
                timezone: timezone,
            })

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile: function (profile) {
            fullNameItem.editProfile(profile)
            emailItem.editProfile(profile)
            phoneItem.editProfile(profile)
        },
        overrideProfile: function (_overrideProfile) {
            overrideProfile = _overrideProfile
            checkChanges()
        },
    }

}

function EditContactPage_PhoneItem (profile,
    overrideProfile, changeListener, closeListener) {

    var classPrefix = 'EditContactPage_PhoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Phone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.phone
    input.value = overrideProfile.phone
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        disable: function () {
            input.disabled = true
            input.blur()
        },
        editProfile: function (profile) {
            input.placeholder = profile.phone
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}

function EditContactPage_TimezoneItem (timezoneList, profile,
    overrideProfile, changeListener, closeListener) {

    function format (value) {
        if (value === null) return ''
        return Timezone_Format(value)
    }

    var classPrefix = 'EditContactPage_TimezoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Timezone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var emptyNode = TextNode(format(profile.timezone))

    var emptyOption = document.createElement('option')
    emptyOption.appendChild(emptyNode)

    var select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.appendChild(emptyOption)
    timezoneList.forEach(function (timezone) {
        var option = document.createElement('option')
        option.value = timezone.value
        option.appendChild(TextNode(timezone.text))
        select.appendChild(option)
    })
    select.value = overrideProfile.timezone
    select.addEventListener('change', changeListener)
    select.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(select)

    return {
        element: element,
        disable: function () {
            select.disabled = true
            select.blur()
        },
        editProfile: function (profile) {
            emptyNode.nodeValue = format(profile.timezone)
        },
        enable: function () {
            select.disabled = false
        },
        getValue: function () {
            var value = select.value
            if (value === '') return null
            return parseInt(value, 10)
        },
    }

}

function EditContactPage_TimezoneItemStyle (getResourceUrl) {
    return '.EditContactPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}

function EmptyFieldError (errorElement) {
    errorElement.appendChild(TextNode('This field is required.'))
}

function FormatBytes () {
    var suffixes = ['KB', 'MB', 'GB', 'TB', 'PB']
    return function FormatBytes (size) {

        var suffix = 'B'
        var index = 0
        while (true) {
            if (size < 1024) break
            size = size / 1024
            suffix = suffixes[index]
            index++
            if (index === suffixes.length) break
        }

        return size.toFixed(1).replace(/\.?0+$/, '') + ' ' + suffix

    }
}

function FormatText (smileys) {

    var smileyComponents = smileys.array.map(function (smiley) {
        return smiley.text.replace(/([()|*])/g, '\\$1')
    }).sort(function (a, b) {
        return a.length > b.length ? -1 : 1
    })

    var pattern = '(?:(' + smileyComponents.join('|') + ')|https?:\/\/\\S+)'

    return function (text, textCallback, linkCallback, smileyCallback) {

        var index = 0
        var regex = new RegExp(pattern, 'gi')
        while (true) {

            var match = regex.exec(text)
            if (match === null) break

            var matchIndex = match.index
            if (matchIndex > index) textCallback(text.substring(index, matchIndex))

            var content = match[0]
            if (match[1] === undefined) linkCallback(content)
            else smileyCallback(content, smileys.map[content.toLowerCase()].file)

            index = matchIndex + content.length

        }
        if (index < text.length) textCallback(text.substring(index, text.length))

    }

}

function FoundContactPage (username, profile,
    openListener, backListener, closeListener) {

    var backButton = BackButton(backListener)

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var classPrefix = 'FoundContactPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(TextNode('Start a Conversation'))
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })
    button.addEventListener('click', openListener)

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" is already in your contacts.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus: function () {
            button.focus()
        },
    }

}

function FoundSelfPage (username, profile, backListener, closeListener) {

    var backButton = BackButton(backListener)

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var classPrefix = 'FoundSelfPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" it\'s you.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
        username: username,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}

function FoundUserPage (session, username, profile,
    addContactListener, backListener, closeListener, invalidSessionListener) {

    function back () {
        destroy()
        backListener()
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        button.disabled = false
        buttonNode.nodeValue = 'Add Contact'

        error = _error
        frameElement.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var backButton = BackButton(back)

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var classPrefix = 'FoundUserPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var buttonNode = TextNode('Add Contact')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })
    button.addEventListener('click', function () {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        var url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        var timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        button.disabled = true
        buttonNode.nodeValue = 'Adding...'

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, requestError)

    })

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus: function () {
            button.focus()
        },
    }

}

function GetJson (url, loadCallback, errorCallback) {
    var request = new XMLHttpRequest
    request.open('get', url)
    request.responseType = 'json'
    request.onerror = errorCallback
    request.onload = loadCallback
    request.send()
    return request
}

function InvitePanel (username, backListener) {

    var classPrefix = 'InvitePanel'

    var link = location.protocol + '//' +
        location.host + location.pathname + '#' + username

    var text = 'You can chat with me at ' + link

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Invite People'))

    var smsButton = document.createElement('a')
    smsButton.target = '_blank'
    smsButton.className = classPrefix + '-smsButton OrangeButton'
    smsButton.appendChild(TextNode('Via SMS'))
    smsButton.href = 'sms:?body=' + encodeURIComponent(text)
    smsButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var emailButton = document.createElement('a')
    emailButton.target = '_blank'
    emailButton.className = classPrefix + '-emailButton OrangeButton'
    emailButton.appendChild(TextNode('Via Email'))
    emailButton.href = 'mailto:?body=' + encodeURIComponent(text)
    emailButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(titleElement)
    element.appendChild(smsButton)
    element.appendChild(emailButton)

    return { element: element }

}

function IsChildElement (parentNode, childNode) {
    while (childNode !== null) {
        if (childNode === parentNode) return true
        childNode = childNode.parentNode
    }
    return false
}

function IsLocalLink () {

    var localPrefix = location.protocol + '//' +
        location.host + location.pathname + '#'

    return function (link) {
        if (link.substr(0, localPrefix.length) !== localPrefix) return false
        var username = link.substr(localPrefix.length)
        return Username_IsValid(username)
    }

}

function LoadingPage_Page () {

    var classPrefix = 'LoadingPage'

    var contentElement = Div(classPrefix + '-content')
    contentElement.appendChild(TextNode('Reconnecting...'))

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(contentElement)

    return { element: element }

}

function Minute (time) {
    return Math.floor(time / (1000 * 60))
}

function NoSuchUserPage (username, closeListener) {

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var classPrefix = 'NoSuchUserPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('There is no such user.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
    }

}

function PageStyle (getResourceUrl) {
    return '.Page {' +
            'background-image: url(' + getResourceUrl('img/grass.svg') + '),' +
                ' url(' + getResourceUrl('img/clouds.svg') + ')' +
        '}'
}

function Password_ContainsOnlyDigits (password) {
    return /^\d+$/.test(password)
}

function Password_IsShort (password) {
    return password.length < Password_minLength
}

function Password_IsValid (password) {
    return !Password_IsShort(password) && !Password_ContainsOnlyDigits(password)
}

var Password_minLength = 6

function PrivacyPage (back) {

    var classPrefix = 'PrivacyPage'

    var backButton = BackButton(back)

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Privacy Policy'))

    var text =
        'We:\n' +
        '* Encrypt your connection using SSL.\n' +
        '* Keep your account details.\n' +
        '* Keep your contact list.\n' +
        '* Queue your messages until delivered.\n' +
        '* Proxy your file transfers.\n' +
        'What don\'t:\n' +
        '* Censor you communication.\n' +
        '* Store your chat history.\n' +
        '* Disclose your data to anyone.\n' +
        '* Sell your data.\n' +
        '* Show ads.'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode(text))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: backButton.focus,
    }

}

function RemoveContactPage (username, session,
    removeListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        removeButton.disabled = false
        cancelButton.disabled = false
        removeNode.nodeValue = 'Remove Contact'

        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
        removeButton.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'RemoveContactPage'

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Are you sure you want to remove "'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" from your contacts?'))

    var removeNode = TextNode('Remove Contact')

    var removeButton = document.createElement('button')
    removeButton.className = classPrefix + '-removeButton GreenButton'
    removeButton.appendChild(removeNode)
    removeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    removeButton.addEventListener('click', function () {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        removeButton.disabled = true
        cancelButton.disabled = true
        removeNode.nodeValue = 'Removing...'

        var url = 'data/removeContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            removeListener()

        }, requestError)

    })

    var cancelButton = document.createElement('button')
    cancelButton.className = classPrefix + '-cancelButton OrangeButton'
    cancelButton.appendChild(TextNode('Cancel'))
    cancelButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    cancelButton.addEventListener('click', closeListener)

    var buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.appendChild(removeButton)
    buttonsElement.appendChild(cancelButton)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(buttonsElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: function () {
            removeButton.focus()
        },
    }

}

function RenderUserLink (username) {
    var prefix = location.protocol + '//' +
        location.host + location.pathname + '#'
    return function (username) {
        return prefix + username
    }
}

function ServiceError () {

    var element = Div('FormError')
    element.appendChild(TextNode('Operation failed.'))
    element.appendChild(document.createElement('br'))
    element.appendChild(TextNode('Please, try again.'))

    return { element: element }

}

function ServiceErrorPage (reloadListener) {

    var classPrefix = 'ServiceErrorPage'

    var text = 'There is a problem at Bazgu. We are fixing it.' +
        ' Reload the page to see if it\'s resolved.'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode(text))

    var buttonNode = TextNode('Reload')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('click', function () {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}

function SignOutPage (confirmListener, closeListener) {

    var classPrefix = 'SignOutPage'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Are you sure'))
    textElement.appendChild(document.createElement('br'))
    textElement.appendChild(TextNode('you want to sign out?'))

    var yesButton = document.createElement('button')
    yesButton.className = classPrefix + '-yesButton GreenButton'
    yesButton.appendChild(TextNode('Sign Out'))
    yesButton.addEventListener('click', confirmListener)
    yesButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var noButton = document.createElement('button')
    noButton.className = classPrefix + '-noButton OrangeButton'
    noButton.appendChild(TextNode('Cancel'))
    noButton.addEventListener('click', closeListener)
    noButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(yesButton)
    frameElement.appendChild(noButton)

    var element = Div(classPrefix)
    element.appendChild(frameElement)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: function () {
            yesButton.focus()
        },
    }

}

function SignUpPage_AdvancedItem (expandListener, collapseListener, backListener) {

    function collapse () {
        button.removeEventListener('click', collapse)
        button.addEventListener('click', expand)
        arrowClassList.remove('expanded')
        collapseListener()
    }

    function expand () {
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        arrowClassList.add('expanded')
        expandListener()
    }

    var classPrefix = 'SignUpPage_AdvancedItem'

    var arrowElement = Div(classPrefix + '-arrow')

    var arrowClassList = arrowElement.classList

    var button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button'
    button.appendChild(arrowElement)
    button.appendChild(TextNode('Advanced settings'))
    button.addEventListener('click', expand)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(button)
    element.appendChild(Div(classPrefix + '-line'))

    return { element: element }

}

function SignUpPage_AdvancedItemStyle (getResourceUrl) {
    return '.SignUpPage_AdvancedItem-arrow {' +
            'background-image: url(' + getResourceUrl('img/arrow-collapsed.svg') + ')' +
        '}' +
        '.SignUpPage_AdvancedItem-arrow.expanded {' +
            'background-image: url(' + getResourceUrl('img/arrow-expanded.svg') + ')' +
        '}'
}

function SignUpPage_AdvancedPanel (timezoneList, backListener) {

    var timezoneItem = SignUpPage_TimezoneItem(timezoneList, backListener)

    var element = Div('SignUpPage_AdvancedPanel')
    element.appendChild(timezoneItem.element)

    var classList = element.classList

    return {
        disable: timezoneItem.disable,
        element: element,
        enable: timezoneItem.enable,
        getTimezoneValue: timezoneItem.getValue,
        hide: function () {
            classList.remove('visible')
        },
        show: function () {
            classList.add('visible')
        },
    }

}

function SignUpPage_CaptchaItem (loadListener, backListener) {

    function disable () {
        input.disabled = true
        input.blur()
    }

    function enable () {
        input.disabled = false
    }

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function requestError () {
        request = null
        imageElement.removeChild(loadingNode)
        showCaptchaError()
    }

    function load () {

        disable()

        request = GetJson('data/captcha', function () {

            var status = request.status,
                response = request.response

            request = null
            imageElement.removeChild(loadingNode)

            if (status !== 200) {
                showCaptchaError()
                return
            }

            if (response === null) {
                showCaptchaError()
                return
            }

            enable()
            setCaptcha(response)
            loadListener()

        }, requestError)

    }

    function setCaptcha (captcha) {
        imageElement.style.backgroundImage = 'url(' + captcha.image + ')'
        token = captcha.token
    }

    function showCaptchaError () {

        var reloadButton = document.createElement('button')
        reloadButton.type = 'button'
        reloadButton.className = classPrefix + '-reloadButton GreenButton'
        reloadButton.appendChild(TextNode('Reload'))
        reloadButton.addEventListener('click', function () {
            imageBarElement.removeChild(reloadButton)
            imageElement.removeChild(errorNode)
            imageElement.appendChild(loadingNode)
            imageClassList.remove('error')
            load()
        })

        var errorNode = TextNode('Failed')

        imageBarElement.appendChild(reloadButton)
        imageElement.appendChild(errorNode)
        imageClassList.add('error')

    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null
    var request = null

    var token = null

    var classPrefix = 'SignUpPage_CaptchaItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Verification'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var loadingNode = TextNode('Loading...')

    var imageElement = Div(classPrefix + '-image')
    imageElement.appendChild(loadingNode)

    var imageClassList = imageElement.classList

    var imageBarElement = Div(classPrefix + '-imageBar')
    imageBarElement.appendChild(imageElement)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(imageBarElement)
    element.appendChild(input)

    load()

    return {
        disable: disable,
        element: element,
        enable: enable,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        destroy: function () {
            if (request === null) return
            request.abort()
            request = null
        },
        getValue: function () {

            var value = input.value.replace(/\s+/g, '')
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }

            return {
                value: value,
                token: token,
            }

        },
        setNewCaptcha: function (newCaptcha) {
            setCaptcha(newCaptcha)
            input.value = ''
        },
    }

}

function SignUpPage_Page (timezoneList, backListener, signUpListener) {

    function back () {
        destroy()
        backListener()
    }

    function destroy () {
        captchaItem.destroy()
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        repeatPasswordItem.enable()
        captchaItem.enable()
        advancedPanel.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Sign Up'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        form.insertBefore(error.element, button)
        button.focus()
    }

    var error = null
    var request = null

    var classPrefix = 'SignUpPage_Page'

    var backButton = BackButton(back)

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Create an Account'))

    var usernameItem = SignUpPage_UsernameItem(back)

    var passwordItem = SignUpPage_PasswordItem(back)

    var repeatPasswordItem = SignUpPage_RepeatPasswordItem(back)

    var captchaItem = SignUpPage_CaptchaItem(function () {
        button.disabled = false
    }, back)

    var advancedPanel = SignUpPage_AdvancedPanel(timezoneList, back)

    var advancedItem = SignUpPage_AdvancedItem(advancedPanel.show, advancedPanel.hide, back)

    var buttonNode = TextNode('Sign Up')

    var button = document.createElement('button')
    button.disabled = true
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(usernameItem.element)
    form.appendChild(passwordItem.element)
    form.appendChild(repeatPasswordItem.element)
    form.appendChild(captchaItem.element)
    form.appendChild(advancedItem.element)
    form.appendChild(advancedPanel.element)
    form.appendChild(button)
    form.addEventListener('submit', function (e) {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()
        repeatPasswordItem.clearError()
        captchaItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var username = usernameItem.getValue()
        if (username === null) return

        var password = passwordItem.getValue()
        if (password === null) return

        var repeatPassword = repeatPasswordItem.getValue(password)
        if (repeatPassword === null) return

        var captcha = captchaItem.getValue()
        if (captcha === null) return

        usernameItem.disable()
        passwordItem.disable()
        repeatPasswordItem.disable()
        captchaItem.disable()
        advancedPanel.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Signing up...'

        var url = 'data/signUp?username=' + encodeURIComponent(username) +
            '&password=' + encodeURIComponent(password) +
            '&captcha_token=' + encodeURIComponent(captcha.token) +
            '&captcha_value=' + encodeURIComponent(captcha.value) +
            '&timezone=' + encodeURIComponent(advancedPanel.getTimezoneValue())

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response.error === 'USERNAME_UNAVAILABLE') {
                enableItems()
                usernameItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The username is unavailable.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Please, try something else.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('What about '))
                    errorElement.appendChild((function () {
                        var b = document.createElement('b')
                        b.className = classPrefix + '-offerUsername'
                        b.appendChild(TextNode(response.offerUsername))
                        return b
                    })())
                    errorElement.appendChild(TextNode('?'))
                })
                return
            }

            if (response.error === 'INVALID_CAPTCHA_TOKEN') {
                enableItems()
                captchaItem.setNewCaptcha(response.newCaptcha)
                captchaItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The verification has expired.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Please, try again.'))
                })
                return
            }

            if (response === 'INVALID_CAPTCHA_VALUE') {
                enableItems()
                captchaItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The verification is invalid.'))
                })
                return
            }

            signUpListener(username, response)

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: usernameItem.focus,
    }

}

function SignUpPage_PasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'SignUpPage_PasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Choose a password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function () {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Password_IsShort(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is too short.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Minimum ' + Password_minLength + ' characters required.'))
                })
                return null
            }
            if (Password_ContainsOnlyDigits(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is too simple.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Use some different symbols.'))
                })
                return null
            }
            return value
        },
    }

}

function SignUpPage_RepeatPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'SignUpPage_RepeatPasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Retype the password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function (password) {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (value !== password) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The passwords doesn\'t match.'))
                })
                return null
            }
            return value
        },
    }

}

function SignUpPage_TimezoneItem (timezoneList, backListener) {

    var classPrefix = 'SignUpPage_TimezoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Timezone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var defaultValue = -(new Date).getTimezoneOffset()

    var defaultOption = document.createElement('option')
    defaultOption.value = defaultValue
    defaultOption.appendChild(TextNode(Timezone_Format(defaultValue)))

    var select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.appendChild(defaultOption)
    timezoneList.forEach(function (timezone) {
        var option = document.createElement('option')
        option.value = timezone.value
        option.appendChild(TextNode(timezone.text))
        select.appendChild(option)
    })
    select.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(select)

    return {
        element: element,
        disable: function () {
            select.disabled = true
            select.blur()
        },
        enable: function () {
            select.disabled = false
        },
        getValue: function () {
            return parseInt(select.value, 10)
        },
    }

}

function SignUpPage_TimezoneItemStyle (getResourceUrl) {
    return '.SignUpPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}

function SignUpPage_UsernameItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'SignUpPage_UsernameItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Username'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.maxLength = Username_maxLength
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            var value = CollapseSpaces(input.value)
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Username_ContainsIllegalChars(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The username is illegal.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Use latin letters, digits, underscore, dot and dash.'))
                })
                return null
            }
            if (Username_IsShort(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The username is too short.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Minimum ' + Username_minLength + ' characters required.'))
                })
                return null
            }
            return value
        },
    }

}

function SimpleContactPage (username,
    profile, openListener, closeListener) {

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var classPrefix = 'SimpleContactPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(TextNode('Start a Conversation'))
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    button.addEventListener('click', openListener)

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" is already in your contacts.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus: function () {
            button.focus()
        },
    }

}

function SimpleSelfPage (username, profile, closeListener) {

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var classPrefix = 'SimpleSelfPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" it\'s you.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
        username: username,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}

function SimpleUserPage (session, username, profile,
    addContactListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        button.disabled = false
        buttonNode.nodeValue = 'Add Contact'

        error = _error
        frameElement.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var classPrefix = 'SimpleUserPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var buttonNode = TextNode('Add Contact')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    button.addEventListener('click', function () {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        var url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        var timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        button.disabled = true
        buttonNode.nodeValue = 'Adding...'

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, requestError)

    })

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus: function () {
            button.focus()
        },
    }

}

function Smileys () {

    var array = [{
        text: ':)',
        file: 'face-smile',
    }, {
        text: ':))',
        file: 'face-smile-more',
    }, {
        text: ':(',
        file: 'face-frown',
    }, {
        text: ':((',
        file: 'face-frown-more',
    }, {
        text: ':d',
        file: 'face-laugh',
    }, {
        text: ':dd',
        file: 'face-laugh-more',
    }, {
        text: ':p',
        file: 'face-tongue',
    }, {
        text: ':pp',
        file: 'face-tongue-more',
    }, {
        text: ';)',
        file: 'face-wink',
    }, {
        text: ';))',
        file: 'face-wink-more',
    }, {
        text: ':o',
        file: 'face-surprised',
    }, {
        text: ':oo',
        file: 'face-surprised-more',
    }, {
        text: ':s',
        file: 'face-confused',
    }, {
        text: ':ss',
        file: 'face-confused-more',
    }, {
        text: ':3',
        file: 'face-cat',
    }, {
        text: ':33',
        file: 'face-cat-more',
    }, {
        text: ':|',
        file: 'face-neutral',
    }, {
        text: ':||',
        file: 'face-neutral-more',
    }, {
        text: ':*',
        file: 'face-kiss',
    }, {
        text: ':**',
        file: 'face-kiss-more',
    }, {
        text: '<3',
        file: 'heart',
    }, {
        text: '</3',
        file: 'heart-broken',
    }]

    var map = Object.create(null)
    array.forEach(function (smiley) {
        map[smiley.text] = smiley
    })

    var table = [
        [':)', ':(', ':d', ':p', ':|'],
        [':))', ':((', ':dd', ':pp', ':||'],
        [';)', ':3', ':s', ':o', ':*'],
        [';))', ':33', ':ss', ':oo', ':**'],
        ['<3', '</3'],
    ]
    table.forEach(function (row) {
        row.forEach(function (text, index) {
            row[index] = map[text]
        })
    })

    return {
        array: array,
        map: map,
        table: table,
    }

}

function SmileysPage_Page (smileys, selectListener, closeListener) {

    function focusAt (e, x, y) {

        var row = buttons[y]
        if (row === undefined) return

        var button = row[x]
        if (button === undefined) return

        e.preventDefault()
        button.focus()

    }

    var classPrefix = 'SmileysPage_Page'

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Smileys'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)

    var buttons = smileys.table.map(function (row, y) {

        var rowElement = Div(classPrefix + '-row')
        var buttons = row.map(function (smiley, x) {

            var text = smiley.text

            var button = document.createElement('button')
            button.className = classPrefix + '-button ' + smiley.file
            button.title = text
            button.addEventListener('click', function () {
                selectListener(text)
            })
            button.addEventListener('keydown', function (e) {

                if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return

                var keyCode = e.keyCode
                if (keyCode === 27) {
                    e.preventDefault()
                    closeListener()
                    return
                }

                if (keyCode === 37) {
                    focusAt(e, x - 1, y)
                    return
                }

                if (keyCode === 38) {
                    focusAt(e, x, y - 1)
                    return
                }

                if (keyCode === 39) {
                    focusAt(e, x + 1, y)
                    return
                }

                if (keyCode === 40) focusAt(e, x, y + 1)

            })

            rowElement.appendChild(button)
            return button

        })

        frameElement.appendChild(rowElement)
        return buttons

    })

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: function () {
            buttons[0][0].focus()
        },
    }

}

function SmileysPage_PageStyle (smileys, getResourceUrl) {

    var style = ''
    smileys.array.forEach(function (smiley) {
        var file = smiley.file
        style +=
            '.SmileysPage_Page-button.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}

function TextNode (text) {
    return document.createTextNode(text)
}

function Timezone_Format (value) {

    var sign
    if (value > 0) {
        sign = '+'
    } else if (value < 0) {
        sign = '-'
        value = -value
    } else {
        sign = '\xb1'
    }

    var hours = TwoDigitPad(Math.floor(value / 60))

    return sign + hours + ':' + TwoDigitPad(value % 60)

}

function Timezone_List () {
    return [
        -12 * 60,
        -11 * 60,
        -10 * 60,
        -9 * 60 + 30,
        -9 * 60,
        -8 * 60,
        -7 * 60,
        -6 * 60,
        -5 * 60,
        -4 * 60 + 30,
        -4 * 60,
        -3 * 60 + 30,
        -3 * 60,
        -2 * 60,
        -1 * 60,
        0,
        1 * 60,
        2 * 60,
        3 * 60,
        3 * 60 + 30,
        4 * 60,
        4 * 60 + 30,
        5 * 60,
        5 * 60 + 30,
        5 * 60 + 45,
        6 * 60,
        6 * 60 + 30,
        7 * 60,
        8 * 60,
        8 * 60 + 45,
        9 * 60,
        9 * 60 + 30,
        10 * 60,
        10 * 60 + 30,
        11 * 60,
        11 * 60 + 30,
        12 * 60,
        12 * 60 + 45,
        13 * 60,
        14 * 60,
    ].map(function (value) {
        return {
            value: value,
            text: Timezone_Format(value),
        }
    })
}

function TwoDigitPad (n) {
    var s = String(n)
    if (s.length === 1) s = '0' + s
    return s
}

function UserInfoPanel_EmailItem (profile) {

    var value = profile.email

    var classPrefix = 'UserInfoPanel_Panel-item'

    var labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.appendChild(TextNode('Email: '))

    var valueNode = TextNode(value)

    var valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.appendChild(valueNode)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(valueElement)

    var classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit: function (profile) {
            value = profile.email
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}

function UserInfoPanel_FullNameItem (profile) {

    var value = profile.fullName

    var classPrefix = 'UserInfoPanel_Panel-item'

    var labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.appendChild(TextNode('Full name: '))

    var valueNode = TextNode(value)

    var valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.appendChild(valueNode)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(valueElement)

    var classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit: function (profile) {
            value = profile.fullName
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}

function UserInfoPanel_Panel (profile) {

    function isEmpty (profile) {
        return profile.fullName === '' && profile.email === '' &&
            profile.phone === '' && profile.timezone === null
    }

    var fullNameItem = UserInfoPanel_FullNameItem(profile)

    var emailItem = UserInfoPanel_EmailItem(profile)

    var phoneItem = UserInfoPanel_PhoneItem(profile)

    var timezoneItem = UserInfoPanel_TimezoneItem(profile)

    var emptyElement = Div('UserInfoPanel_Panel-empty')
    emptyElement.appendChild(TextNode('No more information available'))

    var emptyClassList = emptyElement.classList
    if (!isEmpty(profile)) emptyClassList.add('hidden')

    var element = Div('UserInfoPanel_Panel')
    element.appendChild(fullNameItem.element)
    element.appendChild(emailItem.element)
    element.appendChild(phoneItem.element)
    element.appendChild(timezoneItem.element)
    element.appendChild(emptyElement)

    return {
        element: element,
        edit: function (profile) {

            fullNameItem.edit(profile)
            emailItem.edit(profile)
            phoneItem.edit(profile)
            timezoneItem.edit(profile)

            if (isEmpty(profile)) emptyClassList.remove('hidden')
            else emptyClassList.add('hidden')

        },
    }

}

function UserInfoPanel_PhoneItem (profile) {

    var value = profile.phone

    var classPrefix = 'UserInfoPanel_Panel-item'

    var labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.appendChild(TextNode('Phone: '))

    var valueNode = TextNode(value)

    var valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.appendChild(valueNode)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(valueElement)

    var classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit: function (profile) {
            value = profile.phone
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}

function UserInfoPanel_TimezoneItem (profile) {

    var value = profile.timezone

    var classPrefix = 'UserInfoPanel_Panel-item'

    var labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.appendChild(TextNode('Timezone: '))

    var valueNode = TextNode('')

    var valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.appendChild(valueNode)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(valueElement)

    var classList = element.classList
    if (value === null) classList.add('hidden')
    else valueNode.nodeValue = Timezone_Format(value)

    return {
        element: element,
        edit: function (profile) {
            value = profile.timezone
            if (value === null) {
                classList.add('hidden')
                valueNode.nodeValue = ''
            } else {
                classList.remove('hidden')
                valueNode.nodeValue = Timezone_Format(value)
            }
        },
    }

}

function Username_ContainsIllegalChars (username) {
    return /[^a-z0-9_.-]/i.test(username)
}

function Username_IsShort (username) {
    return username.length < Username_minLength
}

function Username_IsValid (username) {
    return !Username_IsShort(username) &&
        username.length < Username_maxLength &&
        !Username_ContainsIllegalChars(username)
}

var Username_minLength = 6
var Username_maxLength = 32

function WelcomePage_CheckInstalled (element, logoWrapperElement) {

    var mozApps = navigator.mozApps
    if (mozApps === undefined) return

    var manifest = location.protocol + '//' + location.host +
        location.pathname.replace(/[^/]+$/, '') + 'webappManifest'

    var request = mozApps.checkInstalled(manifest)
    request.onsuccess = function () {
        if (request.result !== null) return
        var installItem = WelcomePage_InstallItem(mozApps, manifest, function () {
            element.removeChild(installItem.element)
        })
        element.insertBefore(installItem.element, logoWrapperElement)
    }

}

function WelcomePage_InstallItem (mozApps, manifest, installListener) {

    var button = document.createElement('button')
    button.className = 'WelcomePage_InstallItem OrangeButton'
    button.appendChild(TextNode('Install as a Firefox App'))
    button.addEventListener('click', function () {
        var request = mozApps.install(manifest)
        request.onsuccess = installListener
    })

    return { element: button }

}

function WelcomePage_Page (username, signUpListener,
    privacyListener, signInListener, warningCallback) {

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        staySignedInItem.enable()
        signInButton.disabled = false
        signInNode.nodeValue = 'Sign In'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        signInForm.insertBefore(error.element, signInButton)
        signInButton.focus()
    }

    var error = null
    var warningElement = null
    var request = null

    var classPrefix = 'WelcomePage_Page'

    var usernameItem = WelcomePage_UsernameItem(username)

    var passwordItem = WelcomePage_PasswordItem()

    var signInNode = TextNode('Sign In')

    var signInButton = document.createElement('button')
    signInButton.className = classPrefix + '-signInButton GreenButton'
    signInButton.appendChild(signInNode)

    var staySignedInItem = WelcomePage_StaySignedInItem()

    var signInForm = document.createElement('form')
    signInForm.className = classPrefix + '-signInForm'
    signInForm.appendChild(usernameItem.element)
    signInForm.appendChild(passwordItem.element)
    signInForm.appendChild(staySignedInItem.element)
    signInForm.appendChild(signInButton)
    signInForm.addEventListener('submit', function (e) {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()

        if (warningElement !== null) {
            frameElement.removeChild(warningElement)
            warningElement = null
        }

        if (error !== null) {
            signInForm.removeChild(error.element)
            error = null
        }

        var username = usernameItem.getValue()
        if (username === null) return

        var password = passwordItem.getValue()
        if (password === null) return

        usernameItem.disable()
        passwordItem.disable()
        staySignedInItem.disable()
        signInButton.disabled = true
        signInNode.nodeValue = 'Signing in...'

        var url = 'data/signIn' +
            '?username=' + encodeURIComponent(username.toLowerCase()) +
            '&password=' + encodeURIComponent(password)
        if (staySignedInItem.isChecked()) url += '&longTerm=true'

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_USERNAME') {
                enableItems()
                usernameItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('There is no such user.'))
                })
                return
            }

            if (response === 'INVALID_PASSWORD') {
                enableItems()
                passwordItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is incorrect.'))
                })
                return
            }

            signInListener(response.username, response.session)

        }, requestError)

    })

    var logoElement = Div(classPrefix + '-logo')

    var logoWrapperElement = Div(classPrefix + '-logoWrapper')
    logoWrapperElement.appendChild(logoElement)

    var signUpButton = document.createElement('button')
    signUpButton.className = classPrefix + '-signUpButton OrangeButton'
    signUpButton.appendChild(TextNode('Create an Account \u203a'))
    signUpButton.addEventListener('click', function () {
        destroy()
        signUpListener()
    })

    var privacy = WelcomePage_Privacy(privacyListener)

    var publicLine = WelcomePage_PublicLine()

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(logoWrapperElement)
    frameElement.appendChild(publicLine.element)
    if (warningCallback !== undefined) {

        warningElement = Div(classPrefix + '-warning')

        warningCallback(warningElement)
        frameElement.appendChild(warningElement)

    }
    frameElement.appendChild(signInForm)
    frameElement.appendChild(TextNode('New to Bazgu?'))
    frameElement.appendChild(document.createElement('br'))
    frameElement.appendChild(signUpButton)
    frameElement.appendChild(privacy.element)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    WelcomePage_CheckInstalled(frameElement, logoWrapperElement)

    return {
        destroy: publicLine.destroy,
        element: element,
        focusPrivacy: privacy.focus,
        focus: function () {
            if (username === '') usernameItem.focus()
            else passwordItem.focus()
        },
        focusSignUp: function () {
            signUpButton.focus()
        },
    }

}

function WelcomePage_PageStyle (getResourceUrl) {
    return '.WelcomePage_Page-logo {' +
            'background-image: url(' + getResourceUrl('img/logo.svg') + ')' +
        '}'
}

function WelcomePage_PasswordItem () {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'WelcomePage_PasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Password_IsValid(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is invalid.'))
                })
                return null
            }
            return value
        },
    }

}

function WelcomePage_Privacy (listener) {

    var classPrefix = 'WelcomePage_Privacy'

    var button = document.createElement('button')
    button.className = classPrefix + '-privacy'
    button.appendChild(TextNode('Privacy'))
    button.addEventListener('click', listener)

    var element = Div('WelcomePage_Privacy')
    element.appendChild(button)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}

function WelcomePage_PublicLine () {

    function add (text) {

        var itemElement = Div(classPrefix + '-item')
        itemElement.appendChild(TextNode(text))

        element.appendChild(itemElement)
        itemElements.push(itemElement)

    }

    function animate () {
        timeout = setTimeout(function () {
            itemElements[selectedIndex].classList.remove('visible')
            timeout = setTimeout(function () {
                selectedIndex = (selectedIndex + 1) % itemElements.length
                itemElements[selectedIndex].classList.add('visible')
                animate()
            }, 600)
        }, 8000)
    }

    var selectedIndex = 0
    var itemElements = []

    var classPrefix = 'WelcomePage_PublicLine'

    var element = Div(classPrefix)
    add('Chat without distractions.')
    add('Send unlimited size files.')
    itemElements[0].classList.add('visible')

    var timeout
    animate()

    return {
        element: element,
        destroy: function () {
            clearTimeout(timeout)
        },
    }

}

function WelcomePage_StaySignedInItem () {

    function addListener (listener) {
        clickElement.addEventListener('click', listener)
    }

    function check () {
        checked = true
        buttonClassList.add('checked')
        removeListener(check)
        addListener(uncheck)
        activeListener = uncheck
        button.focus()
    }

    function removeListener (listener) {
        clickElement.removeEventListener('click', listener)
    }

    function uncheck () {
        checked = false
        buttonClassList.remove('checked')
        removeListener(uncheck)
        addListener(check)
        activeListener = check
        button.focus()
    }

    var classPrefix = 'WelcomePage_StaySignedInItem'

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.type = 'button'

    var buttonClassList = button.classList

    var clickElement = Div(classPrefix + '-click')
    clickElement.appendChild(button)
    clickElement.appendChild(TextNode('Stay signed in'))

    var clickClassList = clickElement.classList

    var element = Div(classPrefix)
    element.appendChild(clickElement)

    var checked = true

    var activeListener = check
    addListener(check)

    return {
        element: element,
        disable: function () {
            clickClassList.add('disabled')
            button.disabled = true
            removeListener(activeListener)
        },
        enable: function () {
            clickClassList.remove('disabled')
            button.disabled = false
            addListener(activeListener)
        },
        isChecked: function () {
            return checked
        },
    }

}

function WelcomePage_StaySignedInItemStyle (getResourceUrl) {
    return '.WelcomePage_StaySignedInItem-button.checked {' +
            'background-image: url(' + getResourceUrl('img/checked/normal.svg') + ')' +
        '}' +
        '.WelcomePage_StaySignedInItem-button.checked:active {' +
            'background-image: url(' + getResourceUrl('img/checked/active.svg') + ')' +
        '}'
}

function WelcomePage_UnauthenticatedWarning (username) {
    return function (element) {

        var usernameElement = document.createElement('b')
        usernameElement.className = 'WelcomePage_UnauthenticatedWarning-username'
        usernameElement.appendChild(TextNode(username))

        element.appendChild(TextNode('You need to sign in or create an account to contact "'))
        element.appendChild(usernameElement)
        element.appendChild(TextNode('".'))

    }
}

function WelcomePage_UsernameItem (value) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'WelcomePage_UsernameItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Username'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.value = value
    input.className = classPrefix + '-input'

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            var value = CollapseSpaces(input.value)
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Username_IsValid(value)) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The username is invalid.'))
                })
                return null
            }
            return value
        },
    }

}

function WorkPage_ChatPanel_CloseButton (listener) {

    var button = document.createElement('button')
    button.className = 'WorkPage_ChatPanel_CloseButton'
    button.title = 'Close'
    button.addEventListener('click', listener)

    return {
        element: button,
        addEventListener: function (name, listener) {
            button.addEventListener(name, listener)
        },
        disable: function () {
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        focus: function () {
            button.focus()
        },
    }

}

function WorkPage_ChatPanel_CloseButtonStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_CloseButton {' +
            'background-image: url(' + getResourceUrl('img/close.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_ContactMenu (profileListener, removeListener) {

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    var classPrefix = 'WorkPage_ChatPanel_ContactMenu'

    var profileItemElement = Div(classPrefix + '-item')
    profileItemElement.appendChild(TextNode('Profile'))
    profileItemElement.addEventListener('click', profileListener)

    var removeItemElement = Div(classPrefix + '-item')
    removeItemElement.appendChild(TextNode('Remove'))
    removeItemElement.addEventListener('click', removeListener)

    var element = Div(classPrefix)
    element.appendChild(profileItemElement)
    element.appendChild(removeItemElement)

    var selectedElement = null

    return {
        element: element,
        openSelected: function () {
            if (selectedElement === profileItemElement) profileListener()
            else if (selectedElement === removeItemElement) removeListener()
        },
        reset: function () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown: function () {
            if (selectedElement === profileItemElement) {
                resetSelected()
                selectedElement = removeItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = profileItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp: function () {
            if (selectedElement === removeItemElement) {
                resetSelected()
                selectedElement = profileItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = removeItemElement
            }
            selectedElement.classList.add('active')
        },
    }
}

function WorkPage_ChatPanel_DaySeparator (day) {

    var date = new Date(day * 24 * 60 * 60 * 1000)

    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December']

    var dateString = monthNames[date.getUTCMonth()] + ' ' +
        date.getUTCDate() + ', ' + date.getUTCFullYear()

    var element = Div('WorkPage_ChatPanel_DaySeparator')
    element.appendChild(TextNode(dateString))

    return { element: element }

}

function WorkPage_ChatPanel_FeedFile (token, file,
    progressListener, completeListener, cancelListener, destroyListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function ChunkedReader (file) {
        var offset = 0
        return {
            getOffset: function () {
                return offset
            },
            hasNextChunk: function () {
                return offset < file.size
            },
            readNextChunk: function (chunkSize, callback) {
                var blob = file.slice(offset, offset + chunkSize)
                var reader = new FileReader
                reader.readAsArrayBuffer(blob)
                reader.onload = function () {
                    offset += chunkSize
                    callback(reader.result)
                }
                return function () {
                    reader.abort()
                }
            },
        }
    }

    function next (chunkSize) {
        abortFunction = reader.readNextChunk(chunkSize, function (chunk) {

            var time = Date.now()

            var request = new XMLHttpRequest
            request.open('post', 'data/feedFile?token=' + encodeURIComponent(token))
            request.responseType = 'json'
            request.send(chunk)
            request.onerror = connectionErrorListener
            request.onload = function () {

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    cancelListener()
                    destroyListener()
                    return
                }

                if (response !== true) {
                    crashListener()
                    return
                }

                if (reader.hasNextChunk()) {
                    var elapsed = Date.now() - time
                    if (elapsed < 500 && chunkSize < 1024 * 1024 * 16) {
                        chunkSize *= 2
                    } else if (elapsed > 2000 && chunkSize > 1024) {
                        chunkSize /= 2
                    }
                    progressListener(reader.getOffset())
                    next(chunkSize)
                    return
                }

                completeListener()
                destroyListener()

            }

            abortFunction = function () {
                request.abort()
            }

        })
    }

    var abortFunction

    var reader = ChunkedReader(file)
    next(1024 * 8)

    return function () {
        abortFunction()
    }

}

function WorkPage_ChatPanel_MessageText (formatText,
    isLocalLink, element, text, linkListener) {

    formatText(text, function (text) {
        element.appendChild(TextNode(text))
    }, function (link) {

        var a = document.createElement('a')
        a.href = link
        if (!isLocalLink(link)) a.target = '_blank'
        a.className = 'WorkPage_ChatPanel_MessageText-link'
        a.appendChild(TextNode(link))

        var span = document.createElement('span')
        span.className = 'WorkPage_ChatPanel_MessageText-disabledLink'
        span.appendChild(TextNode(link))

        element.appendChild(a)

        linkListener({
            disable: function () {
                element.replaceChild(span, a)
            },
            enable: function () {
                element.replaceChild(a, span)
            },
        })

    }, function (text, icon) {
        element.appendChild(WorkPage_ChatPanel_Smiley(text, icon))
    })

}

function WorkPage_ChatPanel_MessagesPanel (playAudio,
    formatBytes, formatText, isLocalLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    textareaFocusListener, textareaBlurListener, sendSmileyListener,
    sendContactListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addMessage (message, time) {
        ensureSeparator(time)
        doneMessagesElement.appendChild(message.element)
        sendingMessagesClassList.add('notFirst')
        liveMessages.push(message)
        lastMessage = message
    }

    function addReceivedFileMessage (file, time, token) {
        if (!canMerge('receivedFile', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_ReceivedFileMessage(
                    formatBytes, receivedFiles, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        var part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addReceivedTextMessage (text, time, token) {
        if (!canMerge('receivedText', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_ReceivedTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentFileMessage (file, time, token) {
        if (!canMerge('sentFile', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_SentFileMessage(
                    formatBytes, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        var part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addSentFileMessageAndStore (file, time, token) {
        messages.push(['sentFile', file, time, token])
        return addSentFileMessage(file, time, token)
    }

    function addSentTextMessage (text, time, token) {
        if (!canMerge('sentText', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_SentTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentTextMessageAndStore (text, time, token) {
        addSentTextMessage(text, time, token)
        messages.push(['sentText', text, time, token])
    }

    function canMerge (type, time) {
        return lastMessage !== null &&
            lastMessage.type === type && lastMessage.minute === Minute(time)
    }

    function destroy () {
        typePanel.destroy()
        destroyEvent.emit()
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function ensureSeparator (time) {
        var day = Day(time)
        var separator = daySeparators[day]
        if (separator !== undefined) return
        separator = daySeparators[day] = WorkPage_ChatPanel_DaySeparator(day)
        doneMessagesElement.appendChild(separator.element)
    }

    function getLastSendingMessage (type) {
        var length = sendingMessages.length
        if (length !== 0) {
            var message = sendingMessages[length - 1]
            if (message.type === type) return message
        }
    }

    function scrollDown () {
        contentElement.scrollTop = contentElement.scrollHeight
    }

    function sendNewTextMessage (text) {

        var message = getLastSendingMessage('SendingText')
        if (message === undefined) {
            message = WorkPage_ChatPanel_SendingTextMessage(
                disableEvent, enableEvent, formatText,
                isLocalLink, session, contactUsername,
                function () {
                    destroyEvent.removeListener(message.destroy)
                    sendingMessagesElement.removeChild(message.element)
                    sendingMessages.splice(sendingMessages.indexOf(message), 1)
                    if (sendingMessagesElement.childNodes.length === 0) {
                        sendingMessagesClassList.add('hidden')
                    }
                },
                destroyAndInvalidSession, destroyAndConnectionError,
                destroyAndCrash, destroyAndServiceError
            )
            sendingMessages.push(message)
        }

        message.add(text, function (response) {
            addSentTextMessageAndStore(text, response.time, response.token)
            playAudio('message-seen')
        })

        destroyEvent.addListener(message.destroy)
        sendingMessagesElement.appendChild(message.element)
        if (sendingMessagesElement.childNodes.length === 1) {
            sendingMessagesClassList.remove('hidden')
        }
        scrollDown()

    }

    var disableEvent = WorkPage_Event(),
        enableEvent = WorkPage_Event()

    var timezone = session.profile.timezone,
        timezoneOffset = timezone * 60 * 1000

    var lastMessage = null

    var sendingMessages = []

    var messages = WorkPage_Messages(username, contactUsername)
    var liveMessages = []
    var messagePartsMap = Object.create(null)
    var daySeparators = Object.create(null)
    var destroyEvent = WorkPage_Event()

    var classPrefix = 'WorkPage_ChatPanel_MessagesPanel'

    var doneMessagesElement = Div(classPrefix + '-doneMessages')

    var sendingMessagesElement = Div(classPrefix + '-sendingMessages hidden')

    var sendingMessagesClassList = sendingMessagesElement.classList

    var contentElement = Div(classPrefix + '-content')
    contentElement.appendChild(doneMessagesElement)
    contentElement.appendChild(sendingMessagesElement)

    var typePanel = WorkPage_ChatPanel_TypePanel(function () {
        classList.add('typing')
        textareaFocusListener()
    }, function () {
        classList.remove('typing')
        textareaBlurListener()
    }, sendNewTextMessage, function (files) {
        Array.prototype.forEach.call(files, function (readableFile) {

            var file = {
                name: readableFile.name,
                size: readableFile.size,
            }

            var message = getLastSendingMessage('SendingFile')
            if (message === undefined) {
                message = WorkPage_ChatPanel_SendingFileMessage(
                    formatBytes, session, contactUsername,
                    function () {
                        destroyEvent.removeListener(message.destroy)
                        sendingMessagesElement.removeChild(message.element)
                        sendingMessages.splice(sendingMessages.indexOf(message), 1)
                        if (sendingMessagesElement.childNodes.length === 0) {
                            sendingMessagesClassList.add('hidden')
                        }
                    },
                    destroyAndInvalidSession, destroyAndConnectionError,
                    destroyAndCrash, destroyAndServiceError
                )
                sendingMessages.push(message)
            }

            message.add(file, function (response) {
                var sendToken = response.sendToken
                var startFeed = addSentFileMessageAndStore(file, response.time, response.token).startSend()
                sentFiles.add(sendToken, function (endCallback) {

                    function complete () {
                        progress.complete()
                        endCallback()
                    }

                    var progress = startFeed()
                    if (file.size === 0) complete()
                    else {
                        var destroyFunction = WorkPage_ChatPanel_FeedFile(
                            sendToken, readableFile, progress.progress, complete,
                            function () {
                                progress.cancel()
                                endCallback()
                            },
                            function () {
                                destroyEvent.removeListener(destroyFunction)
                            },
                            destroyAndConnectionError, destroyAndCrash, destroyAndServiceError
                        )
                        destroyEvent.addListener(destroyFunction)
                    }

                })
                playAudio('message-seen')
            })

            destroyEvent.addListener(message.destroy)
            sendingMessagesElement.appendChild(message.element)
            if (sendingMessagesElement.childNodes.length === 1) {
                sendingMessagesClassList.remove('hidden')
            }
            scrollDown()

        })
        scrollDown()
    }, function () {
        sendSmileyListener(sendNewTextMessage)
    }, function () {
        sendContactListener(function (contacts) {
            var text = contacts.map(function (contact) {
                return location.protocol + '//' + location.host +
                    location.pathname + '#' + contact.username
            }).join('\n')
            sendNewTextMessage(text)
        })
    }, closeListener)

    var element = Div(classPrefix)
    element.appendChild(contentElement)
    element.appendChild(typePanel.element)

    var classList = element.classList

    WorkPage_ChatPanel_RestoreMessages(messages, session,
        addReceivedFileMessage, addReceivedTextMessage,
        addSentFileMessage, addSentTextMessage)

    return {
        destroy: destroy,
        element: element,
        sendFileMessage: addSentFileMessageAndStore,
        sendTextMessage: addSentTextMessageAndStore,
        disable: function () {
            typePanel.disable()
            disableEvent.emit()
        },
        editTimezone: function (newTimezone) {

            timezone = newTimezone
            timezoneOffset = timezone * 60 * 1000

            Object.keys(daySeparators).forEach(function (day) {
                doneMessagesElement.removeChild(daySeparators[day].element)
                delete daySeparators[day]
            })

            liveMessages.forEach(function (message) {
                message.editTimezone(timezoneOffset)
                ensureSeparator(message.time + timezoneOffset)
                doneMessagesElement.appendChild(message.element)
            })

        },
        enable: function () {
            typePanel.enable()
            enableEvent.emit()
        },
        focus: function () {
            typePanel.focus()
            scrollDown()
        },
        receiveFileMessage: function (file, time, token) {
            addReceivedFileMessage(file, time, token).showReceive()
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage: function (text, time, token) {
            addReceivedTextMessage(text, time, token)
            messages.push(['receivedText', text, time, token])
        },
    }

}

function WorkPage_ChatPanel_MoreButton (closeListener,
    fileListener, sendSmileyListener, sendContactListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        buttonClassList.add('not_pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        button.addEventListener('keydown', collapsedKeyDown)
        element.removeChild(moreMenu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        moreMenu.reset()
    }

    function collapsedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    }

    function expand () {
        expanded = true
        buttonClassList.remove('not_pressed')
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.removeEventListener('keydown', collapsedKeyDown)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.appendChild(moreMenu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            moreMenu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            button.focus()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            button.focus()
            moreMenu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            button.focus()
            moreMenu.selectDown()
        }
    }

    function windowFocus (e) {
        var target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    var moreMenu = WorkPage_ChatPanel_MoreMenu(keyDown, function (files) {
        collapse()
        fileListener(files)
    }, function () {
        collapse()
        sendSmileyListener()
    }, function () {
        collapse()
        sendContactListener()
    })

    var expanded = false

    var classPrefix = 'WorkPage_ChatPanel_MoreButton'

    var button = document.createElement('button')
    button.className = classPrefix + '-button not_pressed'
    button.appendChild(TextNode('More'))
    button.addEventListener('click', expand)
    button.addEventListener('keydown', collapsedKeyDown)

    var buttonClassList = button.classList

    var element = Div(classPrefix)
    element.appendChild(button)

    var classList = element.classList

    return {
        element: element,
        destroy: function () {
            if (expanded) collapse()
        },
        disable: function () {
            if (expanded) collapse()
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        typeBlur: function () {
            classList.remove('typeFocused')
        },
        typeFocus: function () {
            classList.add('typeFocused')
        },
    }

}

function WorkPage_ChatPanel_MoreButtonStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_MoreButton-button {' +
            'background-image: url(' + getResourceUrl('img/arrow-up.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_MoreButton-button:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-up-active.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_MoreMenu (fileInputKeyDownListener,
    fileListener, smileyListener, contactListener) {

    function addFileInput () {
        fileInput = document.createElement('input')
        fileInput.type = 'file'
        fileInput.multiple = true
        fileInput.className = classPrefix + '-fileInput'
        fileInput.addEventListener('keydown', fileInputKeyDownListener)
        fileInput.addEventListener('change', function () {
            fileListener(fileInput.files)
            fileItemElement.removeChild(fileInput)
            addFileInput()
        })
        fileItemElement.appendChild(fileInput)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    var classPrefix = 'WorkPage_ChatPanel_MoreMenu'

    var fileItemElement = Div(classPrefix + '-item')
    fileItemElement.appendChild(TextNode('File'))

    var smileyItemElement = Div(classPrefix + '-item')
    smileyItemElement.appendChild(TextNode('Smiley'))
    smileyItemElement.addEventListener('click', smileyListener)

    var contactItemElement = Div(classPrefix + '-item')
    contactItemElement.appendChild(TextNode('Contact'))
    contactItemElement.addEventListener('click', contactListener)

    var element = Div(classPrefix)
    element.appendChild(fileItemElement)
    element.appendChild(smileyItemElement)
    element.appendChild(contactItemElement)

    var selectedElement = null

    var fileInput
    addFileInput()

    return {
        element: element,
        openSelected: function () {
            if (selectedElement === fileItemElement) {
            } else if (selectedElement === smileyItemElement) {
                smileyListener()
            } else if (selectedElement === contactItemElement) {
                contactListener()
            }
        },
        reset: function () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown: function () {
            if (selectedElement === fileItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = contactItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            }
            selectedElement.classList.add('active')
        },
        selectUp: function () {
            if (selectedElement === contactItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = contactItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}

function WorkPage_ChatPanel_Panel (playAudio, formatBytes,
    formatText, isLocalLink, sentFiles, receivedFiles, username,
    session, contactUsername, profile, overrideProfile,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_ChatPanel_Panel'

    var title = WorkPage_ChatPanel_Title(contactUsername,
        profile, overrideProfile, profileListener, removeListener)

    var messagesPanel = WorkPage_ChatPanel_MessagesPanel(
        playAudio, formatBytes, formatText, isLocalLink, sentFiles,
        receivedFiles, username, session, contactUsername,
        function () {
            barClassList.add('hidden')
        },
        function () {
            barClassList.remove('hidden')
        },
        sendSmileyListener, sendContactListener,
        closeListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    var closeButton = WorkPage_ChatPanel_CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var barElement = Div(classPrefix + '-bar')
    barElement.appendChild(title.element)
    barElement.appendChild(closeButton.element)

    var barClassList = barElement.classList

    var element = Div(classPrefix)
    element.appendChild(barElement)
    element.appendChild(messagesPanel.element)

    return {
        element: element,
        editContactProfile: title.editContactProfile,
        editTimezone: messagesPanel.editTimezone,
        focus: messagesPanel.focus,
        overrideContactProfile: title.overrideContactProfile,
        receiveFileMessage: messagesPanel.receiveFileMessage,
        receiveTextMessage: messagesPanel.receiveTextMessage,
        sendFileMessage: messagesPanel.sendFileMessage,
        sendTextMessage: messagesPanel.sendTextMessage,
        destroy: function () {
            title.destroy()
            messagesPanel.destroy()
        },
        disable: function () {
            title.disable()
            closeButton.disable()
            messagesPanel.disable()
        },
        enable: function () {
            title.enable()
            closeButton.enable()
            messagesPanel.enable()
        },
    }

}

function WorkPage_ChatPanel_PanelStyle () {
    return '.WorkPage_ChatPanel_Panel {' +
            'background-image: url(' + getResourceUrl('img/light-grass.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_ReceivedFileMessage (
    formatBytes, receivedFiles, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_ReceivedFileMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'receivedFile',
        add: function (file) {

            var nameNode = TextNode(file.name)

            var sizeElement = Div(classPrefix + '-item-size')
            sizeElement.appendChild(TextNode(formatBytes(file.size)))

            var itemElement = Div(classPrefix + '-item')
            itemElement.appendChild(nameNode)
            itemElement.appendChild(sizeElement)

            element.appendChild(itemElement)

            return {
                showReceive: function () {

                    var receiveLink = document.createElement('a')
                    receiveLink.className = classPrefix + '-receiveLink'
                    receiveLink.appendChild(TextNode('Receive'))
                    receiveLink.target = '_blank'
                    receiveLink.href = 'data/receiveFile?token=' + encodeURIComponent(file.token)

                    itemElement.classList.add('withReceiveLink')
                    itemElement.insertBefore(receiveLink, nameNode)
                    receivedFiles.add(file.token, function () {
                        itemElement.classList.remove('withReceiveLink')
                        itemElement.removeChild(receiveLink)
                    })

                },
            }

        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_ReceivedTextMessage (disableEvent,
    enableEvent, formatText, isLocalLink, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_ReceivedTextMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'receivedText',
        add: function (text) {
            var itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, function (link) {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.appendChild(itemElement)
            return {}
        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_RestoreMessages (messages,
    session, addReceivedFileMessage, addReceivedTextMessage,
    addSentFileMessage, addSentTextMessage) {

    messages.array.forEach(function (message) {
        var type = message[0]
        if (type === 'receivedFile') {
            var file = message[1]
            var part = addReceivedFileMessage(file, message[2], message[3])
            if (session.files[file.token] !== undefined) part.showReceive()
        } else if (type === 'receivedText') {
            addReceivedTextMessage(message[1], message[2], message[3])
        } else if (type === 'sentFile') {
            addSentFileMessage(message[1], message[2], message[3])
        } else {
            addSentTextMessage(message[1], message[2], message[3])
        }
    })

}

function WorkPage_ChatPanel_SendingFileMessage (formatBytes, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_ChatPanel_SendingFileMessage'

    var element = Div(classPrefix)

    var requests = []

    return {
        element: element,
        type: 'SendingFile',
        add: function (file, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            var name = file.name,
                size = file.size

            var sizeElement = Div(classPrefix + '-size')
            sizeElement.appendChild(TextNode(formatBytes(size)))

            var itemElement = Div(classPrefix + '-item')
            itemElement.appendChild(TextNode(name))
            itemElement.appendChild(document.createElement('br'))
            itemElement.appendChild(sizeElement)

            element.appendChild(itemElement)

            var url = 'data/sendFileMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&name=' + encodeURIComponent(name) +
                '&size=' + encodeURIComponent(size)

            var request = GetJson(url, function () {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, function () {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy: function () {
            requests.forEach(function (request) {
                request.abort()
            })
        },
    }

}

function WorkPage_ChatPanel_SendingTextMessage (
    disableEvent, enableEvent, formatText, isLocalLink, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_ChatPanel_SendingTextMessage'

    var element = Div(classPrefix)

    var requests = []

    return {
        element: element,
        type: 'SendingText',
        add: function (text, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            var itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, function (link) {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.appendChild(itemElement)

            var url = 'data/sendTextMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&text=' + encodeURIComponent(text)

            var request = GetJson(url, function () {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, function () {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy: function () {
            requests.forEach(function (request) {
                request.abort()
            })
        },
    }

}

function WorkPage_ChatPanel_SentFileMessage (formatBytes, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_SentFileMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentFile',
        add: function (file) {

            var sizeElement = Div(classPrefix + '-item-size')
            sizeElement.appendChild(TextNode(formatBytes(file.size)))

            var itemElement = Div(classPrefix + '-item')
            itemElement.appendChild(TextNode(file.name))
            itemElement.appendChild(sizeElement)

            element.appendChild(itemElement)

            return {
                startSend: function () {

                    var node = TextNode('Waiting...')

                    var sendingElement = Div(classPrefix + '-item-sending')
                    sendingElement.appendChild(node)

                    itemElement.classList.add('sending')
                    itemElement.appendChild(sendingElement)

                    return function () {

                        node.nodeValue = '0%'

                        var doneTextNode = TextNode('0%')

                        var doneTextElement = Div(classPrefix + '-item-sending-done-text')
                        doneTextElement.appendChild(doneTextNode)

                        var doneElement = Div(classPrefix + '-item-sending-done')
                        doneElement.appendChild(doneTextElement)

                        sendingElement.appendChild(doneElement)

                        return {
                            cancel: function () {
                                sendingElement.classList.add('failed')
                                doneTextElement.classList.add('failed')
                            },
                            complete: function () {
                                node.nodeValue = doneTextNode.nodeValue = '100%'
                                doneElement.style.width = '100%'
                            },
                            progress: function (sent) {

                                var percent = Math.round(sent / file.size * 100) + '%'

                                node.nodeValue = doneTextNode.nodeValue = percent
                                doneElement.style.width = percent

                            },
                        }

                    }

                },
            }

        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_SentTextMessage (disableEvent,
    enableEvent, formatText, isLocalLink, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_SentTextMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentText',
        add: function (text) {
            var itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, function (link) {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.appendChild(itemElement)
            return {}
        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_Smiley (text, icon) {
    var element = document.createElement('img')
    element.className = 'WorkPage_ChatPanel_Smiley ' + icon
    element.title = text
    element.alt = text
    element.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQIHWP4zwAAAgEBAMVfG14AAAAASUVORK5CYII='
    return element
}

function WorkPage_ChatPanel_SmileyStyle (smileys, getResourceUrl) {

    var style = ''
    smileys.array.forEach(function (smiley) {
        var file = smiley.file
        style +=
            '.WorkPage_ChatPanel_Smiley.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}

function WorkPage_ChatPanel_Title (contactUsername,
    profile, overrideProfile, profileListener, removeListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function createButtonText () {
        return overrideProfile.fullName || profile.fullName || contactUsername
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.appendChild(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        var target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    var expanded = false

    var classPrefix = 'WorkPage_ChatPanel_Title'

    var menu = WorkPage_ChatPanel_ContactMenu(function () {
        collapse()
        profileListener()
    }, function () {
        collapse()
        removeListener()
    })

    var node = TextNode(createButtonText())

    var buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.appendChild(node)

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.appendChild(buttonTextElement)
    button.addEventListener('click', expand)

    var buttonClassList = button.classList

    var element = Div(classPrefix)
    element.appendChild(button)

    return {
        element: element,
        destroy: function () {
            if (expanded) collapse()
        },
        disable: function () {
            if (expanded) collapse()
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        editContactProfile: function (_profile) {
            profile = _profile
            node.nodeValue = createButtonText()
        },
        overrideContactProfile: function (_overrideProfile) {
            overrideProfile = _overrideProfile
            node.nodeValue = createButtonText()
        },
    }

}

function WorkPage_ChatPanel_TitleStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_Title-button {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_Title-button.pressed {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_Title-button.pressed:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed-active.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_TypePanel (
    focusListener, blurListener, typeListener, fileListener,
    sendSmileyListener, sendContactListener, closeListener) {

    function submit () {
        var value = textarea.value
        if (/\S/.test(value)) typeListener(value)
        textarea.value = ''
    }

    var classPrefix = 'WorkPage_ChatPanel_TypePanel'

    var textarea = document.createElement('textarea')
    textarea.className = classPrefix + '-textarea'
    textarea.placeholder = 'Type a message here'
    textarea.addEventListener('focus', function () {
        moreButton.typeFocus()
        sendButtonClassList.add('typeFocused')
        focusListener()
    })
    textarea.addEventListener('blur', function () {
        moreButton.typeBlur()
        sendButtonClassList.remove('typeFocused')
        blurListener()
    })
    textarea.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            submit()
        } else if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var sendButton = document.createElement('button')
    sendButton.className = classPrefix + '-sendButton GreenButton'
    sendButton.appendChild(TextNode('Send'))
    sendButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    sendButton.addEventListener('click', function () {
        submit()
        textarea.focus()
    })

    var sendButtonClassList = sendButton.classList

    var moreButton = WorkPage_ChatPanel_MoreButton(closeListener,
        fileListener, sendSmileyListener, sendContactListener)

    var element = Div(classPrefix)
    element.appendChild(textarea)
    element.appendChild(moreButton.element)
    element.appendChild(sendButton)

    return {
        element: element,
        destroy: moreButton.destroy,
        disable: function () {
            textarea.disabled = true
            sendButton.disabled = true
            moreButton.disable()
        },
        enable: function () {
            textarea.disabled = false
            sendButton.disabled = false
            moreButton.enable()
        },
        focus: function () {
            textarea.focus()
        },
    }

}

function WorkPage_ContactRequests (element, username, session,
    showListener, hideListener, addContactListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        if (page !== null) page.destroy()
    }

    function getMessages (username) {
        if (username === visibleUsername) return visibleMessages
        var request = requests[username]
        if (request !== undefined) return request.messages
    }

    function requestError () {
        destroy()
        connectionErrorListener()
    }

    function show (username, request) {
        visibleUsername = username
        visibleMessages = request.messages
        page = ContactRequestPage(session, username, request.profile, function (contactData) {
            addContactListener(username, contactData)
            showNext()
        }, showNext, function () {

            page.destroy()
            showNext()

            var url = 'data/removeRequest' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(username)

            var request = GetJson(url, function () {

                if (request.status !== 200) {
                    destroy()
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    destroy()
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    destroy()
                    invalidSessionListener()
                    return
                }

            }, requestError)

        }, function () {
            element.removeChild(page.element)
            invalidSessionListener()
        })
        element.appendChild(page.element)
    }

    function showNext () {

        element.removeChild(page.element)

        if (Object.keys(requests).length === 0) {
            page = null
            visibleUsername = null
            hideListener()
            return
        }

        var username = Object.keys(requests)[0]
        var request = requests[username]
        delete requests[username]
        show(username, request)

    }

    var requests = Object.create(null)

    var page = null,
        visibleUsername = null,
        visibleMessages = null

    return {
        add: function (requestUsername, profile) {

            var request = {
                profile: profile,
                messages: WorkPage_Messages(username, requestUsername),
            }

            if (page === null) {
                show(requestUsername, request)
                showListener()
            } else {
                requests[requestUsername] = request
            }

        },
        edit: function (username, profile) {
            if (visibleUsername === username) page.edit(profile)
            else requests[username].profile = profile
        },
        receiveFileMessage: function (username, file, time, token) {
            var messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage: function (username, text, time, token) {
            var messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedText', text, time, token])
        },
        remove: function (username) {
            if (visibleUsername === username) {
                page.destroy()
                showNext()
            } else {
                delete requests[username]
            }
        },
    }

}

function WorkPage_Event () {
    var listeners = []
    return {
        addListener: function (listener) {
            listeners.push(listener)
        },
        emit: function (args) {
            listeners.forEach(function (listener) {
                listener.apply(null, args)
            })
        },
        removeListener: function (listener) {
            var index = listeners.indexOf(listener)
            if (index === -1) throw 0
            listeners.splice(index, 1)
        },
    }
}

function WorkPage_LocationHash (session, checkContactListener,
    publicProfileListener, noSuchUserListener, connectionErrorListener,
    crashListener, invalidSessionListener, serviceErrorListener) {

    function check () {

        abort()
        changeEvent.emit()

        var username = location.hash.substr(1)
        if (username === '') return

        var lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        var url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                serviceErrorListener()
                return
            }

            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                noSuchUserListener(username)
                return
            }

            publicProfileListener(response.username, response.profile)

        }, connectionErrorListener)

    }

    function abort () {
        if (request === null) return
        request.abort()
        request = null
    }

    var request = null

    var changeEvent = WorkPage_Event()

    addEventListener('hashchange', check)

    return {
        changeEvent: changeEvent,
        check: check,
        destroy: function () {
            removeEventListener('hashchange', check)
            abort()
        },
        reset: function () {
            removeEventListener('hashchange', check)
            location.hash = ''
            addEventListener('hashchange', check)
        },
    }

}

function WorkPage_Messages (username, contactUsername) {

    var key = username + '$' + contactUsername

    var array = (function () {

        try {
            var array = localStorage[key]
        } catch (e) {
            return []
        }

        if (array === undefined) return []

        try {
            return JSON.parse(array)
        } catch (e) {
            return []
        }

    })()

    return {
        array: array,
        push: function (message) {
            array.push(message)
            if (array.length > 1024) array.shift()
            try {
                localStorage[key] = JSON.stringify(array)
            } catch (e) {
            }
        },
    }

}

function WorkPage_Page (smileys, formatBytes, formatText, isLocalLink,
    renderUserLink, timezoneList, username, session, signOutListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        destroyEvent.emit()
        sidePanel.destroy()
        locationHash.destroy()
        removeEventListener('beforeunload', windowBeforeUnload)
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function disableBackground () {
        sidePanel.disable()
        if (chatPanel !== null) chatPanel.disable()
    }

    function editProfile (newProfile) {
        profile = newProfile
        sidePanel.editProfile(profile)
        editProfileEvent.emit([profile])
    }

    function enableBackground () {
        sidePanel.enable()
        if (chatPanel !== null) chatPanel.enable()
    }

    function showAccountPage (readyCallback) {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.editProfile.removeListener(page.editProfile)
            destroyEvent.removeListener(page.destroy)
        }

        var page = AccountPage_Page(timezoneList, userLink, username, session, profile, function (profile) {
            hide()
            enableBackground()
            editProfile(profile)
        }, function () {
            hide()
            ;(function () {

                function hide () {
                    element.removeChild(page.element)
                    destroyEvent.removeListener(page.destroy)
                }

                var page = ChangePasswordPage_Page(session, function () {
                    hide()
                    showAccountPage(function (page) {
                        page.focusChangePassword()
                    })
                }, function () {
                    hide()
                    enableBackground()
                }, function () {
                    hide()
                    destroyAndInvalidSession()
                })

                element.appendChild(page.element)
                page.focus()
                destroyEvent.addListener(page.destroy)

            })()
        }, function () {
            hide()
            enableBackground()
        }, function () {
            hide()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        readyCallback(page)
        pullMessages.events.editProfile.addListener(page.editProfile)
        destroyEvent.addListener(page.destroy)

    }

    function showAddContactPage (showInvite) {

        function hide () {
            element.removeChild(page.element)
            destroyEvent.removeListener(page.destroy)
        }

        var page = AddContactPage_Page(showInvite, session, username, function (checkUsername) {

            if (checkUsername === lowerUsername) {
                hide()
                ;(function () {

                    function hide () {
                        element.removeChild(page.element)
                        editProfileEvent.removeListener(page.editProfile)
                    }

                    var page = FoundSelfPage(username, profile, function () {
                        hide()
                        showAddContactPage(showInvite)
                    }, function () {
                        hide()
                        enableBackground()
                    })

                    element.appendChild(page.element)
                    page.focus()
                    editProfileEvent.addListener(page.editProfile)

                })()
                return true
            }

            var contact = sidePanel.getLowerContact(checkUsername)
            if (contact === undefined) return false

            hide()
            ;(function () {

                function contactRemoveListener () {
                    hide()
                    enableBackground()
                }

                function hide () {
                    element.removeChild(page.element)
                    contact.editProfileEvent.removeListener(page.editProfile)
                    contact.removeEvent.removeListener(contactRemoveListener)
                }

                var page = FoundContactPage(contact.username, contact.getProfile(), function () {
                    hide()
                    enableBackground()
                    contact.ensureSelected()
                }, function () {
                    hide()
                    showAddContactPage(showInvite)
                }, function () {
                    hide()
                    enableBackground()
                })

                element.appendChild(page.element)
                page.focus()
                contact.editProfileEvent.addListener(page.editProfile)
                contact.removeEvent.addListener(contactRemoveListener)

            })()

            return true

        }, function (username, profile) {
            hide()
            ;(function () {

                function hide () {
                    element.removeChild(page.element)
                    pullMessages.events.publicProfile.removeListener(publicProfileListener)
                    destroyEvent.removeListener(page.destroy)
                }

                function publicProfileListener (eventUsername, profile) {
                    if (username === eventUsername) page.editProfile(profile)
                }

                var page = FoundUserPage(session, username, profile, function (contactData) {
                    hide()
                    enableBackground()
                    sidePanel.mergeContact(username, contactData)
                    sidePanel.getContact(username).ensureSelected()
                }, function () {
                    hide()
                    showAddContactPage(showInvite)
                    unwatchPublicProfile(username)
                }, function () {
                    hide()
                    enableBackground()
                    unwatchPublicProfile(username)
                }, function () {
                    hide()
                    destroyAndInvalidSession()
                })

                element.appendChild(page.element)
                page.focus()
                pullMessages.events.publicProfile.addListener(publicProfileListener)
                destroyEvent.addListener(page.destroy)

            })()
        }, function () {
            hide()
            enableBackground()
        }, function () {
            hide()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        page.focus()
        destroyEvent.addListener(page.destroy)

    }

    function unwatchPublicProfile (username) {

        function destroy () {
            request.abort()
        }

        var url = 'data/unwatchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        var request = GetJson(url, function () {

            destroyEvent.removeListener(destroy)

            if (request.status !== 200) {
                destroyAndServiceError()
                return
            }

            var response = request.response
            if (response === null) {
                destroyAndCrash()
                return
            }

            if (response !== true) destroyAndServiceError()

        }, function () {
            destroyEvent.removeListener(destroy)
            destroyAndConnectionError()
        })

        destroyEvent.addListener(destroy)

    }

    function windowBeforeUnload (e) {
        if (sentFiles.empty() && numFeedingFiles === 0) return
        e.preventDefault()
        var message = 'Your file transfers will be aborted if you close.'
        e.returnValue = message
        return message
    }

    var numFeedingFiles = 0
    var userLink = renderUserLink(username)
    var lowerUsername = username.toLowerCase()
    var profile = session.profile

    var destroyEvent = WorkPage_Event(),
        editProfileEvent = WorkPage_Event()

    var sentFiles = WorkPage_SentFiles(),
        receivedFiles = WorkPage_ReceivedFiles()

    var chatPanel = null

    var classPrefix = 'WorkPage_Page'

    var sidePanel = WorkPage_SidePanel_Panel(formatBytes, formatText, isLocalLink, renderUserLink, sentFiles, receivedFiles, username, session, function () {
        disableBackground()
        showAccountPage(function (page) {
            page.focus()
        })
    }, function () {

        function hide () {
            element.removeChild(page.element)
        }

        var page = SignOutPage(function () {

            pullMessages.abort()
            hide()
            destroy()
            signOutListener()

            GetJson('data/signOut?token=' + encodeURIComponent(session.token))

        }, function () {
            hide()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()

    }, function (showInvite) {
        disableBackground()
        showAddContactPage(showInvite)
    }, function (contact) {
        chatPanel = contact.chatPanel
        element.appendChild(chatPanel.element)
        chatPanel.focus()
    }, function () {
        chatPanel.destroy()
        element.removeChild(chatPanel.element)
        chatPanel = null
    }, function (contact) {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.editProfileEvent.removeListener(page.editProfile)
            contact.overrideProfileEvent.removeListener(page.overrideProfile)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        var page = EditContactPage_Page(
            timezoneList, session, contact.userLink, contact.username,
            contact.getProfile(), contact.getOverrideProfile(),
            function (profile) {
                hide()
                enableBackground()
                contact.overrideProfile(profile)
            },
            function () {
                hide()
                enableBackground()
            },
            function () {
                hide()
                destroyAndInvalidSession()
            }
        )

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.editProfileEvent.addListener(page.editProfile)
        contact.overrideProfileEvent.addListener(page.overrideProfile)
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, function (contact, send) {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        var page = SmileysPage_Page(smileys, function (text) {
            hide()
            enableBackground()
            send(text)
        }, function () {
            hide()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, function (contact, contacts, send) {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        var page = ContactSelectPage_Page(contacts, function (contacts) {
            hide()
            enableBackground()
            send(contacts)
        }, function () {
            hide()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, function (contact) {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        var page = RemoveContactPage(contact.username, session, function () {
            hide()
            enableBackground()
            sidePanel.removeContact(contact)
        }, function () {
            hide()
            enableBackground()
        }, function () {
            hide()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, destroyAndInvalidSession, destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)

    var element = Div(classPrefix + ' Page')
    element.appendChild(sidePanel.element)

    var contactRequests = WorkPage_ContactRequests(
        element, username, session, disableBackground, enableBackground,
        function (username, contactData) {
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        },
        destroyAndInvalidSession, destroyAndConnectionError,
        destroyAndCrash, destroyAndServiceError
    )
    for (var i in session.requests) {
        contactRequests.add(i, session.requests[i])
    }

    var pullMessages = WorkPage_PullMessages(session,
        sidePanel.endProcessing, destroyAndInvalidSession,
        destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)
    pullMessages.events.addContact.addListener(function (username, profile) {
        var contact = sidePanel.getContact(username)
        if (contact === undefined) sidePanel.mergeContact(username, profile)
        contactRequests.remove(username)
    })
    pullMessages.events.addRequest.addListener(contactRequests.add)
    pullMessages.events.editContactProfile.addListener(function (username, profile) {
        var contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.editProfile(profile)
    })
    pullMessages.events.editProfile.addListener(editProfile)
    pullMessages.events.editRequest.addListener(contactRequests.edit)
    pullMessages.events.ignoreRequest.addListener(contactRequests.remove)
    pullMessages.events.offline.addListener(sidePanel.offline)
    pullMessages.events.online.addListener(sidePanel.online)
    pullMessages.events.overrideContactProfile.addListener(function (username, overrideProfile) {
        var contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.overrideProfile(overrideProfile)
    })
    pullMessages.events.receiveFileMessage.addListener(function (username, file, time, token) {
        session.files[file.token] = {}
        sidePanel.receiveFileMessage(username, file, time, token)
        contactRequests.receiveFileMessage(username, file, time, token)
    })
    pullMessages.events.receiveTextMessage.addListener(function (username, text, time, token) {
        sidePanel.receiveTextMessage(username, text, time, token)
        contactRequests.receiveTextMessage(username, text, time, token)
    })
    pullMessages.events.removeContact.addListener(function (username) {
        var contact = sidePanel.getContact(username)
        if (contact !== undefined) sidePanel.removeContact(contact)
    })
    pullMessages.events.removeFile.addListener(function (token) {
        receivedFiles.remove(token)
        delete session.files[token]
    })
    pullMessages.events.removeRequest.addListener(contactRequests.remove)
    pullMessages.events.requestFile.addListener(function (token) {
        numFeedingFiles++
        sentFiles.feed(token, function () {
            numFeedingFiles--
        })
    })
    pullMessages.events.sendFileMessage.addListener(sidePanel.sendFileMessage)
    pullMessages.events.sendTextMessage.addListener(sidePanel.sendTextMessage)

    var locationHash = WorkPage_LocationHash(session, function (checkUsername) {

        if (checkUsername === lowerUsername) {
            ;(function () {

                function hide () {
                    element.removeChild(page.element)
                    editProfileEvent.removeListener(page.editProfile)
                    locationHash.changeEvent.removeListener(hide)
                    enableBackground()
                }

                var page = SimpleSelfPage(username, profile, function () {
                    hide()
                    locationHash.reset()
                })

                element.appendChild(page.element)
                page.focus()
                disableBackground()
                editProfileEvent.addListener(page.editProfile)
                locationHash.changeEvent.addListener(hide)

            })()
            return true
        }

        var contact = sidePanel.getLowerContact(checkUsername)
        if (contact === undefined) return false

        ;(function () {

            function contactRemoveListener () {
                hideAndResetHash()
                enableBackground()
            }

            function hide () {
                element.removeChild(page.element)
                contact.editProfileEvent.removeListener(page.editProfile)
                contact.removeEvent.removeListener(contactRemoveListener)
                locationHash.changeEvent.removeListener(hide)
            }

            function hideAndResetHash () {
                hide()
                locationHash.reset()
            }

            var page = SimpleContactPage(contact.username, contact.getProfile(), function () {
                hideAndResetHash()
                enableBackground()
                contact.ensureSelected()
            }, function () {
                hideAndResetHash()
                enableBackground()
            })

            element.appendChild(page.element)
            page.focus()
            disableBackground()
            contact.editProfileEvent.addListener(page.editProfile)
            contact.removeEvent.addListener(contactRemoveListener)
            locationHash.changeEvent.addListener(hide)

        })()

        return true

    }, function (username, profile) {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.publicProfile.removeListener(publicProfileListener)
            destroyEvent.removeListener(page.destroy)
            locationHash.changeEvent.removeListener(hide)
        }

        function hideAndResetHash () {
            hide()
            locationHash.reset()
        }

        function publicProfileListener (eventUsername, profile) {
            if (username === eventUsername) page.editProfile(profile)
        }

        var page = SimpleUserPage(session, username, profile, function (contactData) {
            hideAndResetHash()
            enableBackground()
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        }, function () {
            hideAndResetHash()
            enableBackground()
            unwatchPublicProfile(username)
        }, function () {
            hideAndResetHash()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        pullMessages.events.publicProfile.addListener(publicProfileListener)
        destroyEvent.addListener(page.destroy)
        locationHash.changeEvent.addListener(hide)

    }, function (username) {

        var page = NoSuchUserPage(username, function () {
            element.removeChild(page.element)
            locationHash.reset()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()

    }, destroyAndConnectionError, destroyAndCrash, destroyAndInvalidSession, destroyAndServiceError)

    pullMessages.process(session.messages)
    locationHash.check()
    addEventListener('beforeunload', windowBeforeUnload)

    return { element: element }

}

function WorkPage_PullMessages (session, processedListener,
    invalidSessionListener, connectionErrorListener,
    crashListener, serviceErrorListener) {

    function process (messages) {
        if (messages.length === 0) return
        messages.forEach(function (message) {
            events[message[0]].emit(message[1])
        })
        processedListener()
    }

    function pull () {

        var request = GetJson(url, function () {

            if (request.status !== 200) {
                serviceErrorListener()
                return
            }

            var response = request.response
            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== 'NOTHING_TO_PULL') process(response)
            pull()

        }, connectionErrorListener)

        abortFunction = function () {
            request.abort()
        }

    }

    var abortFunction
    var url = 'data/pullMessages?token=' + encodeURIComponent(session.token)
    pull()

    var events = Object.create(null)
    events.addContact = WorkPage_Event()
    events.addRequest = WorkPage_Event()
    events.editContactProfile = WorkPage_Event()
    events.editContactProfileAndOffline = WorkPage_Event()
    events.editContactProfileAndOnline = WorkPage_Event()
    events.editProfile = WorkPage_Event()
    events.editRequest = WorkPage_Event()
    events.ignoreRequest = WorkPage_Event()
    events.offline = WorkPage_Event()
    events.online = WorkPage_Event()
    events.overrideContactProfile = WorkPage_Event()
    events.publicProfile = WorkPage_Event()
    events.receiveFileMessage = WorkPage_Event()
    events.receiveTextMessage = WorkPage_Event()
    events.removeContact = WorkPage_Event()
    events.removeFile = WorkPage_Event()
    events.removeRequest = WorkPage_Event()
    events.requestFile = WorkPage_Event()
    events.sendFileMessage = WorkPage_Event()
    events.sendTextMessage = WorkPage_Event()

    events.editContactProfileAndOffline.addListener(function (username, profile) {
        events.editContactProfile.emit([username, profile])
        events.offline.emit([username])
    })
    events.editContactProfileAndOnline.addListener(function (username, profile) {
        events.editContactProfile.emit([username, profile])
        events.online.emit([username])
    })

    return {
        events: events,
        process: process,
        abort: function () {
            abortFunction()
        },
    }

}

function WorkPage_ReceivedFiles () {

    var map = Object.create(null)

    return {
        add: function (token, removeCallback) {
            map[token] = { remove: removeCallback }
        },
        remove: function (token) {
            map[token].remove()
            delete map[token]
        },
    }

}

function WorkPage_SentFiles () {

    var map = Object.create(null)
    var size = 0

    return {
        add: function (token, feedCallback) {
            map[token] = { feed: feedCallback }
            size++
        },
        empty: function () {
            return size === 0
        },
        feed: function (token, endCallback) {
            map[token].feed(endCallback)
            delete map[token]
            size--
        },
    }

}

function WorkPage_SidePanel_AccountMenu (sounds,
    accountListener, soundsListener, signOutListener) {

    function flipSounds () {
        if (sounds) {
            sounds = false
            soundsNode.nodeValue = 'Off'
        } else {
            sounds = true
            soundsNode.nodeValue = 'On'
        }
        soundsListener(sounds)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    var classPrefix = 'WorkPage_SidePanel_AccountMenu'

    var accountItemElement = Div(classPrefix + '-item')
    accountItemElement.appendChild(TextNode('Account'))
    accountItemElement.addEventListener('click', accountListener)

    var soundsNode = TextNode(sounds ? 'On' : 'Off')

    var soundsItemElement = Div(classPrefix + '-item')
    soundsItemElement.appendChild(TextNode('Sounds: '))
    soundsItemElement.appendChild(soundsNode)
    soundsItemElement.addEventListener('click', flipSounds)

    var exitItemElement = Div(classPrefix + '-item')
    exitItemElement.appendChild(TextNode('Sign Out'))
    exitItemElement.addEventListener('click', signOutListener)

    var element = Div(classPrefix)
    element.appendChild(accountItemElement)
    element.appendChild(soundsItemElement)
    element.appendChild(exitItemElement)

    var selectedElement = null

    return {
        element: element,
        openSelected: function () {
            if (selectedElement === accountItemElement) accountListener()
            else if (selectedElement === soundsItemElement) flipSounds()
            else if (selectedElement === exitItemElement) signOutListener()
        },
        reset: function () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown: function () {
            if (selectedElement === accountItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = exitItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = accountItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp: function () {
            if (selectedElement === exitItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = accountItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = exitItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}

function WorkPage_SidePanel_Contact (playAudio, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    contactData, addNumberListener, selectListener,
    deselectListener, navigateUpListener, navigateDownListener,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, reorderListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addNumber (increment) {
        number += increment
        numberNode.nodeValue = number
        addNumberListener(increment)
    }

    function computeDisplayName () {
        return overrideProfile.fullName || profile.fullName || contactUsername
    }

    function computeSortName () {
        return displayName.toLowerCase()
    }

    function deselect () {
        selected = false
        classList.remove('selected')
        element.removeEventListener('click', deselectAndCallListener)
        element.addEventListener('click', select)
    }

    function deselectAndCallListener () {
        deselect()
        deselectListener()
    }

    function increaseNumber () {
        if (selected) {
            playAudio('message-seen')
            return
        }
        addNumber(1)
        if (number === 1) {
            classList.add('withMessages')
            numberClassList.add('visible')
        }
        playAudio('message-unseen')
    }

    function select () {

        selected = true
        classList.add('selected')
        element.removeEventListener('click', select)
        element.addEventListener('click', deselectAndCallListener)
        selectListener()

        if (number === 0) return
        addNumber(-number)
        numberClassList.remove('visible')
        classList.remove('withMessages')

    }

    function updateName () {
        displayName = computeDisplayName()
        sortName = computeSortName()
        nameNode.nodeValue = displayName
    }

    var classPrefix = 'WorkPage_SidePanel_Contact'

    var number = 0

    var selected = false

    var profile = contactData.profile,
        overrideProfile = contactData.overrideProfile,
        displayName = computeDisplayName(),
        sortName = computeSortName()

    var chatPanel = WorkPage_ChatPanel_Panel(playAudio,
        formatBytes, formatText, isLocalLink, sentFiles, receivedFiles,
        username, session, contactUsername, profile, overrideProfile,
        profileListener, sendSmileyListener, sendContactListener,
        removeListener, deselectAndCallListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener)

    var nameNode = TextNode(displayName)

    var numberNode = TextNode(number)

    var numberElement = document.createElement('span')
    numberElement.className = classPrefix + '-number'
    numberElement.appendChild(numberNode)

    var numberClassList = numberElement.classList

    var element = document.createElement('button')
    element.className = classPrefix
    element.appendChild(numberElement)
    element.appendChild(nameNode)
    element.addEventListener('click', select)
    element.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 38) {
            e.preventDefault()
            navigateUpListener()
        } else if (keyCode === 40) {
            e.preventDefault()
            navigateDownListener()
        }
    })

    var classList = element.classList
    if (!contactData.online) classList.add('offline')

    var editProfileEvent = WorkPage_Event(),
        overrideProfileEvent = WorkPage_Event(),
        removeEvent = WorkPage_Event()

    return {
        chatPanel: chatPanel,
        deselect: deselect,
        destroy: chatPanel.destroy,
        editProfileEvent: editProfileEvent,
        editTimezone: chatPanel.editTimezone,
        element: element,
        overrideProfileEvent: overrideProfileEvent,
        removeEvent: removeEvent,
        select: select,
        sendFileMessage: chatPanel.sendFileMessage,
        sendTextMessage: chatPanel.sendTextMessage,
        username: contactUsername,
        userLink: renderUserLink(contactUsername),
        disable: function () {
            element.disabled = true
        },
        editProfile: function (_profile) {
            var oldSortName = sortName
            profile = _profile
            updateName()
            chatPanel.editContactProfile(profile)
            if (sortName !== oldSortName) reorderListener()
            editProfileEvent.emit([profile])
        },
        enable: function () {
            element.disabled = false
        },
        focus: function () {
            element.focus()
        },
        getDisplayName: function () {
            return displayName
        },
        getSortName: function () {
            return sortName
        },
        getOverrideProfile: function () {
            return overrideProfile
        },
        getProfile: function () {
            return profile
        },
        ensureSelected: function () {
            if (!selected) select()
        },
        offline: function () {
            classList.add('offline')
        },
        online: function () {
            classList.remove('offline')
        },
        overrideProfile: function (_overrideProfile) {
            var oldSortName = sortName
            overrideProfile = _overrideProfile
            updateName()
            chatPanel.overrideContactProfile(overrideProfile)
            if (sortName !== oldSortName) reorderListener()
            overrideProfileEvent.emit([overrideProfile])
        },
        receiveFileMessage: function (username, file, time, token) {
            chatPanel.receiveFileMessage(username, file, time, token)
            increaseNumber()
        },
        receiveTextMessage: function (username, text, time, token) {
            chatPanel.receiveTextMessage(username, text, time, token)
            increaseNumber()
        },
    }

}

function WorkPage_SidePanel_ContactList (sounds, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, selectListener,
    deselectListener, profileListener, sendSmileyListener,
    sendContactListener, removeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addContact (contactUsername, contactData) {

        function place () {
            var sortName = contact.getSortName()
            for (var i = 0; i < contactsArray.length; i++) {
                if (sortName > contactsArray[i].getSortName()) continue
                contentElement.insertBefore(contact.element, contactsArray[i].element)
                contactsArray.splice(i, 0, contact)
                reorderEvent.emit([contact, i])
                return
            }
            contentElement.appendChild(contact.element)
            contactsArray.push(contact)
            reorderEvent.emit([contact, contactsArray.length - 1])
        }

        var contact = WorkPage_SidePanel_Contact(
            playAudio, formatBytes, formatText, isLocalLink,
            renderUserLink, sentFiles, receivedFiles, username, session,
            contactUsername, contactData, function (number) {
                totalNumber += number
                if (totalNumber === 0) document.title = 'Bazgu'
                else document.title = '(' + totalNumber + ') Bazgu'
            }, function () {
                if (selectedContact !== null) {
                    selectedContact.deselect()
                    deselectListener(selectedContact)
                }
                selectedContact = contact
                selectListener(contact)
            }, function () {
                deselectListener(selectedContact)
                selectedContact = null
                contact.focus()
            }, function () {
                var index = contactsArray.indexOf(contact)
                if (index !== 0) contactsArray[index - 1].focus()
            }, function () {
                var index = contactsArray.indexOf(contact)
                if (index === contactsArray.length - 1) return
                contactsArray[index + 1].focus()
            }, function () {
                profileListener(contact)
            }, function (send) {
                sendSmileyListener(contact, send)
            }, function (send) {
                sendContactListener(contact, contactsArray, send)
            }, function () {
                removeListener(contact)
            }, function () {
                contactsArray.splice(contactsArray.indexOf(contact), 1)
                contentElement.removeChild(contact.element)
                place()
            },
            invalidSessionListener, connectionErrorListener,
            crashListener, serviceErrorListener
        )

        contacts[contactUsername] = contact
        lowerContacts[contactUsername.toLowerCase()] = contact
        addEvent.emit([contact])
        place()

    }

    function playAudio (file) {
        audiosToPlay[file] = true
    }

    var audiosToPlay = Object.create(null)

    var contacts = Object.create(null)
    var lowerContacts = Object.create(null)
    var contactsArray = []

    var selectedContact = null

    var classPrefix = 'WorkPage_SidePanel_ContactList'

    var emptyElement = Div(classPrefix + '-empty')
    emptyElement.appendChild(TextNode('You have no contacts'))

    var invitePanel = InvitePanel(username, function () {})

    var addEvent = WorkPage_Event(),
        reorderEvent = WorkPage_Event()

    var contentElement = Div(classPrefix + '-content')
    ;(function () {
        var contacts = session.contacts
        for (var i in contacts) addContact(i, contacts[i])
    })()
    if (contactsArray.length === 0) {
        contentElement.appendChild(emptyElement)
        contentElement.appendChild(invitePanel.element)
    }

    var element = Div(classPrefix)
    element.appendChild(contentElement)

    var timezone = session.profile.timezone

    var totalNumber = 0
    document.title = 'Bazgu'

    return {
        addEvent: addEvent,
        element: element,
        reorderEvent: reorderEvent,
        destroy: function () {
            contactsArray.forEach(function (contact) {
                contact.destroy()
            })
        },
        disable: function () {
            contactsArray.forEach(function (contact) {
                contact.disable()
            })
        },
        editProfile: function (profile) {
            if (profile.timezone === timezone) return
            timezone = profile.timezone
            contactsArray.forEach(function (contact) {
                contact.editTimezone(timezone)
            })
        },
        enable: function () {
            contactsArray.forEach(function (contact) {
                contact.enable()
            })
        },
        endProcessing: function () {
            if (!sounds) return
            for (var i in audiosToPlay) {
                var audio = new Audio
                audio.src = 'audio/' + i + '.wav'
                audio.play()
                delete audiosToPlay[i]
            }
        },
        getContact: function (username) {
            return contacts[username]
        },
        getLowerContact: function (username) {
            return lowerContacts[username]
        },
        isEmpty: function () {
            return contactsArray.length === 0
        },
        offline: function (username) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.offline()
        },
        mergeContact: function (username, contactData) {

            var contact = contacts[username]
            if (contact === undefined) {
                if (contactsArray.length === 0) {
                    contentElement.removeChild(emptyElement)
                    contentElement.removeChild(invitePanel.element)
                }
                addContact(username, contactData)
                return
            }

            contact.editProfile(contactData.profile)
            contact.overrideProfile(contactData.overrideProfile)
            if (contactData.online) contact.online()
            else contact.offline()

        },
        online: function (username) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.online()
        },
        receiveFileMessage: function (username, file, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.receiveFileMessage(file, time, token)
        },
        receiveTextMessage: function (username, text, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.receiveTextMessage(text, time, token)
        },
        removeContact: function (contact) {
            if (contact === selectedContact) {
                selectedContact.deselect()
                deselectListener(selectedContact)
                selectedContact = null
            }
            contentElement.removeChild(contact.element)
            delete contacts[contact.username]
            delete lowerContacts[contact.username.toLowerCase()]
            contactsArray.splice(contactsArray.indexOf(contact), 1)
            if (contactsArray.length === 0) {
                contentElement.appendChild(emptyElement)
                contentElement.appendChild(invitePanel.element)
            }
            contact.removeEvent.emit()
        },
        sendFileMessage: function (username, file, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.sendFileMessage(file, time, token)
        },
        sendTextMessage: function (username, text, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.sendTextMessage(text, time, token)
        },
        setSounds: function (_sounds) {
            sounds = _sounds
        },
    }

}

function WorkPage_SidePanel_ContactStyle (getResourceUrl) {
    return '.WorkPage_SidePanel_Contact {' +
            'background-image: url(' + getResourceUrl('img/user-online.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.selected {' +
            'background-image: url(' + getResourceUrl('img/user-online-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.selected:active {' +
            'background-image: url(' + getResourceUrl('img/user-online-pressed-active.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline {' +
            'background-image: url(' + getResourceUrl('img/user-offline.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline.selected {' +
            'background-image: url(' + getResourceUrl('img/user-offline-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline.selected:active {' +
            'background-image: url(' + getResourceUrl('img/user-offline-pressed-active.svg') + ')' +
        '}'
}

function WorkPage_SidePanel_Panel (formatBytes, formatText,
    isLocalLink, renderUserLink, sentFiles, receivedFiles, username,
    session, accountListener, signOutListener, addContactListener,
    contactSelectListener, contactDeselectListener, contactProfileListener,
    sendSmileyListener, sendContactListener,
    contactRemoveListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_SidePanel_Panel'

    var addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.appendChild(TextNode('Add Contact'))
    addContactButton.addEventListener('click', function () {
        addContactListener(!contactList.isEmpty())
    })

    var sounds = true
    try {
        sounds = JSON.parse(localStorage[username + '?sounds'])
    } catch (e) {}

    var title = WorkPage_SidePanel_Title(sounds, username, session, accountListener, function (_sounds) {
        sounds = _sounds
        try {
            localStorage[username + '?sounds'] = JSON.stringify(sounds)
        } catch (e) {}
        contactList.setSounds(sounds)
    }, signOutListener)

    var contactList = WorkPage_SidePanel_ContactList(sounds,
        formatBytes, formatText, isLocalLink, renderUserLink,
        sentFiles, receivedFiles, username, session,
        function (contact) {
            contactSelectListener(contact)
            classList.add('chatOpen')
        },
        function (contact) {
            contactDeselectListener(contact)
            classList.remove('chatOpen')
        },
        contactProfileListener, sendSmileyListener, sendContactListener,
        contactRemoveListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    var element = Div(classPrefix)
    element.appendChild(title.element)
    element.appendChild(addContactButton)
    element.appendChild(contactList.element)

    var classList = element.classList

    return {
        addContactEvent: contactList.addEvent,
        element: element,
        endProcessing: contactList.endProcessing,
        getContact: contactList.getContact,
        getLowerContact: contactList.getLowerContact,
        mergeContact: contactList.mergeContact,
        offline: contactList.offline,
        online: contactList.online,
        receiveFileMessage: contactList.receiveFileMessage,
        receiveTextMessage: contactList.receiveTextMessage,
        removeContact: contactList.removeContact,
        reorderEvent: contactList.reorderEvent,
        sendFileMessage: contactList.sendFileMessage,
        sendTextMessage: contactList.sendTextMessage,
        destroy: function () {
            title.destroy()
            contactList.destroy()
        },
        disable: function () {
            addContactButton.disabled = true
            title.disable()
            contactList.disable()
        },
        editProfile: function (profile) {
            title.editProfile(profile)
            contactList.editProfile(profile)
        },
        enable: function () {
            addContactButton.disabled = false
            title.enable()
            contactList.enable()
        },
    }

}

function WorkPage_SidePanel_PanelStyle () {
    return '.WorkPage_SidePanel_Panel {' +
            'background-image: url(' + getResourceUrl('img/light-grass.svg') + ')' +
        '}'
}

function WorkPage_SidePanel_Title (sounds, username,
    session, accountListener, soundsListener, signOutListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.appendChild(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        var target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    var expanded = false

    var classPrefix = 'WorkPage_SidePanel_Title'

    var menu = WorkPage_SidePanel_AccountMenu(sounds, function () {
        collapse()
        accountListener()
    }, soundsListener, function () {
        collapse()
        signOutListener()
    })

    var node = TextNode(session.profile.fullName || username)

    var buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.appendChild(node)

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.appendChild(buttonTextElement)
    button.addEventListener('click', expand)

    var buttonClassList = button.classList

    var element = Div(classPrefix)
    element.appendChild(button)

    return {
        element: element,
        destroy: function () {
            if (expanded) collapse()
        },
        disable: function () {
            if (expanded) collapse()
            button.disabled = true
        },
        editProfile: function (profile) {
            node.nodeValue = profile.fullName || username
        },
        enable: function () {
            button.disabled = false
        },
    }

}

function WorkPage_SidePanel_TitleStyle (getResourceUrl) {
    return '.WorkPage_SidePanel_Title-button {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Title-button.pressed {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Title-button.pressed:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed-active.svg') + ')' +
        '}'
}

(function (getResourceUrl) {

    function checkSession () {

        function hide () {
            body.removeChild(page.element)
        }

        var page = LoadingPage_Page()
        body.appendChild(page.element)

        CheckSession(function (username, warningCallback) {
            hide()
            showWelcomePage(username, function (page) {
                page.focus()
            }, warningCallback)
        }, function (username, session) {
            hide()
            showWorkPage(username, session)
        }, function () {
            hide()
            showConnectionErrorPage()
        }, function () {
            hide()
            showCrashPage()
        }, function () {
            hide()
            showServiceErrorPage()
        })

    }

    function showConnectionErrorPage () {
        var page = ConnectionErrorPage(function () {
            body.removeChild(page.element)
            checkSession()
        })
        body.appendChild(page.element)
        page.focus()
    }

    function showCrashPage () {
        var page = CrashPage(function () {
            location.reload(true)
        })
        body.appendChild(page.element)
        page.focus()
    }

    function showServiceErrorPage () {
        var page = ServiceErrorPage(function () {
            location.reload(true)
        })
        body.appendChild(page.element)
        page.focus()
    }

    function showWelcomePage (username, readyCallback, warningCallback) {

        function hide () {
            body.removeChild(page.element)
            page.destroy()
            removeEventListener('storage', windowStorage)
        }

        function windowStorage (e) {
            if (e.storageArea !== localStorage || e.key !== 'token') return
            var token = e.newValue
            if (token === null) return
            hide()
            checkSession()
        }

        var page = WelcomePage_Page(username, function () {
            hide()
            ;(function () {

                function hide () {
                    body.removeChild(page.element)
                }

                var page = SignUpPage_Page(timezoneList, function () {
                    hide()
                    showWelcomePage(username, function (page) {
                        page.focusSignUp()
                    })
                }, function (username, session) {

                    hide()
                    showWorkPage(username, session)

                    ;(function () {
                        var page = AccountReadyPage(function () {
                            body.removeChild(page.element)
                        })
                        body.appendChild(page.element)
                        page.focus()
                    })()

                })
                body.appendChild(page.element)
                page.focus()

            })()
        }, function () {
            hide()
            ;(function () {

                function hide () {
                    body.removeChild(page.element)
                }

                var page = PrivacyPage(function () {
                    hide()
                    showWelcomePage(username, function (page) {
                        page.focusPrivacy()
                    })
                })
                body.appendChild(page.element)
                page.focus()

            })()
        }, function (username, session) {
            hide()
            showWorkPage(username, session)
        }, warningCallback)

        body.appendChild(page.element)
        readyCallback(page)
        addEventListener('storage', windowStorage)

    }

    function showWorkPage (username, session) {

        function hide () {
            document.title = initialTitle
            body.removeChild(page.element)
        }

        try {
            localStorage.username = username
            localStorage.token = session.token
        } catch (e) {
        }

        var page = WorkPage_Page(
            smileys, formatBytes, formatText, isLocalLink,
            renderUserLink, timezoneList, username, session,
            function () {

                try {
                    delete localStorage.token
                } catch (e) {
                }

                hide()
                showWelcomePage(username, function (page) {
                    page.focus()
                }, function (element) {
                    element.appendChild(TextNode('Signed out successfully.'))
                })

            },
            function () {
                hide()
                checkSession()
            },
            function () {
                hide()
                showConnectionErrorPage()
            },
            function () {
                hide()
                showCrashPage()
            },
            function () {
                hide()
                showServiceErrorPage()
            }
        )
        body.appendChild(page.element)

    }

    var body = document.body
    var initialTitle = document.title

    var formatBytes = FormatBytes(),
        smileys = Smileys(),
        formatText = FormatText(smileys),
        isLocalLink = IsLocalLink(),
        renderUserLink = RenderUserLink(),
        timezoneList = Timezone_List()

    ;(function () {
        var style = document.createElement('style')
        style.type = 'text/css'
        style.innerHTML = AccountPage_PrivacySelectStyle(getResourceUrl) +
            AccountPage_PrivacySelectItemStyle(getResourceUrl) +
            AccountPage_TimezoneItemStyle(getResourceUrl) +
            CloseButtonStyle(getResourceUrl) +
            ContactSelectPage_ItemStyle(getResourceUrl) +
            EditContactPage_TimezoneItemStyle(getResourceUrl) +
            PageStyle(getResourceUrl) +
            SignUpPage_AdvancedItemStyle(getResourceUrl) +
            SignUpPage_TimezoneItemStyle(getResourceUrl) +
            SmileysPage_PageStyle(smileys, getResourceUrl) +
            WelcomePage_PageStyle(getResourceUrl) +
            WelcomePage_StaySignedInItemStyle(getResourceUrl) +
            WorkPage_ChatPanel_CloseButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_MoreButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_PanelStyle(getResourceUrl) +
            WorkPage_ChatPanel_SmileyStyle(smileys, getResourceUrl) +
            WorkPage_ChatPanel_TitleStyle(getResourceUrl) +
            WorkPage_SidePanel_ContactStyle(getResourceUrl) +
            WorkPage_SidePanel_PanelStyle(getResourceUrl) +
            WorkPage_SidePanel_TitleStyle(getResourceUrl)
        document.head.appendChild(style)
    })()

    checkSession()

})(getResourceUrl)

})()
