function FoundSelfPage (username, profile, backListener, closeListener) {

    var backButton = BackButton(backListener)

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var classPrefix = 'FoundSelfPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" it\'s you.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
        username: username,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}
