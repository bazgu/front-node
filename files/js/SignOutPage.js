function SignOutPage (confirmListener, closeListener) {

    var classPrefix = 'SignOutPage'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Are you sure'))
    textElement.appendChild(document.createElement('br'))
    textElement.appendChild(TextNode('you want to sign out?'))

    var yesButton = document.createElement('button')
    yesButton.className = classPrefix + '-yesButton GreenButton'
    yesButton.appendChild(TextNode('Sign Out'))
    yesButton.addEventListener('click', confirmListener)
    yesButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var noButton = document.createElement('button')
    noButton.className = classPrefix + '-noButton OrangeButton'
    noButton.appendChild(TextNode('Cancel'))
    noButton.addEventListener('click', closeListener)
    noButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(yesButton)
    frameElement.appendChild(noButton)

    var element = Div(classPrefix)
    element.appendChild(frameElement)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: function () {
            yesButton.focus()
        },
    }

}
