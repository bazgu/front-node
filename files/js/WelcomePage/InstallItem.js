function WelcomePage_InstallItem (mozApps, manifest, installListener) {

    var button = document.createElement('button')
    button.className = 'WelcomePage_InstallItem OrangeButton'
    button.appendChild(TextNode('Install as a Firefox App'))
    button.addEventListener('click', function () {
        var request = mozApps.install(manifest)
        request.onsuccess = installListener
    })

    return { element: button }

}
