function WelcomePage_UnauthenticatedWarning (username) {
    return function (element) {

        var usernameElement = document.createElement('b')
        usernameElement.className = 'WelcomePage_UnauthenticatedWarning-username'
        usernameElement.appendChild(TextNode(username))

        element.appendChild(TextNode('You need to sign in or create an account to contact "'))
        element.appendChild(usernameElement)
        element.appendChild(TextNode('".'))

    }
}
