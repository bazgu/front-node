function WelcomePage_Page (username, signUpListener,
    privacyListener, signInListener, warningCallback) {

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        staySignedInItem.enable()
        signInButton.disabled = false
        signInNode.nodeValue = 'Sign In'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        signInForm.insertBefore(error.element, signInButton)
        signInButton.focus()
    }

    var error = null
    var warningElement = null
    var request = null

    var classPrefix = 'WelcomePage_Page'

    var usernameItem = WelcomePage_UsernameItem(username)

    var passwordItem = WelcomePage_PasswordItem()

    var signInNode = TextNode('Sign In')

    var signInButton = document.createElement('button')
    signInButton.className = classPrefix + '-signInButton GreenButton'
    signInButton.appendChild(signInNode)

    var staySignedInItem = WelcomePage_StaySignedInItem()

    var signInForm = document.createElement('form')
    signInForm.className = classPrefix + '-signInForm'
    signInForm.appendChild(usernameItem.element)
    signInForm.appendChild(passwordItem.element)
    signInForm.appendChild(staySignedInItem.element)
    signInForm.appendChild(signInButton)
    signInForm.addEventListener('submit', function (e) {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()

        if (warningElement !== null) {
            frameElement.removeChild(warningElement)
            warningElement = null
        }

        if (error !== null) {
            signInForm.removeChild(error.element)
            error = null
        }

        var username = usernameItem.getValue()
        if (username === null) return

        var password = passwordItem.getValue()
        if (password === null) return

        usernameItem.disable()
        passwordItem.disable()
        staySignedInItem.disable()
        signInButton.disabled = true
        signInNode.nodeValue = 'Signing in...'

        var url = 'data/signIn' +
            '?username=' + encodeURIComponent(username.toLowerCase()) +
            '&password=' + encodeURIComponent(password)
        if (staySignedInItem.isChecked()) url += '&longTerm=true'

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_USERNAME') {
                enableItems()
                usernameItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('There is no such user.'))
                })
                return
            }

            if (response === 'INVALID_PASSWORD') {
                enableItems()
                passwordItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is incorrect.'))
                })
                return
            }

            signInListener(response.username, response.session)

        }, requestError)

    })

    var logoElement = Div(classPrefix + '-logo')

    var logoWrapperElement = Div(classPrefix + '-logoWrapper')
    logoWrapperElement.appendChild(logoElement)

    var signUpButton = document.createElement('button')
    signUpButton.className = classPrefix + '-signUpButton OrangeButton'
    signUpButton.appendChild(TextNode('Create an Account \u203a'))
    signUpButton.addEventListener('click', function () {
        destroy()
        signUpListener()
    })

    var privacy = WelcomePage_Privacy(privacyListener)

    var publicLine = WelcomePage_PublicLine()

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(logoWrapperElement)
    frameElement.appendChild(publicLine.element)
    if (warningCallback !== undefined) {

        warningElement = Div(classPrefix + '-warning')

        warningCallback(warningElement)
        frameElement.appendChild(warningElement)

    }
    frameElement.appendChild(signInForm)
    frameElement.appendChild(TextNode('New to Bazgu?'))
    frameElement.appendChild(document.createElement('br'))
    frameElement.appendChild(signUpButton)
    frameElement.appendChild(privacy.element)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    WelcomePage_CheckInstalled(frameElement, logoWrapperElement)

    return {
        destroy: publicLine.destroy,
        element: element,
        focusPrivacy: privacy.focus,
        focus: function () {
            if (username === '') usernameItem.focus()
            else passwordItem.focus()
        },
        focusSignUp: function () {
            signUpButton.focus()
        },
    }

}
