function WelcomePage_PublicLine () {

    function add (text) {

        var itemElement = Div(classPrefix + '-item')
        itemElement.appendChild(TextNode(text))

        element.appendChild(itemElement)
        itemElements.push(itemElement)

    }

    function animate () {
        timeout = setTimeout(function () {
            itemElements[selectedIndex].classList.remove('visible')
            timeout = setTimeout(function () {
                selectedIndex = (selectedIndex + 1) % itemElements.length
                itemElements[selectedIndex].classList.add('visible')
                animate()
            }, 600)
        }, 8000)
    }

    var selectedIndex = 0
    var itemElements = []

    var classPrefix = 'WelcomePage_PublicLine'

    var element = Div(classPrefix)
    add('Chat without distractions.')
    add('Send unlimited size files.')
    itemElements[0].classList.add('visible')

    var timeout
    animate()

    return {
        element: element,
        destroy: function () {
            clearTimeout(timeout)
        },
    }

}
