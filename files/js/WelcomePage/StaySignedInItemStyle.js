function WelcomePage_StaySignedInItemStyle (getResourceUrl) {
    return '.WelcomePage_StaySignedInItem-button.checked {' +
            'background-image: url(' + getResourceUrl('img/checked/normal.svg') + ')' +
        '}' +
        '.WelcomePage_StaySignedInItem-button.checked:active {' +
            'background-image: url(' + getResourceUrl('img/checked/active.svg') + ')' +
        '}'
}
