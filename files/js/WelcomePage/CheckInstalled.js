function WelcomePage_CheckInstalled (element, logoWrapperElement) {

    var mozApps = navigator.mozApps
    if (mozApps === undefined) return

    var manifest = location.protocol + '//' + location.host +
        location.pathname.replace(/[^/]+$/, '') + 'webappManifest'

    var request = mozApps.checkInstalled(manifest)
    request.onsuccess = function () {
        if (request.result !== null) return
        var installItem = WelcomePage_InstallItem(mozApps, manifest, function () {
            element.removeChild(installItem.element)
        })
        element.insertBefore(installItem.element, logoWrapperElement)
    }

}
