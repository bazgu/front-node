function WelcomePage_StaySignedInItem () {

    function addListener (listener) {
        clickElement.addEventListener('click', listener)
    }

    function check () {
        checked = true
        buttonClassList.add('checked')
        removeListener(check)
        addListener(uncheck)
        activeListener = uncheck
        button.focus()
    }

    function removeListener (listener) {
        clickElement.removeEventListener('click', listener)
    }

    function uncheck () {
        checked = false
        buttonClassList.remove('checked')
        removeListener(uncheck)
        addListener(check)
        activeListener = check
        button.focus()
    }

    var classPrefix = 'WelcomePage_StaySignedInItem'

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.type = 'button'

    var buttonClassList = button.classList

    var clickElement = Div(classPrefix + '-click')
    clickElement.appendChild(button)
    clickElement.appendChild(TextNode('Stay signed in'))

    var clickClassList = clickElement.classList

    var element = Div(classPrefix)
    element.appendChild(clickElement)

    var checked = true

    var activeListener = check
    addListener(check)

    return {
        element: element,
        disable: function () {
            clickClassList.add('disabled')
            button.disabled = true
            removeListener(activeListener)
        },
        enable: function () {
            clickClassList.remove('disabled')
            button.disabled = false
            addListener(activeListener)
        },
        isChecked: function () {
            return checked
        },
    }

}
