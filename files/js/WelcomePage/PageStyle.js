function WelcomePage_PageStyle (getResourceUrl) {
    return '.WelcomePage_Page-logo {' +
            'background-image: url(' + getResourceUrl('img/logo.svg') + ')' +
        '}'
}
