function WelcomePage_Privacy (listener) {

    var classPrefix = 'WelcomePage_Privacy'

    var button = document.createElement('button')
    button.className = classPrefix + '-privacy'
    button.appendChild(TextNode('Privacy'))
    button.addEventListener('click', listener)

    var element = Div('WelcomePage_Privacy')
    element.appendChild(button)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}
