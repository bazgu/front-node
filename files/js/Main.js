(function (getResourceUrl) {

    function checkSession () {

        function hide () {
            body.removeChild(page.element)
        }

        var page = LoadingPage_Page()
        body.appendChild(page.element)

        CheckSession(function (username, warningCallback) {
            hide()
            showWelcomePage(username, function (page) {
                page.focus()
            }, warningCallback)
        }, function (username, session) {
            hide()
            showWorkPage(username, session)
        }, function () {
            hide()
            showConnectionErrorPage()
        }, function () {
            hide()
            showCrashPage()
        }, function () {
            hide()
            showServiceErrorPage()
        })

    }

    function showConnectionErrorPage () {
        var page = ConnectionErrorPage(function () {
            body.removeChild(page.element)
            checkSession()
        })
        body.appendChild(page.element)
        page.focus()
    }

    function showCrashPage () {
        var page = CrashPage(function () {
            location.reload(true)
        })
        body.appendChild(page.element)
        page.focus()
    }

    function showServiceErrorPage () {
        var page = ServiceErrorPage(function () {
            location.reload(true)
        })
        body.appendChild(page.element)
        page.focus()
    }

    function showWelcomePage (username, readyCallback, warningCallback) {

        function hide () {
            body.removeChild(page.element)
            page.destroy()
            removeEventListener('storage', windowStorage)
        }

        function windowStorage (e) {
            if (e.storageArea !== localStorage || e.key !== 'token') return
            var token = e.newValue
            if (token === null) return
            hide()
            checkSession()
        }

        var page = WelcomePage_Page(username, function () {
            hide()
            ;(function () {

                function hide () {
                    body.removeChild(page.element)
                }

                var page = SignUpPage_Page(timezoneList, function () {
                    hide()
                    showWelcomePage(username, function (page) {
                        page.focusSignUp()
                    })
                }, function (username, session) {

                    hide()
                    showWorkPage(username, session)

                    ;(function () {
                        var page = AccountReadyPage(function () {
                            body.removeChild(page.element)
                        })
                        body.appendChild(page.element)
                        page.focus()
                    })()

                })
                body.appendChild(page.element)
                page.focus()

            })()
        }, function () {
            hide()
            ;(function () {

                function hide () {
                    body.removeChild(page.element)
                }

                var page = PrivacyPage(function () {
                    hide()
                    showWelcomePage(username, function (page) {
                        page.focusPrivacy()
                    })
                })
                body.appendChild(page.element)
                page.focus()

            })()
        }, function (username, session) {
            hide()
            showWorkPage(username, session)
        }, warningCallback)

        body.appendChild(page.element)
        readyCallback(page)
        addEventListener('storage', windowStorage)

    }

    function showWorkPage (username, session) {

        function hide () {
            document.title = initialTitle
            body.removeChild(page.element)
        }

        try {
            localStorage.username = username
            localStorage.token = session.token
        } catch (e) {
        }

        var page = WorkPage_Page(
            smileys, formatBytes, formatText, isLocalLink,
            renderUserLink, timezoneList, username, session,
            function () {

                try {
                    delete localStorage.token
                } catch (e) {
                }

                hide()
                showWelcomePage(username, function (page) {
                    page.focus()
                }, function (element) {
                    element.appendChild(TextNode('Signed out successfully.'))
                })

            },
            function () {
                hide()
                checkSession()
            },
            function () {
                hide()
                showConnectionErrorPage()
            },
            function () {
                hide()
                showCrashPage()
            },
            function () {
                hide()
                showServiceErrorPage()
            }
        )
        body.appendChild(page.element)

    }

    var body = document.body
    var initialTitle = document.title

    var formatBytes = FormatBytes(),
        smileys = Smileys(),
        formatText = FormatText(smileys),
        isLocalLink = IsLocalLink(),
        renderUserLink = RenderUserLink(),
        timezoneList = Timezone_List()

    ;(function () {
        var style = document.createElement('style')
        style.type = 'text/css'
        style.innerHTML = AccountPage_PrivacySelectStyle(getResourceUrl) +
            AccountPage_PrivacySelectItemStyle(getResourceUrl) +
            AccountPage_TimezoneItemStyle(getResourceUrl) +
            CloseButtonStyle(getResourceUrl) +
            ContactSelectPage_ItemStyle(getResourceUrl) +
            EditContactPage_TimezoneItemStyle(getResourceUrl) +
            PageStyle(getResourceUrl) +
            SignUpPage_AdvancedItemStyle(getResourceUrl) +
            SignUpPage_TimezoneItemStyle(getResourceUrl) +
            SmileysPage_PageStyle(smileys, getResourceUrl) +
            WelcomePage_PageStyle(getResourceUrl) +
            WelcomePage_StaySignedInItemStyle(getResourceUrl) +
            WorkPage_ChatPanel_CloseButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_MoreButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_PanelStyle(getResourceUrl) +
            WorkPage_ChatPanel_SmileyStyle(smileys, getResourceUrl) +
            WorkPage_ChatPanel_TitleStyle(getResourceUrl) +
            WorkPage_SidePanel_ContactStyle(getResourceUrl) +
            WorkPage_SidePanel_PanelStyle(getResourceUrl) +
            WorkPage_SidePanel_TitleStyle(getResourceUrl)
        document.head.appendChild(style)
    })()

    checkSession()

})(getResourceUrl)
