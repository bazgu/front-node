function CrashError () {

    var element = Div('FormError')
    element.appendChild(TextNode('Something went wrong.'))
    element.appendChild(document.createElement('br'))
    element.appendChild(TextNode('Please, try again.'))

    return { element: element }

}
