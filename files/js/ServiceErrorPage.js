function ServiceErrorPage (reloadListener) {

    var classPrefix = 'ServiceErrorPage'

    var text = 'There is a problem at Bazgu. We are fixing it.' +
        ' Reload the page to see if it\'s resolved.'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode(text))

    var buttonNode = TextNode('Reload')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('click', function () {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}
