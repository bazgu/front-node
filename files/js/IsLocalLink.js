function IsLocalLink () {

    var localPrefix = location.protocol + '//' +
        location.host + location.pathname + '#'

    return function (link) {
        if (link.substr(0, localPrefix.length) !== localPrefix) return false
        var username = link.substr(localPrefix.length)
        return Username_IsValid(username)
    }

}
