function GetJson (url, loadCallback, errorCallback) {
    var request = new XMLHttpRequest
    request.open('get', url)
    request.responseType = 'json'
    request.onerror = errorCallback
    request.onload = loadCallback
    request.send()
    return request
}
