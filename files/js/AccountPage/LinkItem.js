function AccountPage_LinkItem (userLink, closeListener) {

    var classPrefix = 'AccountPage_LinkItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Link'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.readOnly = true
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = userLink
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return { element: element }

}
