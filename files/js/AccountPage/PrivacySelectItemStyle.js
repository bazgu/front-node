function AccountPage_PrivacySelectItemStyle (getResourceUrl) {
    return '.AccountPage_PrivacySelectItem.privacy_private {' +
            'background-image: url(' + getResourceUrl('img/privacy/private.svg') + ')' +
        '}' +
        '.AccountPage_PrivacySelectItem.privacy_contacts {' +
            'background-image: url(' + getResourceUrl('img/privacy/contacts.svg') + ')' +
        '}' +
        '.AccountPage_PrivacySelectItem.privacy_public {' +
            'background-image: url(' + getResourceUrl('img/privacy/public.svg') + ')' +
        '}'
}
