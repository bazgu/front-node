function AccountPage_TimezoneItem (timezoneList,
    profile, changeListener, closeListener) {

    var classPrefix = 'AccountPage_TimezoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Timezone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.addEventListener('change', changeListener)
    timezoneList.forEach(function (timezone) {
        var option = document.createElement('option')
        option.value = timezone.value
        option.appendChild(TextNode(timezone.text))
        select.appendChild(option)
    })
    select.value = profile.timezone
    select.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var privacySelect = AccountPage_PrivacySelect(
        profile.timezonePrivacy, changeListener, closeListener)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(select)
    element.appendChild(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable: function () {
            privacySelect.disable()
            select.disabled = true
            select.blur()
        },
        enable: function () {
            privacySelect.enable()
            select.disabled = false
        },
        getValue: function () {
            return parseInt(select.value, 10)
        },
    }

}
