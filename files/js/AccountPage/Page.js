function AccountPage_Page (timezoneList, userLink, username, session,
    profile, editProfileListener, changePasswordListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === profile.fullName &&
            fullNameItem.getPrivacyValue() === profile.fullNamePrivacy &&
            emailItem.getValue() === profile.email &&
            emailItem.getPrivacyValue() === profile.emailPrivacy &&
            phoneItem.getValue() === profile.phone &&
            phoneItem.getPrivacyValue() === profile.phonePrivacy &&
            timezoneItem.getValue() === profile.timezone &&
            timezoneItem.getPrivacyValue() === profile.timezonePrivacy
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'AccountPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var fullNameItem = AccountPage_FullNameItem(profile, checkChanges, close)

    var emailItem = AccountPage_EmailItem(profile, checkChanges, close)

    var phoneItem = AccountPage_PhoneItem(profile, checkChanges, close)

    var timezoneItem = AccountPage_TimezoneItem(timezoneList, profile, checkChanges, close)

    var linkItem = AccountPage_LinkItem(userLink, close)

    var saveChangesNode = TextNode('Save Changes')

    var saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.appendChild(saveChangesNode)
    saveChangesButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(fullNameItem.element)
    form.appendChild(emailItem.element)
    form.appendChild(phoneItem.element)
    form.appendChild(timezoneItem.element)
    form.appendChild(linkItem.element)
    form.appendChild(saveChangesButton)
    form.addEventListener('submit', function (e) {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var fullName = fullNameItem.getValue(),
            fullNamePrivacy = fullNameItem.getPrivacyValue(),
            email = emailItem.getValue(),
            emailPrivacy = emailItem.getPrivacyValue(),
            phone = phoneItem.getValue(),
            phonePrivacy = phoneItem.getPrivacyValue(),
            timezone = timezoneItem.getValue(),
            timezonePrivacy = timezoneItem.getPrivacyValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        var url = 'data/editProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&fullNamePrivacy=' + encodeURIComponent(fullNamePrivacy) +
            '&email=' + encodeURIComponent(email) +
            '&emailPrivacy=' + encodeURIComponent(emailPrivacy) +
            '&phone=' + encodeURIComponent(phone) +
            '&phonePrivacy=' + encodeURIComponent(phonePrivacy) +
            '&timezone=' + encodeURIComponent(timezone) +
            '&timezonePrivacy=' + encodeURIComponent(timezonePrivacy)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            editProfileListener({
                fullName: fullName,
                fullNamePrivacy: fullNamePrivacy,
                email: email,
                emailPrivacy: emailPrivacy,
                phone: phone,
                phonePrivacy: phonePrivacy,
                timezone: timezone,
                timezonePrivacy: timezonePrivacy,
            })

        }, requestError)

    })

    var changePasswordButton = document.createElement('button')
    changePasswordButton.className = classPrefix + '-changePasswordButton OrangeButton'
    changePasswordButton.appendChild(TextNode('Change Password \u203a'))
    changePasswordButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    changePasswordButton.addEventListener('click', function () {
        destroy()
        changePasswordListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)
    frameElement.appendChild(changePasswordButton)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile: function (newProfile) {
            profile = newProfile
            checkChanges()
        },
        focusChangePassword: function () {
            changePasswordButton.focus()
        },
    }

}
