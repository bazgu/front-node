function AccountPage_FullNameItem (profile, changeListener, closeListener) {

    var classPrefix = 'AccountPage_FullNameItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Full name'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.fullName
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var privacySelect = AccountPage_PrivacySelect(
        profile.fullNamePrivacy, changeListener, closeListener)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)
    element.appendChild(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable: function () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable: function () {
            privacySelect.enable()
            input.disabled = false
        },
        focus: function () {
            input.focus()
        },
        getValue: function () {
            return CollapseSpaces(input.value)
        },
    }

}
