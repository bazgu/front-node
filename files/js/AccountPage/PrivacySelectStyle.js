function AccountPage_PrivacySelectStyle (getResourceUrl) {

    function getUrl (url) {
        return 'url(' + getResourceUrl(url) + ')'
    }

    var arrow = getUrl('img/arrow.svg'),
        arrowPressed = getUrl('img/arrow-pressed.svg'),
        arrowPressedActive = getUrl('img/arrow-pressed-active.svg')

    return '.AccountPage_PrivacySelect-button.privacy_private {' +
            'background-image: ' +
                getUrl('img/privacy/private.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.privacy_contacts {' +
            'background-image: ' +
                getUrl('img/privacy/contacts.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.privacy_public {' +
            'background-image: ' +
                getUrl('img/privacy/public.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_private {' +
            'background-image: ' +
                getUrl('img/privacy/private-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_contacts {' +
            'background-image: ' +
                getUrl('img/privacy/contacts-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_public {' +
            'background-image: ' +
                getUrl('img/privacy/public-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_private:active {' +
            'background-image: ' +
                getUrl('img/privacy/private-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_contacts:active {' +
            'background-image: ' +
                getUrl('img/privacy/contacts-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_public:active {' +
            'background-image: ' +
                getUrl('img/privacy/public-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}'
}
