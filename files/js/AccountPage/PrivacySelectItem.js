function AccountPag_PrivacySelectItem (text, value, clickListener) {

    var element = Div('AccountPage_PrivacySelectItem privacy_' + value)
    element.appendChild(TextNode(text))
    element.addEventListener('click', clickListener)

    var classList = element.classList

    return {
        element: element,
        deselect: function () {
            classList.remove('active')
        },
        select: function () {
            classList.add('active')
        },
    }

}
