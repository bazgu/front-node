function SignUpPage_CaptchaItem (loadListener, backListener) {

    function disable () {
        input.disabled = true
        input.blur()
    }

    function enable () {
        input.disabled = false
    }

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function requestError () {
        request = null
        imageElement.removeChild(loadingNode)
        showCaptchaError()
    }

    function load () {

        disable()

        request = GetJson('data/captcha', function () {

            var status = request.status,
                response = request.response

            request = null
            imageElement.removeChild(loadingNode)

            if (status !== 200) {
                showCaptchaError()
                return
            }

            if (response === null) {
                showCaptchaError()
                return
            }

            enable()
            setCaptcha(response)
            loadListener()

        }, requestError)

    }

    function setCaptcha (captcha) {
        imageElement.style.backgroundImage = 'url(' + captcha.image + ')'
        token = captcha.token
    }

    function showCaptchaError () {

        var reloadButton = document.createElement('button')
        reloadButton.type = 'button'
        reloadButton.className = classPrefix + '-reloadButton GreenButton'
        reloadButton.appendChild(TextNode('Reload'))
        reloadButton.addEventListener('click', function () {
            imageBarElement.removeChild(reloadButton)
            imageElement.removeChild(errorNode)
            imageElement.appendChild(loadingNode)
            imageClassList.remove('error')
            load()
        })

        var errorNode = TextNode('Failed')

        imageBarElement.appendChild(reloadButton)
        imageElement.appendChild(errorNode)
        imageClassList.add('error')

    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null
    var request = null

    var token = null

    var classPrefix = 'SignUpPage_CaptchaItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Verification'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var loadingNode = TextNode('Loading...')

    var imageElement = Div(classPrefix + '-image')
    imageElement.appendChild(loadingNode)

    var imageClassList = imageElement.classList

    var imageBarElement = Div(classPrefix + '-imageBar')
    imageBarElement.appendChild(imageElement)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(imageBarElement)
    element.appendChild(input)

    load()

    return {
        disable: disable,
        element: element,
        enable: enable,
        showError: showError,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        destroy: function () {
            if (request === null) return
            request.abort()
            request = null
        },
        getValue: function () {

            var value = input.value.replace(/\s+/g, '')
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }

            return {
                value: value,
                token: token,
            }

        },
        setNewCaptcha: function (newCaptcha) {
            setCaptcha(newCaptcha)
            input.value = ''
        },
    }

}
