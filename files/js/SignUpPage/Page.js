function SignUpPage_Page (timezoneList, backListener, signUpListener) {

    function back () {
        destroy()
        backListener()
    }

    function destroy () {
        captchaItem.destroy()
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        repeatPasswordItem.enable()
        captchaItem.enable()
        advancedPanel.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Sign Up'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        form.insertBefore(error.element, button)
        button.focus()
    }

    var error = null
    var request = null

    var classPrefix = 'SignUpPage_Page'

    var backButton = BackButton(back)

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Create an Account'))

    var usernameItem = SignUpPage_UsernameItem(back)

    var passwordItem = SignUpPage_PasswordItem(back)

    var repeatPasswordItem = SignUpPage_RepeatPasswordItem(back)

    var captchaItem = SignUpPage_CaptchaItem(function () {
        button.disabled = false
    }, back)

    var advancedPanel = SignUpPage_AdvancedPanel(timezoneList, back)

    var advancedItem = SignUpPage_AdvancedItem(advancedPanel.show, advancedPanel.hide, back)

    var buttonNode = TextNode('Sign Up')

    var button = document.createElement('button')
    button.disabled = true
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(usernameItem.element)
    form.appendChild(passwordItem.element)
    form.appendChild(repeatPasswordItem.element)
    form.appendChild(captchaItem.element)
    form.appendChild(advancedItem.element)
    form.appendChild(advancedPanel.element)
    form.appendChild(button)
    form.addEventListener('submit', function (e) {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()
        repeatPasswordItem.clearError()
        captchaItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var username = usernameItem.getValue()
        if (username === null) return

        var password = passwordItem.getValue()
        if (password === null) return

        var repeatPassword = repeatPasswordItem.getValue(password)
        if (repeatPassword === null) return

        var captcha = captchaItem.getValue()
        if (captcha === null) return

        usernameItem.disable()
        passwordItem.disable()
        repeatPasswordItem.disable()
        captchaItem.disable()
        advancedPanel.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Signing up...'

        var url = 'data/signUp?username=' + encodeURIComponent(username) +
            '&password=' + encodeURIComponent(password) +
            '&captcha_token=' + encodeURIComponent(captcha.token) +
            '&captcha_value=' + encodeURIComponent(captcha.value) +
            '&timezone=' + encodeURIComponent(advancedPanel.getTimezoneValue())

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response.error === 'USERNAME_UNAVAILABLE') {
                enableItems()
                usernameItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The username is unavailable.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Please, try something else.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('What about '))
                    errorElement.appendChild((function () {
                        var b = document.createElement('b')
                        b.className = classPrefix + '-offerUsername'
                        b.appendChild(TextNode(response.offerUsername))
                        return b
                    })())
                    errorElement.appendChild(TextNode('?'))
                })
                return
            }

            if (response.error === 'INVALID_CAPTCHA_TOKEN') {
                enableItems()
                captchaItem.setNewCaptcha(response.newCaptcha)
                captchaItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The verification has expired.'))
                    errorElement.appendChild(document.createElement('br'))
                    errorElement.appendChild(TextNode('Please, try again.'))
                })
                return
            }

            if (response === 'INVALID_CAPTCHA_VALUE') {
                enableItems()
                captchaItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The verification is invalid.'))
                })
                return
            }

            signUpListener(username, response)

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: usernameItem.focus,
    }

}
