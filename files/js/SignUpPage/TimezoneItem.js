function SignUpPage_TimezoneItem (timezoneList, backListener) {

    var classPrefix = 'SignUpPage_TimezoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Timezone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var defaultValue = -(new Date).getTimezoneOffset()

    var defaultOption = document.createElement('option')
    defaultOption.value = defaultValue
    defaultOption.appendChild(TextNode(Timezone_Format(defaultValue)))

    var select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.appendChild(defaultOption)
    timezoneList.forEach(function (timezone) {
        var option = document.createElement('option')
        option.value = timezone.value
        option.appendChild(TextNode(timezone.text))
        select.appendChild(option)
    })
    select.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(select)

    return {
        element: element,
        disable: function () {
            select.disabled = true
            select.blur()
        },
        enable: function () {
            select.disabled = false
        },
        getValue: function () {
            return parseInt(select.value, 10)
        },
    }

}
