function SignUpPage_AdvancedItem (expandListener, collapseListener, backListener) {

    function collapse () {
        button.removeEventListener('click', collapse)
        button.addEventListener('click', expand)
        arrowClassList.remove('expanded')
        collapseListener()
    }

    function expand () {
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        arrowClassList.add('expanded')
        expandListener()
    }

    var classPrefix = 'SignUpPage_AdvancedItem'

    var arrowElement = Div(classPrefix + '-arrow')

    var arrowClassList = arrowElement.classList

    var button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button'
    button.appendChild(arrowElement)
    button.appendChild(TextNode('Advanced settings'))
    button.addEventListener('click', expand)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(button)
    element.appendChild(Div(classPrefix + '-line'))

    return { element: element }

}
