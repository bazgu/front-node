function SignUpPage_AdvancedPanel (timezoneList, backListener) {

    var timezoneItem = SignUpPage_TimezoneItem(timezoneList, backListener)

    var element = Div('SignUpPage_AdvancedPanel')
    element.appendChild(timezoneItem.element)

    var classList = element.classList

    return {
        disable: timezoneItem.disable,
        element: element,
        enable: timezoneItem.enable,
        getTimezoneValue: timezoneItem.getValue,
        hide: function () {
            classList.remove('visible')
        },
        show: function () {
            classList.add('visible')
        },
    }

}
