function ConnectionErrorPage (reconnectListener) {

    var classPrefix = 'ConnectionErrorPage'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Connection lost.'))
    textElement.appendChild(document.createElement('br'))
    textElement.appendChild(TextNode('Please, check your network and reconnect.'))

    var buttonNode = TextNode('Reconnect')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('click', function () {
        button.disabled = true
        buttonNode.nodeValue = 'Reconnecting...'
        reconnectListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}
