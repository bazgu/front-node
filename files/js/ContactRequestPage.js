function ContactRequestPage (session, username, profile,
    addContactListener, ignoreListener, closeListener, invalidSessionListener) {

    function clearError () {
        if (error === null) return
        frameElement.removeChild(error.element)
        error = null
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function disableItems () {
        addContactButton.disabled = true
        ignoreButton.disabled = true
    }

    function showAddError (error) {
        showError(error)
        addContactNode.nodeValue = 'Add Contact'
        addContactButton.focus()
    }

    function showError (_error) {
        addContactButton.disabled = false
        ignoreButton.disabled = false
        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
    }

    function showIgnoreError (error) {
        showError(error)
        ignoreNode.nodeValue = 'Ignore'
        ignoreButton.focus()
    }

    var error = null
    var request = null

    var classPrefix = 'ContactRequestPage'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode(
        '" has added you to his/her contacts.' +
        ' Would you like to add him/her to your contacts?'
    ))

    var addContactNode = TextNode('Add Contact')

    var addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.appendChild(addContactNode)
    addContactButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    addContactButton.addEventListener('click', function () {

        clearError()
        disableItems()
        addContactNode.nodeValue = 'Adding...'

        var url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        var timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showAddError(ServiceError())
                return
            }

            if (response === null) {
                showAddError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, function () {
            request = null
            showAddError(ConnectionError())
        })

    })

    var ignoreNode = TextNode('Ignore')

    var ignoreButton = document.createElement('button')
    ignoreButton.className = classPrefix + '-ignoreButton OrangeButton'
    ignoreButton.appendChild(ignoreNode)
    ignoreButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    ignoreButton.addEventListener('click', function () {

        clearError()
        disableItems()
        ignoreNode.nodeValue = 'Ignoring...'

        var url = 'data/ignoreRequest' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showIgnoreError(ServiceError())
                return
            }

            if (response === null) {
                showIgnoreError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            ignoreListener(response)

        }, function () {
            request = null
            showIgnoreError(ConnectionError())
        })

    })

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.appendChild(addContactButton)
    buttonsElement.appendChild(ignoreButton)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)
    frameElement.appendChild(buttonsElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        edit: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}
