function WorkPage_Messages (username, contactUsername) {

    var key = username + '$' + contactUsername

    var array = (function () {

        try {
            var array = localStorage[key]
        } catch (e) {
            return []
        }

        if (array === undefined) return []

        try {
            return JSON.parse(array)
        } catch (e) {
            return []
        }

    })()

    return {
        array: array,
        push: function (message) {
            array.push(message)
            if (array.length > 1024) array.shift()
            try {
                localStorage[key] = JSON.stringify(array)
            } catch (e) {
            }
        },
    }

}
