function WorkPage_ContactRequests (element, username, session,
    showListener, hideListener, addContactListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        if (page !== null) page.destroy()
    }

    function getMessages (username) {
        if (username === visibleUsername) return visibleMessages
        var request = requests[username]
        if (request !== undefined) return request.messages
    }

    function requestError () {
        destroy()
        connectionErrorListener()
    }

    function show (username, request) {
        visibleUsername = username
        visibleMessages = request.messages
        page = ContactRequestPage(session, username, request.profile, function (contactData) {
            addContactListener(username, contactData)
            showNext()
        }, showNext, function () {

            page.destroy()
            showNext()

            var url = 'data/removeRequest' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(username)

            var request = GetJson(url, function () {

                if (request.status !== 200) {
                    destroy()
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    destroy()
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    destroy()
                    invalidSessionListener()
                    return
                }

            }, requestError)

        }, function () {
            element.removeChild(page.element)
            invalidSessionListener()
        })
        element.appendChild(page.element)
    }

    function showNext () {

        element.removeChild(page.element)

        if (Object.keys(requests).length === 0) {
            page = null
            visibleUsername = null
            hideListener()
            return
        }

        var username = Object.keys(requests)[0]
        var request = requests[username]
        delete requests[username]
        show(username, request)

    }

    var requests = Object.create(null)

    var page = null,
        visibleUsername = null,
        visibleMessages = null

    return {
        add: function (requestUsername, profile) {

            var request = {
                profile: profile,
                messages: WorkPage_Messages(username, requestUsername),
            }

            if (page === null) {
                show(requestUsername, request)
                showListener()
            } else {
                requests[requestUsername] = request
            }

        },
        edit: function (username, profile) {
            if (visibleUsername === username) page.edit(profile)
            else requests[username].profile = profile
        },
        receiveFileMessage: function (username, file, time, token) {
            var messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage: function (username, text, time, token) {
            var messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedText', text, time, token])
        },
        remove: function (username) {
            if (visibleUsername === username) {
                page.destroy()
                showNext()
            } else {
                delete requests[username]
            }
        },
    }

}
