function WorkPage_PullMessages (session, processedListener,
    invalidSessionListener, connectionErrorListener,
    crashListener, serviceErrorListener) {

    function process (messages) {
        if (messages.length === 0) return
        messages.forEach(function (message) {
            events[message[0]].emit(message[1])
        })
        processedListener()
    }

    function pull () {

        var request = GetJson(url, function () {

            if (request.status !== 200) {
                serviceErrorListener()
                return
            }

            var response = request.response
            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== 'NOTHING_TO_PULL') process(response)
            pull()

        }, connectionErrorListener)

        abortFunction = function () {
            request.abort()
        }

    }

    var abortFunction
    var url = 'data/pullMessages?token=' + encodeURIComponent(session.token)
    pull()

    var events = Object.create(null)
    events.addContact = WorkPage_Event()
    events.addRequest = WorkPage_Event()
    events.editContactProfile = WorkPage_Event()
    events.editContactProfileAndOffline = WorkPage_Event()
    events.editContactProfileAndOnline = WorkPage_Event()
    events.editProfile = WorkPage_Event()
    events.editRequest = WorkPage_Event()
    events.ignoreRequest = WorkPage_Event()
    events.offline = WorkPage_Event()
    events.online = WorkPage_Event()
    events.overrideContactProfile = WorkPage_Event()
    events.publicProfile = WorkPage_Event()
    events.receiveFileMessage = WorkPage_Event()
    events.receiveTextMessage = WorkPage_Event()
    events.removeContact = WorkPage_Event()
    events.removeFile = WorkPage_Event()
    events.removeRequest = WorkPage_Event()
    events.requestFile = WorkPage_Event()
    events.sendFileMessage = WorkPage_Event()
    events.sendTextMessage = WorkPage_Event()

    events.editContactProfileAndOffline.addListener(function (username, profile) {
        events.editContactProfile.emit([username, profile])
        events.offline.emit([username])
    })
    events.editContactProfileAndOnline.addListener(function (username, profile) {
        events.editContactProfile.emit([username, profile])
        events.online.emit([username])
    })

    return {
        events: events,
        process: process,
        abort: function () {
            abortFunction()
        },
    }

}
