function WorkPage_SentFiles () {

    var map = Object.create(null)
    var size = 0

    return {
        add: function (token, feedCallback) {
            map[token] = { feed: feedCallback }
            size++
        },
        empty: function () {
            return size === 0
        },
        feed: function (token, endCallback) {
            map[token].feed(endCallback)
            delete map[token]
            size--
        },
    }

}
