function WorkPage_ChatPanel_CloseButtonStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_CloseButton {' +
            'background-image: url(' + getResourceUrl('img/close.svg') + ')' +
        '}'
}
