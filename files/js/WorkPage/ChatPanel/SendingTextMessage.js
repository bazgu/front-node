function WorkPage_ChatPanel_SendingTextMessage (
    disableEvent, enableEvent, formatText, isLocalLink, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_ChatPanel_SendingTextMessage'

    var element = Div(classPrefix)

    var requests = []

    return {
        element: element,
        type: 'SendingText',
        add: function (text, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            var itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, function (link) {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.appendChild(itemElement)

            var url = 'data/sendTextMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&text=' + encodeURIComponent(text)

            var request = GetJson(url, function () {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, function () {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy: function () {
            requests.forEach(function (request) {
                request.abort()
            })
        },
    }

}
