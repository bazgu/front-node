function WorkPage_ChatPanel_Smiley (text, icon) {
    var element = document.createElement('img')
    element.className = 'WorkPage_ChatPanel_Smiley ' + icon
    element.title = text
    element.alt = text
    element.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQIHWP4zwAAAgEBAMVfG14AAAAASUVORK5CYII='
    return element
}
