function WorkPage_ChatPanel_TypePanel (
    focusListener, blurListener, typeListener, fileListener,
    sendSmileyListener, sendContactListener, closeListener) {

    function submit () {
        var value = textarea.value
        if (/\S/.test(value)) typeListener(value)
        textarea.value = ''
    }

    var classPrefix = 'WorkPage_ChatPanel_TypePanel'

    var textarea = document.createElement('textarea')
    textarea.className = classPrefix + '-textarea'
    textarea.placeholder = 'Type a message here'
    textarea.addEventListener('focus', function () {
        moreButton.typeFocus()
        sendButtonClassList.add('typeFocused')
        focusListener()
    })
    textarea.addEventListener('blur', function () {
        moreButton.typeBlur()
        sendButtonClassList.remove('typeFocused')
        blurListener()
    })
    textarea.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            submit()
        } else if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var sendButton = document.createElement('button')
    sendButton.className = classPrefix + '-sendButton GreenButton'
    sendButton.appendChild(TextNode('Send'))
    sendButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    sendButton.addEventListener('click', function () {
        submit()
        textarea.focus()
    })

    var sendButtonClassList = sendButton.classList

    var moreButton = WorkPage_ChatPanel_MoreButton(closeListener,
        fileListener, sendSmileyListener, sendContactListener)

    var element = Div(classPrefix)
    element.appendChild(textarea)
    element.appendChild(moreButton.element)
    element.appendChild(sendButton)

    return {
        element: element,
        destroy: moreButton.destroy,
        disable: function () {
            textarea.disabled = true
            sendButton.disabled = true
            moreButton.disable()
        },
        enable: function () {
            textarea.disabled = false
            sendButton.disabled = false
            moreButton.enable()
        },
        focus: function () {
            textarea.focus()
        },
    }

}
