function WorkPage_ChatPanel_DaySeparator (day) {

    var date = new Date(day * 24 * 60 * 60 * 1000)

    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December']

    var dateString = monthNames[date.getUTCMonth()] + ' ' +
        date.getUTCDate() + ', ' + date.getUTCFullYear()

    var element = Div('WorkPage_ChatPanel_DaySeparator')
    element.appendChild(TextNode(dateString))

    return { element: element }

}
