function WorkPage_ChatPanel_SentFileMessage (formatBytes, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_SentFileMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentFile',
        add: function (file) {

            var sizeElement = Div(classPrefix + '-item-size')
            sizeElement.appendChild(TextNode(formatBytes(file.size)))

            var itemElement = Div(classPrefix + '-item')
            itemElement.appendChild(TextNode(file.name))
            itemElement.appendChild(sizeElement)

            element.appendChild(itemElement)

            return {
                startSend: function () {

                    var node = TextNode('Waiting...')

                    var sendingElement = Div(classPrefix + '-item-sending')
                    sendingElement.appendChild(node)

                    itemElement.classList.add('sending')
                    itemElement.appendChild(sendingElement)

                    return function () {

                        node.nodeValue = '0%'

                        var doneTextNode = TextNode('0%')

                        var doneTextElement = Div(classPrefix + '-item-sending-done-text')
                        doneTextElement.appendChild(doneTextNode)

                        var doneElement = Div(classPrefix + '-item-sending-done')
                        doneElement.appendChild(doneTextElement)

                        sendingElement.appendChild(doneElement)

                        return {
                            cancel: function () {
                                sendingElement.classList.add('failed')
                                doneTextElement.classList.add('failed')
                            },
                            complete: function () {
                                node.nodeValue = doneTextNode.nodeValue = '100%'
                                doneElement.style.width = '100%'
                            },
                            progress: function (sent) {

                                var percent = Math.round(sent / file.size * 100) + '%'

                                node.nodeValue = doneTextNode.nodeValue = percent
                                doneElement.style.width = percent

                            },
                        }

                    }

                },
            }

        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}
