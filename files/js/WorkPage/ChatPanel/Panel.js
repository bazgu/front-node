function WorkPage_ChatPanel_Panel (playAudio, formatBytes,
    formatText, isLocalLink, sentFiles, receivedFiles, username,
    session, contactUsername, profile, overrideProfile,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_ChatPanel_Panel'

    var title = WorkPage_ChatPanel_Title(contactUsername,
        profile, overrideProfile, profileListener, removeListener)

    var messagesPanel = WorkPage_ChatPanel_MessagesPanel(
        playAudio, formatBytes, formatText, isLocalLink, sentFiles,
        receivedFiles, username, session, contactUsername,
        function () {
            barClassList.add('hidden')
        },
        function () {
            barClassList.remove('hidden')
        },
        sendSmileyListener, sendContactListener,
        closeListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    var closeButton = WorkPage_ChatPanel_CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var barElement = Div(classPrefix + '-bar')
    barElement.appendChild(title.element)
    barElement.appendChild(closeButton.element)

    var barClassList = barElement.classList

    var element = Div(classPrefix)
    element.appendChild(barElement)
    element.appendChild(messagesPanel.element)

    return {
        element: element,
        editContactProfile: title.editContactProfile,
        editTimezone: messagesPanel.editTimezone,
        focus: messagesPanel.focus,
        overrideContactProfile: title.overrideContactProfile,
        receiveFileMessage: messagesPanel.receiveFileMessage,
        receiveTextMessage: messagesPanel.receiveTextMessage,
        sendFileMessage: messagesPanel.sendFileMessage,
        sendTextMessage: messagesPanel.sendTextMessage,
        destroy: function () {
            title.destroy()
            messagesPanel.destroy()
        },
        disable: function () {
            title.disable()
            closeButton.disable()
            messagesPanel.disable()
        },
        enable: function () {
            title.enable()
            closeButton.enable()
            messagesPanel.enable()
        },
    }

}
