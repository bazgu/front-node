function WorkPage_ChatPanel_CloseButton (listener) {

    var button = document.createElement('button')
    button.className = 'WorkPage_ChatPanel_CloseButton'
    button.title = 'Close'
    button.addEventListener('click', listener)

    return {
        element: button,
        addEventListener: function (name, listener) {
            button.addEventListener(name, listener)
        },
        disable: function () {
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        focus: function () {
            button.focus()
        },
    }

}
