function WorkPage_ChatPanel_FeedFile (token, file,
    progressListener, completeListener, cancelListener, destroyListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function ChunkedReader (file) {
        var offset = 0
        return {
            getOffset: function () {
                return offset
            },
            hasNextChunk: function () {
                return offset < file.size
            },
            readNextChunk: function (chunkSize, callback) {
                var blob = file.slice(offset, offset + chunkSize)
                var reader = new FileReader
                reader.readAsArrayBuffer(blob)
                reader.onload = function () {
                    offset += chunkSize
                    callback(reader.result)
                }
                return function () {
                    reader.abort()
                }
            },
        }
    }

    function next (chunkSize) {
        abortFunction = reader.readNextChunk(chunkSize, function (chunk) {

            var time = Date.now()

            var request = new XMLHttpRequest
            request.open('post', 'data/feedFile?token=' + encodeURIComponent(token))
            request.responseType = 'json'
            request.send(chunk)
            request.onerror = connectionErrorListener
            request.onload = function () {

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    cancelListener()
                    destroyListener()
                    return
                }

                if (response !== true) {
                    crashListener()
                    return
                }

                if (reader.hasNextChunk()) {
                    var elapsed = Date.now() - time
                    if (elapsed < 500 && chunkSize < 1024 * 1024 * 16) {
                        chunkSize *= 2
                    } else if (elapsed > 2000 && chunkSize > 1024) {
                        chunkSize /= 2
                    }
                    progressListener(reader.getOffset())
                    next(chunkSize)
                    return
                }

                completeListener()
                destroyListener()

            }

            abortFunction = function () {
                request.abort()
            }

        })
    }

    var abortFunction

    var reader = ChunkedReader(file)
    next(1024 * 8)

    return function () {
        abortFunction()
    }

}
