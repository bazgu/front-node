function WorkPage_ChatPanel_MessagesPanel (playAudio,
    formatBytes, formatText, isLocalLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    textareaFocusListener, textareaBlurListener, sendSmileyListener,
    sendContactListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addMessage (message, time) {
        ensureSeparator(time)
        doneMessagesElement.appendChild(message.element)
        sendingMessagesClassList.add('notFirst')
        liveMessages.push(message)
        lastMessage = message
    }

    function addReceivedFileMessage (file, time, token) {
        if (!canMerge('receivedFile', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_ReceivedFileMessage(
                    formatBytes, receivedFiles, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        var part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addReceivedTextMessage (text, time, token) {
        if (!canMerge('receivedText', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_ReceivedTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentFileMessage (file, time, token) {
        if (!canMerge('sentFile', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_SentFileMessage(
                    formatBytes, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        var part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addSentFileMessageAndStore (file, time, token) {
        messages.push(['sentFile', file, time, token])
        return addSentFileMessage(file, time, token)
    }

    function addSentTextMessage (text, time, token) {
        if (!canMerge('sentText', time)) {
            ;(function () {
                var message = WorkPage_ChatPanel_SentTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentTextMessageAndStore (text, time, token) {
        addSentTextMessage(text, time, token)
        messages.push(['sentText', text, time, token])
    }

    function canMerge (type, time) {
        return lastMessage !== null &&
            lastMessage.type === type && lastMessage.minute === Minute(time)
    }

    function destroy () {
        typePanel.destroy()
        destroyEvent.emit()
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function ensureSeparator (time) {
        var day = Day(time)
        var separator = daySeparators[day]
        if (separator !== undefined) return
        separator = daySeparators[day] = WorkPage_ChatPanel_DaySeparator(day)
        doneMessagesElement.appendChild(separator.element)
    }

    function getLastSendingMessage (type) {
        var length = sendingMessages.length
        if (length !== 0) {
            var message = sendingMessages[length - 1]
            if (message.type === type) return message
        }
    }

    function scrollDown () {
        contentElement.scrollTop = contentElement.scrollHeight
    }

    function sendNewTextMessage (text) {

        var message = getLastSendingMessage('SendingText')
        if (message === undefined) {
            message = WorkPage_ChatPanel_SendingTextMessage(
                disableEvent, enableEvent, formatText,
                isLocalLink, session, contactUsername,
                function () {
                    destroyEvent.removeListener(message.destroy)
                    sendingMessagesElement.removeChild(message.element)
                    sendingMessages.splice(sendingMessages.indexOf(message), 1)
                    if (sendingMessagesElement.childNodes.length === 0) {
                        sendingMessagesClassList.add('hidden')
                    }
                },
                destroyAndInvalidSession, destroyAndConnectionError,
                destroyAndCrash, destroyAndServiceError
            )
            sendingMessages.push(message)
        }

        message.add(text, function (response) {
            addSentTextMessageAndStore(text, response.time, response.token)
            playAudio('message-seen')
        })

        destroyEvent.addListener(message.destroy)
        sendingMessagesElement.appendChild(message.element)
        if (sendingMessagesElement.childNodes.length === 1) {
            sendingMessagesClassList.remove('hidden')
        }
        scrollDown()

    }

    var disableEvent = WorkPage_Event(),
        enableEvent = WorkPage_Event()

    var timezone = session.profile.timezone,
        timezoneOffset = timezone * 60 * 1000

    var lastMessage = null

    var sendingMessages = []

    var messages = WorkPage_Messages(username, contactUsername)
    var liveMessages = []
    var messagePartsMap = Object.create(null)
    var daySeparators = Object.create(null)
    var destroyEvent = WorkPage_Event()

    var classPrefix = 'WorkPage_ChatPanel_MessagesPanel'

    var doneMessagesElement = Div(classPrefix + '-doneMessages')

    var sendingMessagesElement = Div(classPrefix + '-sendingMessages hidden')

    var sendingMessagesClassList = sendingMessagesElement.classList

    var contentElement = Div(classPrefix + '-content')
    contentElement.appendChild(doneMessagesElement)
    contentElement.appendChild(sendingMessagesElement)

    var typePanel = WorkPage_ChatPanel_TypePanel(function () {
        classList.add('typing')
        textareaFocusListener()
    }, function () {
        classList.remove('typing')
        textareaBlurListener()
    }, sendNewTextMessage, function (files) {
        Array.prototype.forEach.call(files, function (readableFile) {

            var file = {
                name: readableFile.name,
                size: readableFile.size,
            }

            var message = getLastSendingMessage('SendingFile')
            if (message === undefined) {
                message = WorkPage_ChatPanel_SendingFileMessage(
                    formatBytes, session, contactUsername,
                    function () {
                        destroyEvent.removeListener(message.destroy)
                        sendingMessagesElement.removeChild(message.element)
                        sendingMessages.splice(sendingMessages.indexOf(message), 1)
                        if (sendingMessagesElement.childNodes.length === 0) {
                            sendingMessagesClassList.add('hidden')
                        }
                    },
                    destroyAndInvalidSession, destroyAndConnectionError,
                    destroyAndCrash, destroyAndServiceError
                )
                sendingMessages.push(message)
            }

            message.add(file, function (response) {
                var sendToken = response.sendToken
                var startFeed = addSentFileMessageAndStore(file, response.time, response.token).startSend()
                sentFiles.add(sendToken, function (endCallback) {

                    function complete () {
                        progress.complete()
                        endCallback()
                    }

                    var progress = startFeed()
                    if (file.size === 0) complete()
                    else {
                        var destroyFunction = WorkPage_ChatPanel_FeedFile(
                            sendToken, readableFile, progress.progress, complete,
                            function () {
                                progress.cancel()
                                endCallback()
                            },
                            function () {
                                destroyEvent.removeListener(destroyFunction)
                            },
                            destroyAndConnectionError, destroyAndCrash, destroyAndServiceError
                        )
                        destroyEvent.addListener(destroyFunction)
                    }

                })
                playAudio('message-seen')
            })

            destroyEvent.addListener(message.destroy)
            sendingMessagesElement.appendChild(message.element)
            if (sendingMessagesElement.childNodes.length === 1) {
                sendingMessagesClassList.remove('hidden')
            }
            scrollDown()

        })
        scrollDown()
    }, function () {
        sendSmileyListener(sendNewTextMessage)
    }, function () {
        sendContactListener(function (contacts) {
            var text = contacts.map(function (contact) {
                return location.protocol + '//' + location.host +
                    location.pathname + '#' + contact.username
            }).join('\n')
            sendNewTextMessage(text)
        })
    }, closeListener)

    var element = Div(classPrefix)
    element.appendChild(contentElement)
    element.appendChild(typePanel.element)

    var classList = element.classList

    WorkPage_ChatPanel_RestoreMessages(messages, session,
        addReceivedFileMessage, addReceivedTextMessage,
        addSentFileMessage, addSentTextMessage)

    return {
        destroy: destroy,
        element: element,
        sendFileMessage: addSentFileMessageAndStore,
        sendTextMessage: addSentTextMessageAndStore,
        disable: function () {
            typePanel.disable()
            disableEvent.emit()
        },
        editTimezone: function (newTimezone) {

            timezone = newTimezone
            timezoneOffset = timezone * 60 * 1000

            Object.keys(daySeparators).forEach(function (day) {
                doneMessagesElement.removeChild(daySeparators[day].element)
                delete daySeparators[day]
            })

            liveMessages.forEach(function (message) {
                message.editTimezone(timezoneOffset)
                ensureSeparator(message.time + timezoneOffset)
                doneMessagesElement.appendChild(message.element)
            })

        },
        enable: function () {
            typePanel.enable()
            enableEvent.emit()
        },
        focus: function () {
            typePanel.focus()
            scrollDown()
        },
        receiveFileMessage: function (file, time, token) {
            addReceivedFileMessage(file, time, token).showReceive()
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage: function (text, time, token) {
            addReceivedTextMessage(text, time, token)
            messages.push(['receivedText', text, time, token])
        },
    }

}
