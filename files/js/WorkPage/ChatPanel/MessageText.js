function WorkPage_ChatPanel_MessageText (formatText,
    isLocalLink, element, text, linkListener) {

    formatText(text, function (text) {
        element.appendChild(TextNode(text))
    }, function (link) {

        var a = document.createElement('a')
        a.href = link
        if (!isLocalLink(link)) a.target = '_blank'
        a.className = 'WorkPage_ChatPanel_MessageText-link'
        a.appendChild(TextNode(link))

        var span = document.createElement('span')
        span.className = 'WorkPage_ChatPanel_MessageText-disabledLink'
        span.appendChild(TextNode(link))

        element.appendChild(a)

        linkListener({
            disable: function () {
                element.replaceChild(span, a)
            },
            enable: function () {
                element.replaceChild(a, span)
            },
        })

    }, function (text, icon) {
        element.appendChild(WorkPage_ChatPanel_Smiley(text, icon))
    })

}
