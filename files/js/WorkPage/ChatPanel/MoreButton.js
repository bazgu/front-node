function WorkPage_ChatPanel_MoreButton (closeListener,
    fileListener, sendSmileyListener, sendContactListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        buttonClassList.add('not_pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        button.addEventListener('keydown', collapsedKeyDown)
        element.removeChild(moreMenu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        moreMenu.reset()
    }

    function collapsedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    }

    function expand () {
        expanded = true
        buttonClassList.remove('not_pressed')
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.removeEventListener('keydown', collapsedKeyDown)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.appendChild(moreMenu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            moreMenu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            button.focus()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            button.focus()
            moreMenu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            button.focus()
            moreMenu.selectDown()
        }
    }

    function windowFocus (e) {
        var target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    var moreMenu = WorkPage_ChatPanel_MoreMenu(keyDown, function (files) {
        collapse()
        fileListener(files)
    }, function () {
        collapse()
        sendSmileyListener()
    }, function () {
        collapse()
        sendContactListener()
    })

    var expanded = false

    var classPrefix = 'WorkPage_ChatPanel_MoreButton'

    var button = document.createElement('button')
    button.className = classPrefix + '-button not_pressed'
    button.appendChild(TextNode('More'))
    button.addEventListener('click', expand)
    button.addEventListener('keydown', collapsedKeyDown)

    var buttonClassList = button.classList

    var element = Div(classPrefix)
    element.appendChild(button)

    var classList = element.classList

    return {
        element: element,
        destroy: function () {
            if (expanded) collapse()
        },
        disable: function () {
            if (expanded) collapse()
            button.disabled = true
        },
        enable: function () {
            button.disabled = false
        },
        typeBlur: function () {
            classList.remove('typeFocused')
        },
        typeFocus: function () {
            classList.add('typeFocused')
        },
    }

}
