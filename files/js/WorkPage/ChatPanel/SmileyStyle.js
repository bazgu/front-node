function WorkPage_ChatPanel_SmileyStyle (smileys, getResourceUrl) {

    var style = ''
    smileys.array.forEach(function (smiley) {
        var file = smiley.file
        style +=
            '.WorkPage_ChatPanel_Smiley.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}
