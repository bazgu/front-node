function WorkPage_ChatPanel_RestoreMessages (messages,
    session, addReceivedFileMessage, addReceivedTextMessage,
    addSentFileMessage, addSentTextMessage) {

    messages.array.forEach(function (message) {
        var type = message[0]
        if (type === 'receivedFile') {
            var file = message[1]
            var part = addReceivedFileMessage(file, message[2], message[3])
            if (session.files[file.token] !== undefined) part.showReceive()
        } else if (type === 'receivedText') {
            addReceivedTextMessage(message[1], message[2], message[3])
        } else if (type === 'sentFile') {
            addSentFileMessage(message[1], message[2], message[3])
        } else {
            addSentTextMessage(message[1], message[2], message[3])
        }
    })

}
