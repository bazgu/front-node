function WorkPage_ChatPanel_ReceivedFileMessage (
    formatBytes, receivedFiles, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_ReceivedFileMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'receivedFile',
        add: function (file) {

            var nameNode = TextNode(file.name)

            var sizeElement = Div(classPrefix + '-item-size')
            sizeElement.appendChild(TextNode(formatBytes(file.size)))

            var itemElement = Div(classPrefix + '-item')
            itemElement.appendChild(nameNode)
            itemElement.appendChild(sizeElement)

            element.appendChild(itemElement)

            return {
                showReceive: function () {

                    var receiveLink = document.createElement('a')
                    receiveLink.className = classPrefix + '-receiveLink'
                    receiveLink.appendChild(TextNode('Receive'))
                    receiveLink.target = '_blank'
                    receiveLink.href = 'data/receiveFile?token=' + encodeURIComponent(file.token)

                    itemElement.classList.add('withReceiveLink')
                    itemElement.insertBefore(receiveLink, nameNode)
                    receivedFiles.add(file.token, function () {
                        itemElement.classList.remove('withReceiveLink')
                        itemElement.removeChild(receiveLink)
                    })

                },
            }

        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}
