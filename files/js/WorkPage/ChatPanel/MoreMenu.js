function WorkPage_ChatPanel_MoreMenu (fileInputKeyDownListener,
    fileListener, smileyListener, contactListener) {

    function addFileInput () {
        fileInput = document.createElement('input')
        fileInput.type = 'file'
        fileInput.multiple = true
        fileInput.className = classPrefix + '-fileInput'
        fileInput.addEventListener('keydown', fileInputKeyDownListener)
        fileInput.addEventListener('change', function () {
            fileListener(fileInput.files)
            fileItemElement.removeChild(fileInput)
            addFileInput()
        })
        fileItemElement.appendChild(fileInput)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    var classPrefix = 'WorkPage_ChatPanel_MoreMenu'

    var fileItemElement = Div(classPrefix + '-item')
    fileItemElement.appendChild(TextNode('File'))

    var smileyItemElement = Div(classPrefix + '-item')
    smileyItemElement.appendChild(TextNode('Smiley'))
    smileyItemElement.addEventListener('click', smileyListener)

    var contactItemElement = Div(classPrefix + '-item')
    contactItemElement.appendChild(TextNode('Contact'))
    contactItemElement.addEventListener('click', contactListener)

    var element = Div(classPrefix)
    element.appendChild(fileItemElement)
    element.appendChild(smileyItemElement)
    element.appendChild(contactItemElement)

    var selectedElement = null

    var fileInput
    addFileInput()

    return {
        element: element,
        openSelected: function () {
            if (selectedElement === fileItemElement) {
            } else if (selectedElement === smileyItemElement) {
                smileyListener()
            } else if (selectedElement === contactItemElement) {
                contactListener()
            }
        },
        reset: function () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown: function () {
            if (selectedElement === fileItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = contactItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            }
            selectedElement.classList.add('active')
        },
        selectUp: function () {
            if (selectedElement === contactItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = contactItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}
