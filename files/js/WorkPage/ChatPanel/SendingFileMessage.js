function WorkPage_ChatPanel_SendingFileMessage (formatBytes, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_ChatPanel_SendingFileMessage'

    var element = Div(classPrefix)

    var requests = []

    return {
        element: element,
        type: 'SendingFile',
        add: function (file, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            var name = file.name,
                size = file.size

            var sizeElement = Div(classPrefix + '-size')
            sizeElement.appendChild(TextNode(formatBytes(size)))

            var itemElement = Div(classPrefix + '-item')
            itemElement.appendChild(TextNode(name))
            itemElement.appendChild(document.createElement('br'))
            itemElement.appendChild(sizeElement)

            element.appendChild(itemElement)

            var url = 'data/sendFileMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&name=' + encodeURIComponent(name) +
                '&size=' + encodeURIComponent(size)

            var request = GetJson(url, function () {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                var response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, function () {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy: function () {
            requests.forEach(function (request) {
                request.abort()
            })
        },
    }

}
