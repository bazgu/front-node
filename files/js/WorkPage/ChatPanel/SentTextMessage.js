function WorkPage_ChatPanel_SentTextMessage (disableEvent,
    enableEvent, formatText, isLocalLink, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    var date = new Date(time + timezoneOffset)

    var classPrefix = 'WorkPage_ChatPanel_SentTextMessage'

    var timeNode = TextNode(getTimeString())

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(timeNode)

    var element = Div(classPrefix)
    element.appendChild(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentText',
        add: function (text) {
            var itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, function (link) {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.appendChild(itemElement)
            return {}
        },
        editTimezone: function (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}
