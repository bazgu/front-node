function WorkPage_SidePanel_ContactList (sounds, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, selectListener,
    deselectListener, profileListener, sendSmileyListener,
    sendContactListener, removeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addContact (contactUsername, contactData) {

        function place () {
            var sortName = contact.getSortName()
            for (var i = 0; i < contactsArray.length; i++) {
                if (sortName > contactsArray[i].getSortName()) continue
                contentElement.insertBefore(contact.element, contactsArray[i].element)
                contactsArray.splice(i, 0, contact)
                reorderEvent.emit([contact, i])
                return
            }
            contentElement.appendChild(contact.element)
            contactsArray.push(contact)
            reorderEvent.emit([contact, contactsArray.length - 1])
        }

        var contact = WorkPage_SidePanel_Contact(
            playAudio, formatBytes, formatText, isLocalLink,
            renderUserLink, sentFiles, receivedFiles, username, session,
            contactUsername, contactData, function (number) {
                totalNumber += number
                if (totalNumber === 0) document.title = 'Bazgu'
                else document.title = '(' + totalNumber + ') Bazgu'
            }, function () {
                if (selectedContact !== null) {
                    selectedContact.deselect()
                    deselectListener(selectedContact)
                }
                selectedContact = contact
                selectListener(contact)
            }, function () {
                deselectListener(selectedContact)
                selectedContact = null
                contact.focus()
            }, function () {
                var index = contactsArray.indexOf(contact)
                if (index !== 0) contactsArray[index - 1].focus()
            }, function () {
                var index = contactsArray.indexOf(contact)
                if (index === contactsArray.length - 1) return
                contactsArray[index + 1].focus()
            }, function () {
                profileListener(contact)
            }, function (send) {
                sendSmileyListener(contact, send)
            }, function (send) {
                sendContactListener(contact, contactsArray, send)
            }, function () {
                removeListener(contact)
            }, function () {
                contactsArray.splice(contactsArray.indexOf(contact), 1)
                contentElement.removeChild(contact.element)
                place()
            },
            invalidSessionListener, connectionErrorListener,
            crashListener, serviceErrorListener
        )

        contacts[contactUsername] = contact
        lowerContacts[contactUsername.toLowerCase()] = contact
        addEvent.emit([contact])
        place()

    }

    function playAudio (file) {
        audiosToPlay[file] = true
    }

    var audiosToPlay = Object.create(null)

    var contacts = Object.create(null)
    var lowerContacts = Object.create(null)
    var contactsArray = []

    var selectedContact = null

    var classPrefix = 'WorkPage_SidePanel_ContactList'

    var emptyElement = Div(classPrefix + '-empty')
    emptyElement.appendChild(TextNode('You have no contacts'))

    var invitePanel = InvitePanel(username, function () {})

    var addEvent = WorkPage_Event(),
        reorderEvent = WorkPage_Event()

    var contentElement = Div(classPrefix + '-content')
    ;(function () {
        var contacts = session.contacts
        for (var i in contacts) addContact(i, contacts[i])
    })()
    if (contactsArray.length === 0) {
        contentElement.appendChild(emptyElement)
        contentElement.appendChild(invitePanel.element)
    }

    var element = Div(classPrefix)
    element.appendChild(contentElement)

    var timezone = session.profile.timezone

    var totalNumber = 0
    document.title = 'Bazgu'

    return {
        addEvent: addEvent,
        element: element,
        reorderEvent: reorderEvent,
        destroy: function () {
            contactsArray.forEach(function (contact) {
                contact.destroy()
            })
        },
        disable: function () {
            contactsArray.forEach(function (contact) {
                contact.disable()
            })
        },
        editProfile: function (profile) {
            if (profile.timezone === timezone) return
            timezone = profile.timezone
            contactsArray.forEach(function (contact) {
                contact.editTimezone(timezone)
            })
        },
        enable: function () {
            contactsArray.forEach(function (contact) {
                contact.enable()
            })
        },
        endProcessing: function () {
            if (!sounds) return
            for (var i in audiosToPlay) {
                var audio = new Audio
                audio.src = 'audio/' + i + '.wav'
                audio.play()
                delete audiosToPlay[i]
            }
        },
        getContact: function (username) {
            return contacts[username]
        },
        getLowerContact: function (username) {
            return lowerContacts[username]
        },
        isEmpty: function () {
            return contactsArray.length === 0
        },
        offline: function (username) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.offline()
        },
        mergeContact: function (username, contactData) {

            var contact = contacts[username]
            if (contact === undefined) {
                if (contactsArray.length === 0) {
                    contentElement.removeChild(emptyElement)
                    contentElement.removeChild(invitePanel.element)
                }
                addContact(username, contactData)
                return
            }

            contact.editProfile(contactData.profile)
            contact.overrideProfile(contactData.overrideProfile)
            if (contactData.online) contact.online()
            else contact.offline()

        },
        online: function (username) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.online()
        },
        receiveFileMessage: function (username, file, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.receiveFileMessage(file, time, token)
        },
        receiveTextMessage: function (username, text, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.receiveTextMessage(text, time, token)
        },
        removeContact: function (contact) {
            if (contact === selectedContact) {
                selectedContact.deselect()
                deselectListener(selectedContact)
                selectedContact = null
            }
            contentElement.removeChild(contact.element)
            delete contacts[contact.username]
            delete lowerContacts[contact.username.toLowerCase()]
            contactsArray.splice(contactsArray.indexOf(contact), 1)
            if (contactsArray.length === 0) {
                contentElement.appendChild(emptyElement)
                contentElement.appendChild(invitePanel.element)
            }
            contact.removeEvent.emit()
        },
        sendFileMessage: function (username, file, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.sendFileMessage(file, time, token)
        },
        sendTextMessage: function (username, text, time, token) {
            var contact = contacts[username]
            if (contact === undefined) return
            contact.sendTextMessage(text, time, token)
        },
        setSounds: function (_sounds) {
            sounds = _sounds
        },
    }

}
