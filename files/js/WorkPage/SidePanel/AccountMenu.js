function WorkPage_SidePanel_AccountMenu (sounds,
    accountListener, soundsListener, signOutListener) {

    function flipSounds () {
        if (sounds) {
            sounds = false
            soundsNode.nodeValue = 'Off'
        } else {
            sounds = true
            soundsNode.nodeValue = 'On'
        }
        soundsListener(sounds)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    var classPrefix = 'WorkPage_SidePanel_AccountMenu'

    var accountItemElement = Div(classPrefix + '-item')
    accountItemElement.appendChild(TextNode('Account'))
    accountItemElement.addEventListener('click', accountListener)

    var soundsNode = TextNode(sounds ? 'On' : 'Off')

    var soundsItemElement = Div(classPrefix + '-item')
    soundsItemElement.appendChild(TextNode('Sounds: '))
    soundsItemElement.appendChild(soundsNode)
    soundsItemElement.addEventListener('click', flipSounds)

    var exitItemElement = Div(classPrefix + '-item')
    exitItemElement.appendChild(TextNode('Sign Out'))
    exitItemElement.addEventListener('click', signOutListener)

    var element = Div(classPrefix)
    element.appendChild(accountItemElement)
    element.appendChild(soundsItemElement)
    element.appendChild(exitItemElement)

    var selectedElement = null

    return {
        element: element,
        openSelected: function () {
            if (selectedElement === accountItemElement) accountListener()
            else if (selectedElement === soundsItemElement) flipSounds()
            else if (selectedElement === exitItemElement) signOutListener()
        },
        reset: function () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown: function () {
            if (selectedElement === accountItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = exitItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = accountItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp: function () {
            if (selectedElement === exitItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = accountItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = exitItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}
