function WorkPage_SidePanel_Contact (playAudio, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    contactData, addNumberListener, selectListener,
    deselectListener, navigateUpListener, navigateDownListener,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, reorderListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addNumber (increment) {
        number += increment
        numberNode.nodeValue = number
        addNumberListener(increment)
    }

    function computeDisplayName () {
        return overrideProfile.fullName || profile.fullName || contactUsername
    }

    function computeSortName () {
        return displayName.toLowerCase()
    }

    function deselect () {
        selected = false
        classList.remove('selected')
        element.removeEventListener('click', deselectAndCallListener)
        element.addEventListener('click', select)
    }

    function deselectAndCallListener () {
        deselect()
        deselectListener()
    }

    function increaseNumber () {
        if (selected) {
            playAudio('message-seen')
            return
        }
        addNumber(1)
        if (number === 1) {
            classList.add('withMessages')
            numberClassList.add('visible')
        }
        playAudio('message-unseen')
    }

    function select () {

        selected = true
        classList.add('selected')
        element.removeEventListener('click', select)
        element.addEventListener('click', deselectAndCallListener)
        selectListener()

        if (number === 0) return
        addNumber(-number)
        numberClassList.remove('visible')
        classList.remove('withMessages')

    }

    function updateName () {
        displayName = computeDisplayName()
        sortName = computeSortName()
        nameNode.nodeValue = displayName
    }

    var classPrefix = 'WorkPage_SidePanel_Contact'

    var number = 0

    var selected = false

    var profile = contactData.profile,
        overrideProfile = contactData.overrideProfile,
        displayName = computeDisplayName(),
        sortName = computeSortName()

    var chatPanel = WorkPage_ChatPanel_Panel(playAudio,
        formatBytes, formatText, isLocalLink, sentFiles, receivedFiles,
        username, session, contactUsername, profile, overrideProfile,
        profileListener, sendSmileyListener, sendContactListener,
        removeListener, deselectAndCallListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener)

    var nameNode = TextNode(displayName)

    var numberNode = TextNode(number)

    var numberElement = document.createElement('span')
    numberElement.className = classPrefix + '-number'
    numberElement.appendChild(numberNode)

    var numberClassList = numberElement.classList

    var element = document.createElement('button')
    element.className = classPrefix
    element.appendChild(numberElement)
    element.appendChild(nameNode)
    element.addEventListener('click', select)
    element.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 38) {
            e.preventDefault()
            navigateUpListener()
        } else if (keyCode === 40) {
            e.preventDefault()
            navigateDownListener()
        }
    })

    var classList = element.classList
    if (!contactData.online) classList.add('offline')

    var editProfileEvent = WorkPage_Event(),
        overrideProfileEvent = WorkPage_Event(),
        removeEvent = WorkPage_Event()

    return {
        chatPanel: chatPanel,
        deselect: deselect,
        destroy: chatPanel.destroy,
        editProfileEvent: editProfileEvent,
        editTimezone: chatPanel.editTimezone,
        element: element,
        overrideProfileEvent: overrideProfileEvent,
        removeEvent: removeEvent,
        select: select,
        sendFileMessage: chatPanel.sendFileMessage,
        sendTextMessage: chatPanel.sendTextMessage,
        username: contactUsername,
        userLink: renderUserLink(contactUsername),
        disable: function () {
            element.disabled = true
        },
        editProfile: function (_profile) {
            var oldSortName = sortName
            profile = _profile
            updateName()
            chatPanel.editContactProfile(profile)
            if (sortName !== oldSortName) reorderListener()
            editProfileEvent.emit([profile])
        },
        enable: function () {
            element.disabled = false
        },
        focus: function () {
            element.focus()
        },
        getDisplayName: function () {
            return displayName
        },
        getSortName: function () {
            return sortName
        },
        getOverrideProfile: function () {
            return overrideProfile
        },
        getProfile: function () {
            return profile
        },
        ensureSelected: function () {
            if (!selected) select()
        },
        offline: function () {
            classList.add('offline')
        },
        online: function () {
            classList.remove('offline')
        },
        overrideProfile: function (_overrideProfile) {
            var oldSortName = sortName
            overrideProfile = _overrideProfile
            updateName()
            chatPanel.overrideContactProfile(overrideProfile)
            if (sortName !== oldSortName) reorderListener()
            overrideProfileEvent.emit([overrideProfile])
        },
        receiveFileMessage: function (username, file, time, token) {
            chatPanel.receiveFileMessage(username, file, time, token)
            increaseNumber()
        },
        receiveTextMessage: function (username, text, time, token) {
            chatPanel.receiveTextMessage(username, text, time, token)
            increaseNumber()
        },
    }

}
