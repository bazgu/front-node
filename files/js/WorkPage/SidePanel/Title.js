function WorkPage_SidePanel_Title (sounds, username,
    session, accountListener, soundsListener, signOutListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.appendChild(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        var keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        var target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    var expanded = false

    var classPrefix = 'WorkPage_SidePanel_Title'

    var menu = WorkPage_SidePanel_AccountMenu(sounds, function () {
        collapse()
        accountListener()
    }, soundsListener, function () {
        collapse()
        signOutListener()
    })

    var node = TextNode(session.profile.fullName || username)

    var buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.appendChild(node)

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.appendChild(buttonTextElement)
    button.addEventListener('click', expand)

    var buttonClassList = button.classList

    var element = Div(classPrefix)
    element.appendChild(button)

    return {
        element: element,
        destroy: function () {
            if (expanded) collapse()
        },
        disable: function () {
            if (expanded) collapse()
            button.disabled = true
        },
        editProfile: function (profile) {
            node.nodeValue = profile.fullName || username
        },
        enable: function () {
            button.disabled = false
        },
    }

}
