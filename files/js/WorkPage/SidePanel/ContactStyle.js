function WorkPage_SidePanel_ContactStyle (getResourceUrl) {
    return '.WorkPage_SidePanel_Contact {' +
            'background-image: url(' + getResourceUrl('img/user-online.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.selected {' +
            'background-image: url(' + getResourceUrl('img/user-online-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.selected:active {' +
            'background-image: url(' + getResourceUrl('img/user-online-pressed-active.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline {' +
            'background-image: url(' + getResourceUrl('img/user-offline.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline.selected {' +
            'background-image: url(' + getResourceUrl('img/user-offline-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline.selected:active {' +
            'background-image: url(' + getResourceUrl('img/user-offline-pressed-active.svg') + ')' +
        '}'
}
