function WorkPage_SidePanel_Panel (formatBytes, formatText,
    isLocalLink, renderUserLink, sentFiles, receivedFiles, username,
    session, accountListener, signOutListener, addContactListener,
    contactSelectListener, contactDeselectListener, contactProfileListener,
    sendSmileyListener, sendContactListener,
    contactRemoveListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var classPrefix = 'WorkPage_SidePanel_Panel'

    var addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.appendChild(TextNode('Add Contact'))
    addContactButton.addEventListener('click', function () {
        addContactListener(!contactList.isEmpty())
    })

    var sounds = true
    try {
        sounds = JSON.parse(localStorage[username + '?sounds'])
    } catch (e) {}

    var title = WorkPage_SidePanel_Title(sounds, username, session, accountListener, function (_sounds) {
        sounds = _sounds
        try {
            localStorage[username + '?sounds'] = JSON.stringify(sounds)
        } catch (e) {}
        contactList.setSounds(sounds)
    }, signOutListener)

    var contactList = WorkPage_SidePanel_ContactList(sounds,
        formatBytes, formatText, isLocalLink, renderUserLink,
        sentFiles, receivedFiles, username, session,
        function (contact) {
            contactSelectListener(contact)
            classList.add('chatOpen')
        },
        function (contact) {
            contactDeselectListener(contact)
            classList.remove('chatOpen')
        },
        contactProfileListener, sendSmileyListener, sendContactListener,
        contactRemoveListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    var element = Div(classPrefix)
    element.appendChild(title.element)
    element.appendChild(addContactButton)
    element.appendChild(contactList.element)

    var classList = element.classList

    return {
        addContactEvent: contactList.addEvent,
        element: element,
        endProcessing: contactList.endProcessing,
        getContact: contactList.getContact,
        getLowerContact: contactList.getLowerContact,
        mergeContact: contactList.mergeContact,
        offline: contactList.offline,
        online: contactList.online,
        receiveFileMessage: contactList.receiveFileMessage,
        receiveTextMessage: contactList.receiveTextMessage,
        removeContact: contactList.removeContact,
        reorderEvent: contactList.reorderEvent,
        sendFileMessage: contactList.sendFileMessage,
        sendTextMessage: contactList.sendTextMessage,
        destroy: function () {
            title.destroy()
            contactList.destroy()
        },
        disable: function () {
            addContactButton.disabled = true
            title.disable()
            contactList.disable()
        },
        editProfile: function (profile) {
            title.editProfile(profile)
            contactList.editProfile(profile)
        },
        enable: function () {
            addContactButton.disabled = false
            title.enable()
            contactList.enable()
        },
    }

}
