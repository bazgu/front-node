function WorkPage_LocationHash (session, checkContactListener,
    publicProfileListener, noSuchUserListener, connectionErrorListener,
    crashListener, invalidSessionListener, serviceErrorListener) {

    function check () {

        abort()
        changeEvent.emit()

        var username = location.hash.substr(1)
        if (username === '') return

        var lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        var url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                serviceErrorListener()
                return
            }

            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                noSuchUserListener(username)
                return
            }

            publicProfileListener(response.username, response.profile)

        }, connectionErrorListener)

    }

    function abort () {
        if (request === null) return
        request.abort()
        request = null
    }

    var request = null

    var changeEvent = WorkPage_Event()

    addEventListener('hashchange', check)

    return {
        changeEvent: changeEvent,
        check: check,
        destroy: function () {
            removeEventListener('hashchange', check)
            abort()
        },
        reset: function () {
            removeEventListener('hashchange', check)
            location.hash = ''
            addEventListener('hashchange', check)
        },
    }

}
