function WorkPage_Event () {
    var listeners = []
    return {
        addListener: function (listener) {
            listeners.push(listener)
        },
        emit: function (args) {
            listeners.forEach(function (listener) {
                listener.apply(null, args)
            })
        },
        removeListener: function (listener) {
            var index = listeners.indexOf(listener)
            if (index === -1) throw 0
            listeners.splice(index, 1)
        },
    }
}
