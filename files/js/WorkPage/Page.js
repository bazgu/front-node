function WorkPage_Page (smileys, formatBytes, formatText, isLocalLink,
    renderUserLink, timezoneList, username, session, signOutListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        destroyEvent.emit()
        sidePanel.destroy()
        locationHash.destroy()
        removeEventListener('beforeunload', windowBeforeUnload)
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function disableBackground () {
        sidePanel.disable()
        if (chatPanel !== null) chatPanel.disable()
    }

    function editProfile (newProfile) {
        profile = newProfile
        sidePanel.editProfile(profile)
        editProfileEvent.emit([profile])
    }

    function enableBackground () {
        sidePanel.enable()
        if (chatPanel !== null) chatPanel.enable()
    }

    function showAccountPage (readyCallback) {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.editProfile.removeListener(page.editProfile)
            destroyEvent.removeListener(page.destroy)
        }

        var page = AccountPage_Page(timezoneList, userLink, username, session, profile, function (profile) {
            hide()
            enableBackground()
            editProfile(profile)
        }, function () {
            hide()
            ;(function () {

                function hide () {
                    element.removeChild(page.element)
                    destroyEvent.removeListener(page.destroy)
                }

                var page = ChangePasswordPage_Page(session, function () {
                    hide()
                    showAccountPage(function (page) {
                        page.focusChangePassword()
                    })
                }, function () {
                    hide()
                    enableBackground()
                }, function () {
                    hide()
                    destroyAndInvalidSession()
                })

                element.appendChild(page.element)
                page.focus()
                destroyEvent.addListener(page.destroy)

            })()
        }, function () {
            hide()
            enableBackground()
        }, function () {
            hide()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        readyCallback(page)
        pullMessages.events.editProfile.addListener(page.editProfile)
        destroyEvent.addListener(page.destroy)

    }

    function showAddContactPage (showInvite) {

        function hide () {
            element.removeChild(page.element)
            destroyEvent.removeListener(page.destroy)
        }

        var page = AddContactPage_Page(showInvite, session, username, function (checkUsername) {

            if (checkUsername === lowerUsername) {
                hide()
                ;(function () {

                    function hide () {
                        element.removeChild(page.element)
                        editProfileEvent.removeListener(page.editProfile)
                    }

                    var page = FoundSelfPage(username, profile, function () {
                        hide()
                        showAddContactPage(showInvite)
                    }, function () {
                        hide()
                        enableBackground()
                    })

                    element.appendChild(page.element)
                    page.focus()
                    editProfileEvent.addListener(page.editProfile)

                })()
                return true
            }

            var contact = sidePanel.getLowerContact(checkUsername)
            if (contact === undefined) return false

            hide()
            ;(function () {

                function contactRemoveListener () {
                    hide()
                    enableBackground()
                }

                function hide () {
                    element.removeChild(page.element)
                    contact.editProfileEvent.removeListener(page.editProfile)
                    contact.removeEvent.removeListener(contactRemoveListener)
                }

                var page = FoundContactPage(contact.username, contact.getProfile(), function () {
                    hide()
                    enableBackground()
                    contact.ensureSelected()
                }, function () {
                    hide()
                    showAddContactPage(showInvite)
                }, function () {
                    hide()
                    enableBackground()
                })

                element.appendChild(page.element)
                page.focus()
                contact.editProfileEvent.addListener(page.editProfile)
                contact.removeEvent.addListener(contactRemoveListener)

            })()

            return true

        }, function (username, profile) {
            hide()
            ;(function () {

                function hide () {
                    element.removeChild(page.element)
                    pullMessages.events.publicProfile.removeListener(publicProfileListener)
                    destroyEvent.removeListener(page.destroy)
                }

                function publicProfileListener (eventUsername, profile) {
                    if (username === eventUsername) page.editProfile(profile)
                }

                var page = FoundUserPage(session, username, profile, function (contactData) {
                    hide()
                    enableBackground()
                    sidePanel.mergeContact(username, contactData)
                    sidePanel.getContact(username).ensureSelected()
                }, function () {
                    hide()
                    showAddContactPage(showInvite)
                    unwatchPublicProfile(username)
                }, function () {
                    hide()
                    enableBackground()
                    unwatchPublicProfile(username)
                }, function () {
                    hide()
                    destroyAndInvalidSession()
                })

                element.appendChild(page.element)
                page.focus()
                pullMessages.events.publicProfile.addListener(publicProfileListener)
                destroyEvent.addListener(page.destroy)

            })()
        }, function () {
            hide()
            enableBackground()
        }, function () {
            hide()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        page.focus()
        destroyEvent.addListener(page.destroy)

    }

    function unwatchPublicProfile (username) {

        function destroy () {
            request.abort()
        }

        var url = 'data/unwatchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        var request = GetJson(url, function () {

            destroyEvent.removeListener(destroy)

            if (request.status !== 200) {
                destroyAndServiceError()
                return
            }

            var response = request.response
            if (response === null) {
                destroyAndCrash()
                return
            }

            if (response !== true) destroyAndServiceError()

        }, function () {
            destroyEvent.removeListener(destroy)
            destroyAndConnectionError()
        })

        destroyEvent.addListener(destroy)

    }

    function windowBeforeUnload (e) {
        if (sentFiles.empty() && numFeedingFiles === 0) return
        e.preventDefault()
        var message = 'Your file transfers will be aborted if you close.'
        e.returnValue = message
        return message
    }

    var numFeedingFiles = 0
    var userLink = renderUserLink(username)
    var lowerUsername = username.toLowerCase()
    var profile = session.profile

    var destroyEvent = WorkPage_Event(),
        editProfileEvent = WorkPage_Event()

    var sentFiles = WorkPage_SentFiles(),
        receivedFiles = WorkPage_ReceivedFiles()

    var chatPanel = null

    var classPrefix = 'WorkPage_Page'

    var sidePanel = WorkPage_SidePanel_Panel(formatBytes, formatText, isLocalLink, renderUserLink, sentFiles, receivedFiles, username, session, function () {
        disableBackground()
        showAccountPage(function (page) {
            page.focus()
        })
    }, function () {

        function hide () {
            element.removeChild(page.element)
        }

        var page = SignOutPage(function () {

            pullMessages.abort()
            hide()
            destroy()
            signOutListener()

            GetJson('data/signOut?token=' + encodeURIComponent(session.token))

        }, function () {
            hide()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()

    }, function (showInvite) {
        disableBackground()
        showAddContactPage(showInvite)
    }, function (contact) {
        chatPanel = contact.chatPanel
        element.appendChild(chatPanel.element)
        chatPanel.focus()
    }, function () {
        chatPanel.destroy()
        element.removeChild(chatPanel.element)
        chatPanel = null
    }, function (contact) {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.editProfileEvent.removeListener(page.editProfile)
            contact.overrideProfileEvent.removeListener(page.overrideProfile)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        var page = EditContactPage_Page(
            timezoneList, session, contact.userLink, contact.username,
            contact.getProfile(), contact.getOverrideProfile(),
            function (profile) {
                hide()
                enableBackground()
                contact.overrideProfile(profile)
            },
            function () {
                hide()
                enableBackground()
            },
            function () {
                hide()
                destroyAndInvalidSession()
            }
        )

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.editProfileEvent.addListener(page.editProfile)
        contact.overrideProfileEvent.addListener(page.overrideProfile)
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, function (contact, send) {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        var page = SmileysPage_Page(smileys, function (text) {
            hide()
            enableBackground()
            send(text)
        }, function () {
            hide()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, function (contact, contacts, send) {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        var page = ContactSelectPage_Page(contacts, function (contacts) {
            hide()
            enableBackground()
            send(contacts)
        }, function () {
            hide()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, function (contact) {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        var page = RemoveContactPage(contact.username, session, function () {
            hide()
            enableBackground()
            sidePanel.removeContact(contact)
        }, function () {
            hide()
            enableBackground()
        }, function () {
            hide()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, destroyAndInvalidSession, destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)

    var element = Div(classPrefix + ' Page')
    element.appendChild(sidePanel.element)

    var contactRequests = WorkPage_ContactRequests(
        element, username, session, disableBackground, enableBackground,
        function (username, contactData) {
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        },
        destroyAndInvalidSession, destroyAndConnectionError,
        destroyAndCrash, destroyAndServiceError
    )
    for (var i in session.requests) {
        contactRequests.add(i, session.requests[i])
    }

    var pullMessages = WorkPage_PullMessages(session,
        sidePanel.endProcessing, destroyAndInvalidSession,
        destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)
    pullMessages.events.addContact.addListener(function (username, profile) {
        var contact = sidePanel.getContact(username)
        if (contact === undefined) sidePanel.mergeContact(username, profile)
        contactRequests.remove(username)
    })
    pullMessages.events.addRequest.addListener(contactRequests.add)
    pullMessages.events.editContactProfile.addListener(function (username, profile) {
        var contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.editProfile(profile)
    })
    pullMessages.events.editProfile.addListener(editProfile)
    pullMessages.events.editRequest.addListener(contactRequests.edit)
    pullMessages.events.ignoreRequest.addListener(contactRequests.remove)
    pullMessages.events.offline.addListener(sidePanel.offline)
    pullMessages.events.online.addListener(sidePanel.online)
    pullMessages.events.overrideContactProfile.addListener(function (username, overrideProfile) {
        var contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.overrideProfile(overrideProfile)
    })
    pullMessages.events.receiveFileMessage.addListener(function (username, file, time, token) {
        session.files[file.token] = {}
        sidePanel.receiveFileMessage(username, file, time, token)
        contactRequests.receiveFileMessage(username, file, time, token)
    })
    pullMessages.events.receiveTextMessage.addListener(function (username, text, time, token) {
        sidePanel.receiveTextMessage(username, text, time, token)
        contactRequests.receiveTextMessage(username, text, time, token)
    })
    pullMessages.events.removeContact.addListener(function (username) {
        var contact = sidePanel.getContact(username)
        if (contact !== undefined) sidePanel.removeContact(contact)
    })
    pullMessages.events.removeFile.addListener(function (token) {
        receivedFiles.remove(token)
        delete session.files[token]
    })
    pullMessages.events.removeRequest.addListener(contactRequests.remove)
    pullMessages.events.requestFile.addListener(function (token) {
        numFeedingFiles++
        sentFiles.feed(token, function () {
            numFeedingFiles--
        })
    })
    pullMessages.events.sendFileMessage.addListener(sidePanel.sendFileMessage)
    pullMessages.events.sendTextMessage.addListener(sidePanel.sendTextMessage)

    var locationHash = WorkPage_LocationHash(session, function (checkUsername) {

        if (checkUsername === lowerUsername) {
            ;(function () {

                function hide () {
                    element.removeChild(page.element)
                    editProfileEvent.removeListener(page.editProfile)
                    locationHash.changeEvent.removeListener(hide)
                    enableBackground()
                }

                var page = SimpleSelfPage(username, profile, function () {
                    hide()
                    locationHash.reset()
                })

                element.appendChild(page.element)
                page.focus()
                disableBackground()
                editProfileEvent.addListener(page.editProfile)
                locationHash.changeEvent.addListener(hide)

            })()
            return true
        }

        var contact = sidePanel.getLowerContact(checkUsername)
        if (contact === undefined) return false

        ;(function () {

            function contactRemoveListener () {
                hideAndResetHash()
                enableBackground()
            }

            function hide () {
                element.removeChild(page.element)
                contact.editProfileEvent.removeListener(page.editProfile)
                contact.removeEvent.removeListener(contactRemoveListener)
                locationHash.changeEvent.removeListener(hide)
            }

            function hideAndResetHash () {
                hide()
                locationHash.reset()
            }

            var page = SimpleContactPage(contact.username, contact.getProfile(), function () {
                hideAndResetHash()
                enableBackground()
                contact.ensureSelected()
            }, function () {
                hideAndResetHash()
                enableBackground()
            })

            element.appendChild(page.element)
            page.focus()
            disableBackground()
            contact.editProfileEvent.addListener(page.editProfile)
            contact.removeEvent.addListener(contactRemoveListener)
            locationHash.changeEvent.addListener(hide)

        })()

        return true

    }, function (username, profile) {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.publicProfile.removeListener(publicProfileListener)
            destroyEvent.removeListener(page.destroy)
            locationHash.changeEvent.removeListener(hide)
        }

        function hideAndResetHash () {
            hide()
            locationHash.reset()
        }

        function publicProfileListener (eventUsername, profile) {
            if (username === eventUsername) page.editProfile(profile)
        }

        var page = SimpleUserPage(session, username, profile, function (contactData) {
            hideAndResetHash()
            enableBackground()
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        }, function () {
            hideAndResetHash()
            enableBackground()
            unwatchPublicProfile(username)
        }, function () {
            hideAndResetHash()
            destroyAndInvalidSession()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()
        pullMessages.events.publicProfile.addListener(publicProfileListener)
        destroyEvent.addListener(page.destroy)
        locationHash.changeEvent.addListener(hide)

    }, function (username) {

        var page = NoSuchUserPage(username, function () {
            element.removeChild(page.element)
            locationHash.reset()
            enableBackground()
        })

        element.appendChild(page.element)
        page.focus()
        disableBackground()

    }, destroyAndConnectionError, destroyAndCrash, destroyAndInvalidSession, destroyAndServiceError)

    pullMessages.process(session.messages)
    locationHash.check()
    addEventListener('beforeunload', windowBeforeUnload)

    return { element: element }

}
