function FoundContactPage (username, profile,
    openListener, backListener, closeListener) {

    var backButton = BackButton(backListener)

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var classPrefix = 'FoundContactPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(TextNode('Start a Conversation'))
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })
    button.addEventListener('click', openListener)

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('"'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" is already in your contacts.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus: function () {
            button.focus()
        },
    }

}
