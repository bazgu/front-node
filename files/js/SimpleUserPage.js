function SimpleUserPage (session, username, profile,
    addContactListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        button.disabled = false
        buttonNode.nodeValue = 'Add Contact'

        error = _error
        frameElement.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var classPrefix = 'SimpleUserPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var buttonNode = TextNode('Add Contact')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    button.addEventListener('click', function () {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        var url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        var timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        button.disabled = true
        buttonNode.nodeValue = 'Adding...'

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, requestError)

    })

    var userInfoPanel = UserInfoPanel_Panel(profile)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(userInfoPanel.element)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        editProfile: function (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus: function () {
            button.focus()
        },
    }

}
