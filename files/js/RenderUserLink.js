function RenderUserLink (username) {
    var prefix = location.protocol + '//' +
        location.host + location.pathname + '#'
    return function (username) {
        return prefix + username
    }
}
