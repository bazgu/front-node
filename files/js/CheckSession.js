function CheckSession (readyListener, signInListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    var username, token
    try {
        username = localStorage.username
        token = localStorage.token
    } catch (e) {
    }

    var warningCallback
    var hashUsername = location.hash.substr(1)
    if (Username_IsValid(hashUsername)) {
        warningCallback = WelcomePage_UnauthenticatedWarning(hashUsername)
    }

    if (token === undefined) {
        readyListener(username === undefined ? '' : username, warningCallback)
        return
    }

    var url = 'data/restoreSession?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    var request = GetJson(url, function () {

        if (request.status !== 200) {
            serviceErrorListener()
            return
        }

        var response = request.response
        if (response === null) {
            crashListener()
            return
        }

        if (response === 'INVALID_USERNAME' || response === 'INVALID_TOKEN') {
            try {
                delete localStorage.token
            } catch (e) {
            }
            readyListener(username, warningCallback)
            return
        }

        signInListener(username, response)

    }, connectionErrorListener)

}
