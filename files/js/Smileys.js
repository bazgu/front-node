function Smileys () {

    var array = [{
        text: ':)',
        file: 'face-smile',
    }, {
        text: ':))',
        file: 'face-smile-more',
    }, {
        text: ':(',
        file: 'face-frown',
    }, {
        text: ':((',
        file: 'face-frown-more',
    }, {
        text: ':d',
        file: 'face-laugh',
    }, {
        text: ':dd',
        file: 'face-laugh-more',
    }, {
        text: ':p',
        file: 'face-tongue',
    }, {
        text: ':pp',
        file: 'face-tongue-more',
    }, {
        text: ';)',
        file: 'face-wink',
    }, {
        text: ';))',
        file: 'face-wink-more',
    }, {
        text: ':o',
        file: 'face-surprised',
    }, {
        text: ':oo',
        file: 'face-surprised-more',
    }, {
        text: ':s',
        file: 'face-confused',
    }, {
        text: ':ss',
        file: 'face-confused-more',
    }, {
        text: ':3',
        file: 'face-cat',
    }, {
        text: ':33',
        file: 'face-cat-more',
    }, {
        text: ':|',
        file: 'face-neutral',
    }, {
        text: ':||',
        file: 'face-neutral-more',
    }, {
        text: ':*',
        file: 'face-kiss',
    }, {
        text: ':**',
        file: 'face-kiss-more',
    }, {
        text: '<3',
        file: 'heart',
    }, {
        text: '</3',
        file: 'heart-broken',
    }]

    var map = Object.create(null)
    array.forEach(function (smiley) {
        map[smiley.text] = smiley
    })

    var table = [
        [':)', ':(', ':d', ':p', ':|'],
        [':))', ':((', ':dd', ':pp', ':||'],
        [';)', ':3', ':s', ':o', ':*'],
        [';))', ':33', ':ss', ':oo', ':**'],
        ['<3', '</3'],
    ]
    table.forEach(function (row) {
        row.forEach(function (text, index) {
            row[index] = map[text]
        })
    })

    return {
        array: array,
        map: map,
        table: table,
    }

}
