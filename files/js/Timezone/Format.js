function Timezone_Format (value) {

    var sign
    if (value > 0) {
        sign = '+'
    } else if (value < 0) {
        sign = '-'
        value = -value
    } else {
        sign = '\xb1'
    }

    var hours = TwoDigitPad(Math.floor(value / 60))

    return sign + hours + ':' + TwoDigitPad(value % 60)

}
