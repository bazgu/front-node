function SmileysPage_PageStyle (smileys, getResourceUrl) {

    var style = ''
    smileys.array.forEach(function (smiley) {
        var file = smiley.file
        style +=
            '.SmileysPage_Page-button.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}
