function SmileysPage_Page (smileys, selectListener, closeListener) {

    function focusAt (e, x, y) {

        var row = buttons[y]
        if (row === undefined) return

        var button = row[x]
        if (button === undefined) return

        e.preventDefault()
        button.focus()

    }

    var classPrefix = 'SmileysPage_Page'

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Smileys'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)

    var buttons = smileys.table.map(function (row, y) {

        var rowElement = Div(classPrefix + '-row')
        var buttons = row.map(function (smiley, x) {

            var text = smiley.text

            var button = document.createElement('button')
            button.className = classPrefix + '-button ' + smiley.file
            button.title = text
            button.addEventListener('click', function () {
                selectListener(text)
            })
            button.addEventListener('keydown', function (e) {

                if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return

                var keyCode = e.keyCode
                if (keyCode === 27) {
                    e.preventDefault()
                    closeListener()
                    return
                }

                if (keyCode === 37) {
                    focusAt(e, x - 1, y)
                    return
                }

                if (keyCode === 38) {
                    focusAt(e, x, y - 1)
                    return
                }

                if (keyCode === 39) {
                    focusAt(e, x + 1, y)
                    return
                }

                if (keyCode === 40) focusAt(e, x, y + 1)

            })

            rowElement.appendChild(button)
            return button

        })

        frameElement.appendChild(rowElement)
        return buttons

    })

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: function () {
            buttons[0][0].focus()
        },
    }

}
