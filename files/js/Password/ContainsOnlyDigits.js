function Password_ContainsOnlyDigits (password) {
    return /^\d+$/.test(password)
}
