function ConnectionError () {

    var element = Div('FormError')
    element.appendChild(TextNode('Connection failed.'))
    element.appendChild(document.createElement('br'))
    element.appendChild(TextNode('Please, check your network and try again.'))

    return { element: element }

}
