function UserInfoPanel_Panel (profile) {

    function isEmpty (profile) {
        return profile.fullName === '' && profile.email === '' &&
            profile.phone === '' && profile.timezone === null
    }

    var fullNameItem = UserInfoPanel_FullNameItem(profile)

    var emailItem = UserInfoPanel_EmailItem(profile)

    var phoneItem = UserInfoPanel_PhoneItem(profile)

    var timezoneItem = UserInfoPanel_TimezoneItem(profile)

    var emptyElement = Div('UserInfoPanel_Panel-empty')
    emptyElement.appendChild(TextNode('No more information available'))

    var emptyClassList = emptyElement.classList
    if (!isEmpty(profile)) emptyClassList.add('hidden')

    var element = Div('UserInfoPanel_Panel')
    element.appendChild(fullNameItem.element)
    element.appendChild(emailItem.element)
    element.appendChild(phoneItem.element)
    element.appendChild(timezoneItem.element)
    element.appendChild(emptyElement)

    return {
        element: element,
        edit: function (profile) {

            fullNameItem.edit(profile)
            emailItem.edit(profile)
            phoneItem.edit(profile)
            timezoneItem.edit(profile)

            if (isEmpty(profile)) emptyClassList.remove('hidden')
            else emptyClassList.add('hidden')

        },
    }

}
