function UserInfoPanel_TimezoneItem (profile) {

    var value = profile.timezone

    var classPrefix = 'UserInfoPanel_Panel-item'

    var labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.appendChild(TextNode('Timezone: '))

    var valueNode = TextNode('')

    var valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.appendChild(valueNode)

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(valueElement)

    var classList = element.classList
    if (value === null) classList.add('hidden')
    else valueNode.nodeValue = Timezone_Format(value)

    return {
        element: element,
        edit: function (profile) {
            value = profile.timezone
            if (value === null) {
                classList.add('hidden')
                valueNode.nodeValue = ''
            } else {
                classList.remove('hidden')
                valueNode.nodeValue = Timezone_Format(value)
            }
        },
    }

}
