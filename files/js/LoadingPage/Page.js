function LoadingPage_Page () {

    var classPrefix = 'LoadingPage'

    var contentElement = Div(classPrefix + '-content')
    contentElement.appendChild(TextNode('Reconnecting...'))

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(contentElement)

    return { element: element }

}
