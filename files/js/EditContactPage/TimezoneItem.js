function EditContactPage_TimezoneItem (timezoneList, profile,
    overrideProfile, changeListener, closeListener) {

    function format (value) {
        if (value === null) return ''
        return Timezone_Format(value)
    }

    var classPrefix = 'EditContactPage_TimezoneItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Timezone'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var emptyNode = TextNode(format(profile.timezone))

    var emptyOption = document.createElement('option')
    emptyOption.appendChild(emptyNode)

    var select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.appendChild(emptyOption)
    timezoneList.forEach(function (timezone) {
        var option = document.createElement('option')
        option.value = timezone.value
        option.appendChild(TextNode(timezone.text))
        select.appendChild(option)
    })
    select.value = overrideProfile.timezone
    select.addEventListener('change', changeListener)
    select.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(select)

    return {
        element: element,
        disable: function () {
            select.disabled = true
            select.blur()
        },
        editProfile: function (profile) {
            emptyNode.nodeValue = format(profile.timezone)
        },
        enable: function () {
            select.disabled = false
        },
        getValue: function () {
            var value = select.value
            if (value === '') return null
            return parseInt(value, 10)
        },
    }

}
