function EditContactPage_Page (timezoneList, session, userLink,
    username, profile, overrideProfile, overrideProfileListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === overrideProfile.fullName &&
            emailItem.getValue() === overrideProfile.email &&
            phoneItem.getValue() === overrideProfile.phone &&
            timezoneItem.getValue() === overrideProfile.timezone
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'EditContactPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var fullNameItem = EditContactPage_FullNameItem(profile,
        overrideProfile, checkChanges, close)

    var emailItem = EditContactPage_EmailItem(profile,
        overrideProfile, checkChanges, close)

    var phoneItem = EditContactPage_PhoneItem(profile,
        overrideProfile, checkChanges, close)

    var timezoneItem = EditContactPage_TimezoneItem(timezoneList,
        profile, overrideProfile, checkChanges, close)

    var linkItem = EditContactPage_LinkItem(userLink, close)

    var saveChangesNode = TextNode('Save Changes')

    var saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.appendChild(saveChangesNode)
    saveChangesButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(fullNameItem.element)
    form.appendChild(emailItem.element)
    form.appendChild(phoneItem.element)
    form.appendChild(timezoneItem.element)
    form.appendChild(linkItem.element)
    form.appendChild(saveChangesButton)
    form.addEventListener('submit', function (e) {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var fullName = fullNameItem.getValue()
        var email = emailItem.getValue()
        var phone = phoneItem.getValue()
        var timezone = timezoneItem.getValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        var url = 'data/overrideContactProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&email=' + encodeURIComponent(email) +
            '&phone=' + encodeURIComponent(phone)
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            overrideProfileListener({
                fullName: fullName,
                email: email,
                phone: phone,
                timezone: timezone,
            })

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile: function (profile) {
            fullNameItem.editProfile(profile)
            emailItem.editProfile(profile)
            phoneItem.editProfile(profile)
        },
        overrideProfile: function (_overrideProfile) {
            overrideProfile = _overrideProfile
            checkChanges()
        },
    }

}
