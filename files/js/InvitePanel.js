function InvitePanel (username, backListener) {

    var classPrefix = 'InvitePanel'

    var link = location.protocol + '//' +
        location.host + location.pathname + '#' + username

    var text = 'You can chat with me at ' + link

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Invite People'))

    var smsButton = document.createElement('a')
    smsButton.target = '_blank'
    smsButton.className = classPrefix + '-smsButton OrangeButton'
    smsButton.appendChild(TextNode('Via SMS'))
    smsButton.href = 'sms:?body=' + encodeURIComponent(text)
    smsButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var emailButton = document.createElement('a')
    emailButton.target = '_blank'
    emailButton.className = classPrefix + '-emailButton OrangeButton'
    emailButton.appendChild(TextNode('Via Email'))
    emailButton.href = 'mailto:?body=' + encodeURIComponent(text)
    emailButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var element = Div(classPrefix)
    element.appendChild(titleElement)
    element.appendChild(smsButton)
    element.appendChild(emailButton)

    return { element: element }

}
