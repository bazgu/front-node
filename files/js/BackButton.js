function BackButton (listener) {

    var element = document.createElement('button')
    element.className = 'BackButton'
    element.appendChild(TextNode('\u2039 Back'))
    element.addEventListener('click', listener)

    return {
        element: element,
        focus: function () {
            element.focus()
        },
    }

}
