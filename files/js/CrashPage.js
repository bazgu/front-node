function CrashPage (reloadListener) {

    var classPrefix = 'CrashPage'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Something went wrong.'))
    textElement.appendChild(document.createElement('br'))
    textElement.appendChild(TextNode('Reloading may fix the problem.'))

    var buttonNode = TextNode('Reload')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('click', function () {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: function () {
            button.focus()
        },
    }

}
