function FormatText (smileys) {

    var smileyComponents = smileys.array.map(function (smiley) {
        return smiley.text.replace(/([()|*])/g, '\\$1')
    }).sort(function (a, b) {
        return a.length > b.length ? -1 : 1
    })

    var pattern = '(?:(' + smileyComponents.join('|') + ')|https?:\/\/\\S+)'

    return function (text, textCallback, linkCallback, smileyCallback) {

        var index = 0
        var regex = new RegExp(pattern, 'gi')
        while (true) {

            var match = regex.exec(text)
            if (match === null) break

            var matchIndex = match.index
            if (matchIndex > index) textCallback(text.substring(index, matchIndex))

            var content = match[0]
            if (match[1] === undefined) linkCallback(content)
            else smileyCallback(content, smileys.map[content.toLowerCase()].file)

            index = matchIndex + content.length

        }
        if (index < text.length) textCallback(text.substring(index, text.length))

    }

}
