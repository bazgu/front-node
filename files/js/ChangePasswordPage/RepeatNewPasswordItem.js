function ChangePasswordPage_RepeatNewPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.appendChild(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    var errorElement = null

    var classPrefix = 'ChangePasswordPage_RepeatNewPasswordItem'

    var label = document.createElement('label')
    label.htmlFor = Math.random()
    label.appendChild(TextNode('Retype the new password'))

    var labelElement = Div(classPrefix + '-label')
    labelElement.appendChild(label)

    var input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    var inputClassList = input.classList

    var element = Div(classPrefix)
    element.appendChild(labelElement)
    element.appendChild(input)

    return {
        element: element,
        clearError: function () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable: function () {
            input.disabled = true
            input.blur()
        },
        enable: function () {
            input.disabled = false
        },
        getValue: function (password) {
            var value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (value !== password) {
                showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The passwords doesn\'t match.'))
                })
                return null
            }
            return value
        },
    }

}
