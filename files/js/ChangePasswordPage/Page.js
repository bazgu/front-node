function ChangePasswordPage_Page (session,
    backListener, closeListener, invalidSessionListener) {

    function back () {
        destroy()
        backListener()
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        currentPasswordItem.enable()
        newPasswordItem.enable()
        repeatNewPasswordItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Save'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'ChangePasswordPage_Page'

    var backButton = BackButton(back)

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Change Password'))

    var currentPasswordItem = ChangePasswordPage_CurrentPasswordItem(back)

    var newPasswordItem = ChangePasswordPage_NewPasswordItem(back)

    var repeatNewPasswordItem = ChangePasswordPage_RepeatNewPasswordItem(back)

    var buttonNode = TextNode('Save')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    var form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.appendChild(currentPasswordItem.element)
    form.appendChild(newPasswordItem.element)
    form.appendChild(repeatNewPasswordItem.element)
    form.appendChild(button)
    form.addEventListener('submit', function (e) {

        e.preventDefault()
        currentPasswordItem.clearError()
        newPasswordItem.clearError()
        repeatNewPasswordItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var currentPassword = currentPasswordItem.getValue()
        if (currentPassword === null) return

        var newPassword = newPasswordItem.getValue()
        if (newPassword === null) return

        var repeatNewPassword = repeatNewPasswordItem.getValue(newPassword)
        if (repeatNewPassword === null) return

        currentPasswordItem.disable()
        newPasswordItem.disable()
        repeatNewPasswordItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Saving...'

        var url = 'data/changePassword' +
            '?token=' + encodeURIComponent(session.token) +
            '&currentPassword=' + encodeURIComponent(currentPassword) +
            '&newPassword=' + encodeURIComponent(newPassword)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_CURRENT_PASSWORD') {
                currentPasswordItem.enable()
                newPasswordItem.enable()
                repeatNewPasswordItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Save'
                currentPasswordItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('The password is incorrect.'))
                })
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            closeListener()

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: currentPasswordItem.focus,
    }

}
