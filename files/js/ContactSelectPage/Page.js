function ContactSelectPage_Page (contacts, selectListener, closeListener) {

    function addContact (contact) {

        var username = contact.username

        var item = ContactSelectPage_Item(contact, function () {
            selectedContacts.push(item.contact)
            if (selectedContacts.length === 1) button.disabled = false
        }, function () {
            selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            if (selectedContacts.length === 0) button.disabled = true
        }, function () {

            if (item.isSelected()) {
                selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            }

            delete itemsMap[username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            listElement.removeChild(item.element)

        }, close)

        itemsMap[username] = item
        itemsArray.push(item)
        listElement.appendChild(item.element)

    }

    function close () {
        itemsArray.forEach(function (item) {
            item.destroy()
        })
        closeListener()
    }

    var itemsMap = Object.create(null)
    var itemsArray = []
    var selectedContacts = []

    var classPrefix = 'ContactSelectPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('Select Contact'))

    var listElement = Div(classPrefix + '-list')
    contacts.forEach(addContact)

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.disabled = true
    button.appendChild(TextNode('Send'))
    button.addEventListener('click', function () {
        selectListener(selectedContacts)
    })
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(listElement)
    frameElement.appendChild(button)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        addContact: addContact,
        element: element,
        focus: function () {
            itemsArray[0].focus()
        },
        reorder: function (contact, index) {

            var item = itemsMap[contact.username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            itemsArray.splice(index, 0, item)

            var nextItem = itemsArray[index + 1]
            if (nextItem === undefined) {
                listElement.appendChild(item.element)
            } else {
                listElement.insertBefore(item.element, nextItem.element)
            }

            item.update()

        },
    }

}
