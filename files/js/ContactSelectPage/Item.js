function ContactSelectPage_Item (contact, selectListener,
    deselectListener, removeListener, closeListener) {

    function deselect () {
        selected = false
        button.classList.remove('selected')
        clickElement.removeEventListener('click', deselect)
        clickElement.addEventListener('click', select)
        deselectListener()
    }

    function select () {
        selected = true
        button.classList.add('selected')
        clickElement.removeEventListener('click', select)
        clickElement.addEventListener('click', deselect)
        selectListener()
    }

    var selected = false

    var classPrefix = 'ContactSelectPage_Item'

    var button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var clickNode = TextNode(contact.getDisplayName())

    var clickElement = Div(classPrefix + '-click')
    clickElement.appendChild(button)
    clickElement.addEventListener('click', select)
    clickElement.appendChild(clickNode)

    var element = Div(classPrefix)
    element.appendChild(clickElement)

    contact.removeEvent.addListener(removeListener)

    return {
        contact: contact,
        element: element,
        destroy: function () {
            contact.removeEvent.removeListener(removeListener)
        },
        focus: function () {
            button.focus()
        },
        isSelected: function () {
            return selected
        },
        update: function () {
            clickNode.nodeValue = contact.getDisplayName()
        },
    }

}
