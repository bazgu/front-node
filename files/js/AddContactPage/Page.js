function AddContactPage_Page (showInvite, session,
    username, checkContactListener, userFoundListener,
    closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        usernameItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Find User'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'AddContactPage_Page'

    var closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Add Contact'))

    var usernameItem = AddContactPage_UsernameItem(close)

    var buttonNode = TextNode('Find User')

    var button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.appendChild(buttonNode)
    button.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    var form = document.createElement('form')
    form.appendChild(usernameItem.element)
    form.appendChild(button)
    form.addEventListener('submit', function (e) {

        e.preventDefault()
        usernameItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        var username = usernameItem.getValue()
        if (username === null) return

        var lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        usernameItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Finding...'

        var url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                usernameItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Find User'
                usernameItem.showError(function (errorElement) {
                    errorElement.appendChild(TextNode('There is no such user.'))
                })
                return
            }

            userFoundListener(response.username, response.profile)

        }, requestError)

    })

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(form)
    if (showInvite) {

        var invitePanelElement = Div(classPrefix + '-invitePanel')
        invitePanelElement.appendChild(InvitePanel(username, close).element)

        frameElement.appendChild(invitePanelElement)

    }

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: usernameItem.focus,
    }

}
