function ServiceError () {

    var element = Div('FormError')
    element.appendChild(TextNode('Operation failed.'))
    element.appendChild(document.createElement('br'))
    element.appendChild(TextNode('Please, try again.'))

    return { element: element }

}
