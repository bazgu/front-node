function Username_ContainsIllegalChars (username) {
    return /[^a-z0-9_.-]/i.test(username)
}
