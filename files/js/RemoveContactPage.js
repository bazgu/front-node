function RemoveContactPage (username, session,
    removeListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        removeButton.disabled = false
        cancelButton.disabled = false
        removeNode.nodeValue = 'Remove Contact'

        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
        removeButton.focus()

    }

    var error = null
    var request = null

    var classPrefix = 'RemoveContactPage'

    var usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-username'
    usernameElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('Are you sure you want to remove "'))
    textElement.appendChild(usernameElement)
    textElement.appendChild(TextNode('" from your contacts?'))

    var removeNode = TextNode('Remove Contact')

    var removeButton = document.createElement('button')
    removeButton.className = classPrefix + '-removeButton GreenButton'
    removeButton.appendChild(removeNode)
    removeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    removeButton.addEventListener('click', function () {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        removeButton.disabled = true
        cancelButton.disabled = true
        removeNode.nodeValue = 'Removing...'

        var url = 'data/removeContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, function () {

            var status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            removeListener()

        }, requestError)

    })

    var cancelButton = document.createElement('button')
    cancelButton.className = classPrefix + '-cancelButton OrangeButton'
    cancelButton.appendChild(TextNode('Cancel'))
    cancelButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    cancelButton.addEventListener('click', closeListener)

    var buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.appendChild(removeButton)
    buttonsElement.appendChild(cancelButton)

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(textElement)
    frameElement.appendChild(buttonsElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: function () {
            removeButton.focus()
        },
    }

}
