function NoSuchUserPage (username, closeListener) {

    var closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', function (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    var classPrefix = 'NoSuchUserPage'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode(username))

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode('There is no such user.'))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(closeButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix)
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)
    element.addEventListener('click', function (e) {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
    }

}
