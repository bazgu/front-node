function PrivacyPage (back) {

    var classPrefix = 'PrivacyPage'

    var backButton = BackButton(back)

    var titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.appendChild(TextNode('Privacy Policy'))

    var text =
        'We:\n' +
        '* Encrypt your connection using SSL.\n' +
        '* Keep your account details.\n' +
        '* Keep your contact list.\n' +
        '* Queue your messages until delivered.\n' +
        '* Proxy your file transfers.\n' +
        'What don\'t:\n' +
        '* Censor you communication.\n' +
        '* Store your chat history.\n' +
        '* Disclose your data to anyone.\n' +
        '* Sell your data.\n' +
        '* Show ads.'

    var textElement = Div(classPrefix + '-text')
    textElement.appendChild(TextNode(text))

    var frameElement = Div(classPrefix + '-frame')
    frameElement.appendChild(backButton.element)
    frameElement.appendChild(titleElement)
    frameElement.appendChild(textElement)

    var element = Div(classPrefix + ' Page')
    element.appendChild(Div(classPrefix + '-aligner'))
    element.appendChild(frameElement)

    return {
        element: element,
        focus: backButton.focus,
    }

}
