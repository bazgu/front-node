exports.port = 7100
exports.host = '127.0.0.1'

exports.configNode = {
    host: '127.0.0.1',
    port: 7600,
}

exports.log = {
    error: true,
    http: true,
    info: true,
}
