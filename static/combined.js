(() => {

document.head.append((() => {
    const element = document.createElement('style');
    element.innerHTML = "*{margin:0;padding:0;border:0;outline:0;border-radius:0;background:transparent;color:inherit;font:inherit;cursor:default;text-align:inherit;text-decoration:none;resize:none;opacity:1;white-space:nowrap;box-sizing:border-box;word-wrap:break-word}html,body{background-color:white;color:hsl(0,0%,30%);height:100%;font:normal 15px/25px Arial,sans-serif;overflow:hidden;text-align:left}.AccountReadyPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.AccountReadyPage-aligner{display:inline-block;vertical-align:middle;height:100%}.AccountReadyPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.AccountReadyPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.AccountReadyPage-text{line-height:20px;white-space:normal}.AccountReadyPage-button{margin-top:10px;width:100%}\n.BackButton{position:absolute;top:10px;left:10px;line-height:44px;padding-right:14px;padding-left:14px;cursor:pointer;color:hsl(95,60%,55%)}.BackButton:focus{background-color:hsl(95,70%,95%)}.BackButton:active{background-color:hsl(95,70%,85%)}.CloseButton{position:absolute;top:10px;right:10px;width:44px;height:44px;cursor:pointer;background-position:center;background-repeat:no-repeat}.CloseButton:focus{background-color:hsl(95,70%,95%)}.CloseButton:active{background-color:hsl(95,70%,85%)}.ConnectionErrorPage{overflow:auto;text-align:center}.ConnectionErrorPage-aligner{display:inline-block;vertical-align:middle;height:100%}.ConnectionErrorPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.ConnectionErrorPage-text{padding:20px;margin-bottom:10px;white-space:normal;line-height:20px}.ConnectionErrorPage-button{width:100%}.ContactRequestPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.ContactRequestPage-aligner{display:inline-block;vertical-align:middle;height:100%}\n.ContactRequestPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.ContactRequestPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px;margin-bottom:10px}.ContactRequestPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.ContactRequestPage-text-username{font-weight:bold}.ContactRequestPage-buttons{margin-top:10px}.ContactRequestPage-addContactButton{width:calc(60% - 5px)}.ContactRequestPage-ignoreButton{width:calc(40% - 5px);margin-left:10px}.CrashPage{overflow:auto;text-align:center}.CrashPage-aligner{display:inline-block;vertical-align:middle;height:100%}.CrashPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.CrashPage-text{padding:20px;margin-bottom:10px;white-space:normal;line-height:20px}.CrashPage-button{width:100%}.FormError{background-color:hsl(350,70%,55%);color:white;padding:5px;white-space:normal;text-align:center;padding-top:5px;padding-bottom:5px;line-height:20px;margin-top:10px}\n.FoundContactPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.FoundContactPage-aligner{display:inline-block;vertical-align:middle;height:100%}.FoundContactPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;text-align:left}.FoundContactPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px;overflow:hidden;text-overflow:ellipsis}.FoundContactPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.FoundContactPage-text-username{font-weight:bold}.FoundContactPage-button{margin-top:10px;width:100%}.FoundSelfPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.FoundSelfPage-aligner{display:inline-block;vertical-align:middle;height:100%}.FoundSelfPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;text-align:left}\n.FoundSelfPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px;overflow:hidden;text-overflow:ellipsis}.FoundSelfPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.FoundSelfPage-text-username{font-weight:bold}.FoundUserPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.FoundUserPage-aligner{display:inline-block;vertical-align:middle;height:100%}.FoundUserPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;text-align:left}.FoundUserPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px;overflow:hidden;text-overflow:ellipsis}.FoundUserPage-button{margin-top:10px;width:100%}.GreenButton{padding-top:5px;padding-bottom:5px;cursor:pointer;color:white;text-align:center;background-color:hsl(95,60%,55%);border:2px solid hsl(95,60%,55%);min-height:40px}.GreenButton:focus{background-color:hsl(95,60%,65%)}\n.GreenButton:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.GreenButton[disabled]{color:white;background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}.InvitePanel-title{font-size:22px;line-height:44px;text-align:center;color:hsl(95,60%,55%)}.InvitePanel-smsButton{display:inline-block;vertical-align:top;margin-top:10px;width:calc(50% - 5px)}.InvitePanel-emailButton{display:inline-block;vertical-align:top;margin-top:10px;width:calc(50% - 5px);margin-left:10px}.LoadingPage{text-align:center}.LoadingPage-aligner{display:inline-block;vertical-align:middle;height:100%}.LoadingPage-content{display:inline-block;vertical-align:middle;padding:20px;background-color:white}.NoSuchUserPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.NoSuchUserPage-aligner{display:inline-block;vertical-align:middle;height:100%}.NoSuchUserPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}\n.NoSuchUserPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.NoSuchUserPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px;color:hsl(350,70%,55%)}.OrangeButton{padding-top:5px;padding-bottom:5px;cursor:pointer;color:white;text-align:center;background-color:hsl(45,75%,55%);border:2px solid hsl(45,75%,55%);min-height:40px}.OrangeButton:focus{background-color:hsl(45,75%,65%)}.OrangeButton:active{color:hsl(45,75%,70%);background-color:hsl(45,75%,35%);border-color:hsl(45,75%,35%)}.OrangeButton[disabled]{color:white;background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}.Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsl(95,70%,85%);background-repeat:repeat-x,repeat-x;background-position:center bottom,center 30%}.Page_Blocks{display:flex;flex-direction:column}.Page_Blocks.x_small{gap:5px}.PrivacyPage{overflow:auto;text-align:center}.PrivacyPage-aligner{display:inline-block;vertical-align:middle;height:100%}\n.PrivacyPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;text-align:left}.PrivacyPage-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px}.PrivacyPage-text{line-height:20px;white-space:pre-wrap}.RemoveContactPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.RemoveContactPage-aligner{display:inline-block;vertical-align:middle;height:100%}.RemoveContactPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.RemoveContactPage-text{padding:20px;white-space:normal;line-height:20px}.RemoveContactPage-username{font-weight:bold}.RemoveContactPage-buttons{margin-top:10px}.RemoveContactPage-removeButton{width:calc(65% - 5px)}.RemoveContactPage-cancelButton{width:calc(35% - 5px);margin-left:10px}.ServiceErrorPage{overflow:auto;text-align:center}.ServiceErrorPage-aligner{display:inline-block;vertical-align:middle;height:100%}\n.ServiceErrorPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.ServiceErrorPage-text{padding:20px;margin-bottom:10px;white-space:normal;line-height:20px}.ServiceErrorPage-button{width:100%}.SignOutPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SignOutPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SignOutPage-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px}.SignOutPage-text{padding:20px;white-space:normal;line-height:20px;margin-bottom:10px}.SignOutPage-yesButton{width:calc(55% - 5px)}.SignOutPage-noButton{width:calc(45% - 5px);margin-left:10px}.SimpleContactPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SimpleContactPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SimpleContactPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}\n.SimpleContactPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SimpleContactPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.SimpleContactPage-text-username{font-weight:bold}.SimpleContactPage-button{margin-top:10px;width:100%}.SimpleSelfPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SimpleSelfPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SimpleSelfPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.SimpleSelfPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SimpleSelfPage-text{text-align:center;padding:20px;white-space:normal;line-height:20px}.SimpleSelfPage-text-username{font-weight:bold}.SimpleUserPage{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}\n.SimpleUserPage-aligner{display:inline-block;vertical-align:middle;height:100%}.SimpleUserPage-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.SimpleUserPage-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SimpleUserPage-button{margin-top:10px;width:100%}.AccountPage_EmailItem{position:relative}.AccountPage_EmailItem-input{background-color:white;padding:5px 10px;cursor:text;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;min-height:40px}.AccountPage_EmailItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_EmailItem-input[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_FullNameItem{position:relative}.AccountPage_FullNameItem-input{background-color:white;padding:5px 10px;cursor:text;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;min-height:40px}\n.AccountPage_FullNameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_FullNameItem-input[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_LinkItem-input{background-color:hsl(0,0%,95%);padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.AccountPage_LinkItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.AccountPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.AccountPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.AccountPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.AccountPage_Page-form{border-bottom:1px solid hsl(0,0%,90%);padding-bottom:10px;margin-bottom:10px}\n.AccountPage_Page-saveChangesButton{margin-top:10px;width:100%}.AccountPage_Page-changePasswordButton{width:100%}.AccountPage_PhoneItem{position:relative}.AccountPage_PhoneItem-input{background-color:white;padding:5px 10px;cursor:text;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;min-height:40px}.AccountPage_PhoneItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_PhoneItem-input[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_PrivacySelect{position:absolute;right:0;bottom:0;width:58px;height:40px;line-height:40px}.AccountPage_PrivacySelect-button{display:inline-block;vertical-align:top;cursor:pointer;width:100%;height:100%;border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-right-width:2px;border-bottom-width:2px;background-repeat:no-repeat,no-repeat;background-position:2px center,calc(100% - 10px) center}\n.AccountPage_PrivacySelect-button:focus{background-color:hsl(95,70%,95%);border-color:hsl(95,60%,55%)}.AccountPage_PrivacySelect-button:active{background-color:hsl(95,70%,85%);border-color:hsl(95,60%,55%)}.AccountPage_PrivacySelect-button.pressed{color:white;background-color:hsl(95,60%,55%);border-color:hsl(95,60%,55%)}.AccountPage_PrivacySelect-button.pressed:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.AccountPage_PrivacySelect-button[disabled]{background-color:hsl(0,0%,95%)}.AccountPage_PrivacySelect-menu{position:absolute;top:100%;right:0;background-color:white;border:2px solid hsl(95,60%,55%);z-index:1;margin-top:-2px}.AccountPage_PrivacySelectItem{padding-left:40px;padding-right:12px;cursor:pointer;background-position:2px center;background-repeat:no-repeat}.AccountPage_PrivacySelectItem.active,.AccountPage_PrivacySelectItem:active{background-color:hsl(95,70%,85%)}.AccountPage_TimezoneItem{position:relative}.AccountPage_TimezoneItem-select{vertical-align:top;background-color:white;padding:5px 10px;cursor:pointer;width:calc(100% - 58px);border-style:solid;border-color:hsl(0,0%,70%);border-top-width:2px;border-bottom-width:2px;border-left-width:2px;min-height:40px;background-repeat:no-repeat;background-position:calc(100% - 10px) center;appearance:none}\n.AccountPage_TimezoneItem-select:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.AccountPage_TimezoneItem-select[disabled]{background-color:hsl(0,0%,95%)}.AddContactPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.AddContactPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.AddContactPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.AddContactPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.AddContactPage_Page-button{margin-top:10px;width:100%}.AddContactPage_Page-invitePanel{margin-top:10px}.AddContactPage_UsernameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.AddContactPage_UsernameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}\n.AddContactPage_UsernameItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.AddContactPage_UsernameItem-input[disabled]{background-color:hsl(0,0%,95%)}.AddContactPage_UsernameItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ChangePasswordPage_CurrentPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.ChangePasswordPage_CurrentPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.ChangePasswordPage_CurrentPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.ChangePasswordPage_CurrentPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.ChangePasswordPage_CurrentPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ChangePasswordPage_NewPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}\n.ChangePasswordPage_NewPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.ChangePasswordPage_NewPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.ChangePasswordPage_NewPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.ChangePasswordPage_NewPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ChangePasswordPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.ChangePasswordPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.ChangePasswordPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;text-align:left}.ChangePasswordPage_Page-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px}.ChangePasswordPage_Page-button{margin-top:10px;width:100%}\n.ChangePasswordPage_RepeatNewPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.ChangePasswordPage_RepeatNewPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.ChangePasswordPage_RepeatNewPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.ChangePasswordPage_RepeatNewPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.ChangePasswordPage_RepeatNewPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.ContactSelectPage_Item{line-height:25px;margin-top:10px}.ContactSelectPage_Item:first-child{margin-top:0}.ContactSelectPage_Item-click{display:inline-block;vertical-align:top;cursor:pointer;width:100%;overflow:hidden;text-overflow:ellipsis}.ContactSelectPage_Item-button{display:inline-block;vertical-align:top;width:25px;height:25px;border:2px solid hsl(0,0%,70%);cursor:inherit;margin-right:5px;line-height:21px;text-align:center;background-position:center;background-repeat:no-repeat}\n.ContactSelectPage_Item-button:focus{background-color:hsl(95,70%,95%);border-color:hsl(95,60%,55%)}.ContactSelectPage_Item-button:active{background-color:hsl(95,70%,85%)}.ContactSelectPage_Item-button.selected{color:white;background-color:hsl(95,60%,55%);border-color:hsl(95,60%,55%)}.ContactSelectPage_Item-button.selected:active{color:hsl(95,60%,55%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.ContactSelectPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.ContactSelectPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.ContactSelectPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.ContactSelectPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-right:54px;margin-bottom:10px}.ContactSelectPage_Page-button{width:100%;margin-top:10px}.EditContactPage_EmailItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}\n.EditContactPage_EmailItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_EmailItem-input[disabled]{background-color:hsl(0,0%,95%)}.EditContactPage_FullNameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.EditContactPage_FullNameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_FullNameItem-input[disabled]{background-color:hsl(0,0%,95%)}.EditContactPage_LinkItem-input{background-color:hsl(0,0%,95%);padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.EditContactPage_LinkItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.EditContactPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}\n.EditContactPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;padding-top:10px;text-align:left}.EditContactPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.EditContactPage_Page-saveChangesButton{margin-top:10px;width:100%}.EditContactPage_PhoneItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.EditContactPage_PhoneItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_PhoneItem-input[disabled]{background-color:hsl(0,0%,95%)}.EditContactPage_TimezoneItem-select{vertical-align:top;background-color:white;padding:5px 10px;cursor:pointer;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px;background-repeat:no-repeat;background-position:calc(100% - 10px) center;appearance:none}\n.EditContactPage_TimezoneItem-select:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.EditContactPage_TimezoneItem-select[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_AdvancedItem{margin-top:10px;line-height:25px}.SignUpPage_AdvancedItem-button{display:inline-block;vertical-align:top;color:hsl(95,60%,55%);cursor:pointer;padding-right:4px;padding-left:3px;background-color:white}.SignUpPage_AdvancedItem-button:focus{background-color:hsl(95,70%,95%)}.SignUpPage_AdvancedItem-button:active{background-color:hsl(95,70%,85%)}.SignUpPage_AdvancedItem-arrow{display:inline-block;vertical-align:top;width:13px;height:25px;background-position:center;background-repeat:no-repeat;cursor:inherit}.SignUpPage_AdvancedItem-line{margin-top:-13px;margin-bottom:12px;border-bottom:1px solid hsl(0,0%,90%)}.SignUpPage_AdvancedPanel{display:none}.SignUpPage_AdvancedPanel.visible{display:block}.SignUpPage_CaptchaItem-image{display:inline-block;vertical-align:top;background-color:hsl(0,0%,95%);width:140px;min-height:40px;line-height:40px;background-repeat:no-repeat;text-align:center}\n.SignUpPage_CaptchaItem-image.error{background-color:hsl(350,70%,55%);color:white}.SignUpPage_CaptchaItem-reloadButton{display:inline-block;vertical-align:top;margin-left:10px;width:110px}.SignUpPage_CaptchaItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);margin-top:10px;min-height:40px}.SignUpPage_CaptchaItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_CaptchaItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.SignUpPage_CaptchaItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_CaptchaItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SignUpPage_Page{overflow:auto;text-align:center}.SignUpPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.SignUpPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:300px;background-color:white;padding:20px;text-align:left}\n.SignUpPage_Page-title{text-align:center;color:hsl(95,60%,55%);font-size:22px;line-height:44px;margin-top:34px}.SignUpPage_Page-button{margin-top:10px;width:100%}.SignUpPage_Page-offerUsername{font-weight:bold}.SignUpPage_PasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.SignUpPage_PasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_PasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.SignUpPage_PasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_PasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SignUpPage_RepeatPasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.SignUpPage_RepeatPasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}\n.SignUpPage_RepeatPasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.SignUpPage_RepeatPasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_RepeatPasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SignUpPage_TimezoneItem-select{vertical-align:top;background-color:white;padding:5px 10px;cursor:pointer;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px;background-repeat:no-repeat;background-position:calc(100% - 10px) center;appearance:none}.SignUpPage_TimezoneItem-select:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.SignUpPage_TimezoneItem-select[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_UsernameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.SignUpPage_UsernameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}\n.SignUpPage_UsernameItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.SignUpPage_UsernameItem-input[disabled]{background-color:hsl(0,0%,95%)}.SignUpPage_UsernameItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.SmileysPage_Page{position:absolute;top:0;right:0;bottom:0;left:0;background-color:hsla(95,60%,55%,0.5);overflow:auto;text-align:center}.SmileysPage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.SmileysPage_Page-frame{display:inline-block;vertical-align:middle;position:relative;width:240px;margin:auto;background-color:white;padding:20px;padding-top:10px;text-align:left}.SmileysPage_Page-title{color:hsl(95,60%,55%);font-size:22px;line-height:44px;overflow:hidden;text-overflow:ellipsis;margin-right:54px}.SmileysPage_Page-button{display:inline-block;vertical-align:top;width:40px;height:40px;cursor:pointer;background-repeat:no-repeat;background-position:center}.SmileysPage_Page-button:focus{background-color:hsl(95,70%,95%)}\n.SmileysPage_Page-button:active{background-color:hsl(95,70%,85%)}.UserInfoPanel_Panel-empty{color:hsl(0,0%,70%);text-align:center}.UserInfoPanel_Panel-empty.hidden{display:none}.UserInfoPanel_Panel-item{overflow:hidden;text-overflow:ellipsis;line-height:20px}.UserInfoPanel_Panel-item.hidden{display:none}.WelcomePage_Page{overflow:auto;text-align:center}.WelcomePage_Page-aligner{display:inline-block;vertical-align:middle;height:100%}.WelcomePage_Page-frame{display:inline-block;vertical-align:middle;width:300px;background-color:white;padding:20px;text-align:left}.WelcomePage_Page-logoWrapper{padding-top:10px;text-align:center}.WelcomePage_Page-logo{display:inline-block;width:112px;height:154px;vertical-align:top}.WelcomePage_Page-warning{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px;text-align:center}.WelcomePage_Page-signInForm{border-bottom:1px solid hsl(0,0%,90%);padding-bottom:10px}.WelcomePage_Page-signInButton{margin-top:10px;width:100%}\n.WelcomePage_Page-signUpButton{width:100%}.WelcomePage_PasswordItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.WelcomePage_PasswordItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.WelcomePage_PasswordItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.WelcomePage_PasswordItem-input[disabled]{background-color:hsl(0,0%,95%)}.WelcomePage_PasswordItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.WelcomePage_Privacy{margin-top:10px;margin-bottom:-10px;text-align:center}.WelcomePage_Privacy-privacy{display:inline-block;vertical-align:top;color:hsl(95,60%,55%);cursor:pointer;padding-right:4px;padding-left:4px}.WelcomePage_Privacy-privacy:focus{background-color:hsl(95,70%,95%)}.WelcomePage_Privacy-privacy:active{background-color:hsl(95,70%,85%)}.WelcomePage_PublicLine{text-align:center;position:relative;height:30px;margin-bottom:10px}\n.WelcomePage_PublicLine-item{position:absolute;top:0;right:0;left:0;opacity:0;transition:opacity .6s ease-in-out}.WelcomePage_PublicLine-item.visible{opacity:1}.WelcomePage_StaySignedInItem{margin-top:10px;line-height:25px}.WelcomePage_StaySignedInItem-click{display:inline-block;vertical-align:top;cursor:pointer}.WelcomePage_StaySignedInItem-click.disabled{cursor:default}.WelcomePage_StaySignedInItem-button{display:inline-block;vertical-align:top;width:25px;height:25px;line-height:21px;border:2px solid hsl(0,0%,70%);margin-right:5px;cursor:pointer;text-align:center;background-position:center;background-repeat:no-repeat}.WelcomePage_StaySignedInItem-button:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.WelcomePage_StaySignedInItem-button:active{background-color:hsl(95,70%,85%)}.WelcomePage_StaySignedInItem-button.checked{color:white;border-color:hsl(95,60%,55%);background-color:hsl(95,60%,55%)}.WelcomePage_StaySignedInItem-button.checked:focus{background-color:hsl(95,60%,65%)}\n.WelcomePage_StaySignedInItem-button.checked:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.WelcomePage_StaySignedInItem-button[disabled]{background-color:hsl(0,0%,95%);cursor:default}.WelcomePage_UnauthenticatedWarning-username{font-weight:bold}.WelcomePage_UsernameItem-input{background-color:white;padding:5px 10px;cursor:text;width:100%;border:2px solid hsl(0,0%,70%);min-height:40px}.WelcomePage_UsernameItem-input:focus{border-color:hsl(95,60%,55%);background-color:hsl(95,70%,95%)}.WelcomePage_UsernameItem-input.error{border-color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.WelcomePage_UsernameItem-input[disabled]{background-color:hsl(0,0%,95%)}.WelcomePage_UsernameItem-error{color:hsl(350,70%,55%);white-space:normal;line-height:20px;padding-top:5px;padding-bottom:5px}.WorkPage_Page{overflow-y:auto}.WorkPage_ChatPanel_CloseButton{position:absolute;top:20px;right:20px;width:44px;height:44px;cursor:pointer;background-position:center;background-repeat:no-repeat}\n.WorkPage_ChatPanel_CloseButton:focus{background-color:hsl(95,70%,95%)}.WorkPage_ChatPanel_CloseButton:active{background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_ContactMenu{position:absolute;top:100%;left:0;background-color:white;border:2px solid hsl(95,60%,55%);z-index:1;min-width:100%;margin-top:-2px}.WorkPage_ChatPanel_ContactMenu-item{line-height:40px;padding-left:12px;padding-right:12px;cursor:pointer}.WorkPage_ChatPanel_ContactMenu-item.active,.WorkPage_ChatPanel_ContactMenu-item:active{background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_DaySeparator{color:hsl(0,0%,70%);text-align:center;margin-bottom:-10px}.WorkPage_ChatPanel_DaySeparator:first-child{margin-top:-10px}.WorkPage_ChatPanel_MessagesPanel{position:absolute;top:64px;right:0;bottom:0;left:0;max-width:600px;margin-right:auto;margin-left:auto}.WorkPage_ChatPanel_MessagesPanel-content{position:absolute;top:0;right:20px;left:20px;overflow:auto}.WorkPage_ChatPanel_MessagesPanel-doneMessages{padding-top:10px;padding-bottom:10px}\n.WorkPage_ChatPanel_MessagesPanel-sendingMessages{padding-bottom:10px}.WorkPage_ChatPanel_MessagesPanel-sendingMessages.hidden{display:none}@media(max-height:300px){.WorkPage_ChatPanel_MessagesPanel.typing{top:0}}@media(orientation:landscape){.WorkPage_ChatPanel_MessagesPanel-content{bottom:64px}}@media(orientation:portrait),(min-height:400px){.WorkPage_ChatPanel_MessagesPanel-content{bottom:118px}}.WorkPage_ChatPanel_MessageText-link{color:hsl(95,60%,55%);cursor:pointer;white-space:pre-wrap}.WorkPage_ChatPanel_MessageText-link:hover{text-decoration:underline}.WorkPage_ChatPanel_MessageText-disabledLink{white-space:pre-wrap}.WorkPage_ChatPanel_MoreButton{position:absolute;top:0;width:70px}.WorkPage_ChatPanel_MoreButton-button{padding-top:5px;padding-right:12px;padding-bottom:5px;width:100%;color:white;text-align:center;cursor:pointer;background-color:hsl(45,75%,55%);background-position:calc(100% - 10px) center;background-repeat:no-repeat;border:2px solid hsl(45,75%,55%)}.WorkPage_ChatPanel_MoreButton-button.not_pressed:focus{background-color:hsl(45,75%,65%)}\n.WorkPage_ChatPanel_MoreButton-button.pressed:active,.WorkPage_ChatPanel_MoreButton-button.not_pressed:active{color:hsl(45,75%,70%);background-color:hsl(45,75%,35%);border-color:hsl(45,75%,35%)}.WorkPage_ChatPanel_MoreButton-button[disabled]{color:white;background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}@media(orientation:landscape){.WorkPage_ChatPanel_MoreButton{right:80px}}@media(orientation:landscape) and (max-width:400px){.WorkPage_ChatPanel_MoreButton.typeFocused{display:none}}@media(orientation:portrait),(min-height:400px){.WorkPage_ChatPanel_MoreButton{right:0}}.WorkPage_ChatPanel_MoreMenu{position:absolute;right:0;bottom:100%;background-color:white;border:2px solid hsl(45,75%,55%);z-index:1;min-width:100%;margin-top:-2px}.WorkPage_ChatPanel_MoreMenu-item{position:relative;line-height:40px;padding-left:12px;padding-right:12px;cursor:pointer;overflow:hidden}.WorkPage_ChatPanel_MoreMenu-item.active,.WorkPage_ChatPanel_MoreMenu-item:active{background-color:hsl(45,75%,90%)}\n.WorkPage_ChatPanel_MoreMenu-fileInput{position:absolute;top:0;left:0;width:100%;height:100%;cursor:inherit;opacity:0}.WorkPage_ChatPanel_MoreMenu-fileInput::-webkit-file-upload-button{display:none}.WorkPage_ChatPanel_Panel{position:absolute;top:0;right:0;bottom:0;left:0;padding:20px;background-color:white;background-position:center bottom;background-repeat:repeat-x}@media(max-height:300px){.WorkPage_ChatPanel_Panel-bar.hidden{display:none}}@media(min-width:660px){.WorkPage_ChatPanel_Panel{left:330px;border-left:2px solid hsl(95,70%,85%)}}@media(min-width:1100px){.WorkPage_ChatPanel_Panel{left:400px}}.WorkPage_ChatPanel_ReceivedFileMessage{background-color:hsl(95,70%,85%);padding:12px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_ReceivedFileMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_ReceivedFileMessage-item{position:relative;white-space:pre-wrap}.WorkPage_ChatPanel_ReceivedFileMessage-item.withReceiveLink{padding-left:90px}.WorkPage_ChatPanel_ReceivedFileMessage-item-size{color:hsl(0,0%,70%)}\n.WorkPage_ChatPanel_ReceivedFileMessage-receiveLink{position:absolute;top:0;left:0;cursor:pointer;width:80px;color:white;background-color:hsl(95,60%,55%);text-align:center;line-height:40px}.WorkPage_ChatPanel_ReceivedFileMessage-receiveLink:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%)}.WorkPage_ChatPanel_ReceivedTextMessage{background-color:hsl(95,70%,85%);padding:12px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_ReceivedTextMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_ReceivedTextMessage-item{white-space:pre-wrap}.WorkPage_ChatPanel_SendingFileMessage{border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;white-space:pre-wrap;line-height:20px;background-color:white;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SendingFileMessage:first-child{margin-top:0}.WorkPage_ChatPanel_SendingFileMessage-size{color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SendingTextMessage{border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;line-height:20px;background-color:white;color:hsl(0,0%,70%)}\n.WorkPage_ChatPanel_SendingTextMessage:first-child{margin-top:0}.WorkPage_ChatPanel_SendingTextMessage-item{white-space:pre-wrap}.WorkPage_ChatPanel_SentFileMessage{background-color:white;border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_SentFileMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SentFileMessage-item{position:relative;white-space:pre-wrap}.WorkPage_ChatPanel_SentFileMessage-item.sending{padding-left:90px}.WorkPage_ChatPanel_SentFileMessage-item-size{color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SentFileMessage-item-sending{position:absolute;top:0;left:0;width:80px;text-align:center;line-height:40px;background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_SentFileMessage-item-sending.failed{color:hsl(350,70%,55%);background-color:hsl(350,80%,95%)}.WorkPage_ChatPanel_SentFileMessage-item-sending-done{position:absolute;top:0;left:0;width:0;overflow:hidden}.WorkPage_ChatPanel_SentFileMessage-item-sending-done-text{width:80px;background-color:hsl(95,60%,55%);color:white}\n.WorkPage_ChatPanel_SentFileMessage-item-sending-done-text.failed{background-color:hsl(350,70%,55%)}.WorkPage_ChatPanel_SentTextMessage{background-color:white;border:2px solid hsl(95,70%,85%);padding:10px;margin-top:10px;line-height:20px}.WorkPage_ChatPanel_SentTextMessage-time{float:right;text-align:right;line-height:20px;color:hsl(0,0%,70%)}.WorkPage_ChatPanel_SentTextMessage-item{white-space:pre-wrap}.WorkPage_ChatPanel_Smiley{display:inline-block;vertical-align:top;width:20px;height:20px}.WorkPage_ChatPanel_Title{display:inline-block;vertical-align:top;position:relative}.WorkPage_ChatPanel_Title-button{display:inline-block;vertical-align:top;cursor:pointer;line-height:40px;padding-right:26px;padding-left:14px;background-repeat:no-repeat;background-position:calc(100% - 14px) center}.WorkPage_ChatPanel_Title-button:focus{background-color:hsl(95,70%,95%)}.WorkPage_ChatPanel_Title-button:active{background-color:hsl(95,70%,85%)}.WorkPage_ChatPanel_Title-button.pressed{color:white;background-color:hsl(95,60%,55%)}\n.WorkPage_ChatPanel_Title-button.pressed:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%)}.WorkPage_ChatPanel_Title-buttonText{display:inline-block;vertical-align:top;max-width:132px;overflow:hidden;text-overflow:ellipsis;cursor:inherit;font-weight:bold}@media(min-width:400px){.WorkPage_ChatPanel_Title-buttonText{max-width:182px}}@media(min-width:660px){.WorkPage_ChatPanel_Title-buttonText{max-width:144px}}@media(min-width:800px){.WorkPage_ChatPanel_Title-buttonText{max-width:182px}}.WorkPage_ChatPanel_TypePanel{position:absolute;right:20px;bottom:20px;left:20px}.WorkPage_ChatPanel_TypePanel-textarea{padding:10px;display:inline-block;vertical-align:top;height:100%;cursor:text;line-height:20px;border-top:2px solid hsl(0,0%,70%);white-space:pre-wrap}.WorkPage_ChatPanel_TypePanel-textarea:focus{border-color:hsl(95,60%,55%)}.WorkPage_ChatPanel_TypePanel-sendButton{position:absolute;right:0;bottom:0;width:70px}@media(orientation:landscape){.WorkPage_ChatPanel_TypePanel{height:40px}\n.WorkPage_ChatPanel_TypePanel-textarea{width:calc(100% - 160px)}}@media(orientation:landscape) and (max-width:400px){.WorkPage_ChatPanel_TypePanel-textarea:focus{width:calc(100% - 80px)}}@media(orientation:portrait),(min-height:400px){.WorkPage_ChatPanel_TypePanel{height:90px}.WorkPage_ChatPanel_TypePanel-textarea{width:calc(100% - 80px)}}.WorkPage_SidePanel_AccountMenu{position:absolute;top:100%;left:0;background-color:white;border:2px solid hsl(95,60%,55%);z-index:1;min-width:100%;margin-top:-2px}.WorkPage_SidePanel_AccountMenu-item{line-height:40px;padding-left:12px;padding-right:12px;cursor:pointer}.WorkPage_SidePanel_AccountMenu-item.active,.WorkPage_SidePanel_AccountMenu-item:active{background-color:hsl(95,70%,85%)}.WorkPage_SidePanel_Contact{display:block;width:100%;line-height:60px;cursor:pointer;padding-right:18px;padding-left:62px;background-repeat:no-repeat;background-position:left center;overflow:hidden;text-overflow:ellipsis;position:relative;border:2px solid transparent}.WorkPage_SidePanel_Contact.withMessages{padding-right:60px}\n.WorkPage_SidePanel_Contact.offline{color:hsl(0,0%,70%)}.WorkPage_SidePanel_Contact:focus{background-color:hsl(95,70%,95%)}.WorkPage_SidePanel_Contact:active{background-color:hsl(95,70%,85%)}.WorkPage_SidePanel_Contact.selected{color:white;background-color:hsl(95,60%,55%);border-color:hsl(95,60%,55%)}.WorkPage_SidePanel_Contact.selected:focus{background-color:hsl(95,60%,65%)}.WorkPage_SidePanel_Contact.selected:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%);border-color:hsl(95,60%,35%)}.WorkPage_SidePanel_Contact.selected:disabled{background-color:hsl(0,0%,70%);border-color:hsl(0,0%,70%);cursor:default}.WorkPage_SidePanel_Contact-number{position:absolute;top:15px;right:18px;min-width:30px;line-height:30px;background-color:hsl(350,70%,55%);color:white;display:none;text-align:center;padding-right:4px;padding-left:4px}.WorkPage_SidePanel_Contact-number.visible{display:block}@media(min-width:660px){.WorkPage_SidePanel_Contact{width:calc(100% + 20px)}\n}.WorkPage_SidePanel_ContactList{position:absolute;top:64px;right:0;bottom:0;left:0;overflow-y:auto}.WorkPage_SidePanel_ContactList-content{padding-top:10px;padding-bottom:20px;padding-left:20px;padding-right:20px}.WorkPage_SidePanel_ContactList-empty{color:hsl(0,0%,70%);padding:30px;text-align:center}.WorkPage_SidePanel_Panel{position:absolute;top:0;bottom:0;left:0;width:100%;padding:20px;background-color:white;background-position:center bottom;background-repeat:repeat-x}.WorkPage_SidePanel_Panel.chatOpen{display:none}.WorkPage_SidePanel_Panel-addContactButton{position:absolute;top:20px;right:20px;width:118px}@media(min-width:660px){.WorkPage_SidePanel_Panel{width:330px}.WorkPage_SidePanel_Panel.chatOpen{display:block}}@media(min-width:1100px){.WorkPage_SidePanel_Panel{width:400px}}.WorkPage_SidePanel_Title{display:inline-block;vertical-align:top;position:relative}.WorkPage_SidePanel_Title-button{display:inline-block;vertical-align:top;cursor:pointer;line-height:40px;padding-right:26px;padding-left:14px;background-repeat:no-repeat;background-position:calc(100% - 14px) center}\n.WorkPage_SidePanel_Title-button:focus{background-color:hsl(95,70%,95%)}.WorkPage_SidePanel_Title-button:active{background-color:hsl(95,70%,85%)}.WorkPage_SidePanel_Title-button.pressed{color:white;background-color:hsl(95,60%,55%)}.WorkPage_SidePanel_Title-button.pressed:active{color:hsl(95,70%,70%);background-color:hsl(95,60%,35%)}.WorkPage_SidePanel_Title-buttonText{display:inline-block;vertical-align:top;max-width:102px;overflow:hidden;text-overflow:ellipsis;cursor:inherit;font-weight:bold}@media(min-width:400px){.WorkPage_SidePanel_Title-buttonText{max-width:182px}}@media(min-width:660px){.WorkPage_SidePanel_Title-buttonText{max-width:114px}}@media(min-width:1100px){.WorkPage_SidePanel_Title-buttonText{max-width:182px}}"
    return element
})())

const getResourceUrl = (() => {
    const urls = {"img/arrow-collapsed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI5IiB3aWR0aD0iOSI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0My4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggaWQ9InBhdGgyOTg3IiBkPSJtIDEsMTA0My44NjIyIDAsOCA3LC00IHoiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/arrow-expanded.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI5IiB3aWR0aD0iOSI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0My4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggaWQ9InBhdGgyOTg3IiBkPSJtIDguNSwxMDQ0LjM2MjIgLTgsMCA0LDcgeiIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/arrow-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI4IiB3aWR0aD0iOCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0NC4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQ0LjM2MjIpIiBpZD0icGF0aDI5ODciIGQ9Ik0gMCwxIDgsMSA0LDggeiIgc3R5bGU9ImZpbGw6I2E5ZTg3YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/arrow-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI4IiB3aWR0aD0iOCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0NC4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQ0LjM2MjIpIiBpZD0icGF0aDI5ODciIGQ9Ik0gMCwxIDgsMSA0LDggeiIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/arrow-up.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI4IiB3aWR0aD0iOCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0NC4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggaWQ9InBhdGgyOTg3IiBkPSJtIDAsMTA1MS4zNjIyIDgsMCAtNCwtNyB6IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/arrow-up-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI4IiB3aWR0aD0iOCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0NC4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggaWQ9InBhdGgyOTg3IiBkPSJtIDAsMTA1MS4zNjIyIDgsMCAtNCwtNyB6IiBzdHlsZT0iZmlsbDojZTJiNzM2O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/arrow.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI4IiB3aWR0aD0iOCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTA0NC4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDQ0LjM2MjIpIiBpZD0icGF0aDI5ODciIGQ9Ik0gMCwxIDgsMSA0LDggeiIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/checked/active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSIxNyIgd2lkdGg9IjE3Ij48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDM1LjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDI5ODciIGQ9Im0gMCwxMDQzLjg2MjIgNiw2IDExLC0xMCAtMiwtMiAtOSw4IC00LC00IHoiIHN0eWxlPSJjb2xvcjojMDAwMDAwO2ZpbGw6I2E5ZTg3YztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTttYXJrZXI6bm9uZTt2aXNpYmlsaXR5OnZpc2libGU7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIi8+PC9nPjwvc3ZnPgo=","img/checked/normal.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSIxNyIgd2lkdGg9IjE3Ij48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDM1LjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDI5ODciIGQ9Im0gMCwxMDQzLjg2MjIgNiw2IDExLC0xMCAtMiwtMiAtOSw4IC00LC00IHoiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/close.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSIxMiIgd2lkdGg9IjEyIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDQwLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwNDAuMzYyMikiIGlkPSJwYXRoMzc1OSIgZD0ibSAwLDEwIDIsMiA0LC00IDQsNCAyLC0yIEwgOCw2IDEyLDIgMTAsMCA2LDQgMiwwIDAsMiA0LDYgeiIgc3R5bGU9ImZpbGw6IzgwZDE0NztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjE7ZmlsbC1vcGFjaXR5OjEiLz48L2c+PC9zdmc+Cg==","img/clouds.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSIzMDAiIHdpZHRoPSI3MDAiPjxkZWZzIGlkPSJkZWZzNCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsLTc1Mi4zNjIxOCkiIGlkPSJsYXllcjEiPjxwYXRoIHRyYW5zZm9ybT0ibWF0cml4KDIuNDAwMzg4MSwwLDAsMi40MDAzODgxLC0xODYuNzM4MTYsODQxLjAwMjU3KSIgZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIGlkPSJwYXRoMzc1OSIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2MSIgZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDQuMDAwNjQ3MiwwLDAsNC4wMDA2NDcyLC0yNzguMDQ1OTcsNzQzLjc3NzA4KSIvPjxwYXRoIHRyYW5zZm9ybT0ibWF0cml4KDMuMjAwNTE4MiwwLDAsMy4yMDA1MTgyLC05NC4wMTYyNSw3OTcuNzAyNTQpIiBkPSJtIDExMCw1NSBhIDE1LDE1IDAgMSAxIC0zMCwwIDE1LDE1IDAgMSAxIDMwLDAgeiIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzY1IiBkPSJtIDExMCw1NSBhIDE1LDE1IDAgMSAxIC0zMCwwIDE1LDE1IDAgMSAxIDMwLDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMi40MDAzODgxLDAsMCwyLjQwMDM4ODEsLTc4LjAxMzU5LDg1NS43OTUxOSkiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgyLjQwMDM4ODEsMCwwLDIuNDAwMzg4MSwtNjYuMDExNjUsNzk1Ljc4NTQ5KSIgZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIGlkPSJwYXRoMzc2NyIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2OSIgZD0ibSAxMTAsNTUgYSAxNSwxNSAwIDEgMSAtMzAsMCAxNSwxNSAwIDEgMSAzMCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuNjAwMjU4NywwLDAsMS42MDAyNTg3LDExMS42NTQxNiw4ODYuNDA1MDIpIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU0LC0xNDApIiBpZD0iZzM3NjUiPjxwYXRoIHRyYW5zZm9ybT0ibWF0cml4KDIuNDAwMzg4MSwwLDAsMi40MDAzODgxLDIyOC40NjYzNyw4NDguOTg0NzEpIiBkPSJtIDExMCw1NSBjIDAsOC4yODQyNzEgLTYuNzE1NzMsMTUgLTE1LDE1IC04LjI4NDI3MSwwIC0xNSwtNi43MTU3MjkgLTE1LC0xNSAwLC04LjI4NDI3MSA2LjcxNTcyOSwtMTUgMTUsLTE1IDguMjg0MjcsMCAxNSw2LjcxNTcyOSAxNSwxNSB6IiBpZD0icGF0aDM3NzEiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDM3NzMiIGQ9Im0gMTEwLDU1IGMgMCw4LjI4NDI3MSAtNi43MTU3MywxNSAtMTUsMTUgLTguMjg0MjcxLDAgLTE1LC02LjcxNTcyOSAtMTUsLTE1IDAsLTguMjg0MjcxIDYuNzE1NzI5LC0xNSAxNSwtMTUgOC4yODQyNywwIDE1LDYuNzE1NzI5IDE1LDE1IHoiIHRyYW5zZm9ybT0ibWF0cml4KDQuMDAwNjQ3MiwwLDAsNC4wMDA2NDcyLDIzMi4zNTA2Miw3NTMuNDUzMSkiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgzLjIwMDUxODIsMCwwLDMuMjAwNTE4MiwyNjkuNTYxMjQsODE4LjU1NjIyKSIgZD0ibSAxMTAsNTUgYyAwLDguMjg0MjcxIC02LjcxNTczLDE1IC0xNSwxNSAtOC4yODQyNzEsMCAtMTUsLTYuNzE1NzI5IC0xNSwtMTUgMCwtOC4yODQyNzEgNi43MTU3MjksLTE1IDE1LC0xNSA4LjI4NDI3LDAgMTUsNi43MTU3MjkgMTUsMTUgeiIgaWQ9InBhdGgzNzc1IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzc3IiBkPSJtIDExMCw1NSBjIDAsOC4yODQyNzEgLTYuNzE1NzMsMTUgLTE1LDE1IC04LjI4NDI3MSwwIC0xNSwtNi43MTU3MjkgLTE1LC0xNSAwLC04LjI4NDI3MSA2LjcxNTcyOSwtMTUgMTUsLTE1IDguMjg0MjcsMCAxNSw2LjcxNTcyOSAxNSwxNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgyLjQwMDM4ODEsMCwwLDIuNDAwMzg4MSwzMTUuMjY3Miw4MTYuMjQ4NjUpIi8+PHBhdGggdHJhbnNmb3JtPSJtYXRyaXgoMy4wMjI3NDI4LDAsMCwzLjAyMjc0MjgsMjE4LjA3NDIsODIyLjE1MTkyKSIgZD0ibSAxMTAsNTUgYyAwLDguMjg0MjcxIC02LjcxNTczLDE1IC0xNSwxNSAtOC4yODQyNzEsMCAtMTUsLTYuNzE1NzI5IC0xNSwtMTUgMCwtOC4yODQyNzEgNi43MTU3MjksLTE1IDE1LC0xNSA4LjI4NDI3LDAgMTUsNi43MTU3MjkgMTUsMTUgeiIgaWQ9InBhdGgzNzc5IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzgxIiBkPSJtIDExMCw1NSBhIDE1LDE1IDAgMSAxIC0zMCwwIDE1LDE1IDAgMSAxIDMwLDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMi4zMzU3Njg3LDAsMCwyLjMzNTc2ODcsNDU1LjcxMTg5LDg1OC43MzgxOCkiLz48L2c+PC9nPjwvc3ZnPgo=","img/grass.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMTUwIiB3aWR0aD0iNTAwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05MDIuMzYyMTgpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjQ4NDIxNDQyLDAsMCwwLjQ4NDIxNDQyLDI3LjM5MTMwNSw4ODEuODgwNDcpIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzU3IiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNDAzNTEyMDEsMCwwLDAuNDAzNTEyMDEsNzcuODI2MDg2LDg4Ni45NjA3NikiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjUzODUwNzM0LDAsMCwwLjUzODUwNzM0LDIxNS40NjI1OCw4NTYuNzM1MTQpIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIGlkPSJwYXRoMzc1OSIgc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2MSIgZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGMgMCw1MS4zMjYwNSAtNDEuNjA3OTg1NSw5Mi45MzQwMyAtOTIuOTM0MDM2LDkyLjkzNDAzIC01MS4zMjYwNDgsMCAtOTIuOTM0MDM4LC00MS42MDc5OCAtOTIuOTM0MDM4LC05Mi45MzQwMyAwLC01MS4zMjYwNSA0MS42MDc5OSwtOTIuOTM0MDQgOTIuOTM0MDM4LC05Mi45MzQwNCA1MS4zMjYwNTA1LDAgOTIuOTM0MDM2LDQxLjYwNzk5IDkyLjkzNDAzNiw5Mi45MzQwNCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjcwOTQ3MTM1LDAsMCwwLjcwOTQ3MTM1LDMxMi4wODgxLDgwOC41MjkzOCkiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjUwMjMzNTE4LDAsMCwwLjUwMjMzNTE4LDM4OC40MTYzNyw4MzguNDk0MzcpIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2NSIgZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGMgMCw1MS4zMjYwNSAtNDEuNjA3OTg1NSw5Mi45MzQwMyAtOTIuOTM0MDM2LDkyLjkzNDAzIC01MS4zMjYwNDgsMCAtOTIuOTM0MDM4LC00MS42MDc5OCAtOTIuOTM0MDM4LC05Mi45MzQwMyAwLC01MS4zMjYwNSA0MS42MDc5OSwtOTIuOTM0MDQgOTIuOTM0MDM4LC05Mi45MzQwNCA1MS4zMjYwNTA1LDAgOTIuOTM0MDM2LDQxLjYwNzk5IDkyLjkzNDAzNiw5Mi45MzQwNCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjQ0ODQzNzUzLDAsMCwwLjQ0ODQzNzUzLDQ1NS4zNjc0Niw4NjguNTY1ODQpIi8+PHVzZSBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxIiBoZWlnaHQ9IjMwMCIgd2lkdGg9IjUwMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTAwLC01LjQ2MzQ1NzNlLTYpIiBpZD0idXNlMzc2NyIgeGxpbms6aHJlZj0iI3BhdGgyOTg3IiB5PSIwIiB4PSIwIi8+PHBhdGggdHJhbnNmb3JtPSJtYXRyaXgoMC4zNzY2MTEyMSwwLDAsMC4zNzY2MTEyMSwxMzEuMzA0MzUsOTAxLjk4NzUyKSIgZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGMgMCw1MS4zMjYwNSAtNDEuNjA3OTg1NSw5Mi45MzQwMyAtOTIuOTM0MDM2LDkyLjkzNDAzIC01MS4zMjYwNDgsMCAtOTIuOTM0MDM4LC00MS42MDc5OCAtOTIuOTM0MDM4LC05Mi45MzQwMyAwLC01MS4zMjYwNSA0MS42MDc5OSwtOTIuOTM0MDQgOTIuOTM0MDM4LC05Mi45MzQwNCA1MS4zMjYwNTA1LDAgOTIuOTM0MDM2LDQxLjYwNzk5IDkyLjkzNDAzNiw5Mi45MzQwNCB6IiBpZD0icGF0aDM3NjkiIHN0eWxlPSJmaWxsOiNiOGVjOTM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjcwOTQ3MTM1LDAsMCwwLjcwOTQ3MTM1LDM5NC4wODgxLDg1MS41MjkzOCkiIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBjIDAsNTEuMzI2MDUgLTQxLjYwNzk4NTUsOTIuOTM0MDMgLTkyLjkzNDAzNiw5Mi45MzQwMyAtNTEuMzI2MDQ4LDAgLTkyLjkzNDAzOCwtNDEuNjA3OTggLTkyLjkzNDAzOCwtOTIuOTM0MDMgMCwtNTEuMzI2MDUgNDEuNjA3OTksLTkyLjkzNDA0IDkyLjkzNDAzOCwtOTIuOTM0MDQgNTEuMzI2MDUwNSwwIDkyLjkzNDAzNiw0MS42MDc5OSA5Mi45MzQwMzYsOTIuOTM0MDQgeiIgaWQ9InBhdGgzNzcxIiBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTkyIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC43MDk0NzEzNSwwLDAsMC43MDk0NzEzNSwxMzUuMTMzNzYsODYyLjkyMDIzKSIvPjxwYXRoIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsMjQ5LjE5OTcyLDg1OC44NTQyNykiIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBjIDAsNTEuMzI2MDUgLTQxLjYwNzk4NTUsOTIuOTM0MDMgLTkyLjkzNDAzNiw5Mi45MzQwMyAtNTEuMzI2MDQ4LDAgLTkyLjkzNDAzOCwtNDEuNjA3OTggLTkyLjkzNDAzOCwtOTIuOTM0MDMgMCwtNTEuMzI2MDUgNDEuNjA3OTksLTkyLjkzNDA0IDkyLjkzNDAzOCwtOTIuOTM0MDQgNTEuMzI2MDUwNSwwIDkyLjkzNDAzNiw0MS42MDc5OSA5Mi45MzQwMzYsOTIuOTM0MDQgeiIgaWQ9InBhdGgyOTk0IiBzdHlsZT0iZmlsbDojYjhlYzkzO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2I4ZWM5MztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk2IiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsNDY5LjE5OTcyLDg0OC44NTQyNykiLz48L2c+PC9zdmc+Cg==","img/icon/128.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ij48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05MjQuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0ibWF0cml4KDgsMCwwLDgsMCwtNzM2Ni41MzU0KSIgaWQ9ImczNzYwIj48cGF0aCBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDM3NTkiIGQ9Im0gMTIuNSwzLjUgYyAwLDEuNjU2ODU0MiAtMS4zNDMxNDYsMyAtMywzIC0xLjY1Njg1NDIsMCAtMywtMS4zNDMxNDU4IC0zLC0zIDAsLTEuNjU2ODU0MiAxLjM0MzE0NTgsLTMgMywtMyAxLjY1Njg1NCwwIDMsMS4zNDMxNDU4IDMsMyB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjE2NjY2NjcsMCwwLDEuMTY2NjY2NywtMi4wODMzMzM1LDEwMzUuNzc4OSkiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc1OSIgaWQ9InVzZTM3NjEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01LjUsNS40OTk5NzAyKSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2Ii8+PHBhdGggc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2ZmZmZmZjtzdHJva2Utb3BhY2l0eToxIiBpZD0icGF0aDI5ODkiIGQ9Im0gMTUuNSw5LjUgYyAwLDMuMzEzNzA4IC0yLjY4NjI5Miw2IC02LDYgLTMuMzEzNzA4NSwwIC02LC0yLjY4NjI5MiAtNiwtNiAwLC0zLjMxMzcwODUgMi42ODYyOTE1LC02IDYsLTYgMy4zMTM3MDgsMCA2LDIuNjg2MjkxNSA2LDYgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM1Ljg2MjIpIi8+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM2LjM2MjIpIiBkPSJNIDEyLDMuNSBDIDEyLDQuODgwNzExOSAxMC44ODA3MTIsNiA5LjUsNiA4LjExOTI4ODEsNiA3LDQuODgwNzExOSA3LDMuNSA3LDIuMTE5Mjg4MSA4LjExOTI4ODEsMSA5LjUsMSAxMC44ODA3MTIsMSAxMiwyLjExOTI4ODEgMTIsMy41IHoiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzYzIiBpZD0idXNlMzc2NSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjUpIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiLz48cGF0aCBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDM3NjciIGQ9Ik0gOCw5IEMgOCw5LjU1MjI4NDcgNy41NTIyODQ3LDEwIDcsMTAgNi40NDc3MTUzLDEwIDYsOS41NTIyODQ3IDYsOSA2LDguNDQ3NzE1MyA2LjQ0NzcxNTMsOCA3LDggNy41NTIyODQ3LDggOCw4LjQ0NzcxNTMgOCw5IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIsMTAzNC4zNjIyKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzY3IiBpZD0idXNlMzc2OSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIsMikiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxyZWN0IHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOm5vbmUiIGlkPSJyZWN0Mjk5MCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4PSIwIiB5PSIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzYuMzYyMikiLz48L2c+PC9nPjwvc3ZnPgo=","img/icon/16.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMTYiIHdpZHRoPSIxNiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzNi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczNzYwIj48cGF0aCBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDM3NTkiIGQ9Im0gMTIuNSwzLjUgYyAwLDEuNjU2ODU0MiAtMS4zNDMxNDYsMyAtMywzIC0xLjY1Njg1NDIsMCAtMywtMS4zNDMxNDU4IC0zLC0zIDAsLTEuNjU2ODU0MiAxLjM0MzE0NTgsLTMgMywtMyAxLjY1Njg1NCwwIDMsMS4zNDMxNDU4IDMsMyB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjE2NjY2NjcsMCwwLDEuMTY2NjY2NywtMi4wODMzMzM1LDEwMzUuNzc4OSkiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc1OSIgaWQ9InVzZTM3NjEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01LjUsNS40OTk5NzAyKSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2Ii8+PHBhdGggc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2ZmZmZmZjtzdHJva2Utb3BhY2l0eToxIiBpZD0icGF0aDI5ODkiIGQ9Im0gMTUuNSw5LjUgYyAwLDMuMzEzNzA4IC0yLjY4NjI5Miw2IC02LDYgLTMuMzEzNzA4NSwwIC02LC0yLjY4NjI5MiAtNiwtNiAwLC0zLjMxMzcwODUgMi42ODYyOTE1LC02IDYsLTYgMy4zMTM3MDgsMCA2LDIuNjg2MjkxNSA2LDYgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM1Ljg2MjIpIi8+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSwxMDM2LjM2MjIpIiBkPSJNIDEyLDMuNSBDIDEyLDQuODgwNzExOSAxMC44ODA3MTIsNiA5LjUsNiA4LjExOTI4ODEsNiA3LDQuODgwNzExOSA3LDMuNSA3LDIuMTE5Mjg4MSA4LjExOTI4ODEsMSA5LjUsMSAxMC44ODA3MTIsMSAxMiwyLjExOTI4ODEgMTIsMy41IHoiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzYzIiBpZD0idXNlMzc2NSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjUpIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiLz48cGF0aCBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDM3NjciIGQ9Ik0gOCw5IEMgOCw5LjU1MjI4NDcgNy41NTIyODQ3LDEwIDcsMTAgNi40NDc3MTUzLDEwIDYsOS41NTIyODQ3IDYsOSA2LDguNDQ3NzE1MyA2LjQ0NzcxNTMsOCA3LDggNy41NTIyODQ3LDggOCw4LjQ0NzcxNTMgOCw5IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIsMTAzNC4zNjIyKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzY3IiBpZD0idXNlMzc2OSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIsMikiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxyZWN0IHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOm5vbmUiIGlkPSJyZWN0Mjk5MCIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiB4PSIwIiB5PSIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzYuMzYyMikiLz48L2c+PC9nPjwvc3ZnPgo=","img/icon/256.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjU2IiB3aWR0aD0iMjU2Ij48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC03OTYuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0ibWF0cml4KDE2LDAsMCwxNiwwLC0xNTc4NS40MzMpIiBpZD0iZzM3NjAiPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc1OSIgZD0ibSAxMi41LDMuNSBjIDAsMS42NTY4NTQyIC0xLjM0MzE0NiwzIC0zLDMgLTEuNjU2ODU0MiwwIC0zLC0xLjM0MzE0NTggLTMsLTMgMCwtMS42NTY4NTQyIDEuMzQzMTQ1OCwtMyAzLC0zIDEuNjU2ODU0LDAgMywxLjM0MzE0NTggMywzIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMTY2NjY2NywwLDAsMS4xNjY2NjY3LC0yLjA4MzMzMzUsMTAzNS43Nzg5KSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzU5IiBpZD0idXNlMzc2MSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjQ5OTk3MDIpIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiLz48cGF0aCBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZmZmZmZmO3N0cm9rZS1vcGFjaXR5OjEiIGlkPSJwYXRoMjk4OSIgZD0ibSAxNS41LDkuNSBjIDAsMy4zMTM3MDggLTIuNjg2MjkyLDYgLTYsNiAtMy4zMTM3MDg1LDAgLTYsLTIuNjg2MjkyIC02LC02IDAsLTMuMzEzNzA4NSAyLjY4NjI5MTUsLTYgNiwtNiAzLjMxMzcwOCwwIDYsMi42ODYyOTE1IDYsNiB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzUuODYyMikiLz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzYuMzYyMikiIGQ9Ik0gMTIsMy41IEMgMTIsNC44ODA3MTE5IDEwLjg4MDcxMiw2IDkuNSw2IDguMTE5Mjg4MSw2IDcsNC44ODA3MTE5IDcsMy41IDcsMi4xMTkyODgxIDguMTE5Mjg4MSwxIDkuNSwxIDEwLjg4MDcxMiwxIDEyLDIuMTE5Mjg4MSAxMiwzLjUgeiIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NjMiIGlkPSJ1c2UzNzY1IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNSkiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2NyIgZD0iTSA4LDkgQyA4LDkuNTUyMjg0NyA3LjU1MjI4NDcsMTAgNywxMCA2LjQ0NzcxNTMsMTAgNiw5LjU1MjI4NDcgNiw5IDYsOC40NDc3MTUzIDYuNDQ3NzE1Myw4IDcsOCA3LjU1MjI4NDcsOCA4LDguNDQ3NzE1MyA4LDkgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMiwxMDM0LjM2MjIpIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NjciIGlkPSJ1c2UzNzY5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwyKSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2Ii8+PHJlY3Qgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZSIgaWQ9InJlY3QyOTkwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHg9IjAiIHk9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzNi4zNjIyKSIvPjwvZz48L2c+PC9zdmc+Cg==","img/icon/32.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMzIiIHdpZHRoPSIzMiI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAyMC4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgdHJhbnNmb3JtPSJtYXRyaXgoMiwwLDAsMiwwLC0xMDUyLjM2MjIpIiBpZD0iZzM3NjAiPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc1OSIgZD0ibSAxMi41LDMuNSBjIDAsMS42NTY4NTQyIC0xLjM0MzE0NiwzIC0zLDMgLTEuNjU2ODU0MiwwIC0zLC0xLjM0MzE0NTggLTMsLTMgMCwtMS42NTY4NTQyIDEuMzQzMTQ1OCwtMyAzLC0zIDEuNjU2ODU0LDAgMywxLjM0MzE0NTggMywzIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMTY2NjY2NywwLDAsMS4xNjY2NjY3LC0yLjA4MzMzMzUsMTAzNS43Nzg5KSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzU5IiBpZD0idXNlMzc2MSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjQ5OTk3MDIpIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiLz48cGF0aCBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZmZmZmZmO3N0cm9rZS1vcGFjaXR5OjEiIGlkPSJwYXRoMjk4OSIgZD0ibSAxNS41LDkuNSBjIDAsMy4zMTM3MDggLTIuNjg2MjkyLDYgLTYsNiAtMy4zMTM3MDg1LDAgLTYsLTIuNjg2MjkyIC02LC02IDAsLTMuMzEzNzA4NSAyLjY4NjI5MTUsLTYgNiwtNiAzLjMxMzcwOCwwIDYsMi42ODYyOTE1IDYsNiB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzUuODYyMikiLz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzYuMzYyMikiIGQ9Ik0gMTIsMy41IEMgMTIsNC44ODA3MTE5IDEwLjg4MDcxMiw2IDkuNSw2IDguMTE5Mjg4MSw2IDcsNC44ODA3MTE5IDcsMy41IDcsMi4xMTkyODgxIDguMTE5Mjg4MSwxIDkuNSwxIDEwLjg4MDcxMiwxIDEyLDIuMTE5Mjg4MSAxMiwzLjUgeiIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NjMiIGlkPSJ1c2UzNzY1IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNSkiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2NyIgZD0iTSA4LDkgQyA4LDkuNTUyMjg0NyA3LjU1MjI4NDcsMTAgNywxMCA2LjQ0NzcxNTMsMTAgNiw5LjU1MjI4NDcgNiw5IDYsOC40NDc3MTUzIDYuNDQ3NzE1Myw4IDcsOCA3LjU1MjI4NDcsOCA4LDguNDQ3NzE1MyA4LDkgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMiwxMDM0LjM2MjIpIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NjciIGlkPSJ1c2UzNzY5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwyKSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2Ii8+PHJlY3Qgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZSIgaWQ9InJlY3QyOTkwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHg9IjAiIHk9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzNi4zNjIyKSIvPjwvZz48L2c+PC9zdmc+Cg==","img/icon/512.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iNTEyIiB3aWR0aD0iNTEyIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC01NDAuMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0ibWF0cml4KDMyLDAsMCwzMiwwLC0zMjYyMy4yMjgpIiBpZD0iZzM3NjAiPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc1OSIgZD0ibSAxMi41LDMuNSBjIDAsMS42NTY4NTQyIC0xLjM0MzE0NiwzIC0zLDMgLTEuNjU2ODU0MiwwIC0zLC0xLjM0MzE0NTggLTMsLTMgMCwtMS42NTY4NTQyIDEuMzQzMTQ1OCwtMyAzLC0zIDEuNjU2ODU0LDAgMywxLjM0MzE0NTggMywzIHoiIHRyYW5zZm9ybT0ibWF0cml4KDEuMTY2NjY2NywwLDAsMS4xNjY2NjY3LC0yLjA4MzMzMzUsMTAzNS43Nzg5KSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgzNzU5IiBpZD0idXNlMzc2MSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTUuNSw1LjQ5OTk3MDIpIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiLz48cGF0aCBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojZmZmZmZmO3N0cm9rZS1vcGFjaXR5OjEiIGlkPSJwYXRoMjk4OSIgZD0ibSAxNS41LDkuNSBjIDAsMy4zMTM3MDggLTIuNjg2MjkyLDYgLTYsNiAtMy4zMTM3MDg1LDAgLTYsLTIuNjg2MjkyIC02LC02IDAsLTMuMzEzNzA4NSAyLjY4NjI5MTUsLTYgNiwtNiAzLjMxMzcwOCwwIDYsMi42ODYyOTE1IDYsNiB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzUuODYyMikiLz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41LDEwMzYuMzYyMikiIGQ9Ik0gMTIsMy41IEMgMTIsNC44ODA3MTE5IDEwLjg4MDcxMiw2IDkuNSw2IDguMTE5Mjg4MSw2IDcsNC44ODA3MTE5IDcsMy41IDcsMi4xMTkyODgxIDguMTE5Mjg4MSwxIDkuNSwxIDEwLjg4MDcxMiwxIDEyLDIuMTE5Mjg4MSAxMiwzLjUgeiIgaWQ9InBhdGgzNzYzIiBzdHlsZT0iZmlsbDojODFkMTQ3O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NjMiIGlkPSJ1c2UzNzY1IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNSkiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2NyIgZD0iTSA4LDkgQyA4LDkuNTUyMjg0NyA3LjU1MjI4NDcsMTAgNywxMCA2LjQ0NzcxNTMsMTAgNiw5LjU1MjI4NDcgNiw5IDYsOC40NDc3MTUzIDYuNDQ3NzE1Myw4IDcsOCA3LjU1MjI4NDcsOCA4LDguNDQ3NzE1MyA4LDkgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMiwxMDM0LjM2MjIpIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NjciIGlkPSJ1c2UzNzY5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMiwyKSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2Ii8+PHJlY3Qgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6bm9uZSIgaWQ9InJlY3QyOTkwIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHg9IjAiIHk9IjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzNi4zNjIyKSIvPjwvZz48L2c+PC9zdmc+Cg==","img/icon/64.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iNjQiIHdpZHRoPSI2NCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtOTg4LjM2MjIpIiBpZD0ibGF5ZXIxIj48ZyB0cmFuc2Zvcm09Im1hdHJpeCg0LDAsMCw0LDAsLTMxNTcuMDg2NikiIGlkPSJnMzc2MCI+PHBhdGggc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzU5IiBkPSJtIDEyLjUsMy41IGMgMCwxLjY1Njg1NDIgLTEuMzQzMTQ2LDMgLTMsMyAtMS42NTY4NTQyLDAgLTMsLTEuMzQzMTQ1OCAtMywtMyAwLC0xLjY1Njg1NDIgMS4zNDMxNDU4LC0zIDMsLTMgMS42NTY4NTQsMCAzLDEuMzQzMTQ1OCAzLDMgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMS4xNjY2NjY3LDAsMCwxLjE2NjY2NjcsLTIuMDgzMzMzNSwxMDM1Ljc3ODkpIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3NTkiIGlkPSJ1c2UzNzYxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNS41LDUuNDk5OTcwMikiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxwYXRoIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiNmZmZmZmY7c3Ryb2tlLW9wYWNpdHk6MSIgaWQ9InBhdGgyOTg5IiBkPSJtIDE1LjUsOS41IGMgMCwzLjMxMzcwOCAtMi42ODYyOTIsNiAtNiw2IC0zLjMxMzcwODUsMCAtNiwtMi42ODYyOTIgLTYsLTYgMCwtMy4zMTM3MDg1IDIuNjg2MjkxNSwtNiA2LC02IDMuMzEzNzA4LDAgNiwyLjY4NjI5MTUgNiw2IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNS44NjIyKSIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUsMTAzNi4zNjIyKSIgZD0iTSAxMiwzLjUgQyAxMiw0Ljg4MDcxMTkgMTAuODgwNzEyLDYgOS41LDYgOC4xMTkyODgxLDYgNyw0Ljg4MDcxMTkgNywzLjUgNywyLjExOTI4ODEgOC4xMTkyODgxLDEgOS41LDEgMTAuODgwNzEyLDEgMTIsMi4xMTkyODgxIDEyLDMuNSB6IiBpZD0icGF0aDM3NjMiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc2MyIgaWQ9InVzZTM3NjUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01LjUsNS41KSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2Ii8+PHBhdGggc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzY3IiBkPSJNIDgsOSBDIDgsOS41NTIyODQ3IDcuNTUyMjg0NywxMCA3LDEwIDYuNDQ3NzE1MywxMCA2LDkuNTUyMjg0NyA2LDkgNiw4LjQ0NzcxNTMgNi40NDc3MTUzLDggNyw4IDcuNTUyMjg0Nyw4IDgsOC40NDc3MTUzIDgsOSB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyLDEwMzQuMzYyMikiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc2NyIgaWQ9InVzZTM3NjkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yLDIpIiB3aWR0aD0iMTYiIGhlaWdodD0iMTYiLz48cmVjdCBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTpub25lIiBpZD0icmVjdDI5OTAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgeD0iMCIgeT0iMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDM2LjM2MjIpIi8+PC9nPjwvZz48L3N2Zz4K","img/light-grass.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMTAwIiB3aWR0aD0iNTAwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05NTIuMzYyMTgpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjQ4NDIxNDc5LDAsMCwwLjQ4NDIxNDc5LDI3LjM5MTMyNSw4ODYuODgwMzMpIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgaWQ9InBhdGgyOTg3IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzU3IiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNDAzNTEyMDEsMCwwLDAuNDAzNTEyMDEsNzcuODI2MDg2LDg5Ni45NjA3MikiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjQ4NDcwNTc3LDAsMCwwLjQ4NDcwNTc3LDIxNy40MTkxLDg3MS43ODg2MSkiIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBjIDAsNTEuMzI2MDUgLTQxLjYwNzk4NTUsOTIuOTM0MDMgLTkyLjkzNDAzNiw5Mi45MzQwMyAtNTEuMzI2MDQ4LDAgLTkyLjkzNDAzOCwtNDEuNjA3OTggLTkyLjkzNDAzOCwtOTIuOTM0MDMgMCwtNTEuMzI2MDUgNDEuNjA3OTksLTkyLjkzNDA0IDkyLjkzNDAzOCwtOTIuOTM0MDQgNTEuMzI2MDUwNSwwIDkyLjkzNDAzNiw0MS42MDc5OSA5Mi45MzQwMzYsOTIuOTM0MDQgeiIgaWQ9InBhdGgzNzU5IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzYxIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNTE2OTg2NjksMCwwLDAuNTE2OTg2NjksMjk4LjM3MTA5LDg2OC4xNDMzMikiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjI4NzEyODc2LDAsMCwwLjI4NzEyODc2LDM5Ni4yNDI0Niw5MDguNzA4NDMpIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIGlkPSJwYXRoMzc2MyIgc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2NSIgZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGMgMCw1MS4zMjYwNSAtNDEuNjA3OTg1NSw5Mi45MzQwMyAtOTIuOTM0MDM2LDkyLjkzNDAzIC01MS4zMjYwNDgsMCAtOTIuOTM0MDM4LC00MS42MDc5OCAtOTIuOTM0MDM4LC05Mi45MzQwMyAwLC01MS4zMjYwNSA0MS42MDc5OSwtOTIuOTM0MDQgOTIuOTM0MDM4LC05Mi45MzQwNCA1MS4zMjYwNTA1LDAgOTIuOTM0MDM2LDQxLjYwNzk5IDkyLjkzNDAzNiw5Mi45MzQwNCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjQ0ODQzNzUzLDAsMCwwLjQ0ODQzNzUzLDQ2NS4zNjc0Niw4NzguNTY1OCkiLz48dXNlIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjEiIGhlaWdodD0iMzAwIiB3aWR0aD0iNTAwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1MDAsLTQuNTI2MDgzZS02KSIgaWQ9InVzZTM3NjciIHhsaW5rOmhyZWY9IiNwYXRoMjk4NyIgeT0iMCIgeD0iMCIvPjxwYXRoIHRyYW5zZm9ybT0ibWF0cml4KDAuMzc2NjExMjEsMCwwLDAuMzc2NjExMjEsMTM5LjE4MzAzLDkwMS45ODc0OCkiIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBjIDAsNTEuMzI2MDUgLTQxLjYwNzk4NTUsOTIuOTM0MDMgLTkyLjkzNDAzNiw5Mi45MzQwMyAtNTEuMzI2MDQ4LDAgLTkyLjkzNDAzOCwtNDEuNjA3OTggLTkyLjkzNDAzOCwtOTIuOTM0MDMgMCwtNTEuMzI2MDUgNDEuNjA3OTksLTkyLjkzNDA0IDkyLjkzNDAzOCwtOTIuOTM0MDQgNTEuMzI2MDUwNSwwIDkyLjkzNDAzNiw0MS42MDc5OSA5Mi45MzQwMzYsOTIuOTM0MDQgeiIgaWQ9InBhdGgzNzY5IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggdHJhbnNmb3JtPSJtYXRyaXgoMC43MDk0NzEzNSwwLDAsMC43MDk0NzEzNSwzODEuNTQ3OTcsODY5Ljc4ODI3KSIgZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIGlkPSJwYXRoMzc3MSIgc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiNkNGYzYmQ7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMjk5MiIgZD0ibSAzNi4zNjU0OTQsMjc5Ljc5Njk0IGEgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAtMTg1Ljg2ODA3NCwwIDkyLjkzNDAzNiw5Mi45MzQwMzYgMCAxIDEgMTg1Ljg2ODA3NCwwIHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsMTM1LjI0NDU4LDg1OS43OTM1NykiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjcwOTQ3MTM1LDAsMCwwLjcwOTQ3MTM1LDI1MS4zMjEwNCw4NjguNTAwNjgpIiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYSA5Mi45MzQwMzYsOTIuOTM0MDM2IDAgMSAxIC0xODUuODY4MDc0LDAgOTIuOTM0MDM2LDkyLjkzNDAzNiAwIDEgMSAxODUuODY4MDc0LDAgeiIgaWQ9InBhdGgyOTk0IiBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6I2Q0ZjNiZDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk2IiBkPSJtIDM2LjM2NTQ5NCwyNzkuNzk2OTQgYyAwLDUxLjMyNjA1IC00MS42MDc5ODU1LDkyLjkzNDAzIC05Mi45MzQwMzYsOTIuOTM0MDMgLTUxLjMyNjA0OCwwIC05Mi45MzQwMzgsLTQxLjYwNzk4IC05Mi45MzQwMzgsLTkyLjkzNDAzIDAsLTUxLjMyNjA1IDQxLjYwNzk5LC05Mi45MzQwNCA5Mi45MzQwMzgsLTkyLjkzNDA0IDUxLjMyNjA1MDUsMCA5Mi45MzQwMzYsNDEuNjA3OTkgOTIuOTM0MDM2LDkyLjkzNDA0IHoiIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA5NDcxMzUsMCwwLDAuNzA5NDcxMzUsNDY5LjE5OTcyLDg1OC44NTQyMykiLz48cGF0aCBzdHlsZT0iZmlsbDojZDRmM2JkO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5OTUiIGQ9Im0gMzYuMzY1NDk0LDI3OS43OTY5NCBjIDAsNTEuMzI2MDUgLTQxLjYwNzk4NTUsOTIuOTM0MDMgLTkyLjkzNDAzNiw5Mi45MzQwMyAtNTEuMzI2MDQ4LDAgLTkyLjkzNDAzOCwtNDEuNjA3OTggLTkyLjkzNDAzOCwtOTIuOTM0MDMgMCwtNTEuMzI2MDUgNDEuNjA3OTksLTkyLjkzNDA0IDkyLjkzNDAzOCwtOTIuOTM0MDQgNTEuMzI2MDUwNSwwIDkyLjkzNDAzNiw0MS42MDc5OSA5Mi45MzQwMzYsOTIuOTM0MDQgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMC4yODcxMjg3NiwwLDAsMC4yODcxMjg3NiwzNTYuMjQyNDYsOTI4LjcwODQzKSIvPjwvZz48L3N2Zz4K","img/logo.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMTU0IiB3aWR0aD0iMTEyIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC04OTguMzYyMikiIGlkPSJsYXllcjEiPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LC0yLjY3N2UtNCkiIGlkPSJnMzg0OSI+PHBhdGggc3R5bGU9ImZpbGw6IzgxZDE0NztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTg5IiBkPSJtIDE1LjUsOS41IGMgMCwzLjMxMzcwOCAtMi42ODYyOTIsNiAtNiw2IC0zLjMxMzcwODUsMCAtNiwtMi42ODYyOTIgLTYsLTYgMCwtMy4zMTM3MDg1IDIuNjg2MjkxNSwtNiA2LC02IDMuMzEzNzA4LDAgNiwyLjY4NjI5MTUgNiw2IHoiIHRyYW5zZm9ybT0ibWF0cml4KDcuMzMzMzMzNCwwLDAsNy4zMzMzMzM0LDIuMzMzMzMzMiw4OTIuNjk1OCkiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCg4LDAsMCw4LC00LDg5MC4zNjIyKSIgZD0iTSAxMiwzLjUgQyAxMiw0Ljg4MDcxMTkgMTAuODgwNzEyLDYgOS41LDYgOC4xMTkyODgxLDYgNyw0Ljg4MDcxMTkgNywzLjUgNywyLjExOTI4ODEgOC4xMTkyODgxLDEgOS41LDEgMTAuODgwNzEyLDEgMTIsMi4xMTkyODgxIDEyLDMuNSB6IiBpZD0icGF0aDM3NjMiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc2MyIgaWQ9InVzZTM3NjUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC00NCw0NCkiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjxwYXRoIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc2NyIgZD0iTSA4LDkgQyA4LDkuNTUyMjg0NyA3LjU1MjI4NDcsMTAgNywxMCA2LjQ0NzcxNTMsMTAgNiw5LjU1MjI4NDcgNiw5IDYsOC40NDc3MTUzIDYuNDQ3NzE1Myw4IDcsOCA3LjU1MjI4NDcsOCA4LDguNDQ3NzE1MyA4LDkgeiIgdHJhbnNmb3JtPSJtYXRyaXgoOCwwLDAsOCwxNiw4NzQuMzYyMikiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc2NyIgaWQ9InVzZTM3NjkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNiwxNikiIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIvPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTIsMTgpIiBzdHlsZT0ic3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLW9wYWNpdHk6MSIgaWQ9ImczODI3Ij48cGF0aCBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjE7c3Ryb2tlLWRhc2hhcnJheTpub25lIiBkPSJtIDE2LDE0MCAwLDI0IiBpZD0icGF0aDM3ODkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsODYwLjM2MjIpIi8+PHBhdGggc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjE7c3Ryb2tlLWRhc2hhcnJheTpub25lIiBpZD0icGF0aDM3OTEiIGQ9Im0gMzIsMTU2IGMgMCw0LjQxODI4IC0zLjU4MTcyMiw4IC04LDggLTQuNDE4Mjc4LDAgLTgsLTMuNTgxNzIgLTgsLTggMCwtNC40MTgyOCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyIDgsOCB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDg2MC4zNjIyKSIvPjxnIHN0eWxlPSJzdHJva2U6IzgxZDE0NztzdHJva2Utb3BhY2l0eToxIiBpZD0iZzM4MTMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yLDApIj48cGF0aCBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiIGlkPSJwYXRoMzc5MyIgZD0ibSAzMiwxNTYgYyAwLDQuNDE4MjggLTMuNTgxNzIyLDggLTgsOCAtNC40MTgyNzgsMCAtOCwtMy41ODE3MiAtOCwtOCAwLC00LjQxODI4IDMuNTgxNzIyLC04IDgsLTggNC40MTgyNzgsMCA4LDMuNTgxNzIgOCw4IHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDI0LDg2MC4zNjIyKSIvPjxwYXRoIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiIGQ9Im0gNTYsMTAwOC4zNjIyIDAsMTYiIGlkPSJwYXRoMzc5NSIvPjwvZz48cGF0aCBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjE7c3Ryb2tlLWRhc2hhcnJheTpub25lIiBkPSJtIDYwLDEwMDguMzYyMiAxNiwwIC0xNiwxNiAxNiwwIiBpZD0icGF0aDM3OTciLz48ZyBzdHlsZT0ic3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLW9wYWNpdHk6MSIgaWQ9ImczODE3IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNiwwKSI+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNzIsODYwLjM2MjIpIiBkPSJtIDMyLDE1NiBjIDAsNC40MTgyOCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyIC04LC04IDAsLTQuNDE4MjggMy41ODE3MjIsLTggOCwtOCA0LjQxODI3OCwwIDgsMy41ODE3MiA4LDggeiIgaWQ9InBhdGgzNzk5IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiLz48cGF0aCBpZD0icGF0aDM4MDEiIGQ9Im0gMTA0LDEwMDguMzYyMiAwLDE2IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjE7c3Ryb2tlLWRhc2hhcnJheTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIgaWQ9InBhdGgzODAzIiBkPSJtIDMyLDE1NiBjIDAsNC40MTgyOCAtMy41ODE3MjIsOCAtOCw4IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg3Miw4NjguMzYyMikiLz48L2c+PGcgc3R5bGU9InN0cm9rZTojODFkMTQ3O3N0cm9rZS1vcGFjaXR5OjEiIGlkPSJnMzgyMiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTYsMCkiPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDk0LDg2MC4zNjIyKSIgZD0ibSAzMiwxNTYgYyAwLDQuNDE4MjggLTMuNTgxNzIyLDggLTgsOCAtNC40MTgyNzgsMCAtOCwtMy41ODE3MiAtOCwtOCAwLDAgMCwwIDAsMCIgaWQ9InBhdGgzODA1IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojODFkMTQ3O3N0cm9rZS13aWR0aDo0O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjE7c3Ryb2tlLWRhc2hhcnJheTpub25lIi8+PHBhdGggaWQ9InBhdGgzODA3IiBkPSJtIDEyNiwxMDA4LjM2MjIgMCwxNiIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzgxZDE0NztzdHJva2Utd2lkdGg6NDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM4MWQxNDc7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLW9wYWNpdHk6MTtzdHJva2UtZGFzaGFycmF5Om5vbmUiIGQ9Im0gMTEwLDEwMDguMzYyMiAwLDgiIGlkPSJwYXRoMzgwOSIvPjwvZz48L2c+PC9nPjwvc3ZnPgo=","img/privacy/contacts-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDI5ODkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAxMi4zNjIyKSIgZD0iTSAyNS45MDYyNSA5LjQwNjI1IEMgMjQuNzQ2Njk1IDkuNDA2MjUgMjMuNzgwMjY1IDEwLjE0NDk0OSAyMy40Mzc1IDExLjE4NzUgQyAyNC40MjkyMzQgMTIuMTYxNDYyIDI1LjA0OTY1IDEzLjQ4OTYgMjUuMDkzNzUgMTQuOTY4NzUgQyAyNy40ODcxMTEgMTYuNjM4NDAzIDI5LjE0NjkxMSAxOS4yODk1NTkgMjkuMzEyNSAyMi4zNzUgQyAzMC43MzUzNDcgMjEuMzI3NTI0IDMxLjY1NjI1IDE5LjY0OTMzIDMxLjY1NjI1IDE3Ljc1IEMgMzEuNjU2MjUgMTUuNDg4NSAzMC4zMzczNzQgMTMuNTMwMzUgMjguNDM3NSAxMi41OTM3NSBDIDI4LjQ4MjczIDEyLjQwMTE1IDI4LjUgMTIuMjA2MiAyOC41IDEyIEMgMjguNSAxMC41NTgyIDI3LjM0Nzk5OCA5LjQwNjI1IDI1LjkwNjI1IDkuNDA2MjUgeiBNIDE5LjU5Mzc1IDExLjU5Mzc1IEMgMTcuNjUwNTI2IDExLjU5Mzc1IDE2LjA2MjUgMTMuMTgxOCAxNi4wNjI1IDE1LjEyNSBDIDE2LjA2MjUgMTUuNDAzIDE2LjEyNjU0IDE1LjY3ODEgMTYuMTg3NSAxNS45Mzc1IEMgMTQuNjcxNjA2IDE2LjY4NjIgMTMuNDE4ODE4IDE3LjkyNzMgMTIuNjU2MjUgMTkuNDM3NSBDIDEyLjM5NzE4NCAxOS4zNzY1IDEyLjEyMTM1MyAxOS4zNDM3NSAxMS44NDM3NSAxOS4zNDM3NSBDIDkuOTAwNTI1OSAxOS4zNDM3NSA4LjM0Mzc1IDIwLjkzMTggOC4zNDM3NSAyMi44NzUgQyA4LjM0Mzc1IDI0LjgxODIgOS45MDA1MjU5IDI2LjM3NSAxMS44NDM3NSAyNi4zNzUgQyAxMi4xMjEzNTMgMjYuMzc1IDEyLjM5NzE4NCAyNi4zNDIyNSAxMi42NTYyNSAyNi4yODEyNSBDIDEzLjkyMTM3NCAyOC44MzU0NSAxNi41NDU3MzMgMzAuNTkzNzUgMTkuNTkzNzUgMzAuNTkzNzUgQyAyMy44Njg4NDMgMzAuNTkzNzUgMjcuMzQzNzUgMjcuMTUwMSAyNy4zNDM3NSAyMi44NzUgQyAyNy4zNDM3NSAxOS44MjkgMjUuNTU3OTUzIDE3LjIwMDkgMjMgMTUuOTM3NSBDIDIzLjA2MDk2IDE1LjY3ODEgMjMuMTI1IDE1LjQwMyAyMy4xMjUgMTUuMTI1IEMgMjMuMTI1IDEzLjE4MTggMjEuNTM2OTc0IDExLjU5Mzc1IDE5LjU5Mzc1IDExLjU5Mzc1IHogTSAxOS4zMTI1IDE4LjY1NjI1IEMgMTkuNDA0MTIgMTguNjM3MjUgMTkuNDk2NTg5IDE4LjY1NjI1IDE5LjU5Mzc1IDE4LjY1NjI1IEMgMjAuMzcxMDQgMTguNjU2MjUgMjEgMTkuMjg1MiAyMSAyMC4wNjI1IEMgMjEgMjAuODM5OCAyMC4zNzEwNCAyMS40Njg3NSAxOS41OTM3NSAyMS40Njg3NSBDIDE4LjgxNjQ2IDIxLjQ2ODc1IDE4LjE4NzUgMjAuODM5OCAxOC4xODc1IDIwLjA2MjUgQyAxOC4xODc1IDE5LjM4MjQgMTguNjcxMTY4IDE4Ljc4NzQ1IDE5LjMxMjUgMTguNjU2MjUgeiBNIDE2Ljc4MTI1IDIxLjQ2ODc1IEMgMTcuNTU4NTQgMjEuNDY4NzUgMTguMTg3NSAyMi4wOTc3IDE4LjE4NzUgMjIuODc1IEMgMTguMTg3NSAyMy42NTIzIDE3LjU1ODU0IDI0LjI4MTI1IDE2Ljc4MTI1IDI0LjI4MTI1IEMgMTYuMDAzOTYgMjQuMjgxMjUgMTUuMzc1IDIzLjY1MjMgMTUuMzc1IDIyLjg3NSBDIDE1LjM3NSAyMi4wOTc3IDE2LjAwMzk2IDIxLjQ2ODc1IDE2Ljc4MTI1IDIxLjQ2ODc1IHogIiBzdHlsZT0iZmlsbDojYTllODdjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/privacy/contacts-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDI5ODkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAxMi4zNjIyKSIgZD0iTSAyNS45MDYyNSA5LjQwNjI1IEMgMjQuNzQ2Njk1IDkuNDA2MjUgMjMuNzgwMjY1IDEwLjE0NDk0OSAyMy40Mzc1IDExLjE4NzUgQyAyNC40MjkyMzQgMTIuMTYxNDYyIDI1LjA0OTY1IDEzLjQ4OTYgMjUuMDkzNzUgMTQuOTY4NzUgQyAyNy40ODcxMTEgMTYuNjM4NDAzIDI5LjE0NjkxMSAxOS4yODk1NTkgMjkuMzEyNSAyMi4zNzUgQyAzMC43MzUzNDcgMjEuMzI3NTI0IDMxLjY1NjI1IDE5LjY0OTMzIDMxLjY1NjI1IDE3Ljc1IEMgMzEuNjU2MjUgMTUuNDg4NSAzMC4zMzczNzQgMTMuNTMwMzUgMjguNDM3NSAxMi41OTM3NSBDIDI4LjQ4MjczIDEyLjQwMTE1IDI4LjUgMTIuMjA2MiAyOC41IDEyIEMgMjguNSAxMC41NTgyIDI3LjM0Nzk5OCA5LjQwNjI1IDI1LjkwNjI1IDkuNDA2MjUgeiBNIDE5LjU5Mzc1IDExLjU5Mzc1IEMgMTcuNjUwNTI2IDExLjU5Mzc1IDE2LjA2MjUgMTMuMTgxOCAxNi4wNjI1IDE1LjEyNSBDIDE2LjA2MjUgMTUuNDAzIDE2LjEyNjU0IDE1LjY3ODEgMTYuMTg3NSAxNS45Mzc1IEMgMTQuNjcxNjA2IDE2LjY4NjIgMTMuNDE4ODE4IDE3LjkyNzMgMTIuNjU2MjUgMTkuNDM3NSBDIDEyLjM5NzE4NCAxOS4zNzY1IDEyLjEyMTM1MyAxOS4zNDM3NSAxMS44NDM3NSAxOS4zNDM3NSBDIDkuOTAwNTI1OSAxOS4zNDM3NSA4LjM0Mzc1IDIwLjkzMTggOC4zNDM3NSAyMi44NzUgQyA4LjM0Mzc1IDI0LjgxODIgOS45MDA1MjU5IDI2LjM3NSAxMS44NDM3NSAyNi4zNzUgQyAxMi4xMjEzNTMgMjYuMzc1IDEyLjM5NzE4NCAyNi4zNDIyNSAxMi42NTYyNSAyNi4yODEyNSBDIDEzLjkyMTM3NCAyOC44MzU0NSAxNi41NDU3MzMgMzAuNTkzNzUgMTkuNTkzNzUgMzAuNTkzNzUgQyAyMy44Njg4NDMgMzAuNTkzNzUgMjcuMzQzNzUgMjcuMTUwMSAyNy4zNDM3NSAyMi44NzUgQyAyNy4zNDM3NSAxOS44MjkgMjUuNTU3OTUzIDE3LjIwMDkgMjMgMTUuOTM3NSBDIDIzLjA2MDk2IDE1LjY3ODEgMjMuMTI1IDE1LjQwMyAyMy4xMjUgMTUuMTI1IEMgMjMuMTI1IDEzLjE4MTggMjEuNTM2OTc0IDExLjU5Mzc1IDE5LjU5Mzc1IDExLjU5Mzc1IHogTSAxOS4zMTI1IDE4LjY1NjI1IEMgMTkuNDA0MTIgMTguNjM3MjUgMTkuNDk2NTg5IDE4LjY1NjI1IDE5LjU5Mzc1IDE4LjY1NjI1IEMgMjAuMzcxMDQgMTguNjU2MjUgMjEgMTkuMjg1MiAyMSAyMC4wNjI1IEMgMjEgMjAuODM5OCAyMC4zNzEwNCAyMS40Njg3NSAxOS41OTM3NSAyMS40Njg3NSBDIDE4LjgxNjQ2IDIxLjQ2ODc1IDE4LjE4NzUgMjAuODM5OCAxOC4xODc1IDIwLjA2MjUgQyAxOC4xODc1IDE5LjM4MjQgMTguNjcxMTY4IDE4Ljc4NzQ1IDE5LjMxMjUgMTguNjU2MjUgeiBNIDE2Ljc4MTI1IDIxLjQ2ODc1IEMgMTcuNTU4NTQgMjEuNDY4NzUgMTguMTg3NSAyMi4wOTc3IDE4LjE4NzUgMjIuODc1IEMgMTguMTg3NSAyMy42NTIzIDE3LjU1ODU0IDI0LjI4MTI1IDE2Ljc4MTI1IDI0LjI4MTI1IEMgMTYuMDAzOTYgMjQuMjgxMjUgMTUuMzc1IDIzLjY1MjMgMTUuMzc1IDIyLjg3NSBDIDE1LjM3NSAyMi4wOTc3IDE2LjAwMzk2IDIxLjQ2ODc1IDE2Ljc4MTI1IDIxLjQ2ODc1IHogIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/privacy/contacts.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDI5ODkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAxMi4zNjIyKSIgZD0iTSAyNS45MDYyNSA5LjQwNjI1IEMgMjQuNzQ2Njk1IDkuNDA2MjUgMjMuNzgwMjY1IDEwLjE0NDk0OSAyMy40Mzc1IDExLjE4NzUgQyAyNC40MjkyMzQgMTIuMTYxNDYyIDI1LjA0OTY1IDEzLjQ4OTYgMjUuMDkzNzUgMTQuOTY4NzUgQyAyNy40ODcxMTEgMTYuNjM4NDAzIDI5LjE0NjkxMSAxOS4yODk1NTkgMjkuMzEyNSAyMi4zNzUgQyAzMC43MzUzNDcgMjEuMzI3NTI0IDMxLjY1NjI1IDE5LjY0OTMzIDMxLjY1NjI1IDE3Ljc1IEMgMzEuNjU2MjUgMTUuNDg4NSAzMC4zMzczNzQgMTMuNTMwMzUgMjguNDM3NSAxMi41OTM3NSBDIDI4LjQ4MjczIDEyLjQwMTE1IDI4LjUgMTIuMjA2MiAyOC41IDEyIEMgMjguNSAxMC41NTgyIDI3LjM0Nzk5OCA5LjQwNjI1IDI1LjkwNjI1IDkuNDA2MjUgeiBNIDE5LjU5Mzc1IDExLjU5Mzc1IEMgMTcuNjUwNTI2IDExLjU5Mzc1IDE2LjA2MjUgMTMuMTgxOCAxNi4wNjI1IDE1LjEyNSBDIDE2LjA2MjUgMTUuNDAzIDE2LjEyNjU0IDE1LjY3ODEgMTYuMTg3NSAxNS45Mzc1IEMgMTQuNjcxNjA2IDE2LjY4NjIgMTMuNDE4ODE4IDE3LjkyNzMgMTIuNjU2MjUgMTkuNDM3NSBDIDEyLjM5NzE4NCAxOS4zNzY1IDEyLjEyMTM1MyAxOS4zNDM3NSAxMS44NDM3NSAxOS4zNDM3NSBDIDkuOTAwNTI1OSAxOS4zNDM3NSA4LjM0Mzc1IDIwLjkzMTggOC4zNDM3NSAyMi44NzUgQyA4LjM0Mzc1IDI0LjgxODIgOS45MDA1MjU5IDI2LjM3NSAxMS44NDM3NSAyNi4zNzUgQyAxMi4xMjEzNTMgMjYuMzc1IDEyLjM5NzE4NCAyNi4zNDIyNSAxMi42NTYyNSAyNi4yODEyNSBDIDEzLjkyMTM3NCAyOC44MzU0NSAxNi41NDU3MzMgMzAuNTkzNzUgMTkuNTkzNzUgMzAuNTkzNzUgQyAyMy44Njg4NDMgMzAuNTkzNzUgMjcuMzQzNzUgMjcuMTUwMSAyNy4zNDM3NSAyMi44NzUgQyAyNy4zNDM3NSAxOS44MjkgMjUuNTU3OTUzIDE3LjIwMDkgMjMgMTUuOTM3NSBDIDIzLjA2MDk2IDE1LjY3ODEgMjMuMTI1IDE1LjQwMyAyMy4xMjUgMTUuMTI1IEMgMjMuMTI1IDEzLjE4MTggMjEuNTM2OTc0IDExLjU5Mzc1IDE5LjU5Mzc1IDExLjU5Mzc1IHogTSAxOS4zMTI1IDE4LjY1NjI1IEMgMTkuNDA0MTIgMTguNjM3MjUgMTkuNDk2NTg5IDE4LjY1NjI1IDE5LjU5Mzc1IDE4LjY1NjI1IEMgMjAuMzcxMDQgMTguNjU2MjUgMjEgMTkuMjg1MiAyMSAyMC4wNjI1IEMgMjEgMjAuODM5OCAyMC4zNzEwNCAyMS40Njg3NSAxOS41OTM3NSAyMS40Njg3NSBDIDE4LjgxNjQ2IDIxLjQ2ODc1IDE4LjE4NzUgMjAuODM5OCAxOC4xODc1IDIwLjA2MjUgQyAxOC4xODc1IDE5LjM4MjQgMTguNjcxMTY4IDE4Ljc4NzQ1IDE5LjMxMjUgMTguNjU2MjUgeiBNIDE2Ljc4MTI1IDIxLjQ2ODc1IEMgMTcuNTU4NTQgMjEuNDY4NzUgMTguMTg3NSAyMi4wOTc3IDE4LjE4NzUgMjIuODc1IEMgMTguMTg3NSAyMy42NTIzIDE3LjU1ODU0IDI0LjI4MTI1IDE2Ljc4MTI1IDI0LjI4MTI1IEMgMTYuMDAzOTYgMjQuMjgxMjUgMTUuMzc1IDIzLjY1MjMgMTUuMzc1IDIyLjg3NSBDIDE1LjM3NSAyMi4wOTc3IDE2LjAwMzk2IDIxLjQ2ODc1IDE2Ljc4MTI1IDIxLjQ2ODc1IHogIiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/privacy/private-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDMwMDYiIGQ9Im0gMTQsMTAyNy4zNjIyIDAsMyAtMiwwIGMgLTAuNSwwIC0xLDAuNSAtMSwxIGwgMCwxMCBjIDAsMC41IDAuNSwxIDEsMSBsIDE2LDAgYyAwLjUsMCAxLC0wLjUgMSwtMSBsIDAsLTEwIGMgMCwtMC41IC0wLjUsLTEgLTEsLTEgbCAtMiwwIDAsLTMgYyAwLC0zIC0zLC02IC02LC02IC0zLDAgLTYsMyAtNiw2IHogbSA2LC00IGMgMiwwIDQsMiA0LDQgbCAwLDMgLTgsMCAwLC0zIGMgMCwtMiAyLC00IDQsLTQgeiBtIDAsMTAgYyAxLDAgMiwxIDIsMiAwLDEgLTEsMS4wNDA4IC0xLDIgbCAwLDIgLTIsMCAwLC0yIGMgMCwtMC45NTkyIC0xLC0xIC0xLC0yIDAsLTEgMSwtMiAyLC0yIHoiIHN0eWxlPSJmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/private-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDMwMDYtMyIgZD0ibSAxNCwxMDI3LjM2MjIgMCwzIC0yLDAgYyAtMC41LDAgLTEsMC41IC0xLDEgbCAwLDEwIGMgMCwwLjUgMC41LDEgMSwxIGwgMTYsMCBjIDAuNSwwIDEsLTAuNSAxLC0xIGwgMCwtMTAgYyAwLC0wLjUgLTAuNSwtMSAtMSwtMSBsIC0yLDAgMCwtMyBjIDAsLTMgLTMsLTYgLTYsLTYgLTMsMCAtNiwzIC02LDYgeiBtIDYsLTQgYyAyLDAgNCwyIDQsNCBsIDAsMyAtOCwwIDAsLTMgYyAwLC0yIDIsLTQgNCwtNCB6IG0gMCwxMCBjIDEsMCAyLDEgMiwyIDAsMSAtMSwxLjA0MDggLTEsMiBsIDAsMiAtMiwwIDAsLTIgYyAwLC0wLjk1OTIgLTEsLTEgLTEsLTIgMCwtMSAxLC0yIDIsLTIgeiIgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/privacy/private.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCBpZD0icGF0aDMwMDYiIGQ9Im0gMTQsMTAyNy4zNjIyIDAsMyAtMiwwIGMgLTAuNSwwIC0xLDAuNSAtMSwxIGwgMCwxMCBjIDAsMC41IDAuNSwxIDEsMSBsIDE2LDAgYyAwLjUsMCAxLC0wLjUgMSwtMSBsIDAsLTEwIGMgMCwtMC41IC0wLjUsLTEgLTEsLTEgbCAtMiwwIDAsLTMgYyAwLC0zIC0zLC02IC02LC02IC0zLDAgLTYsMyAtNiw2IHogbSA2LC00IGMgMiwwIDQsMiA0LDQgbCAwLDMgLTgsMCAwLC0zIGMgMCwtMiAyLC00IDQsLTQgeiBtIDAsMTAgYyAxLDAgMiwxIDIsMiAwLDEgLTEsMS4wNDA4IC0xLDIgbCAwLDIgLTIsMCAwLC0yIGMgMCwtMC45NTkyIC0xLC0xIC0xLC0yIDAsLTEgMSwtMiAyLC0yIHoiIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/public-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMTIuMzYyMikiIGQ9Im0gMjkuNSwyMCBjIDAsNS4yNDY3MDUgLTQuMjUzMjk1LDkuNSAtOS41LDkuNSAtNS4yNDY3MDUsMCAtOS41LC00LjI1MzI5NSAtOS41LC05LjUgMCwtNS4yNDY3MDUgNC4yNTMyOTUsLTkuNSA5LjUsLTkuNSA1LjI0NjcwNSwwIDkuNSw0LjI1MzI5NSA5LjUsOS41IHoiIGlkPSJwYXRoMzEwNyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2E5ZTg3YztzdHJva2Utd2lkdGg6MjtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIGlkPSJwYXRoMzg3NyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBkPSJNIDE4LjA2MjUgMTEuMDYyNSBMIDEyLjgxMjUgMTMuNDM3NSBMIDExIDE5LjUgTCAxNi4xMjUgMTguMTg3NSBMIDE2IDE0LjkzNzUgTCAxOC41NjI1IDEyLjkzNzUgTCAxOC4wNjI1IDExLjA2MjUgeiBNIDIzLjMxMjUgMTEuNTYyNSBMIDIyIDE1LjMxMjUgTCAyMy41NjI1IDE1LjM3NSBMIDIyLjM3NSAxNy44MTI1IEwgMjUgMTYuNjI1IEwgMjguODEyNSAxNyBMIDI3LjkzNzUgMTQuMTg3NSBMIDIzLjMxMjUgMTEuNTYyNSB6IE0gMjQuNTYyNSAxNy42ODc1IEwgMjAuMzc1IDIwLjc1IEwgMjIuMzc1IDIzLjA2MjUgTCAyMS43NSAyNi42MjUgTCAyMy45Mzc1IDI4LjY4NzUgTCAyOC41NjI1IDI0LjQzNzUgTCAyOS44NzUgMTkuMzEyNSBMIDI0LjU2MjUgMTcuNjg3NSB6IE0gMTMuNSAxOS41IEwgMTEuMTI1IDIwLjUgTCAxMS42ODc1IDI0LjY4NzUgTCAxNi43NSAyOC44NzUgTCAxOC4wNjI1IDI2LjY4NzUgTCAxNS42MjUgMjMuMjUgTCAxNi4xODc1IDIwLjgxMjUgTCAxMy41IDE5LjUgeiAiIHN0eWxlPSJmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/public-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMTIuMzYyMikiIGQ9Im0gMjkuNSwyMCBhIDkuNSw5LjUgMCAxIDEgLTE5LDAgOS41LDkuNSAwIDEgMSAxOSwwIHoiIGlkPSJwYXRoMzEwNyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I2ZmZmZmZjtzdHJva2Utd2lkdGg6MjtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIGlkPSJwYXRoMzg3NyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBkPSJNIDE4LjA2MjUgMTEuMDYyNSBMIDEyLjgxMjUgMTMuNDM3NSBMIDExIDE5LjUgTCAxNi4xMjUgMTguMTg3NSBMIDE2IDE0LjkzNzUgTCAxOC41NjI1IDEyLjkzNzUgTCAxOC4wNjI1IDExLjA2MjUgeiBNIDIzLjMxMjUgMTEuNTYyNSBMIDIyIDE1LjMxMjUgTCAyMy41NjI1IDE1LjM3NSBMIDIyLjM3NSAxNy44MTI1IEwgMjUgMTYuNjI1IEwgMjguODEyNSAxNyBMIDI3LjkzNzUgMTQuMTg3NSBMIDIzLjMxMjUgMTEuNTYyNSB6IE0gMjQuNTYyNSAxNy42ODc1IEwgMjAuMzc1IDIwLjc1IEwgMjIuMzc1IDIzLjA2MjUgTCAyMS43NSAyNi42MjUgTCAyMy45Mzc1IDI4LjY4NzUgTCAyOC41NjI1IDI0LjQzNzUgTCAyOS44NzUgMTkuMzEyNSBMIDI0LjU2MjUgMTcuNjg3NSB6IE0gMTMuNSAxOS41IEwgMTEuMTI1IDIwLjUgTCAxMS42ODc1IDI0LjY4NzUgTCAxNi43NSAyOC44NzUgTCAxOC4wNjI1IDI2LjY4NzUgTCAxNS42MjUgMjMuMjUgTCAxNi4xODc1IDIwLjgxMjUgTCAxMy41IDE5LjUgeiAiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/privacy/public.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI0MCIgd2lkdGg9IjQwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDEyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMTIuMzYyMikiIGQ9Im0gMjkuNSwyMCBhIDkuNSw5LjUgMCAxIDEgLTE5LDAgOS41LDkuNSAwIDEgMSAxOSwwIHoiIGlkPSJwYXRoMzEwNyIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MjtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxO3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIvPjxwYXRoIGlkPSJwYXRoMzg3NyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDEyLjM2MjIpIiBkPSJNIDE4LjA2MjUgMTEuMDYyNSBMIDEyLjgxMjUgMTMuNDM3NSBMIDExIDE5LjUgTCAxNi4xMjUgMTguMTg3NSBMIDE2IDE0LjkzNzUgTCAxOC41NjI1IDEyLjkzNzUgTCAxOC4wNjI1IDExLjA2MjUgeiBNIDIzLjMxMjUgMTEuNTYyNSBMIDIyIDE1LjMxMjUgTCAyMy41NjI1IDE1LjM3NSBMIDIyLjM3NSAxNy44MTI1IEwgMjUgMTYuNjI1IEwgMjguODEyNSAxNyBMIDI3LjkzNzUgMTQuMTg3NSBMIDIzLjMxMjUgMTEuNTYyNSB6IE0gMjQuNTYyNSAxNy42ODc1IEwgMjAuMzc1IDIwLjc1IEwgMjIuMzc1IDIzLjA2MjUgTCAyMS43NSAyNi42MjUgTCAyMy45Mzc1IDI4LjY4NzUgTCAyOC41NjI1IDI0LjQzNzUgTCAyOS44NzUgMTkuMzEyNSBMIDI0LjU2MjUgMTcuNjg3NSB6IE0gMTMuNSAxOS41IEwgMTEuMTI1IDIwLjUgTCAxMS42ODc1IDI0LjY4NzUgTCAxNi43NSAyOC44NzUgTCAxOC4wNjI1IDI2LjY4NzUgTCAxNS42MjUgMjMuMjUgTCAxNi4xODc1IDIwLjgxMjUgTCAxMy41IDE5LjUgeiAiIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/smiley/face-cat.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAxOCwxMSBjIDAsNC40MTgyNzggLTMuNTgxNzIyLDggLTgsOCAtNC40MTgyNzgsMCAtOCwtMy41ODE3MjIgLTgsLTggMCwtNC40MTgyNzggMy41ODE3MjIsLTggOCwtOCA0LjQxODI3OCwwIDgsMy41ODE3MjIgOCw4IHoiIGlkPSJwYXRoMjk5NCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIi8+PGcgaWQ9ImczMDI1Ij48cGF0aCBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDM3OTciIGQ9Im0gMTMuNCw4LjUgYyAwLDAuODI4NDI3MSAtMC42NzE1NzMsMS41IC0xLjUsMS41IC0wLjgyODQyOCwwIC0xLjUsLTAuNjcxNTcyOSAtMS41LC0xLjUgMCwtMC44Mjg0MjcxIDAuNjcxNTcyLC0xLjUgMS41LC0xLjUgMC44Mjg0MjcsMCAxLjUsMC42NzE1NzI5IDEuNSwxLjUgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIgaWQ9InVzZTM4MDMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzIuMzYyMikiIGlkPSJwYXRoMzg0MiIgZD0ibSAyLDEwIDAsLTkuMiA2LDIgeiIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgaGVpZ2h0PSIyMCIgd2lkdGg9IjIwIiB0cmFuc2Zvcm09Im1hdHJpeCgtMSwwLDAsMSwyMCwwLjIpIiBpZD0idXNlMzg0NCIgeGxpbms6aHJlZj0iI3BhdGgzODQyIiB5PSIwIiB4PSIwIi8+PHBhdGggaWQ9InBhdGgzODQ4IiBkPSJtIDYuNSwxMDQ1LjM2MjIgYyAwLjIsMC4yIDAuNywwLjUgMS41LDAuNSAxLjIsMCAyLC0xLjMgMiwtMiAwLDAuNyAwLjgsMiAyLDIgMC44LDAgMS4zLC0wLjMgMS41LC0wLjUiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMi4zNjIyKSIgaWQ9InBhdGgzODYwIiBkPSJtIDEwLjgsMTAuNSAtMS42LDAgYyAwLDAuMSAwLjgsMC4zIDAuOCwxIDAsLTAuNyAwLjgsLTAuOSAwLjgsLTEgeiIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PGcgaWQ9ImczODY2Ij48cGF0aCBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojOWM3YTE2O3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiIGQ9Im0gMTQuNSwxMDQzLjM2MjIgMiwwLjUiIGlkPSJwYXRoMzg2MiIvPjxwYXRoIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM5YzdhMTY7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIgZD0ibSAxNC41LDEwNDEuODYyMiAyLDAiIGlkPSJwYXRoMzg2NCIvPjwvZz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJtYXRyaXgoLTEsMCwwLDEsMjAsMCkiIGlkPSJ1c2UzODcwIiB4bGluazpocmVmPSIjZzM4NjYiIHk9IjAiIHg9IjAiLz48L2c+PC9zdmc+Cg==","img/smiley/face-cat-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PHBhdGggZD0ibSAxOCwxMSBjIDAsNC40MTgyNzggLTMuNTgxNzIyLDggLTgsOCAtNC40MTgyNzgsMCAtOCwtMy41ODE3MjIgLTgsLTggMCwtNC40MTgyNzggMy41ODE3MjIsLTggOCwtOCA0LjQxODI3OCwwIDgsMy41ODE3MjIgOCw4IHoiIGlkPSJwYXRoMjk5NCIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMxLjM2MjIpIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMSkiIGlkPSJnMzAyNSI+PHBhdGggc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzk3IiBkPSJNIDEwLjQsOC40OTk5OTk5IEMgMTAuNCw3LjY3MTU3MjggMTEuMDcxNTczLDcgMTEuOSw3IGMgMC44Mjg0MjcsMCAxLjUsMC42NzE1NzI5IDEuNSwxLjUgbCAtMS41LDAgeiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4yLDEwMzEuODYyMikiLz48dXNlIHg9IjAiIHk9IjAiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIgaWQ9InVzZTM4MDMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC00LjA5OTk5OTYsMCkiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzIuMzYyMikiIGlkPSJwYXRoMzg0MiIgZD0ibSAyLDEwIDAsLTkuMiA2LDIgeiIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgaGVpZ2h0PSIyMCIgd2lkdGg9IjIwIiB0cmFuc2Zvcm09Im1hdHJpeCgtMSwwLDAsMSwyMCwwLjIpIiBpZD0idXNlMzg0NCIgeGxpbms6aHJlZj0iI3BhdGgzODQyIiB5PSIwIiB4PSIwIi8+PHBhdGggaWQ9InBhdGgzODQ4IiBkPSJtIDYuNSwxMDQzLjg2MjIgYyAwLjEsMSAwLjYsMiAxLjUsMiAxLDAgMiwtMS43IDIsLTIuOCAwLDEuMSAxLDIuOCAyLDIuOCAwLjgsMCAxLjQsLTEgMS41LC0yIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCBpZD0icGF0aDM4NjAiIGQ9Im0gMTAuOCwxMDQxLjg2MjIgLTEuNiwwIGMgMC4zMjUwNjkyLDAuNDA4MyAwLjgsMC43IDAuOCwxLjIgMCwtMC41IDAuNDk5ODA1LC0wLjg0MDggMC44LC0xLjIgeiIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMSkiIGlkPSJnMzg2NiI+PHBhdGggc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzljN2ExNjtzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIiBkPSJtIDE0LjUsMTA0My4zNjIyIDIsMC41IiBpZD0icGF0aDM4NjIiLz48cGF0aCBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojOWM3YTE2O3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiIGQ9Im0gMTQuNSwxMDQxLjg2MjIgMiwwIiBpZD0icGF0aDM4NjQiLz48L2c+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0ibWF0cml4KC0xLDAsMCwxLDIwLDApIiBpZD0idXNlMzg3MCIgeGxpbms6aHJlZj0iI2czODY2IiB5PSIwIiB4PSIwIi8+PC9nPjwvc3ZnPgo=","img/smiley/face-confused.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gNi41LDEwNDMuODYyMiBjIDEsMS41IDIuNSwxLjUgMy41LDAuNSAxLC0xIDIuNSwtMSAzLjUsMC41IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgZD0ibSAxMy40LDguNSBjIDAsMC44Mjg0MjcxIC0wLjY3MTU3MywxLjUgLTEuNSwxLjUgLTAuODI4NDI4LDAgLTEuNSwtMC42NzE1NzI5IC0xLjUsLTEuNSAwLC0wLjgyODQyNzEgMC42NzE1NzIsLTEuNSAxLjUsLTEuNSAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSB6IiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIgeT0iMCIgeD0iMCIvPjwvZz48L3N2Zz4K","img/smiley/face-confused-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gNi41LDEwNDMuODYyMiBjIDEsMi41IDIuNSwyLjUgMy41LDEgMSwtMS41IDIuNSwtMS41IDMuNSwxIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojNGM0YzRjO3N0cm9rZS13aWR0aDoxcHg7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1vcGFjaXR5OjEiLz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgwLjk2NTkyNTgzLDAuMjU4ODE5MDUsLTAuMjU4ODE5MDUsMC45NjU5MjU4MywyLjcwNTQ0NDksMTAyNy45NzE5KSIgZD0ibSAxMy40LDguNSBjIDAsMC44Mjg0MjcxIC0wLjY3MTU3MywxLjUgLTEuNSwxLjUgLTAuODI4NDI4LDAgLTEuNSwtMC42NzE1NzI5IC0xLjUsLTEuNSAwLDAgMCwwIDAsLTEwZS04IEwgMTEuOSw4LjUgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0ibWF0cml4KC0xLDAsMCwxLDE5Ljk0ODg4OSwwKSIgaWQ9InVzZTM4MDMiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIgeT0iMCIgeD0iMCIvPjwvZz48L3N2Zz4K","img/smiley/face-frown.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gNS41LDEwNDQuODg3MiBjIDEuNSwtMi43IDcuNSwtMi43IDksMCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGQ9Im0gMTMuNCw4LjUgYyAwLDAuODI4NDI3MSAtMC42NzE1NzMsMS41IC0xLjUsMS41IC0wLjgyODQyOCwwIC0xLjUsLTAuNjcxNTcyOSAtMS41LC0xLjUgMCwtMC44Mjg0MjcxIDAuNjcxNTcyLC0xLjUgMS41LC0xLjUgMC44Mjg0MjcsMCAxLjUsMC42NzE1NzI5IDEuNSwxLjUgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48L2c+PC9zdmc+Cg==","img/smiley/face-frown-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjYsMCwwLDEuMDY2NjY2NiwtMC42OTMzMzMzLDEwMjkuMjk1NikiIGQ9Im0gMTMuNCw4LjUgYyAwLDAuODI4NDI3MSAtMC42NzE1NzMsMS41IC0xLjUsMS41IC0wLjgyODQyOCwwIC0xLjUsLTAuNjcxNTcyOSAtMS41LC0xLjUgMCwwIDAsMCAwLC0xMGUtOCBMIDExLjksOC41IHoiIGlkPSJwYXRoMzc5NyIgc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjx1c2UgaGVpZ2h0PSIyMCIgd2lkdGg9IjIwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMy45OTk5OTk2LDApIiBpZD0idXNlMzgwMyIgeGxpbms6aHJlZj0iI3BhdGgzNzk3IiB5PSIwIiB4PSIwIi8+PHBhdGggaWQ9InBhdGgzNzkzIiBkPSJtIDUuNSwxMDQ1LjgzNzIgYyAxLjQ5OTk5OTUsLTUuMyA3LjUsLTUuMyA5LDAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjwvZz48L3N2Zz4K","img/smiley/face-kiss.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gOS41LDEwNDIuODYyMiBjIDAsMCAxLjUsMC41IDEuNSwxIDAsMC41IC0xLjUsMSAtMS41LDEgMCwwIDEuNSwwLjUgMS41LDEgMCwwLjUgLTEuNSwxIC0xLjUsMSIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48L2c+PC9zdmc+Cg==","img/smiley/face-kiss-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gOS41LDEwNDIuODYyMiBjIDAsMCAxLjUsMC41IDEuNSwxIDAsMC41IC0xLjUsMSAtMS41LDEgMCwwIDEuNSwwLjUgMS41LDEgMCwwLjUgLTEuNSwxIC0xLjUsMSIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggdHJhbnNmb3JtPSJtYXRyaXgoMS4wNjY2NjY2LDAsMCwxLC0wLjY5MzMzMjIzLDEwMzAuODYyMikiIGQ9Ik0gMTAuNCw4LjQ5OTk5OTkgQyAxMC40LDcuNjcxNTcyOCAxMS4wNzE1NzMsNyAxMS45LDcgYyAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSBsIC0xLjUsMCB6IiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTQsMCkiIGlkPSJ1c2UzMDU2IiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48L2c+PC9zdmc+Cg==","img/smiley/face-laugh.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM4MzEiIGQ9Im0gNS41LDEwNDIuODYyMiA5LDAgYyAtMS41LDUuMyAtNy41LDUuNCAtOSwwIHoiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMTAwMDAwMzgsMTAzMS4zNjIyKSIgZD0ibSAxMy40LDguNSBjIDAsMC44Mjg0MjcxIC0wLjY3MTU3MywxLjUgLTEuNSwxLjUgLTAuODI4NDI4LDAgLTEuNSwtMC42NzE1NzI5IC0xLjUsLTEuNSAwLC0wLjgyODQyNzEgMC42NzE1NzIsLTEuNSAxLjUsLTEuNSAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSB6IiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTQsMCkiIGlkPSJ1c2UzMDgyIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48L2c+PC9zdmc+Cg==","img/smiley/face-laugh-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjYsMCwwLDEsLTAuNjkzMzMzMywxMDMwLjg2MjIpIiBkPSJNIDEwLjQsOC40OTk5OTk5IEMgMTAuNCw3LjY3MTU3MjggMTEuMDcxNTczLDcgMTEuOSw3IGMgMC44Mjg0MjcsMCAxLjUsMC42NzE1NzI5IDEuNSwxLjUgbCAtMS41LDAgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48cGF0aCBpZD0icGF0aDM4MzEiIGQ9Im0gNS41LDEwNDEuODYyMiA5LDAgYyAtMS41LDYuNyAtNy41LDYuNyAtOSwwIHoiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjwvZz48L3N2Zz4K","img/smiley/face-neutral.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgZD0ibSAxMy40LDguNSBjIDAsMC44Mjg0MjcxIC0wLjY3MTU3MywxLjUgLTEuNSwxLjUgLTAuODI4NDI4LDAgLTEuNSwtMC42NzE1NzI5IC0xLjUsLTEuNSAwLC0wLjgyODQyNzEgMC42NzE1NzIsLTEuNSAxLjUsLTEuNSAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSB6IiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIgeT0iMCIgeD0iMCIvPjxwYXRoIGlkPSJwYXRoMzgzMSIgZD0ibSA1LjUsMTA0My44NjIyIDksMCIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PC9nPjwvc3ZnPgo=","img/smiley/face-neutral-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMC44NjIyKSIgZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsLTEwZS04IEwgMTEuOSw4LjUgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48cGF0aCBpZD0icGF0aDM4MzEiIGQ9Im0gNS41LDEwNDMuODYyMiA5LDAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjwvZz48L3N2Zz4K","img/smiley/face-smile.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48ZyBpZD0iZzMwMjUiPjxwYXRoIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIgZD0ibSA1LjUsMTA0Mi44NjIyIGMgMS41LDIuNyA3LjUsMi43IDksMCIgaWQ9InBhdGgzNzkzIi8+PHBhdGggc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzNzk3IiBkPSJtIDEzLjQsOC41IGEgMS41LDEuNSAwIDEgMSAtMywwIDEuNSwxLjUgMCAxIDEgMywwIHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMSwxMDMxLjM2MjIpIi8+PHVzZSB4PSIwIiB5PSIwIiB4bGluazpocmVmPSIjcGF0aDM3OTciIGlkPSJ1c2UzODAzIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMy45OTk5OTk2LDApIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiLz48L2c+PC9nPjwvc3ZnPgo=","img/smiley/face-smile-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gNS41LDEwNDEuODYyMiBjIDEuNSw1LjMgNy41LDUuMyA5LDAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIHRyYW5zZm9ybT0ibWF0cml4KDEuMDY2NjY2NiwwLDAsMSwtMC42OTMzMzMzLDEwMzAuODYyMikiIGQ9Ik0gMTAuNCw4LjQ5OTk5OTkgQyAxMC40LDcuNjcxNTcyOCAxMS4wNzE1NzMsNyAxMS45LDcgYyAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSBsIC0xLjUsMCB6IiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuOTk5OTk5NiwwKSIgaWQ9InVzZTM4MDMiIHhsaW5rOmhyZWY9IiNwYXRoMzc5NyIgeT0iMCIgeD0iMCIvPjwvZz48L3N2Zz4K","img/smiley/face-surprised.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEEgMi41LDIuNSAwIDAgMSAyLjUsMTMgMi41LDIuNSAwIDAgMSAwLDEwLjUgMi41LDIuNSAwIDAgMSAyLjUsOCAyLjUsMi41IDAgMCAxIDUsMTAuNSBaIiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGEgOCw4IDAgMCAxIC04LDggOCw4IDAgMCAxIC04LC04IDgsOCAwIDAgMSA4LC04IDgsOCAwIDAgMSA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgZD0iTSAxMy40LDguNSBBIDEuNSwxLjUgMCAwIDEgMTEuOSwxMCAxLjUsMS41IDAgMCAxIDEwLjQsOC41IDEuNSwxLjUgMCAwIDEgMTEuOSw3IDEuNSwxLjUgMCAwIDEgMTMuNCw4LjUgWiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48Y2lyY2xlIHI9IjEuNTAwMDAxOSIgY3k9IjEwNDUuMzYyMiIgY3g9IjEwIiBpZD0icGF0aDQ1NTgiIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiLz48L2c+PC9zdmc+Cg==","img/smiley/face-surprised-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEEgMi41LDIuNSAwIDAgMSAyLjUsMTMgMi41LDIuNSAwIDAgMSAwLDEwLjUgMi41LDIuNSAwIDAgMSAyLjUsOCAyLjUsMi41IDAgMCAxIDUsMTAuNSBaIiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGEgOCw4IDAgMCAxIC04LDggOCw4IDAgMCAxIC04LC04IDgsOCAwIDAgMSA4LC04IDgsOCAwIDAgMSA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMC4zNjIyKSIgZD0iTSAxMy40LDguNSBBIDEuNSwxLjUgMCAwIDEgMTEuOSwxMCAxLjUsMS41IDAgMCAxIDEwLjQsOC41IDEuNSwxLjUgMCAwIDEgMTEuOSw3IDEuNSwxLjUgMCAwIDEgMTMuNCw4LjUgWiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48Y2lyY2xlIHI9IjIuNTAwMDAxOSIgY3k9IjEwNDQuMzYyMiIgY3g9IjEwIiBpZD0icGF0aDQ1NTgiIHN0eWxlPSJjb2xvcjojMDAwMDAwO2Rpc3BsYXk6aW5saW5lO292ZXJmbG93OnZpc2libGU7dmlzaWJpbGl0eTp2aXNpYmxlO2ZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxO21hcmtlcjpub25lO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiLz48L2c+PC9zdmc+Cg==","img/smiley/face-tongue.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM4MzEiIGQ9Im0gNS41LDEwNDMuODYyMiA5LDAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGlkPSJwYXRoNDU3MyIgZD0ibSA4LDEwNDQuMzYyMiAwLDEgYyAwLDEgMC41LDIgMiwyIDEuNSwwIDIsLTEgMiwtMiBsIDAsLTEgeiIgc3R5bGU9ImZpbGw6I2RiMzk1NjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjxwYXRoIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIGlkPSJwYXRoMzc5NyIgZD0ibSAxMy40LDguNSBjIDAsMC44Mjg0MjcxIC0wLjY3MTU3MywxLjUgLTEuNSwxLjUgLTAuODI4NDI4LDAgLTEuNSwtMC42NzE1NzI5IC0xLjUsLTEuNSAwLC0wLjgyODQyNzEgMC42NzE1NzIsLTEuNSAxLjUsLTEuNSAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSB6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEwMDAwMDM4LDEwMzEuMzYyMikiLz48dXNlIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTQsMCkiIGlkPSJ1c2UzMDM3IiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48L2c+PC9zdmc+Cg==","img/smiley/face-tongue-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC40OTk4NzUzZS02LDApIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjYsMCwwLDEsLTAuNjkzMzMzMywxMDMwLjg2MjIpIiBkPSJNIDEwLjQsOC40OTk5OTk5IEMgMTAuNCw3LjY3MTU3MjggMTEuMDcxNTczLDcgMTEuOSw3IGMgMC44Mjg0MjcsMCAxLjUsMC42NzE1NzI5IDEuNSwxLjUgbCAtMS41LDAgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHVzZSBoZWlnaHQ9IjIwIiB3aWR0aD0iMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zLjk5OTk5OTYsMCkiIGlkPSJ1c2UzODAzIiB4bGluazpocmVmPSIjcGF0aDM3OTciIHk9IjAiIHg9IjAiLz48cGF0aCBpZD0icGF0aDM4MzEiIGQ9Im0gNS41LDEwNDEuODYyMiA5LDAiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiM0YzRjNGM7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW9wYWNpdHk6MSIvPjxwYXRoIGlkPSJwYXRoNDU3MyIgZD0ibSA4LDEwNDIuMzYyMiAwLDIgYyAwLDEgMC41LDIgMiwyIDEuNSwwIDIsLTEgMiwtMiBsIDAsLTIgeiIgc3R5bGU9ImZpbGw6I2RiMzk1NjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/smiley/face-wink.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjEsMTAzMS4zNjIyKSIgZD0ibSAxMy40LDguNSBhIDEuNSwxLjUgMCAxIDEgLTMsMCAxLjUsMS41IDAgMSAxIDMsMCB6IiBpZD0icGF0aDM3OTciIHN0eWxlPSJmaWxsOiM0YzRjNGM7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48cGF0aCBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDMxNTAiIGQ9Ik0gMTAuNCw4LjQ5OTk5OTkgQyAxMC40LDcuNjcxNTcyOCAxMS4wNzE1NzMsNyAxMS45LDcgYyAwLjgyODQyNywwIDEuNSwwLjY3MTU3MjkgMS41LDEuNSBsIC0xLjUsMCB6IiB0cmFuc2Zvcm09Im1hdHJpeCgxLjA2NjY2NjcsMCwwLC0xLjA2NjY2NjcsLTQuNjkzMzMyOSwxMDQ4LjQyODgpIi8+PHBhdGggc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIiBkPSJtIDUuNSwxMDQyLjg2MjIgYyAxLjUsMi43IDcuNSwyLjcgOSwwIiBpZD0icGF0aDM3OTMiLz48L2c+PC9zdmc+Cg==","img/smiley/face-wink-more.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzIiIGhlaWdodD0iMjAiIHdpZHRoPSIyMCI+PGRlZnMgaWQ9ImRlZnM0Ii8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtMTAzMi4zNjIyKSIgaWQ9ImxheWVyMSI+PGcgaWQ9ImczODcxIj48cGF0aCBzdHlsZT0iZmlsbDojZjZjZTU1O2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiBpZD0icGF0aDI5ODgiIGQ9Ik0gNSwxMC41IEMgNSwxMS44ODA3MTIgMy44ODA3MTE5LDEzIDIuNSwxMyAxLjExOTI4ODEsMTMgMCwxMS44ODA3MTIgMCwxMC41IDAsOS4xMTkyODgxIDEuMTE5Mjg4MSw4IDIuNSw4IDMuODgwNzExOSw4IDUsOS4xMTkyODgxIDUsMTAuNSB6IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjkxOTIzODM4LDAuOTE5MjM0NTEsLTAuOTE5MjM4MzgsMC45MTkyMzQ1MSwxMi4xMDM5MTQsMTAyNS4xNjIxKSIvPjx1c2UgeD0iMCIgeT0iMCIgeGxpbms6aHJlZj0iI3BhdGgyOTg4IiBpZD0idXNlMzgxOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAuNDk5OTk5LDkuMjMxNDg3NWUtNikiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIvPjxwYXRoIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAsMTAzMS4zNjIyKSIgc3R5bGU9ImZpbGw6I2Y2Y2U1NTtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgyOTk0IiBkPSJtIDE4LDExIGMgMCw0LjQxODI3OCAtMy41ODE3MjIsOCAtOCw4IC00LjQxODI3OCwwIC04LC0zLjU4MTcyMiAtOCwtOCAwLC00LjQxODI3OCAzLjU4MTcyMiwtOCA4LC04IDQuNDE4Mjc4LDAgOCwzLjU4MTcyMiA4LDggeiIvPjwvZz48cGF0aCBpZD0icGF0aDM3OTMiIGQ9Im0gNS41LDEwNDEuODYyMiBjIDAuMDEwMDczLDIuOTgzNCAzLjYwMDMwMDYsNC44NzEyIDYuMzEyNSw0LjA3NSIgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzRjNGM0YztzdHJva2Utd2lkdGg6MXB4O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2Utb3BhY2l0eToxIi8+PHBhdGggdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4xLDEwMzEuMzYyMikiIGQ9Im0gMTMuNCw4LjUgYSAxLjUsMS41IDAgMSAxIC0zLDAgMS41LDEuNSAwIDEgMSAzLDAgeiIgaWQ9InBhdGgzNzk3IiBzdHlsZT0iZmlsbDojNGM0YzRjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PHBhdGggc3R5bGU9ImZpbGw6IzRjNGM0YztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgaWQ9InBhdGgzMTUwIiBkPSJNIDEwLjQsOC40OTk5OTk5IEMgMTAuNCw3LjY3MTU3MjggMTEuMDcxNTczLDcgMTEuOSw3IGMgMC44Mjg0MjcsMCAxLjUsMC42NzE1NzI5IDEuNSwxLjUgbCAtMS41LDAgeiIgdHJhbnNmb3JtPSJtYXRyaXgoMS4wNjY2NjY3LDAsMCwtMS4wNjY2NjY3LC00LjY5MzMzMjksMTA0OC40Mjg4KSIvPjwvZz48L3N2Zz4K","img/smiley/heart.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSIyMCIgd2lkdGg9IjIwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMyLjM2MjIpIiBpZD0ibGF5ZXIxIj48cGF0aCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLDEwMzIuMzYyMikiIGlkPSJwYXRoNDM4NSIgZD0iTSAxMCwxNyBDIC02LDYgNiwtMSAxMCw1IGMgNCwtNiAxNiwxIDAsMTIgeiIgc3R5bGU9ImZpbGw6I2RiMzk1NjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIvPjwvZz48L3N2Zz4K","img/smiley/heart-broken.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSIyMCIgd2lkdGg9IjIwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMyLjM2MjIpIiBpZD0ibGF5ZXIxIj48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMS4wNTMxMDM0LDAuNzg1MSkiIGlkPSJnMzAwMiI+PHBhdGggc3R5bGU9ImZpbGw6I2RiMzk1NjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgZD0ibSAxMS40NTMwNCwxMDQ4LjU1MSBjIDE2LjU1MzUwMywtMTAuMTQ3MyA0LjkzNjQ5NSwtMTcuNzY1NyAwLjYyODAyNywtMTEuOTgzMyBsIC0xLjQxODkwMiwxLjkyNzkgMi43NjUyNzUsMi4zNzYzIC0zLjI4MDc4NCwyLjYwMzcgMi40NjIwMSwyLjEzMiB6IiBpZD0icGF0aDQ5NDIiLz48cGF0aCBpZD0icGF0aDQ5NDQiIGQ9Im0gMTAuNjUzMTU3LDEwNDguNTc3MSBjIC0xNi41NTM0OTA5LC0xMC4xNDczIC00LjkzNjQ4MTUsLTE3Ljc2NTcgLTAuNjI4MDE1LC0xMS45ODMzIGwgLTEuMjA5MTc3NSwyLjA2NTcgMi45OTg1MTI1LDIuMDc0MiAtMi45OTA2MzA5LDIuOTMyNiAyLjY3MDkxNjksMS44NjI3IHoiIHN0eWxlPSJmaWxsOiNkYjM5NTY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9nPjwvc3ZnPgo=","img/user-offline-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI2MCIgd2lkdGg9IjYwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05OTIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGlkPSJwYXRoMjk4OSIgZD0ibSAzMi41LDEwMDQuODYyMiBjIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwwLjUwMDggMC4wNzYyNiwwLjk3MDggMC4xODc1LDEuNDM3NSAtMi43MDQ0OTEsMS4zMzM2IC00LjkwNTYyOCwzLjUyNjQgLTYuMjUsNi4yMTg4IC0wLjQ2NDMzNywtMC4xMTAxIC0wLjkzOTUxOCwtMC4xNTYzIC0xLjQzNzUsLTAuMTU2MyAtMy40NTE3OCwwIC02LjI1LDIuNzk4MiAtNi4yNSw2LjI1IDAsMy40NTE4IDIuNzk4MjIsNi4yNSA2LjI1LDYuMjUgMC41MDA3NzYsMCAwLjk3MDc4NCwtMC4wNzYgMS40Mzc1LC0wLjE4NzUgMi4yNDI2OTksNC41NDc5IDYuODk4NzQ5LDcuNjg3NSAxMi4zMTI1LDcuNjg3NSA3LjU5MzkxNSwwIDEzLjc1LC02LjE1NjEgMTMuNzUsLTEzLjc1IDAsLTUuNDEzOCAtMy4xMzk1NTUsLTEwLjA2OTggLTcuNjg3NSwtMTIuMzEyNSAwLjExMTI0MywtMC40NjY3IDAuMTg3NSwtMC45MzY3IDAuMTg3NSwtMS40Mzc1IDAsLTMuNDUxOCAtMi43OTgyMiwtNi4yNSAtNi4yNSwtNi4yNSB6IiBzdHlsZT0iZmlsbDojYTllODdjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/user-offline-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI2MCIgd2lkdGg9IjYwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05OTIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGlkPSJwYXRoMjk4OSIgZD0ibSAzMi41LDEwMDQuODYyMiBjIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwwLjUwMDggMC4wNzYyNiwwLjk3MDggMC4xODc1LDEuNDM3NSAtMi43MDQ0OTEsMS4zMzM2IC00LjkwNTYyOCwzLjUyNjQgLTYuMjUsNi4yMTg4IC0wLjQ2NDMzNywtMC4xMTAxIC0wLjkzOTUxOCwtMC4xNTYzIC0xLjQzNzUsLTAuMTU2MyAtMy40NTE3OCwwIC02LjI1LDIuNzk4MiAtNi4yNSw2LjI1IDAsMy40NTE4IDIuNzk4MjIsNi4yNSA2LjI1LDYuMjUgMC41MDA3NzYsMCAwLjk3MDc4NCwtMC4wNzYgMS40Mzc1LC0wLjE4NzUgMi4yNDI2OTksNC41NDc5IDYuODk4NzQ5LDcuNjg3NSAxMi4zMTI1LDcuNjg3NSA3LjU5MzkxNSwwIDEzLjc1LC02LjE1NjEgMTMuNzUsLTEzLjc1IDAsLTUuNDEzOCAtMy4xMzk1NTUsLTEwLjA2OTggLTcuNjg3NSwtMTIuMzEyNSAwLjExMTI0MywtMC40NjY3IDAuMTg3NSwtMC45MzY3IDAuMTg3NSwtMS40Mzc1IDAsLTMuNDUxOCAtMi43OTgyMiwtNi4yNSAtNi4yNSwtNi4yNSB6IiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/user-offline.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI2MCIgd2lkdGg9IjYwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05OTIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGlkPSJwYXRoMjk4OSIgZD0ibSAzMi41LDEwMDQuODYyMiBjIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwwLjUwMDggMC4wNzYyNiwwLjk3MDggMC4xODc1LDEuNDM3NSAtMi43MDQ0OTEsMS4zMzM2IC00LjkwNTYyOCwzLjUyNjQgLTYuMjUsNi4yMTg4IC0wLjQ2NDMzNywtMC4xMTAxIC0wLjkzOTUxOCwtMC4xNTYzIC0xLjQzNzUsLTAuMTU2MyAtMy40NTE3OCwwIC02LjI1LDIuNzk4MiAtNi4yNSw2LjI1IDAsMy40NTE4IDIuNzk4MjIsNi4yNSA2LjI1LDYuMjUgMC41MDA3NzYsMCAwLjk3MDc4NCwtMC4wNzYgMS40Mzc1LC0wLjE4NzUgMi4yNDI2OTksNC41NDc5IDYuODk4NzQ5LDcuNjg3NSAxMi4zMTI1LDcuNjg3NSA3LjU5MzkxNSwwIDEzLjc1LC02LjE1NjEgMTMuNzUsLTEzLjc1IDAsLTUuNDEzOCAtMy4xMzk1NTUsLTEwLjA2OTggLTcuNjg3NSwtMTIuMzEyNSAwLjExMTI0MywtMC40NjY3IDAuMTg3NSwtMC45MzY3IDAuMTg3NSwtMS40Mzc1IDAsLTMuNDUxOCAtMi43OTgyMiwtNi4yNSAtNi4yNSwtNi4yNSB6IiBzdHlsZT0iZmlsbDojYzBjMGMwO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIi8+PC9nPjwvc3ZnPgo=","img/user-online-pressed-active.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI2MCIgd2lkdGg9IjYwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05OTIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGlkPSJwYXRoMjk4OSIgZD0ibSAzMi41LDEwMDQuODYyMiBjIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwwLjUwMDggMC4wNzYyNiwwLjk3MDggMC4xODc1LDEuNDM3NSAtMi43MDQ0OTEsMS4zMzM2IC00LjkwNTYyOCwzLjUyNjQgLTYuMjUsNi4yMTg4IC0wLjQ2NDMzNywtMC4xMTAxIC0wLjkzOTUxOCwtMC4xNTYzIC0xLjQzNzUsLTAuMTU2MyAtMy40NTE3OCwwIC02LjI1LDIuNzk4MiAtNi4yNSw2LjI1IDAsMy40NTE4IDIuNzk4MjIsNi4yNSA2LjI1LDYuMjUgMC41MDA3NzYsMCAwLjk3MDc4NCwtMC4wNzYgMS40Mzc1LC0wLjE4NzUgMi4yNDI2OTksNC41NDc5IDYuODk4NzQ5LDcuNjg3NSAxMi4zMTI1LDcuNjg3NSA3LjU5MzkxNSwwIDEzLjc1LC02LjE1NjEgMTMuNzUsLTEzLjc1IDAsLTUuNDEzOCAtMy4xMzk1NTUsLTEwLjA2OTggLTcuNjg3NSwtMTIuMzEyNSAwLjExMTI0MywtMC40NjY3IDAuMTg3NSwtMC45MzY3IDAuMTg3NSwtMS40Mzc1IDAsLTMuNDUxOCAtMi43OTgyMiwtNi4yNSAtNi4yNSwtNi4yNSB6IG0gMCwxMi41IGMgMS4zODA3MTIsMCAyLjUsMS4xMTkzIDIuNSwyLjUgMCwxLjM4MDcgLTEuMTE5Mjg4LDIuNSAtMi41LDIuNSAtMS4zODA3MTIsMCAtMi41LC0xLjExOTMgLTIuNSwtMi41IDAsLTEuMzgwNyAxLjExOTI4OCwtMi41IDIuNSwtMi41IHogbSAtNSw1IGMgMS4zODA3MTIsMCAyLjUsMS4xMTkzIDIuNSwyLjUgMCwxLjM4MDcgLTEuMTE5Mjg4LDIuNSAtMi41LDIuNSAtMS4zODA3MTIsMCAtMi41LC0xLjExOTMgLTIuNSwtMi41IDAsLTEuMzgwNyAxLjExOTI4OCwtMi41IDIuNSwtMi41IHoiIHN0eWxlPSJmaWxsOiNhOWU4N2M7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/user-online-pressed.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI2MCIgd2lkdGg9IjYwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05OTIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGlkPSJwYXRoMjk4OSIgZD0ibSAzMi41LDEwMDQuODYyMiBjIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwwLjUwMDggMC4wNzYyNiwwLjk3MDggMC4xODc1LDEuNDM3NSAtMi43MDQ0OTEsMS4zMzM2IC00LjkwNTYyOCwzLjUyNjQgLTYuMjUsNi4yMTg4IC0wLjQ2NDMzNywtMC4xMTAxIC0wLjkzOTUxOCwtMC4xNTYzIC0xLjQzNzUsLTAuMTU2MyAtMy40NTE3OCwwIC02LjI1LDIuNzk4MiAtNi4yNSw2LjI1IDAsMy40NTE4IDIuNzk4MjIsNi4yNSA2LjI1LDYuMjUgMC41MDA3NzYsMCAwLjk3MDc4NCwtMC4wNzYgMS40Mzc1LC0wLjE4NzUgMi4yNDI2OTksNC41NDc5IDYuODk4NzQ5LDcuNjg3NSAxMi4zMTI1LDcuNjg3NSA3LjU5MzkxNSwwIDEzLjc1LC02LjE1NjEgMTMuNzUsLTEzLjc1IDAsLTUuNDEzOCAtMy4xMzk1NTUsLTEwLjA2OTggLTcuNjg3NSwtMTIuMzEyNSAwLjExMTI0MywtMC40NjY3IDAuMTg3NSwtMC45MzY3IDAuMTg3NSwtMS40Mzc1IDAsLTMuNDUxOCAtMi43OTgyMiwtNi4yNSAtNi4yNSwtNi4yNSB6IG0gMCwxMi41IGMgMS4zODA3MTIsMCAyLjUsMS4xMTkzIDIuNSwyLjUgMCwxLjM4MDcgLTEuMTE5Mjg4LDIuNSAtMi41LDIuNSAtMS4zODA3MTIsMCAtMi41LC0xLjExOTMgLTIuNSwtMi41IDAsLTEuMzgwNyAxLjExOTI4OCwtMi41IDIuNSwtMi41IHogbSAtNSw1IGMgMS4zODA3MTIsMCAyLjUsMS4xMTkzIDIuNSwyLjUgMCwxLjM4MDcgLTEuMTE5Mjg4LDIuNSAtMi41LDIuNSAtMS4zODA3MTIsMCAtMi41LC0xLjExOTMgLTIuNSwtMi41IDAsLTEuMzgwNyAxLjExOTI4OCwtMi41IDIuNSwtMi41IHoiIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg==","img/user-online.svg":"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiBpZD0ic3ZnMiIgaGVpZ2h0PSI2MCIgd2lkdGg9IjYwIj48ZGVmcyBpZD0iZGVmczQiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC05OTIuMzYyMikiIGlkPSJsYXllcjEiPjxwYXRoIGlkPSJwYXRoMjk4OSIgZD0ibSAzMi41LDEwMDQuODYyMiBjIC0zLjQ1MTc4LDAgLTYuMjUsMi43OTgyIC02LjI1LDYuMjUgMCwwLjUwMDggMC4wNzYyNiwwLjk3MDggMC4xODc1LDEuNDM3NSAtMi43MDQ0OTEsMS4zMzM2IC00LjkwNTYyOCwzLjUyNjQgLTYuMjUsNi4yMTg4IC0wLjQ2NDMzNywtMC4xMTAxIC0wLjkzOTUxOCwtMC4xNTYzIC0xLjQzNzUsLTAuMTU2MyAtMy40NTE3OCwwIC02LjI1LDIuNzk4MiAtNi4yNSw2LjI1IDAsMy40NTE4IDIuNzk4MjIsNi4yNSA2LjI1LDYuMjUgMC41MDA3NzYsMCAwLjk3MDc4NCwtMC4wNzYgMS40Mzc1LC0wLjE4NzUgMi4yNDI2OTksNC41NDc5IDYuODk4NzQ5LDcuNjg3NSAxMi4zMTI1LDcuNjg3NSA3LjU5MzkxNSwwIDEzLjc1LC02LjE1NjEgMTMuNzUsLTEzLjc1IDAsLTUuNDEzOCAtMy4xMzk1NTUsLTEwLjA2OTggLTcuNjg3NSwtMTIuMzEyNSAwLjExMTI0MywtMC40NjY3IDAuMTg3NSwtMC45MzY3IDAuMTg3NSwtMS40Mzc1IDAsLTMuNDUxOCAtMi43OTgyMiwtNi4yNSAtNi4yNSwtNi4yNSB6IG0gMCwxMi41IGMgMS4zODA3MTIsMCAyLjUsMS4xMTkzIDIuNSwyLjUgMCwxLjM4MDcgLTEuMTE5Mjg4LDIuNSAtMi41LDIuNSAtMS4zODA3MTIsMCAtMi41LC0xLjExOTMgLTIuNSwtMi41IDAsLTEuMzgwNyAxLjExOTI4OCwtMi41IDIuNSwtMi41IHogbSAtNSw1IGMgMS4zODA3MTIsMCAyLjUsMS4xMTkzIDIuNSwyLjUgMCwxLjM4MDcgLTEuMTE5Mjg4LDIuNSAtMi41LDIuNSAtMS4zODA3MTIsMCAtMi41LC0xLjExOTMgLTIuNSwtMi41IDAsLTEuMzgwNyAxLjExOTI4OCwtMi41IDIuNSwtMi41IHoiIHN0eWxlPSJmaWxsOiM4MWQxNDc7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiLz48L2c+PC9zdmc+Cg=="}
    return url => urls[url]
})()

function AccountPage_EmailItem (profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_EmailItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Email')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.email
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.emailPrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable () {
            privacySelect.enable()
            input.disabled = false
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}

function AccountPage_FullNameItem (profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_FullNameItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Full name')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.fullName
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.fullNamePrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable () {
            privacySelect.enable()
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}

function AccountPage_LinkItem (userLink, closeListener) {

    const classPrefix = 'AccountPage_LinkItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Link')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.readOnly = true
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = userLink
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return { element: element }

}

function AccountPage_Page (timezoneList, userLink, username, session,
    profile, editProfileListener, changePasswordListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === profile.fullName &&
            fullNameItem.getPrivacyValue() === profile.fullNamePrivacy &&
            emailItem.getValue() === profile.email &&
            emailItem.getPrivacyValue() === profile.emailPrivacy &&
            phoneItem.getValue() === profile.phone &&
            phoneItem.getPrivacyValue() === profile.phonePrivacy &&
            timezoneItem.getValue() === profile.timezone &&
            timezoneItem.getPrivacyValue() === profile.timezonePrivacy
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'AccountPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const fullNameItem = AccountPage_FullNameItem(profile, checkChanges, close)

    const emailItem = AccountPage_EmailItem(profile, checkChanges, close)

    const phoneItem = AccountPage_PhoneItem(profile, checkChanges, close)

    const timezoneItem = AccountPage_TimezoneItem(timezoneList, profile, checkChanges, close)

    const linkItem = AccountPage_LinkItem(userLink, close)

    const saveChangesNode = TextNode('Save Changes')

    const saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.append(saveChangesNode)
    saveChangesButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        fullNameItem.element,
        emailItem.element,
        phoneItem.element,
        timezoneItem.element,
        linkItem.element,
    ]))
    form.append(saveChangesButton)
    form.addEventListener('submit', e => {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const fullName = fullNameItem.getValue(),
            fullNamePrivacy = fullNameItem.getPrivacyValue(),
            email = emailItem.getValue(),
            emailPrivacy = emailItem.getPrivacyValue(),
            phone = phoneItem.getValue(),
            phonePrivacy = phoneItem.getPrivacyValue(),
            timezone = timezoneItem.getValue(),
            timezonePrivacy = timezoneItem.getPrivacyValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        const url = 'data/editProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&fullNamePrivacy=' + encodeURIComponent(fullNamePrivacy) +
            '&email=' + encodeURIComponent(email) +
            '&emailPrivacy=' + encodeURIComponent(emailPrivacy) +
            '&phone=' + encodeURIComponent(phone) +
            '&phonePrivacy=' + encodeURIComponent(phonePrivacy) +
            '&timezone=' + encodeURIComponent(timezone) +
            '&timezonePrivacy=' + encodeURIComponent(timezonePrivacy)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            editProfileListener({
                fullName: fullName,
                fullNamePrivacy: fullNamePrivacy,
                email: email,
                emailPrivacy: emailPrivacy,
                phone: phone,
                phonePrivacy: phonePrivacy,
                timezone: timezone,
                timezonePrivacy: timezonePrivacy,
            })

        }, requestError)

    })

    const changePasswordButton = document.createElement('button')
    changePasswordButton.className = classPrefix + '-changePasswordButton OrangeButton'
    changePasswordButton.append('Change Password \u203a')
    changePasswordButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    changePasswordButton.addEventListener('click', () => {
        destroy()
        changePasswordListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)
    frameElement.append(changePasswordButton)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile (newProfile) {
            profile = newProfile
            checkChanges()
        },
        focusChangePassword () {
            changePasswordButton.focus()
        },
    }

}

function AccountPage_PhoneItem (profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_PhoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Phone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.phone
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.phonePrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable () {
            privacySelect.enable()
            input.disabled = false
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}

function AccountPage_PrivacySelect (value, changeListener, closeListener) {

    function add (itemValue, text) {

        function click () {
            if (value !== itemValue) setValue(itemValue)
            collapse()
        }

        const item = AccountPag_PrivacySelectItem(text, itemValue, click)

        menuElement.append(item.element)
        if (value === itemValue) valueIndex = items.length
        items.push({
            click: click,
            value: itemValue,
            deselect: item.deselect,
            select: item.select,
        })

    }

    function collapse () {

        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', expandedKeyDown)
        button.addEventListener('click', expand)
        button.addEventListener('keydown', collapsedKeyDown)

        element.removeChild(menuElement)

        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)

        if (selectedIndex !== null) {
            items[selectedIndex].deselect()
            selectedIndex = null
        }

    }

    function collapsedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        } else if (keyCode === 38) {
            e.preventDefault()
            if (valueIndex === 0) return
            valueIndex--
            setValue(items[valueIndex].value)
        } else if (keyCode === 40) {
            e.preventDefault()
            if (valueIndex === items.length - 1) return
            valueIndex++
            setValue(items[valueIndex].value)
        }
    }

    function expand () {

        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.removeEventListener('keydown', collapsedKeyDown)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', expandedKeyDown)

        element.append(menuElement)

        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)

    }

    function expandedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            if (selectedIndex !== null) {
                items[selectedIndex].click()
            }
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            if (selectedIndex === null) {
                selectedIndex = items.length - 1
                items[selectedIndex].select()
            } else if (selectedIndex === 0) {
                items[0].deselect()
                selectedIndex = items.length - 1
                items[selectedIndex].select()
            } else {
                items[selectedIndex].deselect()
                selectedIndex--
                items[selectedIndex].select()
            }
        } else if (keyCode === 40) {
            e.preventDefault()
            if (selectedIndex === null) {
                selectedIndex = 0
                items[0].select()
            } else if (selectedIndex === items.length - 1) {
                items[selectedIndex].deselect()
                selectedIndex = 0
                items[0].select()
            } else {
                items[selectedIndex].deselect()
                selectedIndex++
                items[selectedIndex].select()
            }
        }
    }

    function setValue (newValue) {
        buttonClassList.remove('privacy_' + value)
        value = newValue
        buttonClassList.add('privacy_' + value)
        changeListener()
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    const items = []
    let valueIndex
    let selectedIndex = null

    const classPrefix = 'AccountPage_PrivacySelect'

    const button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button privacy_' + value
    button.addEventListener('click', expand)
    button.addEventListener('keydown', collapsedKeyDown)

    const buttonClassList = button.classList

    const menuElement = Div(classPrefix + '-menu')
    add('private', 'Me')
    add('contacts', 'Contacts')
    add('public', 'Anyone')

    const element = Div(classPrefix)
    element.append(button)

    return {
        element: element,
        disable () {
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        getValue () {
            return value
        },
    }

}

function AccountPag_PrivacySelectItem (text, value, clickListener) {

    const element = Div('AccountPage_PrivacySelectItem privacy_' + value)
    element.append(text)
    element.addEventListener('click', clickListener)

    const classList = element.classList

    return {
        element: element,
        deselect () {
            classList.remove('active')
        },
        select () {
            classList.add('active')
        },
    }

}

function AccountPage_PrivacySelectItemStyle (getResourceUrl) {
    return '.AccountPage_PrivacySelectItem.privacy_private {' +
            'background-image: url(' + getResourceUrl('img/privacy/private.svg') + ')' +
        '}' +
        '.AccountPage_PrivacySelectItem.privacy_contacts {' +
            'background-image: url(' + getResourceUrl('img/privacy/contacts.svg') + ')' +
        '}' +
        '.AccountPage_PrivacySelectItem.privacy_public {' +
            'background-image: url(' + getResourceUrl('img/privacy/public.svg') + ')' +
        '}'
}

function AccountPage_PrivacySelectStyle (getResourceUrl) {

    function getUrl (url) {
        return 'url(' + getResourceUrl(url) + ')'
    }

    const arrow = getUrl('img/arrow.svg'),
        arrowPressed = getUrl('img/arrow-pressed.svg'),
        arrowPressedActive = getUrl('img/arrow-pressed-active.svg')

    return '.AccountPage_PrivacySelect-button.privacy_private {' +
            'background-image: ' +
                getUrl('img/privacy/private.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.privacy_contacts {' +
            'background-image: ' +
                getUrl('img/privacy/contacts.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.privacy_public {' +
            'background-image: ' +
                getUrl('img/privacy/public.svg') + ',' + arrow +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_private {' +
            'background-image: ' +
                getUrl('img/privacy/private-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_contacts {' +
            'background-image: ' +
                getUrl('img/privacy/contacts-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_public {' +
            'background-image: ' +
                getUrl('img/privacy/public-pressed.svg') + ',' +
                arrowPressed +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_private:active {' +
            'background-image: ' +
                getUrl('img/privacy/private-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_contacts:active {' +
            'background-image: ' +
                getUrl('img/privacy/contacts-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}' +
        '.AccountPage_PrivacySelect-button.pressed.privacy_public:active {' +
            'background-image: ' +
                getUrl('img/privacy/public-pressed-active.svg') + ',' +
                arrowPressedActive +
        '}'
}

function AccountPage_TimezoneItem (timezoneList,
    profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_TimezoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Timezone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.addEventListener('change', changeListener)
    timezoneList.forEach(timezone => {
        const option = document.createElement('option')
        option.value = timezone.value
        option.append(timezone.text)
        select.append(option)
    })
    select.value = profile.timezone
    select.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.timezonePrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(select)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            select.disabled = true
            select.blur()
        },
        enable () {
            privacySelect.enable()
            select.disabled = false
        },
        getValue () {
            return parseInt(select.value, 10)
        },
    }

}

function AccountPage_TimezoneItemStyle (getResourceUrl) {
    return '.AccountPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}

function AccountReadyPage (closeListener) {

    const classPrefix = 'AccountReadyPage'

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Welcome!')

    const textElement = Div(classPrefix + '-text')
    textElement.append(TextNode(
        'Your account is ready. Add some' +
        ' contacts to start a conversation.'
    ))

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(textElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus () {
            closeButton.focus()
        },
    }

}

function AddContactPage_Page (showInvite, session,
    username, checkContactListener, userFoundListener,
    closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        usernameItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Find User'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'AddContactPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Add Contact')

    const usernameItem = AddContactPage_UsernameItem(close)

    const buttonNode = TextNode('Find User')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const form = document.createElement('form')
    form.append(usernameItem.element)
    form.append(button)
    form.addEventListener('submit', e => {

        e.preventDefault()
        usernameItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const username = usernameItem.getValue()
        if (username === null) return

        const lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        usernameItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Finding...'

        const url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                usernameItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Find User'
                usernameItem.showError(errorElement => {
                    errorElement.append('There is no such user.')
                })
                return
            }

            userFoundListener(response.username, response.profile)

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)
    if (showInvite) {

        const invitePanelElement = Div(classPrefix + '-invitePanel')
        invitePanelElement.append(InvitePanel(username, close).element)

        frameElement.append(invitePanelElement)

    }

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: usernameItem.focus,
    }

}

function AddContactPage_UsernameItem (closeListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'AddContactPage_UsernameItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Username')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            const value = CollapseSpaces(input.value)
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Username_IsValid(value)) {
                showError(errorElement => {
                    errorElement.append('The username is invalid.')
                })
                return null
            }
            return value
        },
    }

}

function BackButton (listener) {

    const element = document.createElement('button')
    element.className = 'BackButton'
    element.append('\u2039 Back')
    element.addEventListener('click', listener)

    return {
        element: element,
        focus () {
            element.focus()
        },
    }

}

function ChangePasswordPage_CurrentPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'ChangePasswordPage_CurrentPasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Current password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Password_IsValid(value)) {
                showError(errorElement => {
                    errorElement.append('The password is invalid.')
                })
                return null
            }
            return value
        },
    }

}

function ChangePasswordPage_NewPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'ChangePasswordPage_NewPasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Choose a new password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        getValue () {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Password_IsShort(value)) {
                showError(errorElement => {
                    errorElement.append('The password is too short.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Minimum ' + Password_minLength + ' characters required.')
                })
                return null
            }
            if (Password_ContainsOnlyDigits(value)) {
                showError(errorElement => {
                    errorElement.append('The password is too simple.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Use some different symbols.')
                })
                return null
            }
            return value
        },
    }

}

function ChangePasswordPage_Page (session,
    backListener, closeListener, invalidSessionListener) {

    function back () {
        destroy()
        backListener()
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        currentPasswordItem.enable()
        newPasswordItem.enable()
        repeatNewPasswordItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Save'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'ChangePasswordPage_Page'

    const backButton = BackButton(back)

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Change Password')

    const currentPasswordItem = ChangePasswordPage_CurrentPasswordItem(back)

    const newPasswordItem = ChangePasswordPage_NewPasswordItem(back)

    const repeatNewPasswordItem = ChangePasswordPage_RepeatNewPasswordItem(back)

    const buttonNode = TextNode('Save')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        currentPasswordItem.element,
        newPasswordItem.element,
        repeatNewPasswordItem.element,
    ]))
    form.append(button)
    form.addEventListener('submit', e => {

        e.preventDefault()
        currentPasswordItem.clearError()
        newPasswordItem.clearError()
        repeatNewPasswordItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const currentPassword = currentPasswordItem.getValue()
        if (currentPassword === null) return

        const newPassword = newPasswordItem.getValue()
        if (newPassword === null) return

        const repeatNewPassword = repeatNewPasswordItem.getValue(newPassword)
        if (repeatNewPassword === null) return

        currentPasswordItem.disable()
        newPasswordItem.disable()
        repeatNewPasswordItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Saving...'

        const url = 'data/changePassword' +
            '?token=' + encodeURIComponent(session.token) +
            '&currentPassword=' + encodeURIComponent(currentPassword) +
            '&newPassword=' + encodeURIComponent(newPassword)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_CURRENT_PASSWORD') {
                currentPasswordItem.enable()
                newPasswordItem.enable()
                repeatNewPasswordItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Save'
                currentPasswordItem.showError(errorElement => {
                    errorElement.append('The password is incorrect.')
                })
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            closeListener()

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: currentPasswordItem.focus,
    }

}

function ChangePasswordPage_RepeatNewPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'ChangePasswordPage_RepeatNewPasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Retype the new password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        getValue (password) {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (value !== password) {
                showError(errorElement => {
                    errorElement.append('The passwords doesn\'t match.')
                })
                return null
            }
            return value
        },
    }

}

function CheckSession (readyListener, signInListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    let username, token
    try {
        username = localStorage.username
        token = localStorage.token
    } catch (e) {
    }

    let warningCallback
    const hashUsername = location.hash.substr(1)
    if (Username_IsValid(hashUsername)) {
        warningCallback = WelcomePage_UnauthenticatedWarning(hashUsername)
    }

    if (token === undefined) {
        readyListener(username === undefined ? '' : username, warningCallback)
        return
    }

    const url = 'data/restoreSession?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    const request = GetJson(url, () => {

        if (request.status !== 200) {
            serviceErrorListener()
            return
        }

        const response = request.response
        if (response === null) {
            crashListener()
            return
        }

        if (response === 'INVALID_USERNAME' || response === 'INVALID_TOKEN') {
            try {
                delete localStorage.token
            } catch (e) {
            }
            readyListener(username, warningCallback)
            return
        }

        signInListener(username, response)

    }, connectionErrorListener)

}

function CloseButton (listener) {

    const button = document.createElement('button')
    button.className = 'CloseButton'
    button.title = 'Close'
    button.addEventListener('click', listener)

    return {
        element: button,
        addEventListener (name, listener) {
            button.addEventListener(name, listener)
        },
        disable () {
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        focus () {
            button.focus()
        },
    }

}

function CloseButtonStyle (getResourceUrl) {
    return '.CloseButton {' +
            'background-image: url(' + getResourceUrl('img/close.svg') + ')' +
        '}'
}

function CollapseSpaces (string) {
    return string.replace(/^\s+/, '').replace(/\s+$/, '').replace(/\s{2,}/g, ' ')
}

function ConnectionError () {

    const element = Div('FormError')
    element.append('Connection failed.')
    element.append(document.createElement('br'))
    element.append('Please, check your network and try again.')

    return { element: element }

}

function ConnectionErrorPage (reconnectListener) {

    const classPrefix = 'ConnectionErrorPage'

    const textElement = Div(classPrefix + '-text')
    textElement.append('Connection lost.')
    textElement.append(document.createElement('br'))
    textElement.append('Please, check your network and reconnect.')

    const buttonNode = TextNode('Reconnect')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('click', () => {
        button.disabled = true
        buttonNode.nodeValue = 'Reconnecting...'
        reconnectListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}

function ContactRequestPage (session, username, profile,
    addContactListener, ignoreListener, closeListener, invalidSessionListener) {

    function clearError () {
        if (error === null) return
        frameElement.removeChild(error.element)
        error = null
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function disableItems () {
        addContactButton.disabled = true
        ignoreButton.disabled = true
    }

    function showAddError (error) {
        showError(error)
        addContactNode.nodeValue = 'Add Contact'
        addContactButton.focus()
    }

    function showError (_error) {
        addContactButton.disabled = false
        ignoreButton.disabled = false
        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
    }

    function showIgnoreError (error) {
        showError(error)
        ignoreNode.nodeValue = 'Ignore'
        ignoreButton.focus()
    }

    let error = null
    let request = null

    const classPrefix = 'ContactRequestPage'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append(TextNode(
        '" has added you to his/her contacts.' +
        ' Would you like to add him/her to your contacts?'
    ))

    const addContactNode = TextNode('Add Contact')

    const addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.append(addContactNode)
    addContactButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    addContactButton.addEventListener('click', () => {

        clearError()
        disableItems()
        addContactNode.nodeValue = 'Adding...'

        let url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        const timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showAddError(ServiceError())
                return
            }

            if (response === null) {
                showAddError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, () => {
            request = null
            showAddError(ConnectionError())
        })

    })

    const ignoreNode = TextNode('Ignore')

    const ignoreButton = document.createElement('button')
    ignoreButton.className = classPrefix + '-ignoreButton OrangeButton'
    ignoreButton.append(ignoreNode)
    ignoreButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    ignoreButton.addEventListener('click', () => {

        clearError()
        disableItems()
        ignoreNode.nodeValue = 'Ignoring...'

        const url = 'data/ignoreRequest' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showIgnoreError(ServiceError())
                return
            }

            if (response === null) {
                showIgnoreError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            ignoreListener(response)

        }, () => {
            request = null
            showIgnoreError(ConnectionError())
        })

    })

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.append(addContactButton)
    buttonsElement.append(ignoreButton)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)
    frameElement.append(buttonsElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        edit (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}

function ContactSelectPage_Item (contact, selectListener,
    deselectListener, removeListener, closeListener) {

    function deselect () {
        selected = false
        button.classList.remove('selected')
        clickElement.removeEventListener('click', deselect)
        clickElement.addEventListener('click', select)
        deselectListener()
    }

    function select () {
        selected = true
        button.classList.add('selected')
        clickElement.removeEventListener('click', select)
        clickElement.addEventListener('click', deselect)
        selectListener()
    }

    let selected = false

    const classPrefix = 'ContactSelectPage_Item'

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const clickNode = TextNode(contact.getDisplayName())

    const clickElement = Div(classPrefix + '-click')
    clickElement.append(button)
    clickElement.addEventListener('click', select)
    clickElement.append(clickNode)

    const element = Div(classPrefix)
    element.append(clickElement)

    contact.removeEvent.addListener(removeListener)

    return {
        contact: contact,
        element: element,
        destroy () {
            contact.removeEvent.removeListener(removeListener)
        },
        focus () {
            button.focus()
        },
        isSelected () {
            return selected
        },
        update () {
            clickNode.nodeValue = contact.getDisplayName()
        },
    }

}

function ContactSelectPage_ItemStyle (getResourceUrl) {
    return '.ContactSelectPage_Item-button.selected {' +
            'background-image: url(' + getResourceUrl('img/checked/normal.svg') + ')' +
        '}' +
        '.ContactSelectPage_Item-button.selected:active {' +
            'background-image: url(' + getResourceUrl('img/checked/active.svg') + ')' +
        '}'
}

function ContactSelectPage_Page (contacts, selectListener, closeListener) {

    function addContact (contact) {

        const username = contact.username

        const item = ContactSelectPage_Item(contact, () => {
            selectedContacts.push(item.contact)
            if (selectedContacts.length === 1) button.disabled = false
        }, () => {
            selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            if (selectedContacts.length === 0) button.disabled = true
        }, () => {

            if (item.isSelected()) {
                selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            }

            delete itemsMap[username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            listElement.removeChild(item.element)

        }, close)

        itemsMap[username] = item
        itemsArray.push(item)
        listElement.append(item.element)

    }

    function close () {
        itemsArray.forEach(item => {
            item.destroy()
        })
        closeListener()
    }

    const itemsMap = Object.create(null)
    const itemsArray = []
    const selectedContacts = []

    const classPrefix = 'ContactSelectPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Select Contact')

    const listElement = Div(classPrefix + '-list')
    contacts.forEach(addContact)

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.disabled = true
    button.append('Send')
    button.addEventListener('click', () => {
        selectListener(selectedContacts)
    })
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(listElement)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        addContact: addContact,
        element: element,
        focus () {
            itemsArray[0].focus()
        },
        reorder (contact, index) {

            const item = itemsMap[contact.username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            itemsArray.splice(index, 0, item)

            const nextItem = itemsArray[index + 1]
            if (nextItem === undefined) {
                listElement.append(item.element)
            } else {
                listElement.insertBefore(item.element, nextItem.element)
            }

            item.update()

        },
    }

}

function CrashError () {

    const element = Div('FormError')
    element.append('Something went wrong.')
    element.append(document.createElement('br'))
    element.append('Please, try again.')

    return { element: element }

}

function CrashPage (reloadListener) {

    const classPrefix = 'CrashPage'

    const textElement = Div(classPrefix + '-text')
    textElement.append('Something went wrong.')
    textElement.append(document.createElement('br'))
    textElement.append('Reloading may fix the problem.')

    const buttonNode = TextNode('Reload')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('click', () => {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}

function Day (time) {
    return Math.floor(time / (1000 * 60 * 60 * 24))
}

function Div (className) {
    const div = document.createElement('div')
    div.className = className
    return div
}

function EditContactPage_EmailItem (profile,
    overrideProfile, changeListener, closeListener) {

    const classPrefix = 'EditContactPage_EmailItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Email')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.email
    input.value = overrideProfile.email
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        disable () {
            input.disabled = true
            input.blur()
        },
        editProfile (profile) {
            input.placeholder = profile.email
        },
        enable () {
            input.disabled = false
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}

function EditContactPage_FullNameItem (profile,
    overrideProfile, changeListener, closeListener) {

    const classPrefix = 'EditContactPage_FullNameItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Full name')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.fullName
    input.value = overrideProfile.fullName
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        disable () {
            input.disabled = true
            input.blur()
        },
        editProfile (profile) {
            input.placeholder = profile.fullName
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}

function EditContactPage_LinkItem (userLink, closeListener) {

    const classPrefix = 'EditContactPage_LinkItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Link')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.readOnly = true
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = userLink
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return { element: element }

}

function EditContactPage_Page (timezoneList, session, userLink,
    username, profile, overrideProfile, overrideProfileListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === overrideProfile.fullName &&
            emailItem.getValue() === overrideProfile.email &&
            phoneItem.getValue() === overrideProfile.phone &&
            timezoneItem.getValue() === overrideProfile.timezone
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'EditContactPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const fullNameItem = EditContactPage_FullNameItem(profile,
        overrideProfile, checkChanges, close)

    const emailItem = EditContactPage_EmailItem(profile,
        overrideProfile, checkChanges, close)

    const phoneItem = EditContactPage_PhoneItem(profile,
        overrideProfile, checkChanges, close)

    const timezoneItem = EditContactPage_TimezoneItem(timezoneList,
        profile, overrideProfile, checkChanges, close)

    const linkItem = EditContactPage_LinkItem(userLink, close)

    const saveChangesNode = TextNode('Save Changes')

    const saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.append(saveChangesNode)
    saveChangesButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        fullNameItem.element,
        emailItem.element,
        phoneItem.element,
        timezoneItem.element,
        linkItem.element,
    ]))
    form.append(saveChangesButton)
    form.addEventListener('submit', e => {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const fullName = fullNameItem.getValue()
        const email = emailItem.getValue()
        const phone = phoneItem.getValue()
        const timezone = timezoneItem.getValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        let url = 'data/overrideContactProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&email=' + encodeURIComponent(email) +
            '&phone=' + encodeURIComponent(phone)
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            overrideProfileListener({
                fullName: fullName,
                email: email,
                phone: phone,
                timezone: timezone,
            })

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile (profile) {
            fullNameItem.editProfile(profile)
            emailItem.editProfile(profile)
            phoneItem.editProfile(profile)
        },
        overrideProfile (_overrideProfile) {
            overrideProfile = _overrideProfile
            checkChanges()
        },
    }

}

function EditContactPage_PhoneItem (profile,
    overrideProfile, changeListener, closeListener) {

    const classPrefix = 'EditContactPage_PhoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Phone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.phone
    input.value = overrideProfile.phone
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        disable () {
            input.disabled = true
            input.blur()
        },
        editProfile (profile) {
            input.placeholder = profile.phone
        },
        enable () {
            input.disabled = false
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}

function EditContactPage_TimezoneItem (timezoneList, profile,
    overrideProfile, changeListener, closeListener) {

    function format (value) {
        if (value === null) return ''
        return Timezone_Format(value)
    }

    const classPrefix = 'EditContactPage_TimezoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Timezone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const emptyNode = TextNode(format(profile.timezone))

    const emptyOption = document.createElement('option')
    emptyOption.append(emptyNode)

    const select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.append(emptyOption)
    timezoneList.forEach(timezone => {
        const option = document.createElement('option')
        option.value = timezone.value
        option.append(timezone.text)
        select.append(option)
    })
    select.value = overrideProfile.timezone
    select.addEventListener('change', changeListener)
    select.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(select)

    return {
        element: element,
        disable () {
            select.disabled = true
            select.blur()
        },
        editProfile (profile) {
            emptyNode.nodeValue = format(profile.timezone)
        },
        enable () {
            select.disabled = false
        },
        getValue () {
            const value = select.value
            if (value === '') return null
            return parseInt(value, 10)
        },
    }

}

function EditContactPage_TimezoneItemStyle (getResourceUrl) {
    return '.EditContactPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}

function Element (...args) {

    const name = typeof args[0] === 'string' ? args.shift() : 'div'
    const properties = args[0] instanceof Array ? {} : args.shift()
    const content = args[0] instanceof Array ? args.shift() : []

    const element = document.createElement(name)
    for (const i in properties) {
        if (i === 'style') {
            Object.assign(element.style, properties[i])
            continue
        }
        element[i] = properties[i]
    }
    element.append(...content)
    return element

}

function EmptyFieldError (errorElement) {
    errorElement.append('This field is required.')
}

function FormatBytes () {
    const suffixes = ['KB', 'MB', 'GB', 'TB', 'PB']
    return function FormatBytes (size) {

        let suffix = 'B'
        let index = 0
        while (true) {
            if (size < 1024) break
            size = size / 1024
            suffix = suffixes[index]
            index++
            if (index === suffixes.length) break
        }

        return size.toFixed(1).replace(/\.?0+$/, '') + ' ' + suffix

    }
}

function FormatText (smileys) {

    const smileyComponents = smileys.array.map(smiley => {
        return smiley.text.replace(/([()|*])/g, '\\$1')
    }).sort((a, b) => {
        return a.length > b.length ? -1 : 1
    })

    const pattern = '(?:(' + smileyComponents.join('|') + ')|https?://\\S+)'

    return (text, textCallback, linkCallback, smileyCallback) => {

        let index = 0
        const regex = new RegExp(pattern, 'gi')
        while (true) {

            const match = regex.exec(text)
            if (match === null) break

            const matchIndex = match.index
            if (matchIndex > index) textCallback(text.substring(index, matchIndex))

            const content = match[0]
            if (match[1] === undefined) linkCallback(content)
            else smileyCallback(content, smileys.map[content.toLowerCase()].file)

            index = matchIndex + content.length

        }
        if (index < text.length) textCallback(text.substring(index, text.length))

    }

}

function FoundContactPage (username, profile,
    openListener, backListener, closeListener) {

    const backButton = BackButton(backListener)

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const classPrefix = 'FoundContactPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append('Start a Conversation')
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })
    button.addEventListener('click', openListener)

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append('" is already in your contacts.')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus () {
            button.focus()
        },
    }

}

function FoundSelfPage (username, profile, backListener, closeListener) {

    const backButton = BackButton(backListener)

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const classPrefix = 'FoundSelfPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append('" it\'s you.')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
        username: username,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}

function FoundUserPage (session, username, profile,
    addContactListener, backListener, closeListener, invalidSessionListener) {

    function back () {
        destroy()
        backListener()
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        button.disabled = false
        buttonNode.nodeValue = 'Add Contact'

        error = _error
        frameElement.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const backButton = BackButton(back)

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const classPrefix = 'FoundUserPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const buttonNode = TextNode('Add Contact')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })
    button.addEventListener('click', () => {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        let url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        const timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        button.disabled = true
        buttonNode.nodeValue = 'Adding...'

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, requestError)

    })

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus () {
            button.focus()
        },
    }

}

function GetJson (url, loadCallback, errorCallback) {
    const request = new XMLHttpRequest
    request.open('get', url)
    request.responseType = 'json'
    request.onerror = errorCallback
    request.onload = loadCallback
    request.send()
    return request
}

function InvitePanel (username, backListener) {

    const classPrefix = 'InvitePanel'

    const link = location.protocol + '//' +
        location.host + location.pathname + '#' + username

    const text = 'You can chat with me at ' + link

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Invite People')

    const smsButton = document.createElement('a')
    smsButton.target = '_blank'
    smsButton.className = classPrefix + '-smsButton OrangeButton'
    smsButton.append('Via SMS')
    smsButton.href = 'sms:?body=' + encodeURIComponent(text)
    smsButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const emailButton = document.createElement('a')
    emailButton.target = '_blank'
    emailButton.className = classPrefix + '-emailButton OrangeButton'
    emailButton.append('Via Email')
    emailButton.href = 'mailto:?body=' + encodeURIComponent(text)
    emailButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const element = Div(classPrefix)
    element.append(titleElement)
    element.append(smsButton)
    element.append(emailButton)

    return { element: element }

}

function IsChildElement (parentNode, childNode) {
    while (childNode !== null) {
        if (childNode === parentNode) return true
        childNode = childNode.parentNode
    }
    return false
}

function IsLocalLink () {

    const localPrefix = location.protocol + '//' +
        location.host + location.pathname + '#'

    return link => {
        if (link.substr(0, localPrefix.length) !== localPrefix) return false
        const username = link.substr(localPrefix.length)
        return Username_IsValid(username)
    }

}

function LoadingPage_Page () {

    const classPrefix = 'LoadingPage'

    const contentElement = Div(classPrefix + '-content')
    contentElement.append('Reconnecting...')

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(contentElement)

    return { element: element }

}

function Minute (time) {
    return Math.floor(time / (1000 * 60))
}

function NoSuchUserPage (username, closeListener) {

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const classPrefix = 'NoSuchUserPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('There is no such user.')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(textElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
    }

}

function Page_Blocks (spacing, content) {
    return Element({ className: 'Page_Blocks ' + spacing }, content)
}

function PageStyle (getResourceUrl) {
    return '.Page {' +
            'background-image: url(' + getResourceUrl('img/grass.svg') + '),' +
                ' url(' + getResourceUrl('img/clouds.svg') + ')' +
        '}'
}

function Password_ContainsOnlyDigits (password) {
    return /^\d+$/.test(password)
}

function Password_IsShort (password) {
    return password.length < Password_minLength
}

function Password_IsValid (password) {
    return !Password_IsShort(password) && !Password_ContainsOnlyDigits(password)
}

const Password_minLength = 6

function PrivacyPage (back) {

    const classPrefix = 'PrivacyPage'

    const backButton = BackButton(back)

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Privacy Policy')

    const text =
        'We:\n' +
        '* Encrypt your connection using SSL.\n' +
        '* Keep your account details.\n' +
        '* Keep your contact list.\n' +
        '* Queue your messages until delivered.\n' +
        '* Proxy your file transfers.\n' +
        'What don\'t:\n' +
        '* Censor you communication.\n' +
        '* Store your chat history.\n' +
        '* Disclose your data to anyone.\n' +
        '* Sell your data.\n' +
        '* Show ads.'

    const textElement = Div(classPrefix + '-text')
    textElement.append(text)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(titleElement)
    frameElement.append(textElement)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus: backButton.focus,
    }

}

function RemoveContactPage (username, session,
    removeListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        removeButton.disabled = false
        cancelButton.disabled = false
        removeNode.nodeValue = 'Remove Contact'

        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
        removeButton.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'RemoveContactPage'

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('Are you sure you want to remove "')
    textElement.append(usernameElement)
    textElement.append('" from your contacts?')

    const removeNode = TextNode('Remove Contact')

    const removeButton = document.createElement('button')
    removeButton.className = classPrefix + '-removeButton GreenButton'
    removeButton.append(removeNode)
    removeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    removeButton.addEventListener('click', () => {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        removeButton.disabled = true
        cancelButton.disabled = true
        removeNode.nodeValue = 'Removing...'

        const url = 'data/removeContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            removeListener()

        }, requestError)

    })

    const cancelButton = document.createElement('button')
    cancelButton.className = classPrefix + '-cancelButton OrangeButton'
    cancelButton.append('Cancel')
    cancelButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    cancelButton.addEventListener('click', closeListener)

    const buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.append(removeButton)
    buttonsElement.append(cancelButton)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(buttonsElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus () {
            removeButton.focus()
        },
    }

}

function RenderUserLink () {
    const prefix = location.protocol + '//' +
        location.host + location.pathname + '#'
    return username => {
        return prefix + username
    }
}

function ServiceError () {

    const element = Div('FormError')
    element.append('Operation failed.')
    element.append(document.createElement('br'))
    element.append('Please, try again.')

    return { element: element }

}

function ServiceErrorPage (reloadListener) {

    const classPrefix = 'ServiceErrorPage'

    const text = 'There is a problem at Bazgu. We are fixing it.' +
        ' Reload the page to see if it\'s resolved.'

    const textElement = Div(classPrefix + '-text')
    textElement.append(text)

    const buttonNode = TextNode('Reload')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('click', () => {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}

function SignOutPage (confirmListener, closeListener) {

    const classPrefix = 'SignOutPage'

    const textElement = Div(classPrefix + '-text')
    textElement.append('Are you sure')
    textElement.append(document.createElement('br'))
    textElement.append('you want to sign out?')

    const yesButton = document.createElement('button')
    yesButton.className = classPrefix + '-yesButton GreenButton'
    yesButton.append('Sign Out')
    yesButton.addEventListener('click', confirmListener)
    yesButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const noButton = document.createElement('button')
    noButton.className = classPrefix + '-noButton OrangeButton'
    noButton.append('Cancel')
    noButton.addEventListener('click', closeListener)
    noButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(yesButton)
    frameElement.append(noButton)

    const element = Div(classPrefix)
    element.append(frameElement)
    element.append(Div(classPrefix + '-aligner'))
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus () {
            yesButton.focus()
        },
    }

}

function SignUpPage_AdvancedItem (expandListener, collapseListener, backListener) {

    function collapse () {
        button.removeEventListener('click', collapse)
        button.addEventListener('click', expand)
        arrowClassList.remove('expanded')
        collapseListener()
    }

    function expand () {
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        arrowClassList.add('expanded')
        expandListener()
    }

    const classPrefix = 'SignUpPage_AdvancedItem'

    const arrowElement = Div(classPrefix + '-arrow')

    const arrowClassList = arrowElement.classList

    const button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button'
    button.append(arrowElement)
    button.append('Advanced settings')
    button.addEventListener('click', expand)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const element = Div(classPrefix)
    element.append(button)
    element.append(Div(classPrefix + '-line'))

    return { element: element }

}

function SignUpPage_AdvancedItemStyle (getResourceUrl) {
    return '.SignUpPage_AdvancedItem-arrow {' +
            'background-image: url(' + getResourceUrl('img/arrow-collapsed.svg') + ')' +
        '}' +
        '.SignUpPage_AdvancedItem-arrow.expanded {' +
            'background-image: url(' + getResourceUrl('img/arrow-expanded.svg') + ')' +
        '}'
}

function SignUpPage_AdvancedPanel (timezoneList, backListener) {

    const timezoneItem = SignUpPage_TimezoneItem(timezoneList, backListener)

    const element = Div('SignUpPage_AdvancedPanel')
    element.append(timezoneItem.element)

    const classList = element.classList

    return {
        disable: timezoneItem.disable,
        element: element,
        enable: timezoneItem.enable,
        getTimezoneValue: timezoneItem.getValue,
        hide () {
            classList.remove('visible')
        },
        show () {
            classList.add('visible')
        },
    }

}

function SignUpPage_CaptchaItem (loadListener, backListener) {

    function disable () {
        input.disabled = true
        input.blur()
    }

    function enable () {
        input.disabled = false
    }

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function requestError () {
        request = null
        imageElement.removeChild(loadingNode)
        showCaptchaError()
    }

    function load () {

        disable()

        request = GetJson('data/captcha', () => {

            const status = request.status,
                response = request.response

            request = null
            imageElement.removeChild(loadingNode)

            if (status !== 200) {
                showCaptchaError()
                return
            }

            if (response === null) {
                showCaptchaError()
                return
            }

            enable()
            setCaptcha(response)
            loadListener()

        }, requestError)

    }

    function setCaptcha (captcha) {
        imageElement.style.backgroundImage = 'url(' + captcha.image + ')'
        token = captcha.token
    }

    function showCaptchaError () {

        const reloadButton = document.createElement('button')
        reloadButton.type = 'button'
        reloadButton.className = classPrefix + '-reloadButton GreenButton'
        reloadButton.append('Reload')
        reloadButton.addEventListener('click', () => {
            imageBarElement.removeChild(reloadButton)
            imageElement.removeChild(errorNode)
            imageElement.append(loadingNode)
            imageClassList.remove('error')
            load()
        })

        const errorNode = TextNode('Failed')

        imageBarElement.append(reloadButton)
        imageElement.append(errorNode)
        imageClassList.add('error')

    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null
    let request = null

    let token = null

    const classPrefix = 'SignUpPage_CaptchaItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Verification')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const loadingNode = TextNode('Loading...')

    const imageElement = Div(classPrefix + '-image')
    imageElement.append(loadingNode)

    const imageClassList = imageElement.classList

    const imageBarElement = Div(classPrefix + '-imageBar')
    imageBarElement.append(imageElement)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(imageBarElement)
    element.append(input)

    load()

    return {
        disable: disable,
        element: element,
        enable: enable,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        destroy () {
            if (request === null) return
            request.abort()
            request = null
        },
        getValue () {

            const value = input.value.replace(/\s+/g, '')
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }

            return {
                value: value,
                token: token,
            }

        },
        setNewCaptcha (newCaptcha) {
            setCaptcha(newCaptcha)
            input.value = ''
        },
    }

}

function SignUpPage_Page (timezoneList, backListener, signUpListener) {

    function back () {
        destroy()
        backListener()
    }

    function destroy () {
        captchaItem.destroy()
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        repeatPasswordItem.enable()
        captchaItem.enable()
        advancedPanel.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Sign Up'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        form.insertBefore(error.element, button)
        button.focus()
    }

    let error = null
    let request = null

    const classPrefix = 'SignUpPage_Page'

    const backButton = BackButton(back)

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Create an Account')

    const usernameItem = SignUpPage_UsernameItem(back)

    const passwordItem = SignUpPage_PasswordItem(back)

    const repeatPasswordItem = SignUpPage_RepeatPasswordItem(back)

    const captchaItem = SignUpPage_CaptchaItem(() => {
        button.disabled = false
    }, back)

    const advancedPanel = SignUpPage_AdvancedPanel(timezoneList, back)

    const advancedItem = SignUpPage_AdvancedItem(advancedPanel.show, advancedPanel.hide, back)

    const buttonNode = TextNode('Sign Up')

    const button = document.createElement('button')
    button.disabled = true
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        usernameItem.element,
        passwordItem.element,
        repeatPasswordItem.element,
        captchaItem.element,
        advancedItem.element,
        advancedPanel.element,
    ]))
    form.append(button)
    form.addEventListener('submit', e => {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()
        repeatPasswordItem.clearError()
        captchaItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const username = usernameItem.getValue()
        if (username === null) return

        const password = passwordItem.getValue()
        if (password === null) return

        const repeatPassword = repeatPasswordItem.getValue(password)
        if (repeatPassword === null) return

        const captcha = captchaItem.getValue()
        if (captcha === null) return

        usernameItem.disable()
        passwordItem.disable()
        repeatPasswordItem.disable()
        captchaItem.disable()
        advancedPanel.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Signing up...'

        const url = 'data/signUp?username=' + encodeURIComponent(username) +
            '&password=' + encodeURIComponent(password) +
            '&captcha_token=' + encodeURIComponent(captcha.token) +
            '&captcha_value=' + encodeURIComponent(captcha.value) +
            '&timezone=' + encodeURIComponent(advancedPanel.getTimezoneValue())

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response.error === 'USERNAME_UNAVAILABLE') {
                enableItems()
                usernameItem.showError(errorElement => {
                    errorElement.append('The username is unavailable.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Please, try something else.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('What about ')
                    errorElement.append((() => {
                        const b = document.createElement('b')
                        b.className = classPrefix + '-offerUsername'
                        b.append(response.offerUsername)
                        return b
                    })())
                    errorElement.append('?')
                })
                return
            }

            if (response.error === 'INVALID_CAPTCHA_TOKEN') {
                enableItems()
                captchaItem.setNewCaptcha(response.newCaptcha)
                captchaItem.showError(errorElement => {
                    errorElement.append('The verification has expired.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Please, try again.')
                })
                return
            }

            if (response === 'INVALID_CAPTCHA_VALUE') {
                enableItems()
                captchaItem.showError(errorElement => {
                    errorElement.append('The verification is invalid.')
                })
                return
            }

            signUpListener(username, response)

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus: usernameItem.focus,
    }

}

function SignUpPage_PasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'SignUpPage_PasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Choose a password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        getValue () {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Password_IsShort(value)) {
                showError(errorElement => {
                    errorElement.append('The password is too short.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Minimum ' + Password_minLength + ' characters required.')
                })
                return null
            }
            if (Password_ContainsOnlyDigits(value)) {
                showError(errorElement => {
                    errorElement.append('The password is too simple.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Use some different symbols.')
                })
                return null
            }
            return value
        },
    }

}

function SignUpPage_RepeatPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'SignUpPage_RepeatPasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Retype the password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        getValue (password) {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (value !== password) {
                showError(errorElement => {
                    errorElement.append('The passwords doesn\'t match.')
                })
                return null
            }
            return value
        },
    }

}

function SignUpPage_TimezoneItem (timezoneList, backListener) {

    const classPrefix = 'SignUpPage_TimezoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Timezone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const defaultValue = -(new Date).getTimezoneOffset()

    const defaultOption = document.createElement('option')
    defaultOption.value = defaultValue
    defaultOption.append(Timezone_Format(defaultValue))

    const select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.append(defaultOption)
    timezoneList.forEach(timezone => {
        const option = document.createElement('option')
        option.value = timezone.value
        option.append(timezone.text)
        select.append(option)
    })
    select.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(select)

    return {
        element: element,
        disable () {
            select.disabled = true
            select.blur()
        },
        enable () {
            select.disabled = false
        },
        getValue () {
            return parseInt(select.value, 10)
        },
    }

}

function SignUpPage_TimezoneItemStyle (getResourceUrl) {
    return '.SignUpPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}

function SignUpPage_UsernameItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'SignUpPage_UsernameItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Username')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.maxLength = Username_maxLength
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            const value = CollapseSpaces(input.value)
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Username_ContainsIllegalChars(value)) {
                showError(errorElement => {
                    errorElement.append('The username is illegal.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Use latin letters, digits, underscore, dot and dash.')
                })
                return null
            }
            if (Username_IsShort(value)) {
                showError(errorElement => {
                    errorElement.append('The username is too short.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Minimum ' + Username_minLength + ' characters required.')
                })
                return null
            }
            return value
        },
    }

}

function SimpleContactPage (username,
    profile, openListener, closeListener) {

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const classPrefix = 'SimpleContactPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append('Start a Conversation')
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    button.addEventListener('click', openListener)

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append('" is already in your contacts.')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus () {
            button.focus()
        },
    }

}

function SimpleSelfPage (username, profile, closeListener) {

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const classPrefix = 'SimpleSelfPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append('" it\'s you.')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
        username: username,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}

function SimpleUserPage (session, username, profile,
    addContactListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        button.disabled = false
        buttonNode.nodeValue = 'Add Contact'

        error = _error
        frameElement.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const classPrefix = 'SimpleUserPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const buttonNode = TextNode('Add Contact')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    button.addEventListener('click', () => {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        let url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        const timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        button.disabled = true
        buttonNode.nodeValue = 'Adding...'

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, requestError)

    })

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus () {
            button.focus()
        },
    }

}

function Smileys () {

    const array = [{
        text: ':)',
        file: 'face-smile',
    }, {
        text: ':))',
        file: 'face-smile-more',
    }, {
        text: ':(',
        file: 'face-frown',
    }, {
        text: ':((',
        file: 'face-frown-more',
    }, {
        text: ':d',
        file: 'face-laugh',
    }, {
        text: ':dd',
        file: 'face-laugh-more',
    }, {
        text: ':p',
        file: 'face-tongue',
    }, {
        text: ':pp',
        file: 'face-tongue-more',
    }, {
        text: ';)',
        file: 'face-wink',
    }, {
        text: ';))',
        file: 'face-wink-more',
    }, {
        text: ':o',
        file: 'face-surprised',
    }, {
        text: ':oo',
        file: 'face-surprised-more',
    }, {
        text: ':s',
        file: 'face-confused',
    }, {
        text: ':ss',
        file: 'face-confused-more',
    }, {
        text: ':3',
        file: 'face-cat',
    }, {
        text: ':33',
        file: 'face-cat-more',
    }, {
        text: ':|',
        file: 'face-neutral',
    }, {
        text: ':||',
        file: 'face-neutral-more',
    }, {
        text: ':*',
        file: 'face-kiss',
    }, {
        text: ':**',
        file: 'face-kiss-more',
    }, {
        text: '<3',
        file: 'heart',
    }, {
        text: '</3',
        file: 'heart-broken',
    }]

    const map = Object.create(null)
    array.forEach(smiley => {
        map[smiley.text] = smiley
    })

    const table = [
        [':)', ':(', ':d', ':p', ':|'],
        [':))', ':((', ':dd', ':pp', ':||'],
        [';)', ':3', ':s', ':o', ':*'],
        [';))', ':33', ':ss', ':oo', ':**'],
        ['<3', '</3'],
    ]
    table.forEach(row => {
        row.forEach((text, index) => {
            row[index] = map[text]
        })
    })

    return {
        array: array,
        map: map,
        table: table,
    }

}

function SmileysPage_Page (smileys, selectListener, closeListener) {

    function focusAt (e, x, y) {

        const row = buttons[y]
        if (row === undefined) return

        const button = row[x]
        if (button === undefined) return

        e.preventDefault()
        button.focus()

    }

    const classPrefix = 'SmileysPage_Page'

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Smileys')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)

    const buttons = smileys.table.map((row, y) => {

        const rowElement = Div(classPrefix + '-row')
        const buttons = row.map((smiley, x) => {

            const text = smiley.text

            const button = document.createElement('button')
            button.className = classPrefix + '-button ' + smiley.file
            button.title = text
            button.addEventListener('click', () => {
                selectListener(text)
            })
            button.addEventListener('keydown', e => {

                if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return

                const keyCode = e.keyCode
                if (keyCode === 27) {
                    e.preventDefault()
                    closeListener()
                    return
                }

                if (keyCode === 37) {
                    focusAt(e, x - 1, y)
                    return
                }

                if (keyCode === 38) {
                    focusAt(e, x, y - 1)
                    return
                }

                if (keyCode === 39) {
                    focusAt(e, x + 1, y)
                    return
                }

                if (keyCode === 40) focusAt(e, x, y + 1)

            })

            rowElement.append(button)
            return button

        })

        frameElement.append(rowElement)
        return buttons

    })

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus () {
            buttons[0][0].focus()
        },
    }

}

function SmileysPage_PageStyle (smileys, getResourceUrl) {

    let style = ''
    smileys.array.forEach(smiley => {
        const file = smiley.file
        style +=
            '.SmileysPage_Page-button.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}

function TextNode (text) {
    return document.createTextNode(text)
}

function Timezone_Format (value) {

    let sign
    if (value > 0) {
        sign = '+'
    } else if (value < 0) {
        sign = '-'
        value = -value
    } else {
        sign = '\xb1'
    }

    const hours = TwoDigitPad(Math.floor(value / 60))

    return sign + hours + ':' + TwoDigitPad(value % 60)

}

function Timezone_List () {
    return [
        -12 * 60,
        -11 * 60,
        -10 * 60,
        -9 * 60 + 30,
        -9 * 60,
        -8 * 60,
        -7 * 60,
        -6 * 60,
        -5 * 60,
        -4 * 60 + 30,
        -4 * 60,
        -3 * 60 + 30,
        -3 * 60,
        -2 * 60,
        -1 * 60,
        0,
        1 * 60,
        2 * 60,
        3 * 60,
        3 * 60 + 30,
        4 * 60,
        4 * 60 + 30,
        5 * 60,
        5 * 60 + 30,
        5 * 60 + 45,
        6 * 60,
        6 * 60 + 30,
        7 * 60,
        8 * 60,
        8 * 60 + 45,
        9 * 60,
        9 * 60 + 30,
        10 * 60,
        10 * 60 + 30,
        11 * 60,
        11 * 60 + 30,
        12 * 60,
        12 * 60 + 45,
        13 * 60,
        14 * 60,
    ].map(value => {
        return {
            value: value,
            text: Timezone_Format(value),
        }
    })
}

function TwoDigitPad (s) {
    return String(s).padStart(2, '0')
}

function UserInfoPanel_EmailItem (profile) {

    let value = profile.email

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Email: ')

    const valueNode = TextNode(value)

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit (profile) {
            value = profile.email
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}

function UserInfoPanel_FullNameItem (profile) {

    let value = profile.fullName

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Full name: ')

    const valueNode = TextNode(value)

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit (profile) {
            value = profile.fullName
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}

function UserInfoPanel_Panel (profile) {

    function isEmpty (profile) {
        return profile.fullName === '' && profile.email === '' &&
            profile.phone === '' && profile.timezone === null
    }

    const fullNameItem = UserInfoPanel_FullNameItem(profile)

    const emailItem = UserInfoPanel_EmailItem(profile)

    const phoneItem = UserInfoPanel_PhoneItem(profile)

    const timezoneItem = UserInfoPanel_TimezoneItem(profile)

    const emptyElement = Div('UserInfoPanel_Panel-empty')
    emptyElement.append('No more information available')

    const emptyClassList = emptyElement.classList
    if (!isEmpty(profile)) emptyClassList.add('hidden')

    const element = Div('UserInfoPanel_Panel')
    element.append(fullNameItem.element)
    element.append(emailItem.element)
    element.append(phoneItem.element)
    element.append(timezoneItem.element)
    element.append(emptyElement)

    return {
        element: element,
        edit (profile) {

            fullNameItem.edit(profile)
            emailItem.edit(profile)
            phoneItem.edit(profile)
            timezoneItem.edit(profile)

            if (isEmpty(profile)) emptyClassList.remove('hidden')
            else emptyClassList.add('hidden')

        },
    }

}

function UserInfoPanel_PhoneItem (profile) {

    let value = profile.phone

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Phone: ')

    const valueNode = TextNode(value)

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit (profile) {
            value = profile.phone
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}

function UserInfoPanel_TimezoneItem (profile) {

    let value = profile.timezone

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Timezone: ')

    const valueNode = TextNode('')

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === null) classList.add('hidden')
    else valueNode.nodeValue = Timezone_Format(value)

    return {
        element: element,
        edit (profile) {
            value = profile.timezone
            if (value === null) {
                classList.add('hidden')
                valueNode.nodeValue = ''
            } else {
                classList.remove('hidden')
                valueNode.nodeValue = Timezone_Format(value)
            }
        },
    }

}

function Username_ContainsIllegalChars (username) {
    return /[^a-z0-9_.-]/i.test(username)
}

function Username_IsShort (username) {
    return username.length < Username_minLength
}

function Username_IsValid (username) {
    return !Username_IsShort(username) &&
        username.length < Username_maxLength &&
        !Username_ContainsIllegalChars(username)
}

const Username_minLength = 6
const Username_maxLength = 32

function WelcomePage_Page (username, signUpListener,
    privacyListener, signInListener, warningCallback) {

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        staySignedInItem.enable()
        signInButton.disabled = false
        signInNode.nodeValue = 'Sign In'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        signInForm.insertBefore(error.element, signInButton)
        signInButton.focus()
    }

    let error = null
    let warningElement = null
    let request = null

    const classPrefix = 'WelcomePage_Page'

    const usernameItem = WelcomePage_UsernameItem(username)

    const passwordItem = WelcomePage_PasswordItem()

    const signInNode = TextNode('Sign In')

    const signInButton = document.createElement('button')
    signInButton.className = classPrefix + '-signInButton GreenButton'
    signInButton.append(signInNode)

    const staySignedInItem = WelcomePage_StaySignedInItem()

    const signInForm = document.createElement('form')
    signInForm.className = classPrefix + '-signInForm'
    signInForm.append(Page_Blocks('x_small', [
        usernameItem.element,
        passwordItem.element,
        staySignedInItem.element,
    ]))
    signInForm.append(signInButton)
    signInForm.addEventListener('submit', e => {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()

        if (warningElement !== null) {
            frameElement.removeChild(warningElement)
            warningElement = null
        }

        if (error !== null) {
            signInForm.removeChild(error.element)
            error = null
        }

        const username = usernameItem.getValue()
        if (username === null) return

        const password = passwordItem.getValue()
        if (password === null) return

        usernameItem.disable()
        passwordItem.disable()
        staySignedInItem.disable()
        signInButton.disabled = true
        signInNode.nodeValue = 'Signing in...'

        let url = 'data/signIn' +
            '?username=' + encodeURIComponent(username.toLowerCase()) +
            '&password=' + encodeURIComponent(password)
        if (staySignedInItem.isChecked()) url += '&longTerm=true'

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_USERNAME') {
                enableItems()
                usernameItem.showError(errorElement => {
                    errorElement.append('There is no such user.')
                })
                return
            }

            if (response === 'INVALID_PASSWORD') {
                enableItems()
                passwordItem.showError(errorElement => {
                    errorElement.append('The password is incorrect.')
                })
                return
            }

            signInListener(response.username, response.session)

        }, requestError)

    })

    const logoElement = Div(classPrefix + '-logo')

    const logoWrapperElement = Div(classPrefix + '-logoWrapper')
    logoWrapperElement.append(logoElement)

    const signUpButton = document.createElement('button')
    signUpButton.className = classPrefix + '-signUpButton OrangeButton'
    signUpButton.append('Create an Account \u203a')
    signUpButton.addEventListener('click', () => {
        destroy()
        signUpListener()
    })

    const privacy = WelcomePage_Privacy(privacyListener)

    const publicLine = WelcomePage_PublicLine()

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(logoWrapperElement)
    frameElement.append(publicLine.element)
    if (warningCallback !== undefined) {

        warningElement = Div(classPrefix + '-warning')

        warningCallback(warningElement)
        frameElement.append(warningElement)

    }
    frameElement.append(signInForm)
    frameElement.append('New to Bazgu?')
    frameElement.append(document.createElement('br'))
    frameElement.append(signUpButton)
    frameElement.append(privacy.element)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        destroy: publicLine.destroy,
        element: element,
        focusPrivacy: privacy.focus,
        focus () {
            if (username === '') usernameItem.focus()
            else passwordItem.focus()
        },
        focusSignUp () {
            signUpButton.focus()
        },
    }

}

function WelcomePage_PageStyle (getResourceUrl) {
    return '.WelcomePage_Page-logo {' +
            'background-image: url(' + getResourceUrl('img/logo.svg') + ')' +
        '}'
}

function WelcomePage_PasswordItem () {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'WelcomePage_PasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Password_IsValid(value)) {
                showError(errorElement => {
                    errorElement.append('The password is invalid.')
                })
                return null
            }
            return value
        },
    }

}

function WelcomePage_Privacy (listener) {

    const classPrefix = 'WelcomePage_Privacy'

    const button = document.createElement('button')
    button.className = classPrefix + '-privacy'
    button.append('Privacy')
    button.addEventListener('click', listener)

    const element = Div('WelcomePage_Privacy')
    element.append(button)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}

function WelcomePage_PublicLine () {

    function add (text) {

        const itemElement = Div(classPrefix + '-item')
        itemElement.append(text)

        element.append(itemElement)
        itemElements.push(itemElement)

    }

    function animate () {
        timeout = setTimeout(() => {
            itemElements[selectedIndex].classList.remove('visible')
            timeout = setTimeout(() => {
                selectedIndex = (selectedIndex + 1) % itemElements.length
                itemElements[selectedIndex].classList.add('visible')
                animate()
            }, 600)
        }, 8000)
    }

    let selectedIndex = 0
    const itemElements = []

    const classPrefix = 'WelcomePage_PublicLine'

    const element = Div(classPrefix)
    add('Chat without distractions.')
    add('Send unlimited size files.')
    itemElements[0].classList.add('visible')

    let timeout
    animate()

    return {
        element: element,
        destroy () {
            clearTimeout(timeout)
        },
    }

}

function WelcomePage_StaySignedInItem () {

    function addListener (listener) {
        clickElement.addEventListener('click', listener)
    }

    function check () {
        checked = true
        buttonClassList.add('checked')
        removeListener(check)
        addListener(uncheck)
        activeListener = uncheck
        button.focus()
    }

    function removeListener (listener) {
        clickElement.removeEventListener('click', listener)
    }

    function uncheck () {
        checked = false
        buttonClassList.remove('checked')
        removeListener(uncheck)
        addListener(check)
        activeListener = check
        button.focus()
    }

    const classPrefix = 'WelcomePage_StaySignedInItem'

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.type = 'button'

    const buttonClassList = button.classList

    const clickElement = Div(classPrefix + '-click')
    clickElement.append(button)
    clickElement.append('Stay signed in')

    const clickClassList = clickElement.classList

    const element = Div(classPrefix)
    element.append(clickElement)

    let checked = true

    let activeListener = check
    addListener(check)

    return {
        element: element,
        disable () {
            clickClassList.add('disabled')
            button.disabled = true
            removeListener(activeListener)
        },
        enable () {
            clickClassList.remove('disabled')
            button.disabled = false
            addListener(activeListener)
        },
        isChecked () {
            return checked
        },
    }

}

function WelcomePage_StaySignedInItemStyle (getResourceUrl) {
    return '.WelcomePage_StaySignedInItem-button.checked {' +
            'background-image: url(' + getResourceUrl('img/checked/normal.svg') + ')' +
        '}' +
        '.WelcomePage_StaySignedInItem-button.checked:active {' +
            'background-image: url(' + getResourceUrl('img/checked/active.svg') + ')' +
        '}'
}

function WelcomePage_UnauthenticatedWarning (username) {
    return element => {

        const usernameElement = document.createElement('b')
        usernameElement.className = 'WelcomePage_UnauthenticatedWarning-username'
        usernameElement.append(username)

        element.append('You need to sign in or create an account to contact "')
        element.append(usernameElement)
        element.append('".')

    }
}

function WelcomePage_UsernameItem (value) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'WelcomePage_UsernameItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Username')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.value = value
    input.className = classPrefix + '-input'

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            const value = CollapseSpaces(input.value)
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Username_IsValid(value)) {
                showError(errorElement => {
                    errorElement.append('The username is invalid.')
                })
                return null
            }
            return value
        },
    }

}

function WorkPage_ChatPanel_CloseButton (listener) {

    const button = document.createElement('button')
    button.className = 'WorkPage_ChatPanel_CloseButton'
    button.title = 'Close'
    button.addEventListener('click', listener)

    return {
        element: button,
        addEventListener (name, listener) {
            button.addEventListener(name, listener)
        },
        disable () {
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        focus () {
            button.focus()
        },
    }

}

function WorkPage_ChatPanel_CloseButtonStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_CloseButton {' +
            'background-image: url(' + getResourceUrl('img/close.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_ContactMenu (profileListener, removeListener) {

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    const classPrefix = 'WorkPage_ChatPanel_ContactMenu'

    const profileItemElement = Div(classPrefix + '-item')
    profileItemElement.append('Profile')
    profileItemElement.addEventListener('click', profileListener)

    const removeItemElement = Div(classPrefix + '-item')
    removeItemElement.append('Remove')
    removeItemElement.addEventListener('click', removeListener)

    const element = Div(classPrefix)
    element.append(profileItemElement)
    element.append(removeItemElement)

    let selectedElement = null

    return {
        element: element,
        openSelected () {
            if (selectedElement === profileItemElement) profileListener()
            else if (selectedElement === removeItemElement) removeListener()
        },
        reset () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown () {
            if (selectedElement === profileItemElement) {
                resetSelected()
                selectedElement = removeItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = profileItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp () {
            if (selectedElement === removeItemElement) {
                resetSelected()
                selectedElement = profileItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = removeItemElement
            }
            selectedElement.classList.add('active')
        },
    }
}

function WorkPage_ChatPanel_DaySeparator (day) {

    const date = new Date(day * 24 * 60 * 60 * 1000)

    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December']

    const dateString = monthNames[date.getUTCMonth()] + ' ' +
        date.getUTCDate() + ', ' + date.getUTCFullYear()

    const element = Div('WorkPage_ChatPanel_DaySeparator')
    element.append(dateString)

    return { element: element }

}

function WorkPage_ChatPanel_FeedFile (token, file,
    progressListener, completeListener, cancelListener, destroyListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function ChunkedReader (file) {
        let offset = 0
        return {
            getOffset () {
                return offset
            },
            hasNextChunk () {
                return offset < file.size
            },
            readNextChunk (chunkSize, callback) {
                const blob = file.slice(offset, offset + chunkSize)
                const reader = new FileReader
                reader.readAsArrayBuffer(blob)
                reader.onload = () => {
                    offset += chunkSize
                    callback(reader.result)
                }
                return () => {
                    reader.abort()
                }
            },
        }
    }

    function next (chunkSize) {
        abortFunction = reader.readNextChunk(chunkSize, chunk => {

            const time = Date.now()

            const request = new XMLHttpRequest
            request.open('post', 'data/feedFile?token=' + encodeURIComponent(token))
            request.responseType = 'json'
            request.send(chunk)
            request.onerror = connectionErrorListener
            request.onload = () => {

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    cancelListener()
                    destroyListener()
                    return
                }

                if (response !== true) {
                    crashListener()
                    return
                }

                if (reader.hasNextChunk()) {
                    const elapsed = Date.now() - time
                    if (elapsed < 500 && chunkSize < 1024 * 1024 * 16) {
                        chunkSize *= 2
                    } else if (elapsed > 2000 && chunkSize > 1024) {
                        chunkSize /= 2
                    }
                    progressListener(reader.getOffset())
                    next(chunkSize)
                    return
                }

                completeListener()
                destroyListener()

            }

            abortFunction = () => {
                request.abort()
            }

        })
    }

    let abortFunction

    const reader = ChunkedReader(file)
    next(1024 * 8)

    return () => {
        abortFunction()
    }

}

function WorkPage_ChatPanel_MessageText (formatText,
    isLocalLink, element, text, linkListener) {

    formatText(text, text => {
        element.append(text)
    }, link => {

        const a = document.createElement('a')
        a.href = link
        if (!isLocalLink(link)) a.target = '_blank'
        a.className = 'WorkPage_ChatPanel_MessageText-link'
        a.append(link)

        const span = document.createElement('span')
        span.className = 'WorkPage_ChatPanel_MessageText-disabledLink'
        span.append(link)

        element.append(a)

        linkListener({
            disable () {
                element.replaceChild(span, a)
            },
            enable () {
                element.replaceChild(a, span)
            },
        })

    }, (text, icon) => {
        element.append(WorkPage_ChatPanel_Smiley(text, icon))
    })

}

function WorkPage_ChatPanel_MessagesPanel (playAudio,
    formatBytes, formatText, isLocalLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    textareaFocusListener, textareaBlurListener, sendSmileyListener,
    sendContactListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addMessage (message, time) {
        ensureSeparator(time)
        doneMessagesElement.append(message.element)
        sendingMessagesClassList.add('notFirst')
        liveMessages.push(message)
        lastMessage = message
    }

    function addReceivedFileMessage (file, time, token) {
        if (!canMerge('receivedFile', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_ReceivedFileMessage(
                    formatBytes, receivedFiles, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        const part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addReceivedTextMessage (text, time, token) {
        if (!canMerge('receivedText', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_ReceivedTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentFileMessage (file, time, token) {
        if (!canMerge('sentFile', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_SentFileMessage(
                    formatBytes, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        const part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addSentFileMessageAndStore (file, time, token) {
        messages.push(['sentFile', file, time, token])
        return addSentFileMessage(file, time, token)
    }

    function addSentTextMessage (text, time, token) {
        if (!canMerge('sentText', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_SentTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentTextMessageAndStore (text, time, token) {
        addSentTextMessage(text, time, token)
        messages.push(['sentText', text, time, token])
    }

    function canMerge (type, time) {
        return lastMessage !== null &&
            lastMessage.type === type && lastMessage.minute === Minute(time)
    }

    function destroy () {
        typePanel.destroy()
        destroyEvent.emit()
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function ensureSeparator (time) {
        const day = Day(time)
        let separator = daySeparators[day]
        if (separator !== undefined) return
        separator = daySeparators[day] = WorkPage_ChatPanel_DaySeparator(day)
        doneMessagesElement.append(separator.element)
    }

    function getLastSendingMessage (type) {
        const length = sendingMessages.length
        if (length !== 0) {
            const message = sendingMessages[length - 1]
            if (message.type === type) return message
        }
    }

    function scrollDown () {
        contentElement.scrollTop = contentElement.scrollHeight
    }

    function sendNewTextMessage (text) {

        let message = getLastSendingMessage('SendingText')
        if (message === undefined) {
            message = WorkPage_ChatPanel_SendingTextMessage(
                disableEvent, enableEvent, formatText,
                isLocalLink, session, contactUsername,
                () => {
                    destroyEvent.removeListener(message.destroy)
                    sendingMessagesElement.removeChild(message.element)
                    sendingMessages.splice(sendingMessages.indexOf(message), 1)
                    if (sendingMessagesElement.childNodes.length === 0) {
                        sendingMessagesClassList.add('hidden')
                    }
                },
                destroyAndInvalidSession, destroyAndConnectionError,
                destroyAndCrash, destroyAndServiceError
            )
            sendingMessages.push(message)
        }

        message.add(text, response => {
            addSentTextMessageAndStore(text, response.time, response.token)
            playAudio('message-seen')
        })

        destroyEvent.addListener(message.destroy)
        sendingMessagesElement.append(message.element)
        if (sendingMessagesElement.childNodes.length === 1) {
            sendingMessagesClassList.remove('hidden')
        }
        scrollDown()

    }

    const disableEvent = WorkPage_Event(),
        enableEvent = WorkPage_Event()

    let timezone = session.profile.timezone,
        timezoneOffset = timezone * 60 * 1000

    let lastMessage = null

    const sendingMessages = []

    const messages = WorkPage_Messages(username, contactUsername)
    const liveMessages = []
    const messagePartsMap = Object.create(null)
    const daySeparators = Object.create(null)
    const destroyEvent = WorkPage_Event()

    const classPrefix = 'WorkPage_ChatPanel_MessagesPanel'

    const doneMessagesElement = Div(classPrefix + '-doneMessages')

    const sendingMessagesElement = Div(classPrefix + '-sendingMessages hidden')

    const sendingMessagesClassList = sendingMessagesElement.classList

    const contentElement = Div(classPrefix + '-content')
    contentElement.append(doneMessagesElement)
    contentElement.append(sendingMessagesElement)

    const typePanel = WorkPage_ChatPanel_TypePanel(() => {
        classList.add('typing')
        textareaFocusListener()
    }, () => {
        classList.remove('typing')
        textareaBlurListener()
    }, sendNewTextMessage, files => {
        Array.prototype.forEach.call(files, readableFile => {

            const file = {
                name: readableFile.name,
                size: readableFile.size,
            }

            let message = getLastSendingMessage('SendingFile')
            if (message === undefined) {
                message = WorkPage_ChatPanel_SendingFileMessage(
                    formatBytes, session, contactUsername,
                    () => {
                        destroyEvent.removeListener(message.destroy)
                        sendingMessagesElement.removeChild(message.element)
                        sendingMessages.splice(sendingMessages.indexOf(message), 1)
                        if (sendingMessagesElement.childNodes.length === 0) {
                            sendingMessagesClassList.add('hidden')
                        }
                    },
                    destroyAndInvalidSession, destroyAndConnectionError,
                    destroyAndCrash, destroyAndServiceError
                )
                sendingMessages.push(message)
            }

            message.add(file, response => {
                const sendToken = response.sendToken
                const startFeed = addSentFileMessageAndStore(file, response.time, response.token).startSend()
                sentFiles.add(sendToken, endCallback => {

                    function complete () {
                        progress.complete()
                        endCallback()
                    }

                    const progress = startFeed()
                    if (file.size === 0) complete()
                    else {
                        const destroyFunction = WorkPage_ChatPanel_FeedFile(
                            sendToken, readableFile, progress.progress, complete,
                            () => {
                                progress.cancel()
                                endCallback()
                            },
                            () => {
                                destroyEvent.removeListener(destroyFunction)
                            },
                            destroyAndConnectionError, destroyAndCrash, destroyAndServiceError
                        )
                        destroyEvent.addListener(destroyFunction)
                    }

                })
                playAudio('message-seen')
            })

            destroyEvent.addListener(message.destroy)
            sendingMessagesElement.append(message.element)
            if (sendingMessagesElement.childNodes.length === 1) {
                sendingMessagesClassList.remove('hidden')
            }
            scrollDown()

        })
        scrollDown()
    }, () => {
        sendSmileyListener(sendNewTextMessage)
    }, () => {
        sendContactListener(contacts => {
            const text = contacts.map(contact => {
                return location.protocol + '//' + location.host +
                    location.pathname + '#' + contact.username
            }).join('\n')
            sendNewTextMessage(text)
        })
    }, closeListener)

    const element = Div(classPrefix)
    element.append(contentElement)
    element.append(typePanel.element)

    const classList = element.classList

    WorkPage_ChatPanel_RestoreMessages(messages, session,
        addReceivedFileMessage, addReceivedTextMessage,
        addSentFileMessage, addSentTextMessage)

    return {
        destroy: destroy,
        element: element,
        sendFileMessage: addSentFileMessageAndStore,
        sendTextMessage: addSentTextMessageAndStore,
        disable () {
            typePanel.disable()
            disableEvent.emit()
        },
        editTimezone (newTimezone) {

            timezone = newTimezone
            timezoneOffset = timezone * 60 * 1000

            Object.keys(daySeparators).forEach(day => {
                doneMessagesElement.removeChild(daySeparators[day].element)
                delete daySeparators[day]
            })

            liveMessages.forEach(message => {
                message.editTimezone(timezoneOffset)
                ensureSeparator(message.time + timezoneOffset)
                doneMessagesElement.append(message.element)
            })

        },
        enable () {
            typePanel.enable()
            enableEvent.emit()
        },
        focus () {
            typePanel.focus()
            scrollDown()
        },
        receiveFileMessage (file, time, token) {
            addReceivedFileMessage(file, time, token).showReceive()
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage (text, time, token) {
            addReceivedTextMessage(text, time, token)
            messages.push(['receivedText', text, time, token])
        },
    }

}

function WorkPage_ChatPanel_MoreButton (closeListener,
    fileListener, sendSmileyListener, sendContactListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        buttonClassList.add('not_pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        button.addEventListener('keydown', collapsedKeyDown)
        element.removeChild(moreMenu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        moreMenu.reset()
    }

    function collapsedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    }

    function expand () {
        expanded = true
        buttonClassList.remove('not_pressed')
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.removeEventListener('keydown', collapsedKeyDown)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.append(moreMenu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            moreMenu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            button.focus()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            button.focus()
            moreMenu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            button.focus()
            moreMenu.selectDown()
        }
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    const moreMenu = WorkPage_ChatPanel_MoreMenu(keyDown, files => {
        collapse()
        fileListener(files)
    }, () => {
        collapse()
        sendSmileyListener()
    }, () => {
        collapse()
        sendContactListener()
    })

    let expanded = false

    const classPrefix = 'WorkPage_ChatPanel_MoreButton'

    const button = document.createElement('button')
    button.className = classPrefix + '-button not_pressed'
    button.append('More')
    button.addEventListener('click', expand)
    button.addEventListener('keydown', collapsedKeyDown)

    const buttonClassList = button.classList

    const element = Div(classPrefix)
    element.append(button)

    const classList = element.classList

    return {
        element: element,
        destroy () {
            if (expanded) collapse()
        },
        disable () {
            if (expanded) collapse()
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        typeBlur () {
            classList.remove('typeFocused')
        },
        typeFocus () {
            classList.add('typeFocused')
        },
    }

}

function WorkPage_ChatPanel_MoreButtonStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_MoreButton-button {' +
            'background-image: url(' + getResourceUrl('img/arrow-up.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_MoreButton-button:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-up-active.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_MoreMenu (fileInputKeyDownListener,
    fileListener, smileyListener, contactListener) {

    function addFileInput () {
        fileInput = document.createElement('input')
        fileInput.type = 'file'
        fileInput.multiple = true
        fileInput.className = classPrefix + '-fileInput'
        fileInput.addEventListener('keydown', fileInputKeyDownListener)
        fileInput.addEventListener('change', () => {
            fileListener(fileInput.files)
            fileItemElement.removeChild(fileInput)
            addFileInput()
        })
        fileItemElement.append(fileInput)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    const classPrefix = 'WorkPage_ChatPanel_MoreMenu'

    const fileItemElement = Div(classPrefix + '-item')
    fileItemElement.append('File')

    const smileyItemElement = Div(classPrefix + '-item')
    smileyItemElement.append('Smiley')
    smileyItemElement.addEventListener('click', smileyListener)

    const contactItemElement = Div(classPrefix + '-item')
    contactItemElement.append('Contact')
    contactItemElement.addEventListener('click', contactListener)

    const element = Div(classPrefix)
    element.append(fileItemElement)
    element.append(smileyItemElement)
    element.append(contactItemElement)

    let selectedElement = null

    let fileInput
    addFileInput()

    return {
        element: element,
        openSelected () {
            if (selectedElement === fileItemElement) {
            } else if (selectedElement === smileyItemElement) {
                smileyListener()
            } else if (selectedElement === contactItemElement) {
                contactListener()
            }
        },
        reset () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown () {
            if (selectedElement === fileItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = contactItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            }
            selectedElement.classList.add('active')
        },
        selectUp () {
            if (selectedElement === contactItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = contactItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}

function WorkPage_ChatPanel_Panel (playAudio, formatBytes,
    formatText, isLocalLink, sentFiles, receivedFiles, username,
    session, contactUsername, profile, overrideProfile,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_ChatPanel_Panel'

    const title = WorkPage_ChatPanel_Title(contactUsername,
        profile, overrideProfile, profileListener, removeListener)

    const messagesPanel = WorkPage_ChatPanel_MessagesPanel(
        playAudio, formatBytes, formatText, isLocalLink, sentFiles,
        receivedFiles, username, session, contactUsername,
        () => {
            barClassList.add('hidden')
        },
        () => {
            barClassList.remove('hidden')
        },
        sendSmileyListener, sendContactListener,
        closeListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    const closeButton = WorkPage_ChatPanel_CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const barElement = Div(classPrefix + '-bar')
    barElement.append(title.element)
    barElement.append(closeButton.element)

    const barClassList = barElement.classList

    const element = Div(classPrefix)
    element.append(barElement)
    element.append(messagesPanel.element)

    return {
        element: element,
        editContactProfile: title.editContactProfile,
        editTimezone: messagesPanel.editTimezone,
        focus: messagesPanel.focus,
        overrideContactProfile: title.overrideContactProfile,
        receiveFileMessage: messagesPanel.receiveFileMessage,
        receiveTextMessage: messagesPanel.receiveTextMessage,
        sendFileMessage: messagesPanel.sendFileMessage,
        sendTextMessage: messagesPanel.sendTextMessage,
        destroy () {
            title.destroy()
            messagesPanel.destroy()
        },
        disable () {
            title.disable()
            closeButton.disable()
            messagesPanel.disable()
        },
        enable () {
            title.enable()
            closeButton.enable()
            messagesPanel.enable()
        },
    }

}

function WorkPage_ChatPanel_PanelStyle () {
    return '.WorkPage_ChatPanel_Panel {' +
            'background-image: url(' + getResourceUrl('img/light-grass.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_ReceivedFileMessage (
    formatBytes, receivedFiles, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_ReceivedFileMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'receivedFile',
        add (file) {

            const nameNode = TextNode(file.name)

            const sizeElement = Div(classPrefix + '-item-size')
            sizeElement.append(formatBytes(file.size))

            const itemElement = Div(classPrefix + '-item')
            itemElement.append(nameNode)
            itemElement.append(sizeElement)

            element.append(itemElement)

            return {
                showReceive () {

                    const receiveLink = document.createElement('a')
                    receiveLink.className = classPrefix + '-receiveLink'
                    receiveLink.append('Receive')
                    receiveLink.target = '_blank'
                    receiveLink.href = 'data/receiveFile?token=' + encodeURIComponent(file.token)

                    itemElement.classList.add('withReceiveLink')
                    itemElement.insertBefore(receiveLink, nameNode)
                    receivedFiles.add(file.token, () => {
                        itemElement.classList.remove('withReceiveLink')
                        itemElement.removeChild(receiveLink)
                    })

                },
            }

        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_ReceivedTextMessage (disableEvent,
    enableEvent, formatText, isLocalLink, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_ReceivedTextMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'receivedText',
        add (text) {
            const itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, link => {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.append(itemElement)
            return {}
        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_RestoreMessages (messages,
    session, addReceivedFileMessage, addReceivedTextMessage,
    addSentFileMessage, addSentTextMessage) {

    messages.array.forEach(message => {
        const type = message[0]
        if (type === 'receivedFile') {
            const file = message[1]
            const part = addReceivedFileMessage(file, message[2], message[3])
            if (session.files[file.token] !== undefined) part.showReceive()
        } else if (type === 'receivedText') {
            addReceivedTextMessage(message[1], message[2], message[3])
        } else if (type === 'sentFile') {
            addSentFileMessage(message[1], message[2], message[3])
        } else {
            addSentTextMessage(message[1], message[2], message[3])
        }
    })

}

function WorkPage_ChatPanel_SendingFileMessage (formatBytes, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_ChatPanel_SendingFileMessage'

    const element = Div(classPrefix)

    const requests = []

    return {
        element: element,
        type: 'SendingFile',
        add (file, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            const name = file.name,
                size = file.size

            const sizeElement = Div(classPrefix + '-size')
            sizeElement.append(formatBytes(size))

            const itemElement = Div(classPrefix + '-item')
            itemElement.append(name)
            itemElement.append(document.createElement('br'))
            itemElement.append(sizeElement)

            element.append(itemElement)

            const url = 'data/sendFileMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&name=' + encodeURIComponent(name) +
                '&size=' + encodeURIComponent(size)

            const request = GetJson(url, () => {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, () => {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy () {
            requests.forEach(request => {
                request.abort()
            })
        },
    }

}

function WorkPage_ChatPanel_SendingTextMessage (
    disableEvent, enableEvent, formatText, isLocalLink, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_ChatPanel_SendingTextMessage'

    const element = Div(classPrefix)

    const requests = []

    return {
        element: element,
        type: 'SendingText',
        add (text, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            const itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, link => {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.append(itemElement)

            const url = 'data/sendTextMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&text=' + encodeURIComponent(text)

            const request = GetJson(url, () => {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, () => {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy () {
            requests.forEach(request => {
                request.abort()
            })
        },
    }

}

function WorkPage_ChatPanel_SentFileMessage (formatBytes, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_SentFileMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentFile',
        add (file) {

            const sizeElement = Div(classPrefix + '-item-size')
            sizeElement.append(formatBytes(file.size))

            const itemElement = Div(classPrefix + '-item')
            itemElement.append(file.name)
            itemElement.append(sizeElement)

            element.append(itemElement)

            return {
                startSend () {

                    const node = TextNode('Waiting...')

                    const sendingElement = Div(classPrefix + '-item-sending')
                    sendingElement.append(node)

                    itemElement.classList.add('sending')
                    itemElement.append(sendingElement)

                    return () => {

                        node.nodeValue = '0%'

                        const doneTextNode = TextNode('0%')

                        const doneTextElement = Div(classPrefix + '-item-sending-done-text')
                        doneTextElement.append(doneTextNode)

                        const doneElement = Div(classPrefix + '-item-sending-done')
                        doneElement.append(doneTextElement)

                        sendingElement.append(doneElement)

                        return {
                            cancel () {
                                sendingElement.classList.add('failed')
                                doneTextElement.classList.add('failed')
                            },
                            complete () {
                                node.nodeValue = doneTextNode.nodeValue = '100%'
                                doneElement.style.width = '100%'
                            },
                            progress (sent) {

                                const percent = Math.round(sent / file.size * 100) + '%'

                                node.nodeValue = doneTextNode.nodeValue = percent
                                doneElement.style.width = percent

                            },
                        }

                    }

                },
            }

        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_SentTextMessage (disableEvent,
    enableEvent, formatText, isLocalLink, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_SentTextMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentText',
        add (text) {
            const itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, link => {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.append(itemElement)
            return {}
        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}

function WorkPage_ChatPanel_Smiley (text, icon) {
    const element = document.createElement('img')
    element.className = 'WorkPage_ChatPanel_Smiley ' + icon
    element.title = text
    element.alt = text
    element.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQIHWP4zwAAAgEBAMVfG14AAAAASUVORK5CYII='
    return element
}

function WorkPage_ChatPanel_SmileyStyle (smileys, getResourceUrl) {

    let style = ''
    smileys.array.forEach(smiley => {
        const file = smiley.file
        style +=
            '.WorkPage_ChatPanel_Smiley.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}

function WorkPage_ChatPanel_Title (contactUsername,
    profile, overrideProfile, profileListener, removeListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function createButtonText () {
        return overrideProfile.fullName || profile.fullName || contactUsername
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.append(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    let expanded = false

    const classPrefix = 'WorkPage_ChatPanel_Title'

    const menu = WorkPage_ChatPanel_ContactMenu(() => {
        collapse()
        profileListener()
    }, () => {
        collapse()
        removeListener()
    })

    const node = TextNode(createButtonText())

    const buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.append(node)

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.append(buttonTextElement)
    button.addEventListener('click', expand)

    const buttonClassList = button.classList

    const element = Div(classPrefix)
    element.append(button)

    return {
        element: element,
        destroy () {
            if (expanded) collapse()
        },
        disable () {
            if (expanded) collapse()
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        editContactProfile (_profile) {
            profile = _profile
            node.nodeValue = createButtonText()
        },
        overrideContactProfile (_overrideProfile) {
            overrideProfile = _overrideProfile
            node.nodeValue = createButtonText()
        },
    }

}

function WorkPage_ChatPanel_TitleStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_Title-button {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_Title-button.pressed {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_Title-button.pressed:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed-active.svg') + ')' +
        '}'
}

function WorkPage_ChatPanel_TypePanel (
    focusListener, blurListener, typeListener, fileListener,
    sendSmileyListener, sendContactListener, closeListener) {

    function submit () {
        const value = textarea.value
        if (/\S/.test(value)) typeListener(value)
        textarea.value = ''
    }

    const classPrefix = 'WorkPage_ChatPanel_TypePanel'

    const textarea = document.createElement('textarea')
    textarea.className = classPrefix + '-textarea'
    textarea.placeholder = 'Type a message here'
    textarea.addEventListener('focus', () => {
        moreButton.typeFocus()
        sendButtonClassList.add('typeFocused')
        focusListener()
    })
    textarea.addEventListener('blur', () => {
        moreButton.typeBlur()
        sendButtonClassList.remove('typeFocused')
        blurListener()
    })
    textarea.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            submit()
        } else if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const sendButton = document.createElement('button')
    sendButton.className = classPrefix + '-sendButton GreenButton'
    sendButton.append('Send')
    sendButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    sendButton.addEventListener('click', () => {
        submit()
        textarea.focus()
    })

    const sendButtonClassList = sendButton.classList

    const moreButton = WorkPage_ChatPanel_MoreButton(closeListener,
        fileListener, sendSmileyListener, sendContactListener)

    const element = Div(classPrefix)
    element.append(textarea)
    element.append(moreButton.element)
    element.append(sendButton)

    return {
        element: element,
        destroy: moreButton.destroy,
        disable () {
            textarea.disabled = true
            sendButton.disabled = true
            moreButton.disable()
        },
        enable () {
            textarea.disabled = false
            sendButton.disabled = false
            moreButton.enable()
        },
        focus () {
            textarea.focus()
        },
    }

}

function WorkPage_ContactRequests (element, username, session,
    showListener, hideListener, addContactListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        if (page !== null) page.destroy()
    }

    function getMessages (username) {
        if (username === visibleUsername) return visibleMessages
        const request = requests[username]
        if (request !== undefined) return request.messages
    }

    function requestError () {
        destroy()
        connectionErrorListener()
    }

    function show (username, request) {
        visibleUsername = username
        visibleMessages = request.messages
        page = ContactRequestPage(session, username, request.profile, contactData => {
            addContactListener(username, contactData)
            showNext()
        }, showNext, () => {

            page.destroy()
            showNext()

            const url = 'data/removeRequest' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(username)

            const request = GetJson(url, () => {

                if (request.status !== 200) {
                    destroy()
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    destroy()
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    destroy()
                    invalidSessionListener()
                    return
                }

            }, requestError)

        }, () => {
            element.removeChild(page.element)
            invalidSessionListener()
        })
        element.append(page.element)
    }

    function showNext () {

        element.removeChild(page.element)

        if (Object.keys(requests).length === 0) {
            page = null
            visibleUsername = null
            hideListener()
            return
        }

        const username = Object.keys(requests)[0]
        const request = requests[username]
        delete requests[username]
        show(username, request)

    }

    const requests = Object.create(null)

    let page = null,
        visibleUsername = null,
        visibleMessages = null

    return {
        add (requestUsername, profile) {

            const request = {
                profile: profile,
                messages: WorkPage_Messages(username, requestUsername),
            }

            if (page === null) {
                show(requestUsername, request)
                showListener()
            } else {
                requests[requestUsername] = request
            }

        },
        edit (username, profile) {
            if (visibleUsername === username) page.edit(profile)
            else requests[username].profile = profile
        },
        receiveFileMessage (username, file, time, token) {
            const messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage (username, text, time, token) {
            const messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedText', text, time, token])
        },
        remove (username) {
            if (visibleUsername === username) {
                page.destroy()
                showNext()
            } else {
                delete requests[username]
            }
        },
    }

}

function WorkPage_Event () {
    const listeners = []
    return {
        addListener (listener) {
            listeners.push(listener)
        },
        emit (args) {
            listeners.forEach(listener => {
                listener.apply(null, args)
            })
        },
        removeListener (listener) {
            const index = listeners.indexOf(listener)
            if (index === -1) throw 0
            listeners.splice(index, 1)
        },
    }
}

function WorkPage_LocationHash (session, checkContactListener,
    publicProfileListener, noSuchUserListener, connectionErrorListener,
    crashListener, invalidSessionListener, serviceErrorListener) {

    function check () {

        abort()
        changeEvent.emit()

        const username = location.hash.substr(1)
        if (username === '') return

        const lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        const url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                serviceErrorListener()
                return
            }

            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                noSuchUserListener(username)
                return
            }

            publicProfileListener(response.username, response.profile)

        }, connectionErrorListener)

    }

    function abort () {
        if (request === null) return
        request.abort()
        request = null
    }

    let request = null

    const changeEvent = WorkPage_Event()

    addEventListener('hashchange', check)

    return {
        changeEvent: changeEvent,
        check: check,
        destroy () {
            removeEventListener('hashchange', check)
            abort()
        },
        reset () {
            removeEventListener('hashchange', check)
            location.hash = ''
            addEventListener('hashchange', check)
        },
    }

}

function WorkPage_Messages (username, contactUsername) {

    const key = username + '$' + contactUsername

    const array = (() => {

        let array
        try {
            array = localStorage[key]
        } catch (e) {
            return []
        }

        if (array === undefined) return []

        try {
            return JSON.parse(array)
        } catch (e) {
            return []
        }

    })()

    return {
        array,
        push (message) {
            array.push(message)
            if (array.length > 1024) array.shift()
            try {
                localStorage[key] = JSON.stringify(array)
            } catch (e) {
            }
        },
    }

}

function WorkPage_Page (smileys, formatBytes, formatText, isLocalLink,
    renderUserLink, timezoneList, username, session, signOutListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        destroyEvent.emit()
        sidePanel.destroy()
        locationHash.destroy()
        removeEventListener('beforeunload', windowBeforeUnload)
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function disableBackground () {
        sidePanel.disable()
        if (chatPanel !== null) chatPanel.disable()
    }

    function editProfile (newProfile) {
        profile = newProfile
        sidePanel.editProfile(profile)
        editProfileEvent.emit([profile])
    }

    function enableBackground () {
        sidePanel.enable()
        if (chatPanel !== null) chatPanel.enable()
    }

    function showAccountPage (readyCallback) {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.editProfile.removeListener(page.editProfile)
            destroyEvent.removeListener(page.destroy)
        }

        const page = AccountPage_Page(timezoneList, userLink, username, session, profile, profile => {
            hide()
            enableBackground()
            editProfile(profile)
        }, () => {
            hide()
            ;(() => {

                function hide () {
                    element.removeChild(page.element)
                    destroyEvent.removeListener(page.destroy)
                }

                const page = ChangePasswordPage_Page(session, () => {
                    hide()
                    showAccountPage(page => {
                        page.focusChangePassword()
                    })
                }, () => {
                    hide()
                    enableBackground()
                }, () => {
                    hide()
                    destroyAndInvalidSession()
                })

                element.append(page.element)
                page.focus()
                destroyEvent.addListener(page.destroy)

            })()
        }, () => {
            hide()
            enableBackground()
        }, () => {
            hide()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        readyCallback(page)
        pullMessages.events.editProfile.addListener(page.editProfile)
        destroyEvent.addListener(page.destroy)

    }

    function showAddContactPage (showInvite) {

        function hide () {
            element.removeChild(page.element)
            destroyEvent.removeListener(page.destroy)
        }

        const page = AddContactPage_Page(showInvite, session, username, checkUsername => {

            if (checkUsername === lowerUsername) {
                hide()
                ;(() => {

                    function hide () {
                        element.removeChild(page.element)
                        editProfileEvent.removeListener(page.editProfile)
                    }

                    const page = FoundSelfPage(username, profile, () => {
                        hide()
                        showAddContactPage(showInvite)
                    }, () => {
                        hide()
                        enableBackground()
                    })

                    element.append(page.element)
                    page.focus()
                    editProfileEvent.addListener(page.editProfile)

                })()
                return true
            }

            const contact = sidePanel.getLowerContact(checkUsername)
            if (contact === undefined) return false

            hide()
            ;(() => {

                function contactRemoveListener () {
                    hide()
                    enableBackground()
                }

                function hide () {
                    element.removeChild(page.element)
                    contact.editProfileEvent.removeListener(page.editProfile)
                    contact.removeEvent.removeListener(contactRemoveListener)
                }

                const page = FoundContactPage(contact.username, contact.getProfile(), () => {
                    hide()
                    enableBackground()
                    contact.ensureSelected()
                }, () => {
                    hide()
                    showAddContactPage(showInvite)
                }, () => {
                    hide()
                    enableBackground()
                })

                element.append(page.element)
                page.focus()
                contact.editProfileEvent.addListener(page.editProfile)
                contact.removeEvent.addListener(contactRemoveListener)

            })()

            return true

        }, (username, profile) => {
            hide()
            ;(() => {

                function hide () {
                    element.removeChild(page.element)
                    pullMessages.events.publicProfile.removeListener(publicProfileListener)
                    destroyEvent.removeListener(page.destroy)
                }

                function publicProfileListener (eventUsername, profile) {
                    if (username === eventUsername) page.editProfile(profile)
                }

                const page = FoundUserPage(session, username, profile, contactData => {
                    hide()
                    enableBackground()
                    sidePanel.mergeContact(username, contactData)
                    sidePanel.getContact(username).ensureSelected()
                }, () => {
                    hide()
                    showAddContactPage(showInvite)
                    unwatchPublicProfile(username)
                }, () => {
                    hide()
                    enableBackground()
                    unwatchPublicProfile(username)
                }, () => {
                    hide()
                    destroyAndInvalidSession()
                })

                element.append(page.element)
                page.focus()
                pullMessages.events.publicProfile.addListener(publicProfileListener)
                destroyEvent.addListener(page.destroy)

            })()
        }, () => {
            hide()
            enableBackground()
        }, () => {
            hide()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        page.focus()
        destroyEvent.addListener(page.destroy)

    }

    function unwatchPublicProfile (username) {

        function destroy () {
            request.abort()
        }

        const url = 'data/unwatchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        const request = GetJson(url, () => {

            destroyEvent.removeListener(destroy)

            if (request.status !== 200) {
                destroyAndServiceError()
                return
            }

            const response = request.response
            if (response === null) {
                destroyAndCrash()
                return
            }

            if (response !== true) destroyAndServiceError()

        }, () => {
            destroyEvent.removeListener(destroy)
            destroyAndConnectionError()
        })

        destroyEvent.addListener(destroy)

    }

    function windowBeforeUnload (e) {
        if (sentFiles.empty() && numFeedingFiles === 0) return
        e.preventDefault()
        const message = 'Your file transfers will be aborted if you close.'
        e.returnValue = message
        return message
    }

    let numFeedingFiles = 0
    const userLink = renderUserLink(username)
    const lowerUsername = username.toLowerCase()
    let profile = session.profile

    const destroyEvent = WorkPage_Event(),
        editProfileEvent = WorkPage_Event()

    const sentFiles = WorkPage_SentFiles(),
        receivedFiles = WorkPage_ReceivedFiles()

    let chatPanel = null

    const classPrefix = 'WorkPage_Page'

    const sidePanel = WorkPage_SidePanel_Panel(formatBytes, formatText, isLocalLink, renderUserLink, sentFiles, receivedFiles, username, session, () => {
        disableBackground()
        showAccountPage(page => {
            page.focus()
        })
    }, () => {

        function hide () {
            element.removeChild(page.element)
        }

        const page = SignOutPage(() => {

            pullMessages.abort()
            hide()
            destroy()
            signOutListener()

            GetJson('data/signOut?token=' + encodeURIComponent(session.token))

        }, () => {
            hide()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()

    }, showInvite => {
        disableBackground()
        showAddContactPage(showInvite)
    }, contact => {
        chatPanel = contact.chatPanel
        element.append(chatPanel.element)
        chatPanel.focus()
    }, () => {
        chatPanel.destroy()
        element.removeChild(chatPanel.element)
        chatPanel = null
    }, contact => {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.editProfileEvent.removeListener(page.editProfile)
            contact.overrideProfileEvent.removeListener(page.overrideProfile)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        const page = EditContactPage_Page(
            timezoneList, session, contact.userLink, contact.username,
            contact.getProfile(), contact.getOverrideProfile(),
            profile => {
                hide()
                enableBackground()
                contact.overrideProfile(profile)
            },
            () => {
                hide()
                enableBackground()
            },
            () => {
                hide()
                destroyAndInvalidSession()
            }
        )

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.editProfileEvent.addListener(page.editProfile)
        contact.overrideProfileEvent.addListener(page.overrideProfile)
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, (contact, send) => {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        const page = SmileysPage_Page(smileys, text => {
            hide()
            enableBackground()
            send(text)
        }, () => {
            hide()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, (contact, contacts, send) => {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        const page = ContactSelectPage_Page(contacts, contacts => {
            hide()
            enableBackground()
            send(contacts)
        }, () => {
            hide()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, contact => {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        const page = RemoveContactPage(contact.username, session, () => {
            hide()
            enableBackground()
            sidePanel.removeContact(contact)
        }, () => {
            hide()
            enableBackground()
        }, () => {
            hide()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, destroyAndInvalidSession, destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)

    const element = Div(classPrefix + ' Page')
    element.append(sidePanel.element)

    const contactRequests = WorkPage_ContactRequests(
        element, username, session, disableBackground, enableBackground,
        (username, contactData) => {
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        },
        destroyAndInvalidSession, destroyAndConnectionError,
        destroyAndCrash, destroyAndServiceError
    )
    for (const i in session.requests) {
        contactRequests.add(i, session.requests[i])
    }

    const pullMessages = WorkPage_PullMessages(session,
        sidePanel.endProcessing, destroyAndInvalidSession,
        destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)
    pullMessages.events.addContact.addListener((username, profile) => {
        const contact = sidePanel.getContact(username)
        if (contact === undefined) sidePanel.mergeContact(username, profile)
        contactRequests.remove(username)
    })
    pullMessages.events.addRequest.addListener(contactRequests.add)
    pullMessages.events.editContactProfile.addListener((username, profile) => {
        const contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.editProfile(profile)
    })
    pullMessages.events.editProfile.addListener(editProfile)
    pullMessages.events.editRequest.addListener(contactRequests.edit)
    pullMessages.events.ignoreRequest.addListener(contactRequests.remove)
    pullMessages.events.offline.addListener(sidePanel.offline)
    pullMessages.events.online.addListener(sidePanel.online)
    pullMessages.events.overrideContactProfile.addListener((username, overrideProfile) => {
        const contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.overrideProfile(overrideProfile)
    })
    pullMessages.events.receiveFileMessage.addListener((username, file, time, token) => {
        session.files[file.token] = {}
        sidePanel.receiveFileMessage(username, file, time, token)
        contactRequests.receiveFileMessage(username, file, time, token)
    })
    pullMessages.events.receiveTextMessage.addListener((username, text, time, token) => {
        sidePanel.receiveTextMessage(username, text, time, token)
        contactRequests.receiveTextMessage(username, text, time, token)
    })
    pullMessages.events.removeContact.addListener(username => {
        const contact = sidePanel.getContact(username)
        if (contact !== undefined) sidePanel.removeContact(contact)
    })
    pullMessages.events.removeFile.addListener(token => {
        receivedFiles.remove(token)
        delete session.files[token]
    })
    pullMessages.events.removeRequest.addListener(contactRequests.remove)
    pullMessages.events.requestFile.addListener(token => {
        numFeedingFiles++
        sentFiles.feed(token, () => {
            numFeedingFiles--
        })
    })
    pullMessages.events.sendFileMessage.addListener(sidePanel.sendFileMessage)
    pullMessages.events.sendTextMessage.addListener(sidePanel.sendTextMessage)

    const locationHash = WorkPage_LocationHash(session, checkUsername => {

        if (checkUsername === lowerUsername) {
            ;(() => {

                function hide () {
                    element.removeChild(page.element)
                    editProfileEvent.removeListener(page.editProfile)
                    locationHash.changeEvent.removeListener(hide)
                    enableBackground()
                }

                const page = SimpleSelfPage(username, profile, () => {
                    hide()
                    locationHash.reset()
                })

                element.append(page.element)
                page.focus()
                disableBackground()
                editProfileEvent.addListener(page.editProfile)
                locationHash.changeEvent.addListener(hide)

            })()
            return true
        }

        const contact = sidePanel.getLowerContact(checkUsername)
        if (contact === undefined) return false

        ;(() => {

            function contactRemoveListener () {
                hideAndResetHash()
                enableBackground()
            }

            function hide () {
                element.removeChild(page.element)
                contact.editProfileEvent.removeListener(page.editProfile)
                contact.removeEvent.removeListener(contactRemoveListener)
                locationHash.changeEvent.removeListener(hide)
            }

            function hideAndResetHash () {
                hide()
                locationHash.reset()
            }

            const page = SimpleContactPage(contact.username, contact.getProfile(), () => {
                hideAndResetHash()
                enableBackground()
                contact.ensureSelected()
            }, () => {
                hideAndResetHash()
                enableBackground()
            })

            element.append(page.element)
            page.focus()
            disableBackground()
            contact.editProfileEvent.addListener(page.editProfile)
            contact.removeEvent.addListener(contactRemoveListener)
            locationHash.changeEvent.addListener(hide)

        })()

        return true

    }, (username, profile) => {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.publicProfile.removeListener(publicProfileListener)
            destroyEvent.removeListener(page.destroy)
            locationHash.changeEvent.removeListener(hide)
        }

        function hideAndResetHash () {
            hide()
            locationHash.reset()
        }

        function publicProfileListener (eventUsername, profile) {
            if (username === eventUsername) page.editProfile(profile)
        }

        const page = SimpleUserPage(session, username, profile, contactData => {
            hideAndResetHash()
            enableBackground()
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        }, () => {
            hideAndResetHash()
            enableBackground()
            unwatchPublicProfile(username)
        }, () => {
            hideAndResetHash()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        pullMessages.events.publicProfile.addListener(publicProfileListener)
        destroyEvent.addListener(page.destroy)
        locationHash.changeEvent.addListener(hide)

    }, username => {

        const page = NoSuchUserPage(username, () => {
            element.removeChild(page.element)
            locationHash.reset()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()

    }, destroyAndConnectionError, destroyAndCrash, destroyAndInvalidSession, destroyAndServiceError)

    pullMessages.process(session.messages)
    locationHash.check()
    addEventListener('beforeunload', windowBeforeUnload)

    return { element: element }

}

function WorkPage_PullMessages (session, processedListener,
    invalidSessionListener, connectionErrorListener,
    crashListener, serviceErrorListener) {

    function process (messages) {
        if (messages.length === 0) return
        messages.forEach(message => {
            events[message[0]].emit(message[1])
        })
        processedListener()
    }

    function pull () {

        const request = GetJson(url, () => {

            if (request.status !== 200) {
                serviceErrorListener()
                return
            }

            const response = request.response
            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== 'NOTHING_TO_PULL') process(response)
            pull()

        }, connectionErrorListener)

        abortFunction = () => {
            request.abort()
        }

    }

    let abortFunction
    const url = 'data/pullMessages?token=' + encodeURIComponent(session.token)
    pull()

    const events = Object.create(null)
    events.addContact = WorkPage_Event()
    events.addRequest = WorkPage_Event()
    events.editContactProfile = WorkPage_Event()
    events.editContactProfileAndOffline = WorkPage_Event()
    events.editContactProfileAndOnline = WorkPage_Event()
    events.editProfile = WorkPage_Event()
    events.editRequest = WorkPage_Event()
    events.ignoreRequest = WorkPage_Event()
    events.offline = WorkPage_Event()
    events.online = WorkPage_Event()
    events.overrideContactProfile = WorkPage_Event()
    events.publicProfile = WorkPage_Event()
    events.receiveFileMessage = WorkPage_Event()
    events.receiveTextMessage = WorkPage_Event()
    events.removeContact = WorkPage_Event()
    events.removeFile = WorkPage_Event()
    events.removeRequest = WorkPage_Event()
    events.requestFile = WorkPage_Event()
    events.sendFileMessage = WorkPage_Event()
    events.sendTextMessage = WorkPage_Event()

    events.editContactProfileAndOffline.addListener((username, profile) => {
        events.editContactProfile.emit([username, profile])
        events.offline.emit([username])
    })
    events.editContactProfileAndOnline.addListener((username, profile) => {
        events.editContactProfile.emit([username, profile])
        events.online.emit([username])
    })

    return {
        events: events,
        process: process,
        abort () {
            abortFunction()
        },
    }

}

function WorkPage_ReceivedFiles () {

    const map = Object.create(null)

    return {
        add (token, removeCallback) {
            map[token] = { remove: removeCallback }
        },
        remove (token) {
            map[token].remove()
            delete map[token]
        },
    }

}

function WorkPage_SentFiles () {

    const map = Object.create(null)
    let size = 0

    return {
        add (token, feedCallback) {
            map[token] = { feed: feedCallback }
            size++
        },
        empty () {
            return size === 0
        },
        feed (token, endCallback) {
            map[token].feed(endCallback)
            delete map[token]
            size--
        },
    }

}

function WorkPage_SidePanel_AccountMenu (sounds,
    accountListener, soundsListener, signOutListener) {

    function flipSounds () {
        if (sounds) {
            sounds = false
            soundsNode.nodeValue = 'Off'
        } else {
            sounds = true
            soundsNode.nodeValue = 'On'
        }
        soundsListener(sounds)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    const classPrefix = 'WorkPage_SidePanel_AccountMenu'

    const accountItemElement = Div(classPrefix + '-item')
    accountItemElement.append('Account')
    accountItemElement.addEventListener('click', accountListener)

    const soundsNode = TextNode(sounds ? 'On' : 'Off')

    const soundsItemElement = Div(classPrefix + '-item')
    soundsItemElement.append('Sounds: ')
    soundsItemElement.append(soundsNode)
    soundsItemElement.addEventListener('click', flipSounds)

    const exitItemElement = Div(classPrefix + '-item')
    exitItemElement.append('Sign Out')
    exitItemElement.addEventListener('click', signOutListener)

    const element = Div(classPrefix)
    element.append(accountItemElement)
    element.append(soundsItemElement)
    element.append(exitItemElement)

    let selectedElement = null

    return {
        element: element,
        openSelected () {
            if (selectedElement === accountItemElement) accountListener()
            else if (selectedElement === soundsItemElement) flipSounds()
            else if (selectedElement === exitItemElement) signOutListener()
        },
        reset () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown () {
            if (selectedElement === accountItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = exitItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = accountItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp () {
            if (selectedElement === exitItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = accountItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = exitItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}

function WorkPage_SidePanel_Contact (playAudio, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    contactData, addNumberListener, selectListener,
    deselectListener, navigateUpListener, navigateDownListener,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, reorderListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addNumber (increment) {
        number += increment
        numberNode.nodeValue = number
        addNumberListener(increment)
    }

    function computeDisplayName () {
        return overrideProfile.fullName || profile.fullName || contactUsername
    }

    function computeSortName () {
        return displayName.toLowerCase()
    }

    function deselect () {
        selected = false
        classList.remove('selected')
        element.removeEventListener('click', deselectAndCallListener)
        element.addEventListener('click', select)
    }

    function deselectAndCallListener () {
        deselect()
        deselectListener()
    }

    function increaseNumber () {
        if (selected) {
            playAudio('message-seen')
            return
        }
        addNumber(1)
        if (number === 1) {
            classList.add('withMessages')
            numberClassList.add('visible')
        }
        playAudio('message-unseen')
    }

    function select () {

        selected = true
        classList.add('selected')
        element.removeEventListener('click', select)
        element.addEventListener('click', deselectAndCallListener)
        selectListener()

        if (number === 0) return
        addNumber(-number)
        numberClassList.remove('visible')
        classList.remove('withMessages')

    }

    function updateName () {
        displayName = computeDisplayName()
        sortName = computeSortName()
        nameNode.nodeValue = displayName
    }

    const classPrefix = 'WorkPage_SidePanel_Contact'

    let number = 0

    let selected = false

    let profile = contactData.profile,
        overrideProfile = contactData.overrideProfile,
        displayName = computeDisplayName(),
        sortName = computeSortName()

    const chatPanel = WorkPage_ChatPanel_Panel(playAudio,
        formatBytes, formatText, isLocalLink, sentFiles, receivedFiles,
        username, session, contactUsername, profile, overrideProfile,
        profileListener, sendSmileyListener, sendContactListener,
        removeListener, deselectAndCallListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener)

    const nameNode = TextNode(displayName)

    const numberNode = TextNode(number)

    const numberElement = document.createElement('span')
    numberElement.className = classPrefix + '-number'
    numberElement.append(numberNode)

    const numberClassList = numberElement.classList

    const element = document.createElement('button')
    element.className = classPrefix
    element.append(numberElement)
    element.append(nameNode)
    element.addEventListener('click', select)
    element.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 38) {
            e.preventDefault()
            navigateUpListener()
        } else if (keyCode === 40) {
            e.preventDefault()
            navigateDownListener()
        }
    })

    const classList = element.classList
    if (!contactData.online) classList.add('offline')

    const editProfileEvent = WorkPage_Event(),
        overrideProfileEvent = WorkPage_Event(),
        removeEvent = WorkPage_Event()

    return {
        chatPanel: chatPanel,
        deselect: deselect,
        destroy: chatPanel.destroy,
        editProfileEvent: editProfileEvent,
        editTimezone: chatPanel.editTimezone,
        element: element,
        overrideProfileEvent: overrideProfileEvent,
        removeEvent: removeEvent,
        select: select,
        sendFileMessage: chatPanel.sendFileMessage,
        sendTextMessage: chatPanel.sendTextMessage,
        username: contactUsername,
        userLink: renderUserLink(contactUsername),
        disable () {
            element.disabled = true
        },
        editProfile (_profile) {
            const oldSortName = sortName
            profile = _profile
            updateName()
            chatPanel.editContactProfile(profile)
            if (sortName !== oldSortName) reorderListener()
            editProfileEvent.emit([profile])
        },
        enable () {
            element.disabled = false
        },
        focus () {
            element.focus()
        },
        getDisplayName () {
            return displayName
        },
        getSortName () {
            return sortName
        },
        getOverrideProfile () {
            return overrideProfile
        },
        getProfile () {
            return profile
        },
        ensureSelected () {
            if (!selected) select()
        },
        offline () {
            classList.add('offline')
        },
        online () {
            classList.remove('offline')
        },
        overrideProfile (_overrideProfile) {
            const oldSortName = sortName
            overrideProfile = _overrideProfile
            updateName()
            chatPanel.overrideContactProfile(overrideProfile)
            if (sortName !== oldSortName) reorderListener()
            overrideProfileEvent.emit([overrideProfile])
        },
        receiveFileMessage (username, file, time, token) {
            chatPanel.receiveFileMessage(username, file, time, token)
            increaseNumber()
        },
        receiveTextMessage (username, text, time, token) {
            chatPanel.receiveTextMessage(username, text, time, token)
            increaseNumber()
        },
    }

}

function WorkPage_SidePanel_ContactList (sounds, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, selectListener,
    deselectListener, profileListener, sendSmileyListener,
    sendContactListener, removeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addContact (contactUsername, contactData) {

        function place () {
            const sortName = contact.getSortName()
            for (let i = 0; i < contactsArray.length; i++) {
                if (sortName > contactsArray[i].getSortName()) continue
                contentElement.insertBefore(contact.element, contactsArray[i].element)
                contactsArray.splice(i, 0, contact)
                reorderEvent.emit([contact, i])
                return
            }
            contentElement.append(contact.element)
            contactsArray.push(contact)
            reorderEvent.emit([contact, contactsArray.length - 1])
        }

        const contact = WorkPage_SidePanel_Contact(
            playAudio, formatBytes, formatText, isLocalLink,
            renderUserLink, sentFiles, receivedFiles, username, session,
            contactUsername, contactData, number => {
                totalNumber += number
                if (totalNumber === 0) document.title = 'Bazgu'
                else document.title = '(' + totalNumber + ') Bazgu'
            }, () => {
                if (selectedContact !== null) {
                    selectedContact.deselect()
                    deselectListener(selectedContact)
                }
                selectedContact = contact
                selectListener(contact)
            }, () => {
                deselectListener(selectedContact)
                selectedContact = null
                contact.focus()
            }, () => {
                const index = contactsArray.indexOf(contact)
                if (index !== 0) contactsArray[index - 1].focus()
            }, () => {
                const index = contactsArray.indexOf(contact)
                if (index === contactsArray.length - 1) return
                contactsArray[index + 1].focus()
            }, () => {
                profileListener(contact)
            }, send => {
                sendSmileyListener(contact, send)
            }, send => {
                sendContactListener(contact, contactsArray, send)
            }, () => {
                removeListener(contact)
            }, () => {
                contactsArray.splice(contactsArray.indexOf(contact), 1)
                contentElement.removeChild(contact.element)
                place()
            },
            invalidSessionListener, connectionErrorListener,
            crashListener, serviceErrorListener
        )

        contacts[contactUsername] = contact
        lowerContacts[contactUsername.toLowerCase()] = contact
        addEvent.emit([contact])
        place()

    }

    function playAudio (file) {
        audiosToPlay[file] = true
    }

    const audiosToPlay = Object.create(null)

    const contacts = Object.create(null)
    const lowerContacts = Object.create(null)
    const contactsArray = []

    let selectedContact = null

    const classPrefix = 'WorkPage_SidePanel_ContactList'

    const emptyElement = Div(classPrefix + '-empty')
    emptyElement.append('You have no contacts')

    const invitePanel = InvitePanel(username, () => {})

    const addEvent = WorkPage_Event(),
        reorderEvent = WorkPage_Event()

    const contentElement = Div(classPrefix + '-content')
    ;(() => {
        const contacts = session.contacts
        for (const i in contacts) addContact(i, contacts[i])
    })()
    if (contactsArray.length === 0) {
        contentElement.append(emptyElement)
        contentElement.append(invitePanel.element)
    }

    const element = Div(classPrefix)
    element.append(contentElement)

    let timezone = session.profile.timezone

    let totalNumber = 0
    document.title = 'Bazgu'

    return {
        addEvent: addEvent,
        element: element,
        reorderEvent: reorderEvent,
        destroy () {
            contactsArray.forEach(contact => {
                contact.destroy()
            })
        },
        disable () {
            contactsArray.forEach(contact => {
                contact.disable()
            })
        },
        editProfile (profile) {
            if (profile.timezone === timezone) return
            timezone = profile.timezone
            contactsArray.forEach(contact => {
                contact.editTimezone(timezone)
            })
        },
        enable () {
            contactsArray.forEach(contact => {
                contact.enable()
            })
        },
        endProcessing () {
            if (!sounds) return
            for (const i in audiosToPlay) {
                const audio = new Audio
                audio.src = 'audio/' + i + '.wav'
                audio.play()
                delete audiosToPlay[i]
            }
        },
        getContact (username) {
            return contacts[username]
        },
        getLowerContact (username) {
            return lowerContacts[username]
        },
        isEmpty () {
            return contactsArray.length === 0
        },
        offline (username) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.offline()
        },
        mergeContact (username, contactData) {

            const contact = contacts[username]
            if (contact === undefined) {
                if (contactsArray.length === 0) {
                    contentElement.removeChild(emptyElement)
                    contentElement.removeChild(invitePanel.element)
                }
                addContact(username, contactData)
                return
            }

            contact.editProfile(contactData.profile)
            contact.overrideProfile(contactData.overrideProfile)
            if (contactData.online) contact.online()
            else contact.offline()

        },
        online (username) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.online()
        },
        receiveFileMessage (username, file, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.receiveFileMessage(file, time, token)
        },
        receiveTextMessage (username, text, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.receiveTextMessage(text, time, token)
        },
        removeContact (contact) {
            if (contact === selectedContact) {
                selectedContact.deselect()
                deselectListener(selectedContact)
                selectedContact = null
            }
            contentElement.removeChild(contact.element)
            delete contacts[contact.username]
            delete lowerContacts[contact.username.toLowerCase()]
            contactsArray.splice(contactsArray.indexOf(contact), 1)
            if (contactsArray.length === 0) {
                contentElement.append(emptyElement)
                contentElement.append(invitePanel.element)
            }
            contact.removeEvent.emit()
        },
        sendFileMessage (username, file, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.sendFileMessage(file, time, token)
        },
        sendTextMessage (username, text, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.sendTextMessage(text, time, token)
        },
        setSounds (_sounds) {
            sounds = _sounds
        },
    }

}

function WorkPage_SidePanel_ContactStyle (getResourceUrl) {
    return '.WorkPage_SidePanel_Contact {' +
            'background-image: url(' + getResourceUrl('img/user-online.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.selected {' +
            'background-image: url(' + getResourceUrl('img/user-online-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.selected:active {' +
            'background-image: url(' + getResourceUrl('img/user-online-pressed-active.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline {' +
            'background-image: url(' + getResourceUrl('img/user-offline.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline.selected {' +
            'background-image: url(' + getResourceUrl('img/user-offline-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Contact.offline.selected:active {' +
            'background-image: url(' + getResourceUrl('img/user-offline-pressed-active.svg') + ')' +
        '}'
}

function WorkPage_SidePanel_Panel (formatBytes, formatText,
    isLocalLink, renderUserLink, sentFiles, receivedFiles, username,
    session, accountListener, signOutListener, addContactListener,
    contactSelectListener, contactDeselectListener, contactProfileListener,
    sendSmileyListener, sendContactListener,
    contactRemoveListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_SidePanel_Panel'

    const addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.append('Add Contact')
    addContactButton.addEventListener('click', () => {
        addContactListener(!contactList.isEmpty())
    })

    let sounds = true
    try {
        sounds = JSON.parse(localStorage[username + '?sounds'])
    } catch (e) {}

    const title = WorkPage_SidePanel_Title(sounds, username, session, accountListener, _sounds => {
        sounds = _sounds
        try {
            localStorage[username + '?sounds'] = JSON.stringify(sounds)
        } catch (e) {}
        contactList.setSounds(sounds)
    }, signOutListener)

    const contactList = WorkPage_SidePanel_ContactList(sounds,
        formatBytes, formatText, isLocalLink, renderUserLink,
        sentFiles, receivedFiles, username, session,
        contact => {
            contactSelectListener(contact)
            classList.add('chatOpen')
        },
        contact => {
            contactDeselectListener(contact)
            classList.remove('chatOpen')
        },
        contactProfileListener, sendSmileyListener, sendContactListener,
        contactRemoveListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    const element = Div(classPrefix)
    element.append(title.element)
    element.append(addContactButton)
    element.append(contactList.element)

    const classList = element.classList

    return {
        addContactEvent: contactList.addEvent,
        element: element,
        endProcessing: contactList.endProcessing,
        getContact: contactList.getContact,
        getLowerContact: contactList.getLowerContact,
        mergeContact: contactList.mergeContact,
        offline: contactList.offline,
        online: contactList.online,
        receiveFileMessage: contactList.receiveFileMessage,
        receiveTextMessage: contactList.receiveTextMessage,
        removeContact: contactList.removeContact,
        reorderEvent: contactList.reorderEvent,
        sendFileMessage: contactList.sendFileMessage,
        sendTextMessage: contactList.sendTextMessage,
        destroy () {
            title.destroy()
            contactList.destroy()
        },
        disable () {
            addContactButton.disabled = true
            title.disable()
            contactList.disable()
        },
        editProfile (profile) {
            title.editProfile(profile)
            contactList.editProfile(profile)
        },
        enable () {
            addContactButton.disabled = false
            title.enable()
            contactList.enable()
        },
    }

}

function WorkPage_SidePanel_PanelStyle () {
    return '.WorkPage_SidePanel_Panel {' +
            'background-image: url(' + getResourceUrl('img/light-grass.svg') + ')' +
        '}'
}

function WorkPage_SidePanel_Title (sounds, username,
    session, accountListener, soundsListener, signOutListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.append(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    let expanded = false

    const classPrefix = 'WorkPage_SidePanel_Title'

    const menu = WorkPage_SidePanel_AccountMenu(sounds, () => {
        collapse()
        accountListener()
    }, soundsListener, () => {
        collapse()
        signOutListener()
    })

    const node = TextNode(session.profile.fullName || username)

    const buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.append(node)

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.append(buttonTextElement)
    button.addEventListener('click', expand)

    const buttonClassList = button.classList

    const element = Div(classPrefix)
    element.append(button)

    return {
        element: element,
        destroy () {
            if (expanded) collapse()
        },
        disable () {
            if (expanded) collapse()
            button.disabled = true
        },
        editProfile (profile) {
            node.nodeValue = profile.fullName || username
        },
        enable () {
            button.disabled = false
        },
    }

}

function WorkPage_SidePanel_TitleStyle (getResourceUrl) {
    return '.WorkPage_SidePanel_Title-button {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Title-button.pressed {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Title-button.pressed:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed-active.svg') + ')' +
        '}'
}

(getResourceUrl => {

    function checkSession () {

        function hide () {
            body.removeChild(page.element)
        }

        const page = LoadingPage_Page()
        body.append(page.element)

        CheckSession((username, warningCallback) => {
            hide()
            showWelcomePage(username, page => {
                page.focus()
            }, warningCallback)
        }, (username, session) => {
            hide()
            showWorkPage(username, session)
        }, () => {
            hide()
            showConnectionErrorPage()
        }, () => {
            hide()
            showCrashPage()
        }, () => {
            hide()
            showServiceErrorPage()
        })

    }

    function showConnectionErrorPage () {
        const page = ConnectionErrorPage(() => {
            body.removeChild(page.element)
            checkSession()
        })
        body.append(page.element)
        page.focus()
    }

    function showCrashPage () {
        const page = CrashPage(() => {
            location.reload(true)
        })
        body.append(page.element)
        page.focus()
    }

    function showServiceErrorPage () {
        const page = ServiceErrorPage(() => {
            location.reload(true)
        })
        body.append(page.element)
        page.focus()
    }

    function showWelcomePage (username, readyCallback, warningCallback) {

        function hide () {
            body.removeChild(page.element)
            page.destroy()
            removeEventListener('storage', windowStorage)
        }

        function windowStorage (e) {
            if (e.storageArea !== localStorage || e.key !== 'token') return
            const token = e.newValue
            if (token === null) return
            hide()
            checkSession()
        }

        const page = WelcomePage_Page(username, () => {
            hide()
            ;(() => {

                function hide () {
                    body.removeChild(page.element)
                }

                const page = SignUpPage_Page(timezoneList, () => {
                    hide()
                    showWelcomePage(username, page => {
                        page.focusSignUp()
                    })
                }, (username, session) => {

                    hide()
                    showWorkPage(username, session)

                    ;(() => {
                        const page = AccountReadyPage(() => {
                            body.removeChild(page.element)
                        })
                        body.append(page.element)
                        page.focus()
                    })()

                })
                body.append(page.element)
                page.focus()

            })()
        }, () => {
            hide()
            ;(() => {

                function hide () {
                    body.removeChild(page.element)
                }

                const page = PrivacyPage(() => {
                    hide()
                    showWelcomePage(username, page => {
                        page.focusPrivacy()
                    })
                })
                body.append(page.element)
                page.focus()

            })()
        }, (username, session) => {
            hide()
            showWorkPage(username, session)
        }, warningCallback)

        body.append(page.element)
        readyCallback(page)
        addEventListener('storage', windowStorage)

    }

    function showWorkPage (username, session) {

        function hide () {
            document.title = initialTitle
            body.removeChild(page.element)
        }

        try {
            localStorage.username = username
            localStorage.token = session.token
        } catch (e) {
        }

        const page = WorkPage_Page(
            smileys, formatBytes, formatText, isLocalLink,
            renderUserLink, timezoneList, username, session,
            () => {

                try {
                    delete localStorage.token
                } catch (e) {
                }

                hide()
                showWelcomePage(username, page => {
                    page.focus()
                }, element => {
                    element.append('Signed out successfully.')
                })

            },
            () => {
                hide()
                checkSession()
            },
            () => {
                hide()
                showConnectionErrorPage()
            },
            () => {
                hide()
                showCrashPage()
            },
            () => {
                hide()
                showServiceErrorPage()
            }
        )
        body.append(page.element)

    }

    const body = document.body
    const initialTitle = document.title

    const formatBytes = FormatBytes(),
        smileys = Smileys(),
        formatText = FormatText(smileys),
        isLocalLink = IsLocalLink(),
        renderUserLink = RenderUserLink(),
        timezoneList = Timezone_List()

    ;(() => {
        const style = document.createElement('style')
        style.type = 'text/css'
        style.innerHTML = AccountPage_PrivacySelectStyle(getResourceUrl) +
            AccountPage_PrivacySelectItemStyle(getResourceUrl) +
            AccountPage_TimezoneItemStyle(getResourceUrl) +
            CloseButtonStyle(getResourceUrl) +
            ContactSelectPage_ItemStyle(getResourceUrl) +
            EditContactPage_TimezoneItemStyle(getResourceUrl) +
            PageStyle(getResourceUrl) +
            SignUpPage_AdvancedItemStyle(getResourceUrl) +
            SignUpPage_TimezoneItemStyle(getResourceUrl) +
            SmileysPage_PageStyle(smileys, getResourceUrl) +
            WelcomePage_PageStyle(getResourceUrl) +
            WelcomePage_StaySignedInItemStyle(getResourceUrl) +
            WorkPage_ChatPanel_CloseButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_MoreButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_PanelStyle(getResourceUrl) +
            WorkPage_ChatPanel_SmileyStyle(smileys, getResourceUrl) +
            WorkPage_ChatPanel_TitleStyle(getResourceUrl) +
            WorkPage_SidePanel_ContactStyle(getResourceUrl) +
            WorkPage_SidePanel_PanelStyle(getResourceUrl) +
            WorkPage_SidePanel_TitleStyle(getResourceUrl)
        document.head.append(style)
    })()

    checkSession()

})(getResourceUrl)

})()
