function SignOutPage (confirmListener, closeListener) {

    const classPrefix = 'SignOutPage'

    const textElement = Div(classPrefix + '-text')
    textElement.append('Are you sure')
    textElement.append(document.createElement('br'))
    textElement.append('you want to sign out?')

    const yesButton = document.createElement('button')
    yesButton.className = classPrefix + '-yesButton GreenButton'
    yesButton.append('Sign Out')
    yesButton.addEventListener('click', confirmListener)
    yesButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const noButton = document.createElement('button')
    noButton.className = classPrefix + '-noButton OrangeButton'
    noButton.append('Cancel')
    noButton.addEventListener('click', closeListener)
    noButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(yesButton)
    frameElement.append(noButton)

    const element = Div(classPrefix)
    element.append(frameElement)
    element.append(Div(classPrefix + '-aligner'))
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus () {
            yesButton.focus()
        },
    }

}
