function AddContactPage_Page (showInvite, session,
    username, checkContactListener, userFoundListener,
    closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        usernameItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Find User'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'AddContactPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Add Contact')

    const usernameItem = AddContactPage_UsernameItem(close)

    const buttonNode = TextNode('Find User')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const form = document.createElement('form')
    form.append(usernameItem.element)
    form.append(button)
    form.addEventListener('submit', e => {

        e.preventDefault()
        usernameItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const username = usernameItem.getValue()
        if (username === null) return

        const lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        usernameItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Finding...'

        const url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                usernameItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Find User'
                usernameItem.showError(errorElement => {
                    errorElement.append('There is no such user.')
                })
                return
            }

            userFoundListener(response.username, response.profile)

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)
    if (showInvite) {

        const invitePanelElement = Div(classPrefix + '-invitePanel')
        invitePanelElement.append(InvitePanel(username, close).element)

        frameElement.append(invitePanelElement)

    }

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: usernameItem.focus,
    }

}
