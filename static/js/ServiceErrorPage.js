function ServiceErrorPage (reloadListener) {

    const classPrefix = 'ServiceErrorPage'

    const text = 'There is a problem at Bazgu. We are fixing it.' +
        ' Reload the page to see if it\'s resolved.'

    const textElement = Div(classPrefix + '-text')
    textElement.append(text)

    const buttonNode = TextNode('Reload')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('click', () => {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}
