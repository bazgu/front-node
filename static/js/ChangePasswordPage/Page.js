function ChangePasswordPage_Page (session,
    backListener, closeListener, invalidSessionListener) {

    function back () {
        destroy()
        backListener()
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        currentPasswordItem.enable()
        newPasswordItem.enable()
        repeatNewPasswordItem.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Save'

        error = _error
        form.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'ChangePasswordPage_Page'

    const backButton = BackButton(back)

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Change Password')

    const currentPasswordItem = ChangePasswordPage_CurrentPasswordItem(back)

    const newPasswordItem = ChangePasswordPage_NewPasswordItem(back)

    const repeatNewPasswordItem = ChangePasswordPage_RepeatNewPasswordItem(back)

    const buttonNode = TextNode('Save')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        currentPasswordItem.element,
        newPasswordItem.element,
        repeatNewPasswordItem.element,
    ]))
    form.append(button)
    form.addEventListener('submit', e => {

        e.preventDefault()
        currentPasswordItem.clearError()
        newPasswordItem.clearError()
        repeatNewPasswordItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const currentPassword = currentPasswordItem.getValue()
        if (currentPassword === null) return

        const newPassword = newPasswordItem.getValue()
        if (newPassword === null) return

        const repeatNewPassword = repeatNewPasswordItem.getValue(newPassword)
        if (repeatNewPassword === null) return

        currentPasswordItem.disable()
        newPasswordItem.disable()
        repeatNewPasswordItem.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Saving...'

        const url = 'data/changePassword' +
            '?token=' + encodeURIComponent(session.token) +
            '&currentPassword=' + encodeURIComponent(currentPassword) +
            '&newPassword=' + encodeURIComponent(newPassword)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_CURRENT_PASSWORD') {
                currentPasswordItem.enable()
                newPasswordItem.enable()
                repeatNewPasswordItem.enable()
                button.disabled = false
                buttonNode.nodeValue = 'Save'
                currentPasswordItem.showError(errorElement => {
                    errorElement.append('The password is incorrect.')
                })
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            closeListener()

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: currentPasswordItem.focus,
    }

}
