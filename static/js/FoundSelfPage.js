function FoundSelfPage (username, profile, backListener, closeListener) {

    const backButton = BackButton(backListener)

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const classPrefix = 'FoundSelfPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append('" it\'s you.')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus: closeButton.focus,
        username: username,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}
