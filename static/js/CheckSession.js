function CheckSession (readyListener, signInListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    let username, token
    try {
        username = localStorage.username
        token = localStorage.token
    } catch (e) {
    }

    let warningCallback
    const hashUsername = location.hash.substr(1)
    if (Username_IsValid(hashUsername)) {
        warningCallback = WelcomePage_UnauthenticatedWarning(hashUsername)
    }

    if (token === undefined) {
        readyListener(username === undefined ? '' : username, warningCallback)
        return
    }

    const url = 'data/restoreSession?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    const request = GetJson(url, () => {

        if (request.status !== 200) {
            serviceErrorListener()
            return
        }

        const response = request.response
        if (response === null) {
            crashListener()
            return
        }

        if (response === 'INVALID_USERNAME' || response === 'INVALID_TOKEN') {
            try {
                delete localStorage.token
            } catch (e) {
            }
            readyListener(username, warningCallback)
            return
        }

        signInListener(username, response)

    }, connectionErrorListener)

}
