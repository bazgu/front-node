function CrashPage (reloadListener) {

    const classPrefix = 'CrashPage'

    const textElement = Div(classPrefix + '-text')
    textElement.append('Something went wrong.')
    textElement.append(document.createElement('br'))
    textElement.append('Reloading may fix the problem.')

    const buttonNode = TextNode('Reload')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('click', () => {
        button.disabled = true
        buttonNode.nodeValue = 'Reloading...'
        reloadListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}
