function ConnectionErrorPage (reconnectListener) {

    const classPrefix = 'ConnectionErrorPage'

    const textElement = Div(classPrefix + '-text')
    textElement.append('Connection lost.')
    textElement.append(document.createElement('br'))
    textElement.append('Please, check your network and reconnect.')

    const buttonNode = TextNode('Reconnect')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('click', () => {
        button.disabled = true
        buttonNode.nodeValue = 'Reconnecting...'
        reconnectListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(button)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}
