function InvitePanel (username, backListener) {

    const classPrefix = 'InvitePanel'

    const link = location.protocol + '//' +
        location.host + location.pathname + '#' + username

    const text = 'You can chat with me at ' + link

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Invite People')

    const smsButton = document.createElement('a')
    smsButton.target = '_blank'
    smsButton.className = classPrefix + '-smsButton OrangeButton'
    smsButton.append('Via SMS')
    smsButton.href = 'sms:?body=' + encodeURIComponent(text)
    smsButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const emailButton = document.createElement('a')
    emailButton.target = '_blank'
    emailButton.className = classPrefix + '-emailButton OrangeButton'
    emailButton.append('Via Email')
    emailButton.href = 'mailto:?body=' + encodeURIComponent(text)
    emailButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const element = Div(classPrefix)
    element.append(titleElement)
    element.append(smsButton)
    element.append(emailButton)

    return { element: element }

}
