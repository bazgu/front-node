function IsLocalLink () {

    const localPrefix = location.protocol + '//' +
        location.host + location.pathname + '#'

    return link => {
        if (link.substr(0, localPrefix.length) !== localPrefix) return false
        const username = link.substr(localPrefix.length)
        return Username_IsValid(username)
    }

}
