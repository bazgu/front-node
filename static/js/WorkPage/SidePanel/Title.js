function WorkPage_SidePanel_Title (sounds, username,
    session, accountListener, soundsListener, signOutListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.append(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    let expanded = false

    const classPrefix = 'WorkPage_SidePanel_Title'

    const menu = WorkPage_SidePanel_AccountMenu(sounds, () => {
        collapse()
        accountListener()
    }, soundsListener, () => {
        collapse()
        signOutListener()
    })

    const node = TextNode(session.profile.fullName || username)

    const buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.append(node)

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.append(buttonTextElement)
    button.addEventListener('click', expand)

    const buttonClassList = button.classList

    const element = Div(classPrefix)
    element.append(button)

    return {
        element: element,
        destroy () {
            if (expanded) collapse()
        },
        disable () {
            if (expanded) collapse()
            button.disabled = true
        },
        editProfile (profile) {
            node.nodeValue = profile.fullName || username
        },
        enable () {
            button.disabled = false
        },
    }

}
