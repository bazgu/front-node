function WorkPage_SidePanel_Panel (formatBytes, formatText,
    isLocalLink, renderUserLink, sentFiles, receivedFiles, username,
    session, accountListener, signOutListener, addContactListener,
    contactSelectListener, contactDeselectListener, contactProfileListener,
    sendSmileyListener, sendContactListener,
    contactRemoveListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_SidePanel_Panel'

    const addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.append('Add Contact')
    addContactButton.addEventListener('click', () => {
        addContactListener(!contactList.isEmpty())
    })

    let sounds = true
    try {
        sounds = JSON.parse(localStorage[username + '?sounds'])
    } catch (e) {}

    const title = WorkPage_SidePanel_Title(sounds, username, session, accountListener, _sounds => {
        sounds = _sounds
        try {
            localStorage[username + '?sounds'] = JSON.stringify(sounds)
        } catch (e) {}
        contactList.setSounds(sounds)
    }, signOutListener)

    const contactList = WorkPage_SidePanel_ContactList(sounds,
        formatBytes, formatText, isLocalLink, renderUserLink,
        sentFiles, receivedFiles, username, session,
        contact => {
            contactSelectListener(contact)
            classList.add('chatOpen')
        },
        contact => {
            contactDeselectListener(contact)
            classList.remove('chatOpen')
        },
        contactProfileListener, sendSmileyListener, sendContactListener,
        contactRemoveListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    const element = Div(classPrefix)
    element.append(title.element)
    element.append(addContactButton)
    element.append(contactList.element)

    const classList = element.classList

    return {
        addContactEvent: contactList.addEvent,
        element: element,
        endProcessing: contactList.endProcessing,
        getContact: contactList.getContact,
        getLowerContact: contactList.getLowerContact,
        mergeContact: contactList.mergeContact,
        offline: contactList.offline,
        online: contactList.online,
        receiveFileMessage: contactList.receiveFileMessage,
        receiveTextMessage: contactList.receiveTextMessage,
        removeContact: contactList.removeContact,
        reorderEvent: contactList.reorderEvent,
        sendFileMessage: contactList.sendFileMessage,
        sendTextMessage: contactList.sendTextMessage,
        destroy () {
            title.destroy()
            contactList.destroy()
        },
        disable () {
            addContactButton.disabled = true
            title.disable()
            contactList.disable()
        },
        editProfile (profile) {
            title.editProfile(profile)
            contactList.editProfile(profile)
        },
        enable () {
            addContactButton.disabled = false
            title.enable()
            contactList.enable()
        },
    }

}
