function WorkPage_SidePanel_AccountMenu (sounds,
    accountListener, soundsListener, signOutListener) {

    function flipSounds () {
        if (sounds) {
            sounds = false
            soundsNode.nodeValue = 'Off'
        } else {
            sounds = true
            soundsNode.nodeValue = 'On'
        }
        soundsListener(sounds)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    const classPrefix = 'WorkPage_SidePanel_AccountMenu'

    const accountItemElement = Div(classPrefix + '-item')
    accountItemElement.append('Account')
    accountItemElement.addEventListener('click', accountListener)

    const soundsNode = TextNode(sounds ? 'On' : 'Off')

    const soundsItemElement = Div(classPrefix + '-item')
    soundsItemElement.append('Sounds: ')
    soundsItemElement.append(soundsNode)
    soundsItemElement.addEventListener('click', flipSounds)

    const exitItemElement = Div(classPrefix + '-item')
    exitItemElement.append('Sign Out')
    exitItemElement.addEventListener('click', signOutListener)

    const element = Div(classPrefix)
    element.append(accountItemElement)
    element.append(soundsItemElement)
    element.append(exitItemElement)

    let selectedElement = null

    return {
        element: element,
        openSelected () {
            if (selectedElement === accountItemElement) accountListener()
            else if (selectedElement === soundsItemElement) flipSounds()
            else if (selectedElement === exitItemElement) signOutListener()
        },
        reset () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown () {
            if (selectedElement === accountItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = exitItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = accountItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp () {
            if (selectedElement === exitItemElement) {
                resetSelected()
                selectedElement = soundsItemElement
            } else if (selectedElement === soundsItemElement) {
                resetSelected()
                selectedElement = accountItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = exitItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}
