function WorkPage_SidePanel_TitleStyle (getResourceUrl) {
    return '.WorkPage_SidePanel_Title-button {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Title-button.pressed {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed.svg') + ')' +
        '}' +
        '.WorkPage_SidePanel_Title-button.pressed:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed-active.svg') + ')' +
        '}'
}
