function WorkPage_SidePanel_ContactList (sounds, formatBytes,
    formatText, isLocalLink, renderUserLink, sentFiles,
    receivedFiles, username, session, selectListener,
    deselectListener, profileListener, sendSmileyListener,
    sendContactListener, removeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addContact (contactUsername, contactData) {

        function place () {
            const sortName = contact.getSortName()
            for (let i = 0; i < contactsArray.length; i++) {
                if (sortName > contactsArray[i].getSortName()) continue
                contentElement.insertBefore(contact.element, contactsArray[i].element)
                contactsArray.splice(i, 0, contact)
                reorderEvent.emit([contact, i])
                return
            }
            contentElement.append(contact.element)
            contactsArray.push(contact)
            reorderEvent.emit([contact, contactsArray.length - 1])
        }

        const contact = WorkPage_SidePanel_Contact(
            playAudio, formatBytes, formatText, isLocalLink,
            renderUserLink, sentFiles, receivedFiles, username, session,
            contactUsername, contactData, number => {
                totalNumber += number
                if (totalNumber === 0) document.title = 'Bazgu'
                else document.title = '(' + totalNumber + ') Bazgu'
            }, () => {
                if (selectedContact !== null) {
                    selectedContact.deselect()
                    deselectListener(selectedContact)
                }
                selectedContact = contact
                selectListener(contact)
            }, () => {
                deselectListener(selectedContact)
                selectedContact = null
                contact.focus()
            }, () => {
                const index = contactsArray.indexOf(contact)
                if (index !== 0) contactsArray[index - 1].focus()
            }, () => {
                const index = contactsArray.indexOf(contact)
                if (index === contactsArray.length - 1) return
                contactsArray[index + 1].focus()
            }, () => {
                profileListener(contact)
            }, send => {
                sendSmileyListener(contact, send)
            }, send => {
                sendContactListener(contact, contactsArray, send)
            }, () => {
                removeListener(contact)
            }, () => {
                contactsArray.splice(contactsArray.indexOf(contact), 1)
                contentElement.removeChild(contact.element)
                place()
            },
            invalidSessionListener, connectionErrorListener,
            crashListener, serviceErrorListener
        )

        contacts[contactUsername] = contact
        lowerContacts[contactUsername.toLowerCase()] = contact
        addEvent.emit([contact])
        place()

    }

    function playAudio (file) {
        audiosToPlay[file] = true
    }

    const audiosToPlay = Object.create(null)

    const contacts = Object.create(null)
    const lowerContacts = Object.create(null)
    const contactsArray = []

    let selectedContact = null

    const classPrefix = 'WorkPage_SidePanel_ContactList'

    const emptyElement = Div(classPrefix + '-empty')
    emptyElement.append('You have no contacts')

    const invitePanel = InvitePanel(username, () => {})

    const addEvent = WorkPage_Event(),
        reorderEvent = WorkPage_Event()

    const contentElement = Div(classPrefix + '-content')
    ;(() => {
        const contacts = session.contacts
        for (const i in contacts) addContact(i, contacts[i])
    })()
    if (contactsArray.length === 0) {
        contentElement.append(emptyElement)
        contentElement.append(invitePanel.element)
    }

    const element = Div(classPrefix)
    element.append(contentElement)

    let timezone = session.profile.timezone

    let totalNumber = 0
    document.title = 'Bazgu'

    return {
        addEvent: addEvent,
        element: element,
        reorderEvent: reorderEvent,
        destroy () {
            contactsArray.forEach(contact => {
                contact.destroy()
            })
        },
        disable () {
            contactsArray.forEach(contact => {
                contact.disable()
            })
        },
        editProfile (profile) {
            if (profile.timezone === timezone) return
            timezone = profile.timezone
            contactsArray.forEach(contact => {
                contact.editTimezone(timezone)
            })
        },
        enable () {
            contactsArray.forEach(contact => {
                contact.enable()
            })
        },
        endProcessing () {
            if (!sounds) return
            for (const i in audiosToPlay) {
                const audio = new Audio
                audio.src = 'audio/' + i + '.wav'
                audio.play()
                delete audiosToPlay[i]
            }
        },
        getContact (username) {
            return contacts[username]
        },
        getLowerContact (username) {
            return lowerContacts[username]
        },
        isEmpty () {
            return contactsArray.length === 0
        },
        offline (username) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.offline()
        },
        mergeContact (username, contactData) {

            const contact = contacts[username]
            if (contact === undefined) {
                if (contactsArray.length === 0) {
                    contentElement.removeChild(emptyElement)
                    contentElement.removeChild(invitePanel.element)
                }
                addContact(username, contactData)
                return
            }

            contact.editProfile(contactData.profile)
            contact.overrideProfile(contactData.overrideProfile)
            if (contactData.online) contact.online()
            else contact.offline()

        },
        online (username) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.online()
        },
        receiveFileMessage (username, file, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.receiveFileMessage(file, time, token)
        },
        receiveTextMessage (username, text, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.receiveTextMessage(text, time, token)
        },
        removeContact (contact) {
            if (contact === selectedContact) {
                selectedContact.deselect()
                deselectListener(selectedContact)
                selectedContact = null
            }
            contentElement.removeChild(contact.element)
            delete contacts[contact.username]
            delete lowerContacts[contact.username.toLowerCase()]
            contactsArray.splice(contactsArray.indexOf(contact), 1)
            if (contactsArray.length === 0) {
                contentElement.append(emptyElement)
                contentElement.append(invitePanel.element)
            }
            contact.removeEvent.emit()
        },
        sendFileMessage (username, file, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.sendFileMessage(file, time, token)
        },
        sendTextMessage (username, text, time, token) {
            const contact = contacts[username]
            if (contact === undefined) return
            contact.sendTextMessage(text, time, token)
        },
        setSounds (_sounds) {
            sounds = _sounds
        },
    }

}
