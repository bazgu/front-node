function WorkPage_ChatPanel_MessagesPanel (playAudio,
    formatBytes, formatText, isLocalLink, sentFiles,
    receivedFiles, username, session, contactUsername,
    textareaFocusListener, textareaBlurListener, sendSmileyListener,
    sendContactListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function addMessage (message, time) {
        ensureSeparator(time)
        doneMessagesElement.append(message.element)
        sendingMessagesClassList.add('notFirst')
        liveMessages.push(message)
        lastMessage = message
    }

    function addReceivedFileMessage (file, time, token) {
        if (!canMerge('receivedFile', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_ReceivedFileMessage(
                    formatBytes, receivedFiles, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        const part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addReceivedTextMessage (text, time, token) {
        if (!canMerge('receivedText', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_ReceivedTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentFileMessage (file, time, token) {
        if (!canMerge('sentFile', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_SentFileMessage(
                    formatBytes, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        const part = messagePartsMap[token] = lastMessage.add(file)
        scrollDown()
        return part
    }

    function addSentFileMessageAndStore (file, time, token) {
        messages.push(['sentFile', file, time, token])
        return addSentFileMessage(file, time, token)
    }

    function addSentTextMessage (text, time, token) {
        if (!canMerge('sentText', time)) {
            ;(() => {
                const message = WorkPage_ChatPanel_SentTextMessage(
                    disableEvent, enableEvent, formatText,
                    isLocalLink, time, timezoneOffset)
                addMessage(message, time + timezoneOffset)
            })()
        }
        messagePartsMap[token] = lastMessage.add(text)
        scrollDown()
    }

    function addSentTextMessageAndStore (text, time, token) {
        addSentTextMessage(text, time, token)
        messages.push(['sentText', text, time, token])
    }

    function canMerge (type, time) {
        return lastMessage !== null &&
            lastMessage.type === type && lastMessage.minute === Minute(time)
    }

    function destroy () {
        typePanel.destroy()
        destroyEvent.emit()
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function ensureSeparator (time) {
        const day = Day(time)
        let separator = daySeparators[day]
        if (separator !== undefined) return
        separator = daySeparators[day] = WorkPage_ChatPanel_DaySeparator(day)
        doneMessagesElement.append(separator.element)
    }

    function getLastSendingMessage (type) {
        const length = sendingMessages.length
        if (length !== 0) {
            const message = sendingMessages[length - 1]
            if (message.type === type) return message
        }
    }

    function scrollDown () {
        contentElement.scrollTop = contentElement.scrollHeight
    }

    function sendNewTextMessage (text) {

        let message = getLastSendingMessage('SendingText')
        if (message === undefined) {
            message = WorkPage_ChatPanel_SendingTextMessage(
                disableEvent, enableEvent, formatText,
                isLocalLink, session, contactUsername,
                () => {
                    destroyEvent.removeListener(message.destroy)
                    sendingMessagesElement.removeChild(message.element)
                    sendingMessages.splice(sendingMessages.indexOf(message), 1)
                    if (sendingMessagesElement.childNodes.length === 0) {
                        sendingMessagesClassList.add('hidden')
                    }
                },
                destroyAndInvalidSession, destroyAndConnectionError,
                destroyAndCrash, destroyAndServiceError
            )
            sendingMessages.push(message)
        }

        message.add(text, response => {
            addSentTextMessageAndStore(text, response.time, response.token)
            playAudio('message-seen')
        })

        destroyEvent.addListener(message.destroy)
        sendingMessagesElement.append(message.element)
        if (sendingMessagesElement.childNodes.length === 1) {
            sendingMessagesClassList.remove('hidden')
        }
        scrollDown()

    }

    const disableEvent = WorkPage_Event(),
        enableEvent = WorkPage_Event()

    let timezone = session.profile.timezone,
        timezoneOffset = timezone * 60 * 1000

    let lastMessage = null

    const sendingMessages = []

    const messages = WorkPage_Messages(username, contactUsername)
    const liveMessages = []
    const messagePartsMap = Object.create(null)
    const daySeparators = Object.create(null)
    const destroyEvent = WorkPage_Event()

    const classPrefix = 'WorkPage_ChatPanel_MessagesPanel'

    const doneMessagesElement = Div(classPrefix + '-doneMessages')

    const sendingMessagesElement = Div(classPrefix + '-sendingMessages hidden')

    const sendingMessagesClassList = sendingMessagesElement.classList

    const contentElement = Div(classPrefix + '-content')
    contentElement.append(doneMessagesElement)
    contentElement.append(sendingMessagesElement)

    const typePanel = WorkPage_ChatPanel_TypePanel(() => {
        classList.add('typing')
        textareaFocusListener()
    }, () => {
        classList.remove('typing')
        textareaBlurListener()
    }, sendNewTextMessage, files => {
        Array.prototype.forEach.call(files, readableFile => {

            const file = {
                name: readableFile.name,
                size: readableFile.size,
            }

            let message = getLastSendingMessage('SendingFile')
            if (message === undefined) {
                message = WorkPage_ChatPanel_SendingFileMessage(
                    formatBytes, session, contactUsername,
                    () => {
                        destroyEvent.removeListener(message.destroy)
                        sendingMessagesElement.removeChild(message.element)
                        sendingMessages.splice(sendingMessages.indexOf(message), 1)
                        if (sendingMessagesElement.childNodes.length === 0) {
                            sendingMessagesClassList.add('hidden')
                        }
                    },
                    destroyAndInvalidSession, destroyAndConnectionError,
                    destroyAndCrash, destroyAndServiceError
                )
                sendingMessages.push(message)
            }

            message.add(file, response => {
                const sendToken = response.sendToken
                const startFeed = addSentFileMessageAndStore(file, response.time, response.token).startSend()
                sentFiles.add(sendToken, endCallback => {

                    function complete () {
                        progress.complete()
                        endCallback()
                    }

                    const progress = startFeed()
                    if (file.size === 0) complete()
                    else {
                        const destroyFunction = WorkPage_ChatPanel_FeedFile(
                            sendToken, readableFile, progress.progress, complete,
                            () => {
                                progress.cancel()
                                endCallback()
                            },
                            () => {
                                destroyEvent.removeListener(destroyFunction)
                            },
                            destroyAndConnectionError, destroyAndCrash, destroyAndServiceError
                        )
                        destroyEvent.addListener(destroyFunction)
                    }

                })
                playAudio('message-seen')
            })

            destroyEvent.addListener(message.destroy)
            sendingMessagesElement.append(message.element)
            if (sendingMessagesElement.childNodes.length === 1) {
                sendingMessagesClassList.remove('hidden')
            }
            scrollDown()

        })
        scrollDown()
    }, () => {
        sendSmileyListener(sendNewTextMessage)
    }, () => {
        sendContactListener(contacts => {
            const text = contacts.map(contact => {
                return location.protocol + '//' + location.host +
                    location.pathname + '#' + contact.username
            }).join('\n')
            sendNewTextMessage(text)
        })
    }, closeListener)

    const element = Div(classPrefix)
    element.append(contentElement)
    element.append(typePanel.element)

    const classList = element.classList

    WorkPage_ChatPanel_RestoreMessages(messages, session,
        addReceivedFileMessage, addReceivedTextMessage,
        addSentFileMessage, addSentTextMessage)

    return {
        destroy: destroy,
        element: element,
        sendFileMessage: addSentFileMessageAndStore,
        sendTextMessage: addSentTextMessageAndStore,
        disable () {
            typePanel.disable()
            disableEvent.emit()
        },
        editTimezone (newTimezone) {

            timezone = newTimezone
            timezoneOffset = timezone * 60 * 1000

            Object.keys(daySeparators).forEach(day => {
                doneMessagesElement.removeChild(daySeparators[day].element)
                delete daySeparators[day]
            })

            liveMessages.forEach(message => {
                message.editTimezone(timezoneOffset)
                ensureSeparator(message.time + timezoneOffset)
                doneMessagesElement.append(message.element)
            })

        },
        enable () {
            typePanel.enable()
            enableEvent.emit()
        },
        focus () {
            typePanel.focus()
            scrollDown()
        },
        receiveFileMessage (file, time, token) {
            addReceivedFileMessage(file, time, token).showReceive()
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage (text, time, token) {
            addReceivedTextMessage(text, time, token)
            messages.push(['receivedText', text, time, token])
        },
    }

}
