function WorkPage_ChatPanel_MoreMenu (fileInputKeyDownListener,
    fileListener, smileyListener, contactListener) {

    function addFileInput () {
        fileInput = document.createElement('input')
        fileInput.type = 'file'
        fileInput.multiple = true
        fileInput.className = classPrefix + '-fileInput'
        fileInput.addEventListener('keydown', fileInputKeyDownListener)
        fileInput.addEventListener('change', () => {
            fileListener(fileInput.files)
            fileItemElement.removeChild(fileInput)
            addFileInput()
        })
        fileItemElement.append(fileInput)
    }

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    const classPrefix = 'WorkPage_ChatPanel_MoreMenu'

    const fileItemElement = Div(classPrefix + '-item')
    fileItemElement.append('File')

    const smileyItemElement = Div(classPrefix + '-item')
    smileyItemElement.append('Smiley')
    smileyItemElement.addEventListener('click', smileyListener)

    const contactItemElement = Div(classPrefix + '-item')
    contactItemElement.append('Contact')
    contactItemElement.addEventListener('click', contactListener)

    const element = Div(classPrefix)
    element.append(fileItemElement)
    element.append(smileyItemElement)
    element.append(contactItemElement)

    let selectedElement = null

    let fileInput
    addFileInput()

    return {
        element: element,
        openSelected () {
            if (selectedElement === fileItemElement) {
            } else if (selectedElement === smileyItemElement) {
                smileyListener()
            } else if (selectedElement === contactItemElement) {
                contactListener()
            }
        },
        reset () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown () {
            if (selectedElement === fileItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = contactItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            }
            selectedElement.classList.add('active')
        },
        selectUp () {
            if (selectedElement === contactItemElement) {
                resetSelected()
                selectedElement = smileyItemElement
            } else if (selectedElement === smileyItemElement) {
                resetSelected()
                selectedElement = fileItemElement
                fileInput.focus()
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = contactItemElement
            }
            selectedElement.classList.add('active')
        },
    }

}
