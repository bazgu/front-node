function WorkPage_ChatPanel_SentFileMessage (formatBytes, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_SentFileMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentFile',
        add (file) {

            const sizeElement = Div(classPrefix + '-item-size')
            sizeElement.append(formatBytes(file.size))

            const itemElement = Div(classPrefix + '-item')
            itemElement.append(file.name)
            itemElement.append(sizeElement)

            element.append(itemElement)

            return {
                startSend () {

                    const node = TextNode('Waiting...')

                    const sendingElement = Div(classPrefix + '-item-sending')
                    sendingElement.append(node)

                    itemElement.classList.add('sending')
                    itemElement.append(sendingElement)

                    return () => {

                        node.nodeValue = '0%'

                        const doneTextNode = TextNode('0%')

                        const doneTextElement = Div(classPrefix + '-item-sending-done-text')
                        doneTextElement.append(doneTextNode)

                        const doneElement = Div(classPrefix + '-item-sending-done')
                        doneElement.append(doneTextElement)

                        sendingElement.append(doneElement)

                        return {
                            cancel () {
                                sendingElement.classList.add('failed')
                                doneTextElement.classList.add('failed')
                            },
                            complete () {
                                node.nodeValue = doneTextNode.nodeValue = '100%'
                                doneElement.style.width = '100%'
                            },
                            progress (sent) {

                                const percent = Math.round(sent / file.size * 100) + '%'

                                node.nodeValue = doneTextNode.nodeValue = percent
                                doneElement.style.width = percent

                            },
                        }

                    }

                },
            }

        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}
