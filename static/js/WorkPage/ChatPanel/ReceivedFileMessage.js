function WorkPage_ChatPanel_ReceivedFileMessage (
    formatBytes, receivedFiles, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_ReceivedFileMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'receivedFile',
        add (file) {

            const nameNode = TextNode(file.name)

            const sizeElement = Div(classPrefix + '-item-size')
            sizeElement.append(formatBytes(file.size))

            const itemElement = Div(classPrefix + '-item')
            itemElement.append(nameNode)
            itemElement.append(sizeElement)

            element.append(itemElement)

            return {
                showReceive () {

                    const receiveLink = document.createElement('a')
                    receiveLink.className = classPrefix + '-receiveLink'
                    receiveLink.append('Receive')
                    receiveLink.target = '_blank'
                    receiveLink.href = 'data/receiveFile?token=' + encodeURIComponent(file.token)

                    itemElement.classList.add('withReceiveLink')
                    itemElement.insertBefore(receiveLink, nameNode)
                    receivedFiles.add(file.token, () => {
                        itemElement.classList.remove('withReceiveLink')
                        itemElement.removeChild(receiveLink)
                    })

                },
            }

        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}
