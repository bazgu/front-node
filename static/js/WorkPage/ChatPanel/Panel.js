function WorkPage_ChatPanel_Panel (playAudio, formatBytes,
    formatText, isLocalLink, sentFiles, receivedFiles, username,
    session, contactUsername, profile, overrideProfile,
    profileListener, sendSmileyListener, sendContactListener,
    removeListener, closeListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_ChatPanel_Panel'

    const title = WorkPage_ChatPanel_Title(contactUsername,
        profile, overrideProfile, profileListener, removeListener)

    const messagesPanel = WorkPage_ChatPanel_MessagesPanel(
        playAudio, formatBytes, formatText, isLocalLink, sentFiles,
        receivedFiles, username, session, contactUsername,
        () => {
            barClassList.add('hidden')
        },
        () => {
            barClassList.remove('hidden')
        },
        sendSmileyListener, sendContactListener,
        closeListener, invalidSessionListener,
        connectionErrorListener, crashListener, serviceErrorListener
    )

    const closeButton = WorkPage_ChatPanel_CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const barElement = Div(classPrefix + '-bar')
    barElement.append(title.element)
    barElement.append(closeButton.element)

    const barClassList = barElement.classList

    const element = Div(classPrefix)
    element.append(barElement)
    element.append(messagesPanel.element)

    return {
        element: element,
        editContactProfile: title.editContactProfile,
        editTimezone: messagesPanel.editTimezone,
        focus: messagesPanel.focus,
        overrideContactProfile: title.overrideContactProfile,
        receiveFileMessage: messagesPanel.receiveFileMessage,
        receiveTextMessage: messagesPanel.receiveTextMessage,
        sendFileMessage: messagesPanel.sendFileMessage,
        sendTextMessage: messagesPanel.sendTextMessage,
        destroy () {
            title.destroy()
            messagesPanel.destroy()
        },
        disable () {
            title.disable()
            closeButton.disable()
            messagesPanel.disable()
        },
        enable () {
            title.enable()
            closeButton.enable()
            messagesPanel.enable()
        },
    }

}
