function WorkPage_ChatPanel_SendingFileMessage (formatBytes, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_ChatPanel_SendingFileMessage'

    const element = Div(classPrefix)

    const requests = []

    return {
        element: element,
        type: 'SendingFile',
        add (file, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            const name = file.name,
                size = file.size

            const sizeElement = Div(classPrefix + '-size')
            sizeElement.append(formatBytes(size))

            const itemElement = Div(classPrefix + '-item')
            itemElement.append(name)
            itemElement.append(document.createElement('br'))
            itemElement.append(sizeElement)

            element.append(itemElement)

            const url = 'data/sendFileMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&name=' + encodeURIComponent(name) +
                '&size=' + encodeURIComponent(size)

            const request = GetJson(url, () => {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, () => {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy () {
            requests.forEach(request => {
                request.abort()
            })
        },
    }

}
