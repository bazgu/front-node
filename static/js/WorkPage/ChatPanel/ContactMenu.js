function WorkPage_ChatPanel_ContactMenu (profileListener, removeListener) {

    function resetSelected () {
        selectedElement.classList.remove('active')
    }

    const classPrefix = 'WorkPage_ChatPanel_ContactMenu'

    const profileItemElement = Div(classPrefix + '-item')
    profileItemElement.append('Profile')
    profileItemElement.addEventListener('click', profileListener)

    const removeItemElement = Div(classPrefix + '-item')
    removeItemElement.append('Remove')
    removeItemElement.addEventListener('click', removeListener)

    const element = Div(classPrefix)
    element.append(profileItemElement)
    element.append(removeItemElement)

    let selectedElement = null

    return {
        element: element,
        openSelected () {
            if (selectedElement === profileItemElement) profileListener()
            else if (selectedElement === removeItemElement) removeListener()
        },
        reset () {
            if (selectedElement === null) return
            resetSelected()
            selectedElement = null
        },
        selectDown () {
            if (selectedElement === profileItemElement) {
                resetSelected()
                selectedElement = removeItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = profileItemElement
            }
            selectedElement.classList.add('active')
        },
        selectUp () {
            if (selectedElement === removeItemElement) {
                resetSelected()
                selectedElement = profileItemElement
            } else {
                if (selectedElement !== null) resetSelected()
                selectedElement = removeItemElement
            }
            selectedElement.classList.add('active')
        },
    }
}
