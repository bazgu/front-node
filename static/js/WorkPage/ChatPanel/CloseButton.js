function WorkPage_ChatPanel_CloseButton (listener) {

    const button = document.createElement('button')
    button.className = 'WorkPage_ChatPanel_CloseButton'
    button.title = 'Close'
    button.addEventListener('click', listener)

    return {
        element: button,
        addEventListener (name, listener) {
            button.addEventListener(name, listener)
        },
        disable () {
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        focus () {
            button.focus()
        },
    }

}
