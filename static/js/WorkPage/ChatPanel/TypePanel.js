function WorkPage_ChatPanel_TypePanel (
    focusListener, blurListener, typeListener, fileListener,
    sendSmileyListener, sendContactListener, closeListener) {

    function submit () {
        const value = textarea.value
        if (/\S/.test(value)) typeListener(value)
        textarea.value = ''
    }

    const classPrefix = 'WorkPage_ChatPanel_TypePanel'

    const textarea = document.createElement('textarea')
    textarea.className = classPrefix + '-textarea'
    textarea.placeholder = 'Type a message here'
    textarea.addEventListener('focus', () => {
        moreButton.typeFocus()
        sendButtonClassList.add('typeFocused')
        focusListener()
    })
    textarea.addEventListener('blur', () => {
        moreButton.typeBlur()
        sendButtonClassList.remove('typeFocused')
        blurListener()
    })
    textarea.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            submit()
        } else if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const sendButton = document.createElement('button')
    sendButton.className = classPrefix + '-sendButton GreenButton'
    sendButton.append('Send')
    sendButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    sendButton.addEventListener('click', () => {
        submit()
        textarea.focus()
    })

    const sendButtonClassList = sendButton.classList

    const moreButton = WorkPage_ChatPanel_MoreButton(closeListener,
        fileListener, sendSmileyListener, sendContactListener)

    const element = Div(classPrefix)
    element.append(textarea)
    element.append(moreButton.element)
    element.append(sendButton)

    return {
        element: element,
        destroy: moreButton.destroy,
        disable () {
            textarea.disabled = true
            sendButton.disabled = true
            moreButton.disable()
        },
        enable () {
            textarea.disabled = false
            sendButton.disabled = false
            moreButton.enable()
        },
        focus () {
            textarea.focus()
        },
    }

}
