function WorkPage_ChatPanel_SentTextMessage (disableEvent,
    enableEvent, formatText, isLocalLink, time, timezoneOffset) {

    function getTimeString () {
        return TwoDigitPad(date.getUTCHours()) +
            ':' + TwoDigitPad(date.getUTCMinutes())
    }

    const date = new Date(time + timezoneOffset)

    const classPrefix = 'WorkPage_ChatPanel_SentTextMessage'

    const timeNode = TextNode(getTimeString())

    const timeElement = Div(classPrefix + '-time')
    timeElement.append(timeNode)

    const element = Div(classPrefix)
    element.append(timeElement)

    return {
        element: element,
        minute: Minute(time),
        time: time,
        type: 'sentText',
        add (text) {
            const itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, link => {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.append(itemElement)
            return {}
        },
        editTimezone (timezoneOffset) {
            date.setTime(time + timezoneOffset)
            timeNode.nodeValue = getTimeString()
        },
    }

}
