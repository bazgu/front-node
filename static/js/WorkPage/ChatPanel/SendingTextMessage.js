function WorkPage_ChatPanel_SendingTextMessage (
    disableEvent, enableEvent, formatText, isLocalLink, session,
    contactUsername, doneListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    const classPrefix = 'WorkPage_ChatPanel_SendingTextMessage'

    const element = Div(classPrefix)

    const requests = []

    return {
        element: element,
        type: 'SendingText',
        add (text, sentListener) {

            function removeRequest () {
                requests.splice(requests.indexOf(request), 1)
            }

            const itemElement = Div(classPrefix + '-item')
            WorkPage_ChatPanel_MessageText(formatText, isLocalLink, itemElement, text, link => {
                disableEvent.addListener(link.disable)
                enableEvent.addListener(link.enable)
            })
            element.append(itemElement)

            const url = 'data/sendTextMessage' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(contactUsername) +
                '&text=' + encodeURIComponent(text)

            const request = GetJson(url, () => {

                removeRequest()

                if (request.status !== 200) {
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    invalidSessionListener()
                    return
                }

                element.removeChild(itemElement)
                sentListener(response)
                if (requests.length === 0) doneListener()

            }, () => {
                removeRequest()
                connectionErrorListener()
            })

            requests.push(request)

        },
        destroy () {
            requests.forEach(request => {
                request.abort()
            })
        },
    }

}
