function WorkPage_ChatPanel_MessageText (formatText,
    isLocalLink, element, text, linkListener) {

    formatText(text, text => {
        element.append(text)
    }, link => {

        const a = document.createElement('a')
        a.href = link
        if (!isLocalLink(link)) a.target = '_blank'
        a.className = 'WorkPage_ChatPanel_MessageText-link'
        a.append(link)

        const span = document.createElement('span')
        span.className = 'WorkPage_ChatPanel_MessageText-disabledLink'
        span.append(link)

        element.append(a)

        linkListener({
            disable () {
                element.replaceChild(span, a)
            },
            enable () {
                element.replaceChild(a, span)
            },
        })

    }, (text, icon) => {
        element.append(WorkPage_ChatPanel_Smiley(text, icon))
    })

}
