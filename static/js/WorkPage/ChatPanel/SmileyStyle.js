function WorkPage_ChatPanel_SmileyStyle (smileys, getResourceUrl) {

    let style = ''
    smileys.array.forEach(smiley => {
        const file = smiley.file
        style +=
            '.WorkPage_ChatPanel_Smiley.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}
