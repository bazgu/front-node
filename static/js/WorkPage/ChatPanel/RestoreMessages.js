function WorkPage_ChatPanel_RestoreMessages (messages,
    session, addReceivedFileMessage, addReceivedTextMessage,
    addSentFileMessage, addSentTextMessage) {

    messages.array.forEach(message => {
        const type = message[0]
        if (type === 'receivedFile') {
            const file = message[1]
            const part = addReceivedFileMessage(file, message[2], message[3])
            if (session.files[file.token] !== undefined) part.showReceive()
        } else if (type === 'receivedText') {
            addReceivedTextMessage(message[1], message[2], message[3])
        } else if (type === 'sentFile') {
            addSentFileMessage(message[1], message[2], message[3])
        } else {
            addSentTextMessage(message[1], message[2], message[3])
        }
    })

}
