function WorkPage_ChatPanel_TitleStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_Title-button {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_Title-button.pressed {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_Title-button.pressed:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-pressed-active.svg') + ')' +
        '}'
}
