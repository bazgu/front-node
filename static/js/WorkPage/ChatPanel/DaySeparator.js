function WorkPage_ChatPanel_DaySeparator (day) {

    const date = new Date(day * 24 * 60 * 60 * 1000)

    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December']

    const dateString = monthNames[date.getUTCMonth()] + ' ' +
        date.getUTCDate() + ', ' + date.getUTCFullYear()

    const element = Div('WorkPage_ChatPanel_DaySeparator')
    element.append(dateString)

    return { element: element }

}
