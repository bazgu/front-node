function WorkPage_ChatPanel_MoreButtonStyle (getResourceUrl) {
    return '.WorkPage_ChatPanel_MoreButton-button {' +
            'background-image: url(' + getResourceUrl('img/arrow-up.svg') + ')' +
        '}' +
        '.WorkPage_ChatPanel_MoreButton-button:active {' +
            'background-image: url(' + getResourceUrl('img/arrow-up-active.svg') + ')' +
        '}'
}
