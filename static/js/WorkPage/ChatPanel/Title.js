function WorkPage_ChatPanel_Title (contactUsername,
    profile, overrideProfile, profileListener, removeListener) {

    function collapse () {
        expanded = false
        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', keyDown)
        button.addEventListener('click', expand)
        element.removeChild(menu.element)
        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)
        menu.reset()
    }

    function createButtonText () {
        return overrideProfile.fullName || profile.fullName || contactUsername
    }

    function expand () {
        expanded = true
        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', keyDown)
        element.append(menu.element)
        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)
    }

    function keyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            menu.openSelected()
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            menu.selectUp()
        } else if (keyCode === 40) {
            e.preventDefault()
            menu.selectDown()
        }
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    let expanded = false

    const classPrefix = 'WorkPage_ChatPanel_Title'

    const menu = WorkPage_ChatPanel_ContactMenu(() => {
        collapse()
        profileListener()
    }, () => {
        collapse()
        removeListener()
    })

    const node = TextNode(createButtonText())

    const buttonTextElement = document.createElement('span')
    buttonTextElement.className = classPrefix + '-buttonText'
    buttonTextElement.append(node)

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.append(buttonTextElement)
    button.addEventListener('click', expand)

    const buttonClassList = button.classList

    const element = Div(classPrefix)
    element.append(button)

    return {
        element: element,
        destroy () {
            if (expanded) collapse()
        },
        disable () {
            if (expanded) collapse()
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        editContactProfile (_profile) {
            profile = _profile
            node.nodeValue = createButtonText()
        },
        overrideContactProfile (_overrideProfile) {
            overrideProfile = _overrideProfile
            node.nodeValue = createButtonText()
        },
    }

}
