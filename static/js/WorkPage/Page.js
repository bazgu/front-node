function WorkPage_Page (smileys, formatBytes, formatText, isLocalLink,
    renderUserLink, timezoneList, username, session, signOutListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        destroyEvent.emit()
        sidePanel.destroy()
        locationHash.destroy()
        removeEventListener('beforeunload', windowBeforeUnload)
    }

    function destroyAndConnectionError () {
        destroy()
        connectionErrorListener()
    }

    function destroyAndCrash () {
        destroy()
        crashListener()
    }

    function destroyAndInvalidSession () {
        destroy()
        invalidSessionListener()
    }

    function destroyAndServiceError () {
        destroy()
        serviceErrorListener()
    }

    function disableBackground () {
        sidePanel.disable()
        if (chatPanel !== null) chatPanel.disable()
    }

    function editProfile (newProfile) {
        profile = newProfile
        sidePanel.editProfile(profile)
        editProfileEvent.emit([profile])
    }

    function enableBackground () {
        sidePanel.enable()
        if (chatPanel !== null) chatPanel.enable()
    }

    function showAccountPage (readyCallback) {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.editProfile.removeListener(page.editProfile)
            destroyEvent.removeListener(page.destroy)
        }

        const page = AccountPage_Page(timezoneList, userLink, username, session, profile, profile => {
            hide()
            enableBackground()
            editProfile(profile)
        }, () => {
            hide()
            ;(() => {

                function hide () {
                    element.removeChild(page.element)
                    destroyEvent.removeListener(page.destroy)
                }

                const page = ChangePasswordPage_Page(session, () => {
                    hide()
                    showAccountPage(page => {
                        page.focusChangePassword()
                    })
                }, () => {
                    hide()
                    enableBackground()
                }, () => {
                    hide()
                    destroyAndInvalidSession()
                })

                element.append(page.element)
                page.focus()
                destroyEvent.addListener(page.destroy)

            })()
        }, () => {
            hide()
            enableBackground()
        }, () => {
            hide()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        readyCallback(page)
        pullMessages.events.editProfile.addListener(page.editProfile)
        destroyEvent.addListener(page.destroy)

    }

    function showAddContactPage (showInvite) {

        function hide () {
            element.removeChild(page.element)
            destroyEvent.removeListener(page.destroy)
        }

        const page = AddContactPage_Page(showInvite, session, username, checkUsername => {

            if (checkUsername === lowerUsername) {
                hide()
                ;(() => {

                    function hide () {
                        element.removeChild(page.element)
                        editProfileEvent.removeListener(page.editProfile)
                    }

                    const page = FoundSelfPage(username, profile, () => {
                        hide()
                        showAddContactPage(showInvite)
                    }, () => {
                        hide()
                        enableBackground()
                    })

                    element.append(page.element)
                    page.focus()
                    editProfileEvent.addListener(page.editProfile)

                })()
                return true
            }

            const contact = sidePanel.getLowerContact(checkUsername)
            if (contact === undefined) return false

            hide()
            ;(() => {

                function contactRemoveListener () {
                    hide()
                    enableBackground()
                }

                function hide () {
                    element.removeChild(page.element)
                    contact.editProfileEvent.removeListener(page.editProfile)
                    contact.removeEvent.removeListener(contactRemoveListener)
                }

                const page = FoundContactPage(contact.username, contact.getProfile(), () => {
                    hide()
                    enableBackground()
                    contact.ensureSelected()
                }, () => {
                    hide()
                    showAddContactPage(showInvite)
                }, () => {
                    hide()
                    enableBackground()
                })

                element.append(page.element)
                page.focus()
                contact.editProfileEvent.addListener(page.editProfile)
                contact.removeEvent.addListener(contactRemoveListener)

            })()

            return true

        }, (username, profile) => {
            hide()
            ;(() => {

                function hide () {
                    element.removeChild(page.element)
                    pullMessages.events.publicProfile.removeListener(publicProfileListener)
                    destroyEvent.removeListener(page.destroy)
                }

                function publicProfileListener (eventUsername, profile) {
                    if (username === eventUsername) page.editProfile(profile)
                }

                const page = FoundUserPage(session, username, profile, contactData => {
                    hide()
                    enableBackground()
                    sidePanel.mergeContact(username, contactData)
                    sidePanel.getContact(username).ensureSelected()
                }, () => {
                    hide()
                    showAddContactPage(showInvite)
                    unwatchPublicProfile(username)
                }, () => {
                    hide()
                    enableBackground()
                    unwatchPublicProfile(username)
                }, () => {
                    hide()
                    destroyAndInvalidSession()
                })

                element.append(page.element)
                page.focus()
                pullMessages.events.publicProfile.addListener(publicProfileListener)
                destroyEvent.addListener(page.destroy)

            })()
        }, () => {
            hide()
            enableBackground()
        }, () => {
            hide()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        page.focus()
        destroyEvent.addListener(page.destroy)

    }

    function unwatchPublicProfile (username) {

        function destroy () {
            request.abort()
        }

        const url = 'data/unwatchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        const request = GetJson(url, () => {

            destroyEvent.removeListener(destroy)

            if (request.status !== 200) {
                destroyAndServiceError()
                return
            }

            const response = request.response
            if (response === null) {
                destroyAndCrash()
                return
            }

            if (response !== true) destroyAndServiceError()

        }, () => {
            destroyEvent.removeListener(destroy)
            destroyAndConnectionError()
        })

        destroyEvent.addListener(destroy)

    }

    function windowBeforeUnload (e) {
        if (sentFiles.empty() && numFeedingFiles === 0) return
        e.preventDefault()
        const message = 'Your file transfers will be aborted if you close.'
        e.returnValue = message
        return message
    }

    let numFeedingFiles = 0
    const userLink = renderUserLink(username)
    const lowerUsername = username.toLowerCase()
    let profile = session.profile

    const destroyEvent = WorkPage_Event(),
        editProfileEvent = WorkPage_Event()

    const sentFiles = WorkPage_SentFiles(),
        receivedFiles = WorkPage_ReceivedFiles()

    let chatPanel = null

    const classPrefix = 'WorkPage_Page'

    const sidePanel = WorkPage_SidePanel_Panel(formatBytes, formatText, isLocalLink, renderUserLink, sentFiles, receivedFiles, username, session, () => {
        disableBackground()
        showAccountPage(page => {
            page.focus()
        })
    }, () => {

        function hide () {
            element.removeChild(page.element)
        }

        const page = SignOutPage(() => {

            pullMessages.abort()
            hide()
            destroy()
            signOutListener()

            GetJson('data/signOut?token=' + encodeURIComponent(session.token))

        }, () => {
            hide()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()

    }, showInvite => {
        disableBackground()
        showAddContactPage(showInvite)
    }, contact => {
        chatPanel = contact.chatPanel
        element.append(chatPanel.element)
        chatPanel.focus()
    }, () => {
        chatPanel.destroy()
        element.removeChild(chatPanel.element)
        chatPanel = null
    }, contact => {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.editProfileEvent.removeListener(page.editProfile)
            contact.overrideProfileEvent.removeListener(page.overrideProfile)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        const page = EditContactPage_Page(
            timezoneList, session, contact.userLink, contact.username,
            contact.getProfile(), contact.getOverrideProfile(),
            profile => {
                hide()
                enableBackground()
                contact.overrideProfile(profile)
            },
            () => {
                hide()
                enableBackground()
            },
            () => {
                hide()
                destroyAndInvalidSession()
            }
        )

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.editProfileEvent.addListener(page.editProfile)
        contact.overrideProfileEvent.addListener(page.overrideProfile)
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, (contact, send) => {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        const page = SmileysPage_Page(smileys, text => {
            hide()
            enableBackground()
            send(text)
        }, () => {
            hide()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, (contact, contacts, send) => {

        function contactRemoveListener () {
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            sidePanel.addContactEvent.removeListener(page.addContact)
            sidePanel.reorderEvent.removeListener(page.reorder)
        }

        const page = ContactSelectPage_Page(contacts, contacts => {
            hide()
            enableBackground()
            send(contacts)
        }, () => {
            hide()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        sidePanel.addContactEvent.addListener(page.addContact)
        sidePanel.reorderEvent.addListener(page.reorder)

    }, contact => {

        function contactRemoveListener () {
            page.destroy()
            hide()
            enableBackground()
        }

        function hide () {
            element.removeChild(page.element)
            contact.removeEvent.removeListener(contactRemoveListener)
            destroyEvent.removeListener(page.destroy)
        }

        const page = RemoveContactPage(contact.username, session, () => {
            hide()
            enableBackground()
            sidePanel.removeContact(contact)
        }, () => {
            hide()
            enableBackground()
        }, () => {
            hide()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        contact.removeEvent.addListener(contactRemoveListener)
        destroyEvent.addListener(page.destroy)

    }, destroyAndInvalidSession, destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)

    const element = Div(classPrefix + ' Page')
    element.append(sidePanel.element)

    const contactRequests = WorkPage_ContactRequests(
        element, username, session, disableBackground, enableBackground,
        (username, contactData) => {
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        },
        destroyAndInvalidSession, destroyAndConnectionError,
        destroyAndCrash, destroyAndServiceError
    )
    for (const i in session.requests) {
        contactRequests.add(i, session.requests[i])
    }

    const pullMessages = WorkPage_PullMessages(session,
        sidePanel.endProcessing, destroyAndInvalidSession,
        destroyAndConnectionError, destroyAndCrash, destroyAndServiceError)
    pullMessages.events.addContact.addListener((username, profile) => {
        const contact = sidePanel.getContact(username)
        if (contact === undefined) sidePanel.mergeContact(username, profile)
        contactRequests.remove(username)
    })
    pullMessages.events.addRequest.addListener(contactRequests.add)
    pullMessages.events.editContactProfile.addListener((username, profile) => {
        const contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.editProfile(profile)
    })
    pullMessages.events.editProfile.addListener(editProfile)
    pullMessages.events.editRequest.addListener(contactRequests.edit)
    pullMessages.events.ignoreRequest.addListener(contactRequests.remove)
    pullMessages.events.offline.addListener(sidePanel.offline)
    pullMessages.events.online.addListener(sidePanel.online)
    pullMessages.events.overrideContactProfile.addListener((username, overrideProfile) => {
        const contact = sidePanel.getContact(username)
        if (contact !== undefined) contact.overrideProfile(overrideProfile)
    })
    pullMessages.events.receiveFileMessage.addListener((username, file, time, token) => {
        session.files[file.token] = {}
        sidePanel.receiveFileMessage(username, file, time, token)
        contactRequests.receiveFileMessage(username, file, time, token)
    })
    pullMessages.events.receiveTextMessage.addListener((username, text, time, token) => {
        sidePanel.receiveTextMessage(username, text, time, token)
        contactRequests.receiveTextMessage(username, text, time, token)
    })
    pullMessages.events.removeContact.addListener(username => {
        const contact = sidePanel.getContact(username)
        if (contact !== undefined) sidePanel.removeContact(contact)
    })
    pullMessages.events.removeFile.addListener(token => {
        receivedFiles.remove(token)
        delete session.files[token]
    })
    pullMessages.events.removeRequest.addListener(contactRequests.remove)
    pullMessages.events.requestFile.addListener(token => {
        numFeedingFiles++
        sentFiles.feed(token, () => {
            numFeedingFiles--
        })
    })
    pullMessages.events.sendFileMessage.addListener(sidePanel.sendFileMessage)
    pullMessages.events.sendTextMessage.addListener(sidePanel.sendTextMessage)

    const locationHash = WorkPage_LocationHash(session, checkUsername => {

        if (checkUsername === lowerUsername) {
            ;(() => {

                function hide () {
                    element.removeChild(page.element)
                    editProfileEvent.removeListener(page.editProfile)
                    locationHash.changeEvent.removeListener(hide)
                    enableBackground()
                }

                const page = SimpleSelfPage(username, profile, () => {
                    hide()
                    locationHash.reset()
                })

                element.append(page.element)
                page.focus()
                disableBackground()
                editProfileEvent.addListener(page.editProfile)
                locationHash.changeEvent.addListener(hide)

            })()
            return true
        }

        const contact = sidePanel.getLowerContact(checkUsername)
        if (contact === undefined) return false

        ;(() => {

            function contactRemoveListener () {
                hideAndResetHash()
                enableBackground()
            }

            function hide () {
                element.removeChild(page.element)
                contact.editProfileEvent.removeListener(page.editProfile)
                contact.removeEvent.removeListener(contactRemoveListener)
                locationHash.changeEvent.removeListener(hide)
            }

            function hideAndResetHash () {
                hide()
                locationHash.reset()
            }

            const page = SimpleContactPage(contact.username, contact.getProfile(), () => {
                hideAndResetHash()
                enableBackground()
                contact.ensureSelected()
            }, () => {
                hideAndResetHash()
                enableBackground()
            })

            element.append(page.element)
            page.focus()
            disableBackground()
            contact.editProfileEvent.addListener(page.editProfile)
            contact.removeEvent.addListener(contactRemoveListener)
            locationHash.changeEvent.addListener(hide)

        })()

        return true

    }, (username, profile) => {

        function hide () {
            element.removeChild(page.element)
            pullMessages.events.publicProfile.removeListener(publicProfileListener)
            destroyEvent.removeListener(page.destroy)
            locationHash.changeEvent.removeListener(hide)
        }

        function hideAndResetHash () {
            hide()
            locationHash.reset()
        }

        function publicProfileListener (eventUsername, profile) {
            if (username === eventUsername) page.editProfile(profile)
        }

        const page = SimpleUserPage(session, username, profile, contactData => {
            hideAndResetHash()
            enableBackground()
            sidePanel.mergeContact(username, contactData)
            sidePanel.getContact(username).ensureSelected()
        }, () => {
            hideAndResetHash()
            enableBackground()
            unwatchPublicProfile(username)
        }, () => {
            hideAndResetHash()
            destroyAndInvalidSession()
        })

        element.append(page.element)
        page.focus()
        disableBackground()
        pullMessages.events.publicProfile.addListener(publicProfileListener)
        destroyEvent.addListener(page.destroy)
        locationHash.changeEvent.addListener(hide)

    }, username => {

        const page = NoSuchUserPage(username, () => {
            element.removeChild(page.element)
            locationHash.reset()
            enableBackground()
        })

        element.append(page.element)
        page.focus()
        disableBackground()

    }, destroyAndConnectionError, destroyAndCrash, destroyAndInvalidSession, destroyAndServiceError)

    pullMessages.process(session.messages)
    locationHash.check()
    addEventListener('beforeunload', windowBeforeUnload)

    return { element: element }

}
