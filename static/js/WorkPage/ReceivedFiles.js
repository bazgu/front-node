function WorkPage_ReceivedFiles () {

    const map = Object.create(null)

    return {
        add (token, removeCallback) {
            map[token] = { remove: removeCallback }
        },
        remove (token) {
            map[token].remove()
            delete map[token]
        },
    }

}
