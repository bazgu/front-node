function WorkPage_ContactRequests (element, username, session,
    showListener, hideListener, addContactListener, invalidSessionListener,
    connectionErrorListener, crashListener, serviceErrorListener) {

    function destroy () {
        if (page !== null) page.destroy()
    }

    function getMessages (username) {
        if (username === visibleUsername) return visibleMessages
        const request = requests[username]
        if (request !== undefined) return request.messages
    }

    function requestError () {
        destroy()
        connectionErrorListener()
    }

    function show (username, request) {
        visibleUsername = username
        visibleMessages = request.messages
        page = ContactRequestPage(session, username, request.profile, contactData => {
            addContactListener(username, contactData)
            showNext()
        }, showNext, () => {

            page.destroy()
            showNext()

            const url = 'data/removeRequest' +
                '?token=' + encodeURIComponent(session.token) +
                '&username=' + encodeURIComponent(username)

            const request = GetJson(url, () => {

                if (request.status !== 200) {
                    destroy()
                    serviceErrorListener()
                    return
                }

                const response = request.response
                if (response === null) {
                    destroy()
                    crashListener()
                    return
                }

                if (response === 'INVALID_TOKEN') {
                    destroy()
                    invalidSessionListener()
                    return
                }

            }, requestError)

        }, () => {
            element.removeChild(page.element)
            invalidSessionListener()
        })
        element.append(page.element)
    }

    function showNext () {

        element.removeChild(page.element)

        if (Object.keys(requests).length === 0) {
            page = null
            visibleUsername = null
            hideListener()
            return
        }

        const username = Object.keys(requests)[0]
        const request = requests[username]
        delete requests[username]
        show(username, request)

    }

    const requests = Object.create(null)

    let page = null,
        visibleUsername = null,
        visibleMessages = null

    return {
        add (requestUsername, profile) {

            const request = {
                profile: profile,
                messages: WorkPage_Messages(username, requestUsername),
            }

            if (page === null) {
                show(requestUsername, request)
                showListener()
            } else {
                requests[requestUsername] = request
            }

        },
        edit (username, profile) {
            if (visibleUsername === username) page.edit(profile)
            else requests[username].profile = profile
        },
        receiveFileMessage (username, file, time, token) {
            const messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedFile', file, time, token])
        },
        receiveTextMessage (username, text, time, token) {
            const messages = getMessages(username)
            if (messages === undefined) return
            messages.push(['receivedText', text, time, token])
        },
        remove (username) {
            if (visibleUsername === username) {
                page.destroy()
                showNext()
            } else {
                delete requests[username]
            }
        },
    }

}
