function WorkPage_Messages (username, contactUsername) {

    const key = username + '$' + contactUsername

    const array = (() => {

        let array
        try {
            array = localStorage[key]
        } catch (e) {
            return []
        }

        if (array === undefined) return []

        try {
            return JSON.parse(array)
        } catch (e) {
            return []
        }

    })()

    return {
        array,
        push (message) {
            array.push(message)
            if (array.length > 1024) array.shift()
            try {
                localStorage[key] = JSON.stringify(array)
            } catch (e) {
            }
        },
    }

}
