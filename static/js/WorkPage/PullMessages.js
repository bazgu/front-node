function WorkPage_PullMessages (session, processedListener,
    invalidSessionListener, connectionErrorListener,
    crashListener, serviceErrorListener) {

    function process (messages) {
        if (messages.length === 0) return
        messages.forEach(message => {
            events[message[0]].emit(message[1])
        })
        processedListener()
    }

    function pull () {

        const request = GetJson(url, () => {

            if (request.status !== 200) {
                serviceErrorListener()
                return
            }

            const response = request.response
            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== 'NOTHING_TO_PULL') process(response)
            pull()

        }, connectionErrorListener)

        abortFunction = () => {
            request.abort()
        }

    }

    let abortFunction
    const url = 'data/pullMessages?token=' + encodeURIComponent(session.token)
    pull()

    const events = Object.create(null)
    events.addContact = WorkPage_Event()
    events.addRequest = WorkPage_Event()
    events.editContactProfile = WorkPage_Event()
    events.editContactProfileAndOffline = WorkPage_Event()
    events.editContactProfileAndOnline = WorkPage_Event()
    events.editProfile = WorkPage_Event()
    events.editRequest = WorkPage_Event()
    events.ignoreRequest = WorkPage_Event()
    events.offline = WorkPage_Event()
    events.online = WorkPage_Event()
    events.overrideContactProfile = WorkPage_Event()
    events.publicProfile = WorkPage_Event()
    events.receiveFileMessage = WorkPage_Event()
    events.receiveTextMessage = WorkPage_Event()
    events.removeContact = WorkPage_Event()
    events.removeFile = WorkPage_Event()
    events.removeRequest = WorkPage_Event()
    events.requestFile = WorkPage_Event()
    events.sendFileMessage = WorkPage_Event()
    events.sendTextMessage = WorkPage_Event()

    events.editContactProfileAndOffline.addListener((username, profile) => {
        events.editContactProfile.emit([username, profile])
        events.offline.emit([username])
    })
    events.editContactProfileAndOnline.addListener((username, profile) => {
        events.editContactProfile.emit([username, profile])
        events.online.emit([username])
    })

    return {
        events: events,
        process: process,
        abort () {
            abortFunction()
        },
    }

}
