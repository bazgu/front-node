function WorkPage_Event () {
    const listeners = []
    return {
        addListener (listener) {
            listeners.push(listener)
        },
        emit (args) {
            listeners.forEach(listener => {
                listener.apply(null, args)
            })
        },
        removeListener (listener) {
            const index = listeners.indexOf(listener)
            if (index === -1) throw 0
            listeners.splice(index, 1)
        },
    }
}
