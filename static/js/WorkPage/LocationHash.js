function WorkPage_LocationHash (session, checkContactListener,
    publicProfileListener, noSuchUserListener, connectionErrorListener,
    crashListener, invalidSessionListener, serviceErrorListener) {

    function check () {

        abort()
        changeEvent.emit()

        const username = location.hash.substr(1)
        if (username === '') return

        const lowerUsername = username.toLowerCase()
        if (checkContactListener(lowerUsername) === true) return

        const url = 'data/watchPublicProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(lowerUsername)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                serviceErrorListener()
                return
            }

            if (response === null) {
                crashListener()
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response === 'INVALID_USERNAME') {
                noSuchUserListener(username)
                return
            }

            publicProfileListener(response.username, response.profile)

        }, connectionErrorListener)

    }

    function abort () {
        if (request === null) return
        request.abort()
        request = null
    }

    let request = null

    const changeEvent = WorkPage_Event()

    addEventListener('hashchange', check)

    return {
        changeEvent: changeEvent,
        check: check,
        destroy () {
            removeEventListener('hashchange', check)
            abort()
        },
        reset () {
            removeEventListener('hashchange', check)
            location.hash = ''
            addEventListener('hashchange', check)
        },
    }

}
