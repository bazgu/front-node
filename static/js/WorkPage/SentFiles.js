function WorkPage_SentFiles () {

    const map = Object.create(null)
    let size = 0

    return {
        add (token, feedCallback) {
            map[token] = { feed: feedCallback }
            size++
        },
        empty () {
            return size === 0
        },
        feed (token, endCallback) {
            map[token].feed(endCallback)
            delete map[token]
            size--
        },
    }

}
