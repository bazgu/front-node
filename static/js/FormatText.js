function FormatText (smileys) {

    const smileyComponents = smileys.array.map(smiley => {
        return smiley.text.replace(/([()|*])/g, '\\$1')
    }).sort((a, b) => {
        return a.length > b.length ? -1 : 1
    })

    const pattern = '(?:(' + smileyComponents.join('|') + ')|https?://\\S+)'

    return (text, textCallback, linkCallback, smileyCallback) => {

        let index = 0
        const regex = new RegExp(pattern, 'gi')
        while (true) {

            const match = regex.exec(text)
            if (match === null) break

            const matchIndex = match.index
            if (matchIndex > index) textCallback(text.substring(index, matchIndex))

            const content = match[0]
            if (match[1] === undefined) linkCallback(content)
            else smileyCallback(content, smileys.map[content.toLowerCase()].file)

            index = matchIndex + content.length

        }
        if (index < text.length) textCallback(text.substring(index, text.length))

    }

}
