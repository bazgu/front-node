function PageStyle (getResourceUrl) {
    return '.Page {' +
            'background-image: url(' + getResourceUrl('img/grass.svg') + '),' +
                ' url(' + getResourceUrl('img/clouds.svg') + ')' +
        '}'
}
