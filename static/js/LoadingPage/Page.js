function LoadingPage_Page () {

    const classPrefix = 'LoadingPage'

    const contentElement = Div(classPrefix + '-content')
    contentElement.append('Reconnecting...')

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(contentElement)

    return { element: element }

}
