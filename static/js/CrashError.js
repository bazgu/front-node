function CrashError () {

    const element = Div('FormError')
    element.append('Something went wrong.')
    element.append(document.createElement('br'))
    element.append('Please, try again.')

    return { element: element }

}
