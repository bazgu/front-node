(getResourceUrl => {

    function checkSession () {

        function hide () {
            body.removeChild(page.element)
        }

        const page = LoadingPage_Page()
        body.append(page.element)

        CheckSession((username, warningCallback) => {
            hide()
            showWelcomePage(username, page => {
                page.focus()
            }, warningCallback)
        }, (username, session) => {
            hide()
            showWorkPage(username, session)
        }, () => {
            hide()
            showConnectionErrorPage()
        }, () => {
            hide()
            showCrashPage()
        }, () => {
            hide()
            showServiceErrorPage()
        })

    }

    function showConnectionErrorPage () {
        const page = ConnectionErrorPage(() => {
            body.removeChild(page.element)
            checkSession()
        })
        body.append(page.element)
        page.focus()
    }

    function showCrashPage () {
        const page = CrashPage(() => {
            location.reload(true)
        })
        body.append(page.element)
        page.focus()
    }

    function showServiceErrorPage () {
        const page = ServiceErrorPage(() => {
            location.reload(true)
        })
        body.append(page.element)
        page.focus()
    }

    function showWelcomePage (username, readyCallback, warningCallback) {

        function hide () {
            body.removeChild(page.element)
            page.destroy()
            removeEventListener('storage', windowStorage)
        }

        function windowStorage (e) {
            if (e.storageArea !== localStorage || e.key !== 'token') return
            const token = e.newValue
            if (token === null) return
            hide()
            checkSession()
        }

        const page = WelcomePage_Page(username, () => {
            hide()
            ;(() => {

                function hide () {
                    body.removeChild(page.element)
                }

                const page = SignUpPage_Page(timezoneList, () => {
                    hide()
                    showWelcomePage(username, page => {
                        page.focusSignUp()
                    })
                }, (username, session) => {

                    hide()
                    showWorkPage(username, session)

                    ;(() => {
                        const page = AccountReadyPage(() => {
                            body.removeChild(page.element)
                        })
                        body.append(page.element)
                        page.focus()
                    })()

                })
                body.append(page.element)
                page.focus()

            })()
        }, () => {
            hide()
            ;(() => {

                function hide () {
                    body.removeChild(page.element)
                }

                const page = PrivacyPage(() => {
                    hide()
                    showWelcomePage(username, page => {
                        page.focusPrivacy()
                    })
                })
                body.append(page.element)
                page.focus()

            })()
        }, (username, session) => {
            hide()
            showWorkPage(username, session)
        }, warningCallback)

        body.append(page.element)
        readyCallback(page)
        addEventListener('storage', windowStorage)

    }

    function showWorkPage (username, session) {

        function hide () {
            document.title = initialTitle
            body.removeChild(page.element)
        }

        try {
            localStorage.username = username
            localStorage.token = session.token
        } catch (e) {
        }

        const page = WorkPage_Page(
            smileys, formatBytes, formatText, isLocalLink,
            renderUserLink, timezoneList, username, session,
            () => {

                try {
                    delete localStorage.token
                } catch (e) {
                }

                hide()
                showWelcomePage(username, page => {
                    page.focus()
                }, element => {
                    element.append('Signed out successfully.')
                })

            },
            () => {
                hide()
                checkSession()
            },
            () => {
                hide()
                showConnectionErrorPage()
            },
            () => {
                hide()
                showCrashPage()
            },
            () => {
                hide()
                showServiceErrorPage()
            }
        )
        body.append(page.element)

    }

    const body = document.body
    const initialTitle = document.title

    const formatBytes = FormatBytes(),
        smileys = Smileys(),
        formatText = FormatText(smileys),
        isLocalLink = IsLocalLink(),
        renderUserLink = RenderUserLink(),
        timezoneList = Timezone_List()

    ;(() => {
        const style = document.createElement('style')
        style.type = 'text/css'
        style.innerHTML = AccountPage_PrivacySelectStyle(getResourceUrl) +
            AccountPage_PrivacySelectItemStyle(getResourceUrl) +
            AccountPage_TimezoneItemStyle(getResourceUrl) +
            CloseButtonStyle(getResourceUrl) +
            ContactSelectPage_ItemStyle(getResourceUrl) +
            EditContactPage_TimezoneItemStyle(getResourceUrl) +
            PageStyle(getResourceUrl) +
            SignUpPage_AdvancedItemStyle(getResourceUrl) +
            SignUpPage_TimezoneItemStyle(getResourceUrl) +
            SmileysPage_PageStyle(smileys, getResourceUrl) +
            WelcomePage_PageStyle(getResourceUrl) +
            WelcomePage_StaySignedInItemStyle(getResourceUrl) +
            WorkPage_ChatPanel_CloseButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_MoreButtonStyle(getResourceUrl) +
            WorkPage_ChatPanel_PanelStyle(getResourceUrl) +
            WorkPage_ChatPanel_SmileyStyle(smileys, getResourceUrl) +
            WorkPage_ChatPanel_TitleStyle(getResourceUrl) +
            WorkPage_SidePanel_ContactStyle(getResourceUrl) +
            WorkPage_SidePanel_PanelStyle(getResourceUrl) +
            WorkPage_SidePanel_TitleStyle(getResourceUrl)
        document.head.append(style)
    })()

    checkSession()

})(getResourceUrl)
