function SmileysPage_PageStyle (smileys, getResourceUrl) {

    let style = ''
    smileys.array.forEach(smiley => {
        const file = smiley.file
        style +=
            '.SmileysPage_Page-button.' + file + ' {' +
                'background-image: url(' + getResourceUrl('img/smiley/' + file + '.svg') + ')' +
            '}'
    })

    return style

}
