function SmileysPage_Page (smileys, selectListener, closeListener) {

    function focusAt (e, x, y) {

        const row = buttons[y]
        if (row === undefined) return

        const button = row[x]
        if (button === undefined) return

        e.preventDefault()
        button.focus()

    }

    const classPrefix = 'SmileysPage_Page'

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Smileys')

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)

    const buttons = smileys.table.map((row, y) => {

        const rowElement = Div(classPrefix + '-row')
        const buttons = row.map((smiley, x) => {

            const text = smiley.text

            const button = document.createElement('button')
            button.className = classPrefix + '-button ' + smiley.file
            button.title = text
            button.addEventListener('click', () => {
                selectListener(text)
            })
            button.addEventListener('keydown', e => {

                if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return

                const keyCode = e.keyCode
                if (keyCode === 27) {
                    e.preventDefault()
                    closeListener()
                    return
                }

                if (keyCode === 37) {
                    focusAt(e, x - 1, y)
                    return
                }

                if (keyCode === 38) {
                    focusAt(e, x, y - 1)
                    return
                }

                if (keyCode === 39) {
                    focusAt(e, x + 1, y)
                    return
                }

                if (keyCode === 40) focusAt(e, x, y + 1)

            })

            rowElement.append(button)
            return button

        })

        frameElement.append(rowElement)
        return buttons

    })

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus () {
            buttons[0][0].focus()
        },
    }

}
