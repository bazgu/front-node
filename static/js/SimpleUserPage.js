function SimpleUserPage (session, username, profile,
    addContactListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        button.disabled = false
        buttonNode.nodeValue = 'Add Contact'

        error = _error
        frameElement.insertBefore(error.element, button)
        button.focus()

    }

    let error = null
    let request = null

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const classPrefix = 'SimpleUserPage'

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const buttonNode = TextNode('Add Contact')

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    button.addEventListener('click', () => {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        let url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        const timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        button.disabled = true
        buttonNode.nodeValue = 'Adding...'

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, requestError)

    })

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        editProfile (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
        focus () {
            button.focus()
        },
    }

}
