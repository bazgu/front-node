function EditContactPage_PhoneItem (profile,
    overrideProfile, changeListener, closeListener) {

    const classPrefix = 'EditContactPage_PhoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Phone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.placeholder = profile.phone
    input.value = overrideProfile.phone
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        disable () {
            input.disabled = true
            input.blur()
        },
        editProfile (profile) {
            input.placeholder = profile.phone
        },
        enable () {
            input.disabled = false
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}
