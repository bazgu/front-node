function EditContactPage_TimezoneItem (timezoneList, profile,
    overrideProfile, changeListener, closeListener) {

    function format (value) {
        if (value === null) return ''
        return Timezone_Format(value)
    }

    const classPrefix = 'EditContactPage_TimezoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Timezone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const emptyNode = TextNode(format(profile.timezone))

    const emptyOption = document.createElement('option')
    emptyOption.append(emptyNode)

    const select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.append(emptyOption)
    timezoneList.forEach(timezone => {
        const option = document.createElement('option')
        option.value = timezone.value
        option.append(timezone.text)
        select.append(option)
    })
    select.value = overrideProfile.timezone
    select.addEventListener('change', changeListener)
    select.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(select)

    return {
        element: element,
        disable () {
            select.disabled = true
            select.blur()
        },
        editProfile (profile) {
            emptyNode.nodeValue = format(profile.timezone)
        },
        enable () {
            select.disabled = false
        },
        getValue () {
            const value = select.value
            if (value === '') return null
            return parseInt(value, 10)
        },
    }

}
