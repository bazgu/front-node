function EditContactPage_TimezoneItemStyle (getResourceUrl) {
    return '.EditContactPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}
