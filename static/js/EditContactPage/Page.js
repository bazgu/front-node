function EditContactPage_Page (timezoneList, session, userLink,
    username, profile, overrideProfile, overrideProfileListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === overrideProfile.fullName &&
            emailItem.getValue() === overrideProfile.email &&
            phoneItem.getValue() === overrideProfile.phone &&
            timezoneItem.getValue() === overrideProfile.timezone
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'EditContactPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const fullNameItem = EditContactPage_FullNameItem(profile,
        overrideProfile, checkChanges, close)

    const emailItem = EditContactPage_EmailItem(profile,
        overrideProfile, checkChanges, close)

    const phoneItem = EditContactPage_PhoneItem(profile,
        overrideProfile, checkChanges, close)

    const timezoneItem = EditContactPage_TimezoneItem(timezoneList,
        profile, overrideProfile, checkChanges, close)

    const linkItem = EditContactPage_LinkItem(userLink, close)

    const saveChangesNode = TextNode('Save Changes')

    const saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.append(saveChangesNode)
    saveChangesButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        fullNameItem.element,
        emailItem.element,
        phoneItem.element,
        timezoneItem.element,
        linkItem.element,
    ]))
    form.append(saveChangesButton)
    form.addEventListener('submit', e => {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const fullName = fullNameItem.getValue()
        const email = emailItem.getValue()
        const phone = phoneItem.getValue()
        const timezone = timezoneItem.getValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        let url = 'data/overrideContactProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&email=' + encodeURIComponent(email) +
            '&phone=' + encodeURIComponent(phone)
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            overrideProfileListener({
                fullName: fullName,
                email: email,
                phone: phone,
                timezone: timezone,
            })

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile (profile) {
            fullNameItem.editProfile(profile)
            emailItem.editProfile(profile)
            phoneItem.editProfile(profile)
        },
        overrideProfile (_overrideProfile) {
            overrideProfile = _overrideProfile
            checkChanges()
        },
    }

}
