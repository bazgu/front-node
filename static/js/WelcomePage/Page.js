function WelcomePage_Page (username, signUpListener,
    privacyListener, signInListener, warningCallback) {

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        staySignedInItem.enable()
        signInButton.disabled = false
        signInNode.nodeValue = 'Sign In'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        signInForm.insertBefore(error.element, signInButton)
        signInButton.focus()
    }

    let error = null
    let warningElement = null
    let request = null

    const classPrefix = 'WelcomePage_Page'

    const usernameItem = WelcomePage_UsernameItem(username)

    const passwordItem = WelcomePage_PasswordItem()

    const signInNode = TextNode('Sign In')

    const signInButton = document.createElement('button')
    signInButton.className = classPrefix + '-signInButton GreenButton'
    signInButton.append(signInNode)

    const staySignedInItem = WelcomePage_StaySignedInItem()

    const signInForm = document.createElement('form')
    signInForm.className = classPrefix + '-signInForm'
    signInForm.append(Page_Blocks('x_small', [
        usernameItem.element,
        passwordItem.element,
        staySignedInItem.element,
    ]))
    signInForm.append(signInButton)
    signInForm.addEventListener('submit', e => {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()

        if (warningElement !== null) {
            frameElement.removeChild(warningElement)
            warningElement = null
        }

        if (error !== null) {
            signInForm.removeChild(error.element)
            error = null
        }

        const username = usernameItem.getValue()
        if (username === null) return

        const password = passwordItem.getValue()
        if (password === null) return

        usernameItem.disable()
        passwordItem.disable()
        staySignedInItem.disable()
        signInButton.disabled = true
        signInNode.nodeValue = 'Signing in...'

        let url = 'data/signIn' +
            '?username=' + encodeURIComponent(username.toLowerCase()) +
            '&password=' + encodeURIComponent(password)
        if (staySignedInItem.isChecked()) url += '&longTerm=true'

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_USERNAME') {
                enableItems()
                usernameItem.showError(errorElement => {
                    errorElement.append('There is no such user.')
                })
                return
            }

            if (response === 'INVALID_PASSWORD') {
                enableItems()
                passwordItem.showError(errorElement => {
                    errorElement.append('The password is incorrect.')
                })
                return
            }

            signInListener(response.username, response.session)

        }, requestError)

    })

    const logoElement = Div(classPrefix + '-logo')

    const logoWrapperElement = Div(classPrefix + '-logoWrapper')
    logoWrapperElement.append(logoElement)

    const signUpButton = document.createElement('button')
    signUpButton.className = classPrefix + '-signUpButton OrangeButton'
    signUpButton.append('Create an Account \u203a')
    signUpButton.addEventListener('click', () => {
        destroy()
        signUpListener()
    })

    const privacy = WelcomePage_Privacy(privacyListener)

    const publicLine = WelcomePage_PublicLine()

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(logoWrapperElement)
    frameElement.append(publicLine.element)
    if (warningCallback !== undefined) {

        warningElement = Div(classPrefix + '-warning')

        warningCallback(warningElement)
        frameElement.append(warningElement)

    }
    frameElement.append(signInForm)
    frameElement.append('New to Bazgu?')
    frameElement.append(document.createElement('br'))
    frameElement.append(signUpButton)
    frameElement.append(privacy.element)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        destroy: publicLine.destroy,
        element: element,
        focusPrivacy: privacy.focus,
        focus () {
            if (username === '') usernameItem.focus()
            else passwordItem.focus()
        },
        focusSignUp () {
            signUpButton.focus()
        },
    }

}
