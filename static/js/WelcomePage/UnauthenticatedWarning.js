function WelcomePage_UnauthenticatedWarning (username) {
    return element => {

        const usernameElement = document.createElement('b')
        usernameElement.className = 'WelcomePage_UnauthenticatedWarning-username'
        usernameElement.append(username)

        element.append('You need to sign in or create an account to contact "')
        element.append(usernameElement)
        element.append('".')

    }
}
