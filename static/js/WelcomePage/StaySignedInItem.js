function WelcomePage_StaySignedInItem () {

    function addListener (listener) {
        clickElement.addEventListener('click', listener)
    }

    function check () {
        checked = true
        buttonClassList.add('checked')
        removeListener(check)
        addListener(uncheck)
        activeListener = uncheck
        button.focus()
    }

    function removeListener (listener) {
        clickElement.removeEventListener('click', listener)
    }

    function uncheck () {
        checked = false
        buttonClassList.remove('checked')
        removeListener(uncheck)
        addListener(check)
        activeListener = check
        button.focus()
    }

    const classPrefix = 'WelcomePage_StaySignedInItem'

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.type = 'button'

    const buttonClassList = button.classList

    const clickElement = Div(classPrefix + '-click')
    clickElement.append(button)
    clickElement.append('Stay signed in')

    const clickClassList = clickElement.classList

    const element = Div(classPrefix)
    element.append(clickElement)

    let checked = true

    let activeListener = check
    addListener(check)

    return {
        element: element,
        disable () {
            clickClassList.add('disabled')
            button.disabled = true
            removeListener(activeListener)
        },
        enable () {
            clickClassList.remove('disabled')
            button.disabled = false
            addListener(activeListener)
        },
        isChecked () {
            return checked
        },
    }

}
