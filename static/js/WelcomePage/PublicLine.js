function WelcomePage_PublicLine () {

    function add (text) {

        const itemElement = Div(classPrefix + '-item')
        itemElement.append(text)

        element.append(itemElement)
        itemElements.push(itemElement)

    }

    function animate () {
        timeout = setTimeout(() => {
            itemElements[selectedIndex].classList.remove('visible')
            timeout = setTimeout(() => {
                selectedIndex = (selectedIndex + 1) % itemElements.length
                itemElements[selectedIndex].classList.add('visible')
                animate()
            }, 600)
        }, 8000)
    }

    let selectedIndex = 0
    const itemElements = []

    const classPrefix = 'WelcomePage_PublicLine'

    const element = Div(classPrefix)
    add('Chat without distractions.')
    add('Send unlimited size files.')
    itemElements[0].classList.add('visible')

    let timeout
    animate()

    return {
        element: element,
        destroy () {
            clearTimeout(timeout)
        },
    }

}
