function WelcomePage_PasswordItem () {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'WelcomePage_PasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (!Password_IsValid(value)) {
                showError(errorElement => {
                    errorElement.append('The password is invalid.')
                })
                return null
            }
            return value
        },
    }

}
