function WelcomePage_Privacy (listener) {

    const classPrefix = 'WelcomePage_Privacy'

    const button = document.createElement('button')
    button.className = classPrefix + '-privacy'
    button.append('Privacy')
    button.addEventListener('click', listener)

    const element = Div('WelcomePage_Privacy')
    element.append(button)

    return {
        element: element,
        focus () {
            button.focus()
        },
    }

}
