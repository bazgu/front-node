function AccountPage_TimezoneItem (timezoneList,
    profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_TimezoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Timezone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.addEventListener('change', changeListener)
    timezoneList.forEach(timezone => {
        const option = document.createElement('option')
        option.value = timezone.value
        option.append(timezone.text)
        select.append(option)
    })
    select.value = profile.timezone
    select.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.timezonePrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(select)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            select.disabled = true
            select.blur()
        },
        enable () {
            privacySelect.enable()
            select.disabled = false
        },
        getValue () {
            return parseInt(select.value, 10)
        },
    }

}
