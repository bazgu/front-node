function AccountPage_EmailItem (profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_EmailItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Email')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.email
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.emailPrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable () {
            privacySelect.enable()
            input.disabled = false
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}
