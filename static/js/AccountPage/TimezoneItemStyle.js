function AccountPage_TimezoneItemStyle (getResourceUrl) {
    return '.AccountPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}
