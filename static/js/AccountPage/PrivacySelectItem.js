function AccountPag_PrivacySelectItem (text, value, clickListener) {

    const element = Div('AccountPage_PrivacySelectItem privacy_' + value)
    element.append(text)
    element.addEventListener('click', clickListener)

    const classList = element.classList

    return {
        element: element,
        deselect () {
            classList.remove('active')
        },
        select () {
            classList.add('active')
        },
    }

}
