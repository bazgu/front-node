function AccountPage_Page (timezoneList, userLink, username, session,
    profile, editProfileListener, changePasswordListener,
    closeListener, invalidSessionListener) {

    function checkChanges () {
        saveChangesButton.disabled =
            fullNameItem.getValue() === profile.fullName &&
            fullNameItem.getPrivacyValue() === profile.fullNamePrivacy &&
            emailItem.getValue() === profile.email &&
            emailItem.getPrivacyValue() === profile.emailPrivacy &&
            phoneItem.getValue() === profile.phone &&
            phoneItem.getPrivacyValue() === profile.phonePrivacy &&
            timezoneItem.getValue() === profile.timezone &&
            timezoneItem.getPrivacyValue() === profile.timezonePrivacy
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        fullNameItem.enable()
        emailItem.enable()
        phoneItem.enable()
        timezoneItem.enable()
        saveChangesButton.disabled = false
        saveChangesNode.nodeValue = 'Save Changes'

        error = _error
        form.insertBefore(error.element, saveChangesButton)
        saveChangesButton.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'AccountPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const fullNameItem = AccountPage_FullNameItem(profile, checkChanges, close)

    const emailItem = AccountPage_EmailItem(profile, checkChanges, close)

    const phoneItem = AccountPage_PhoneItem(profile, checkChanges, close)

    const timezoneItem = AccountPage_TimezoneItem(timezoneList, profile, checkChanges, close)

    const linkItem = AccountPage_LinkItem(userLink, close)

    const saveChangesNode = TextNode('Save Changes')

    const saveChangesButton = document.createElement('button')
    saveChangesButton.disabled = true
    saveChangesButton.className = classPrefix + '-saveChangesButton GreenButton'
    saveChangesButton.append(saveChangesNode)
    saveChangesButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        fullNameItem.element,
        emailItem.element,
        phoneItem.element,
        timezoneItem.element,
        linkItem.element,
    ]))
    form.append(saveChangesButton)
    form.addEventListener('submit', e => {

        e.preventDefault()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const fullName = fullNameItem.getValue(),
            fullNamePrivacy = fullNameItem.getPrivacyValue(),
            email = emailItem.getValue(),
            emailPrivacy = emailItem.getPrivacyValue(),
            phone = phoneItem.getValue(),
            phonePrivacy = phoneItem.getPrivacyValue(),
            timezone = timezoneItem.getValue(),
            timezonePrivacy = timezoneItem.getPrivacyValue()

        fullNameItem.disable()
        emailItem.disable()
        phoneItem.disable()
        timezoneItem.disable()
        saveChangesButton.disabled = true
        saveChangesNode.nodeValue = 'Saving...'

        const url = 'data/editProfile' +
            '?token=' + encodeURIComponent(session.token) +
            '&fullName=' + encodeURIComponent(fullName) +
            '&fullNamePrivacy=' + encodeURIComponent(fullNamePrivacy) +
            '&email=' + encodeURIComponent(email) +
            '&emailPrivacy=' + encodeURIComponent(emailPrivacy) +
            '&phone=' + encodeURIComponent(phone) +
            '&phonePrivacy=' + encodeURIComponent(phonePrivacy) +
            '&timezone=' + encodeURIComponent(timezone) +
            '&timezonePrivacy=' + encodeURIComponent(timezonePrivacy)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            editProfileListener({
                fullName: fullName,
                fullNamePrivacy: fullNamePrivacy,
                email: email,
                emailPrivacy: emailPrivacy,
                phone: phone,
                phonePrivacy: phonePrivacy,
                timezone: timezone,
                timezonePrivacy: timezonePrivacy,
            })

        }, requestError)

    })

    const changePasswordButton = document.createElement('button')
    changePasswordButton.className = classPrefix + '-changePasswordButton OrangeButton'
    changePasswordButton.append('Change Password \u203a')
    changePasswordButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })
    changePasswordButton.addEventListener('click', () => {
        destroy()
        changePasswordListener()
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)
    frameElement.append(changePasswordButton)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus: fullNameItem.focus,
        editProfile (newProfile) {
            profile = newProfile
            checkChanges()
        },
        focusChangePassword () {
            changePasswordButton.focus()
        },
    }

}
