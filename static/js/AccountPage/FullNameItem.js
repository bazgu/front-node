function AccountPage_FullNameItem (profile, changeListener, closeListener) {

    const classPrefix = 'AccountPage_FullNameItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Full name')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = profile.fullName
    input.addEventListener('input', changeListener)
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const privacySelect = AccountPage_PrivacySelect(
        profile.fullNamePrivacy, changeListener, closeListener)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)
    element.append(privacySelect.element)

    return {
        element: element,
        getPrivacyValue: privacySelect.getValue,
        disable () {
            privacySelect.disable()
            input.disabled = true
            input.blur()
        },
        enable () {
            privacySelect.enable()
            input.disabled = false
        },
        focus () {
            input.focus()
        },
        getValue () {
            return CollapseSpaces(input.value)
        },
    }

}
