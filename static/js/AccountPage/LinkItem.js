function AccountPage_LinkItem (userLink, closeListener) {

    const classPrefix = 'AccountPage_LinkItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Link')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.readOnly = true
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.value = userLink
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return { element: element }

}
