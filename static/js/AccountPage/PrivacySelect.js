function AccountPage_PrivacySelect (value, changeListener, closeListener) {

    function add (itemValue, text) {

        function click () {
            if (value !== itemValue) setValue(itemValue)
            collapse()
        }

        const item = AccountPag_PrivacySelectItem(text, itemValue, click)

        menuElement.append(item.element)
        if (value === itemValue) valueIndex = items.length
        items.push({
            click: click,
            value: itemValue,
            deselect: item.deselect,
            select: item.select,
        })

    }

    function collapse () {

        buttonClassList.remove('pressed')
        button.removeEventListener('click', collapse)
        button.removeEventListener('keydown', expandedKeyDown)
        button.addEventListener('click', expand)
        button.addEventListener('keydown', collapsedKeyDown)

        element.removeChild(menuElement)

        removeEventListener('focus', windowFocus, true)
        removeEventListener('mousedown', windowMouseDown)

        if (selectedIndex !== null) {
            items[selectedIndex].deselect()
            selectedIndex = null
        }

    }

    function collapsedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 27) {
            e.preventDefault()
            closeListener()
        } else if (keyCode === 38) {
            e.preventDefault()
            if (valueIndex === 0) return
            valueIndex--
            setValue(items[valueIndex].value)
        } else if (keyCode === 40) {
            e.preventDefault()
            if (valueIndex === items.length - 1) return
            valueIndex++
            setValue(items[valueIndex].value)
        }
    }

    function expand () {

        buttonClassList.add('pressed')
        button.removeEventListener('click', expand)
        button.removeEventListener('keydown', collapsedKeyDown)
        button.addEventListener('click', collapse)
        button.addEventListener('keydown', expandedKeyDown)

        element.append(menuElement)

        addEventListener('focus', windowFocus, true)
        addEventListener('mousedown', windowMouseDown)

    }

    function expandedKeyDown (e) {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        const keyCode = e.keyCode
        if (keyCode === 13) {
            e.preventDefault()
            if (selectedIndex !== null) {
                items[selectedIndex].click()
            }
        } else if (keyCode === 27) {
            e.preventDefault()
            collapse()
        } else if (keyCode === 38) {
            e.preventDefault()
            if (selectedIndex === null) {
                selectedIndex = items.length - 1
                items[selectedIndex].select()
            } else if (selectedIndex === 0) {
                items[0].deselect()
                selectedIndex = items.length - 1
                items[selectedIndex].select()
            } else {
                items[selectedIndex].deselect()
                selectedIndex--
                items[selectedIndex].select()
            }
        } else if (keyCode === 40) {
            e.preventDefault()
            if (selectedIndex === null) {
                selectedIndex = 0
                items[0].select()
            } else if (selectedIndex === items.length - 1) {
                items[selectedIndex].deselect()
                selectedIndex = 0
                items[0].select()
            } else {
                items[selectedIndex].deselect()
                selectedIndex++
                items[selectedIndex].select()
            }
        }
    }

    function setValue (newValue) {
        buttonClassList.remove('privacy_' + value)
        value = newValue
        buttonClassList.add('privacy_' + value)
        changeListener()
    }

    function windowFocus (e) {
        const target = e.target
        if (target === window || IsChildElement(element, target)) return
        collapse()
    }

    function windowMouseDown (e) {
        if (e.button !== 0) return
        if (IsChildElement(element, e.target)) return
        collapse()
    }

    const items = []
    let valueIndex
    let selectedIndex = null

    const classPrefix = 'AccountPage_PrivacySelect'

    const button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button privacy_' + value
    button.addEventListener('click', expand)
    button.addEventListener('keydown', collapsedKeyDown)

    const buttonClassList = button.classList

    const menuElement = Div(classPrefix + '-menu')
    add('private', 'Me')
    add('contacts', 'Contacts')
    add('public', 'Anyone')

    const element = Div(classPrefix)
    element.append(button)

    return {
        element: element,
        disable () {
            button.disabled = true
        },
        enable () {
            button.disabled = false
        },
        getValue () {
            return value
        },
    }

}
