function Timezone_Format (value) {

    let sign
    if (value > 0) {
        sign = '+'
    } else if (value < 0) {
        sign = '-'
        value = -value
    } else {
        sign = '\xb1'
    }

    const hours = TwoDigitPad(Math.floor(value / 60))

    return sign + hours + ':' + TwoDigitPad(value % 60)

}
