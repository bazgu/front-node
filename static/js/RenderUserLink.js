function RenderUserLink () {
    const prefix = location.protocol + '//' +
        location.host + location.pathname + '#'
    return username => {
        return prefix + username
    }
}
