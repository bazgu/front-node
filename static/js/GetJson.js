function GetJson (url, loadCallback, errorCallback) {
    const request = new XMLHttpRequest
    request.open('get', url)
    request.responseType = 'json'
    request.onerror = errorCallback
    request.onload = loadCallback
    request.send()
    return request
}
