function CloseButtonStyle (getResourceUrl) {
    return '.CloseButton {' +
            'background-image: url(' + getResourceUrl('img/close.svg') + ')' +
        '}'
}
