function RemoveContactPage (username, session,
    removeListener, closeListener, invalidSessionListener) {

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {

        removeButton.disabled = false
        cancelButton.disabled = false
        removeNode.nodeValue = 'Remove Contact'

        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
        removeButton.focus()

    }

    let error = null
    let request = null

    const classPrefix = 'RemoveContactPage'

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('Are you sure you want to remove "')
    textElement.append(usernameElement)
    textElement.append('" from your contacts?')

    const removeNode = TextNode('Remove Contact')

    const removeButton = document.createElement('button')
    removeButton.className = classPrefix + '-removeButton GreenButton'
    removeButton.append(removeNode)
    removeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    removeButton.addEventListener('click', () => {

        if (error !== null) {
            frameElement.removeChild(error.element)
            error = null
        }

        removeButton.disabled = true
        cancelButton.disabled = true
        removeNode.nodeValue = 'Removing...'

        const url = 'data/removeContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            if (response !== true) {
                showError(CrashError())
                return
            }

            removeListener()

        }, requestError)

    })

    const cancelButton = document.createElement('button')
    cancelButton.className = classPrefix + '-cancelButton OrangeButton'
    cancelButton.append('Cancel')
    cancelButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    cancelButton.addEventListener('click', closeListener)

    const buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.append(removeButton)
    buttonsElement.append(cancelButton)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(textElement)
    frameElement.append(buttonsElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        focus () {
            removeButton.focus()
        },
    }

}
