function PrivacyPage (back) {

    const classPrefix = 'PrivacyPage'

    const backButton = BackButton(back)

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Privacy Policy')

    const text =
        'We:\n' +
        '* Encrypt your connection using SSL.\n' +
        '* Keep your account details.\n' +
        '* Keep your contact list.\n' +
        '* Queue your messages until delivered.\n' +
        '* Proxy your file transfers.\n' +
        'What don\'t:\n' +
        '* Censor you communication.\n' +
        '* Store your chat history.\n' +
        '* Disclose your data to anyone.\n' +
        '* Sell your data.\n' +
        '* Show ads.'

    const textElement = Div(classPrefix + '-text')
    textElement.append(text)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(titleElement)
    frameElement.append(textElement)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus: backButton.focus,
    }

}
