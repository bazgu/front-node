function ContactRequestPage (session, username, profile,
    addContactListener, ignoreListener, closeListener, invalidSessionListener) {

    function clearError () {
        if (error === null) return
        frameElement.removeChild(error.element)
        error = null
    }

    function close () {
        destroy()
        closeListener()
    }

    function destroy () {
        if (request === null) return
        request.abort()
        request = null
    }

    function disableItems () {
        addContactButton.disabled = true
        ignoreButton.disabled = true
    }

    function showAddError (error) {
        showError(error)
        addContactNode.nodeValue = 'Add Contact'
        addContactButton.focus()
    }

    function showError (_error) {
        addContactButton.disabled = false
        ignoreButton.disabled = false
        error = _error
        frameElement.insertBefore(error.element, buttonsElement)
    }

    function showIgnoreError (error) {
        showError(error)
        ignoreNode.nodeValue = 'Ignore'
        ignoreButton.focus()
    }

    let error = null
    let request = null

    const classPrefix = 'ContactRequestPage'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append(username)

    const usernameElement = document.createElement('b')
    usernameElement.className = classPrefix + '-text-username'
    usernameElement.append(username)

    const textElement = Div(classPrefix + '-text')
    textElement.append('"')
    textElement.append(usernameElement)
    textElement.append(TextNode(
        '" has added you to his/her contacts.' +
        ' Would you like to add him/her to your contacts?'
    ))

    const addContactNode = TextNode('Add Contact')

    const addContactButton = document.createElement('button')
    addContactButton.className = classPrefix + '-addContactButton GreenButton'
    addContactButton.append(addContactNode)
    addContactButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    addContactButton.addEventListener('click', () => {

        clearError()
        disableItems()
        addContactNode.nodeValue = 'Adding...'

        let url = 'data/addContact' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username) +
            '&fullName=' + encodeURIComponent(profile.fullName) +
            '&email=' + encodeURIComponent(profile.email) +
            '&phone=' + encodeURIComponent(profile.phone)

        const timezone = profile.timezone
        if (timezone !== null) url += '&timezone=' + timezone

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showAddError(ServiceError())
                return
            }

            if (response === null) {
                showAddError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            addContactListener(response)

        }, () => {
            request = null
            showAddError(ConnectionError())
        })

    })

    const ignoreNode = TextNode('Ignore')

    const ignoreButton = document.createElement('button')
    ignoreButton.className = classPrefix + '-ignoreButton OrangeButton'
    ignoreButton.append(ignoreNode)
    ignoreButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })
    ignoreButton.addEventListener('click', () => {

        clearError()
        disableItems()
        ignoreNode.nodeValue = 'Ignoring...'

        const url = 'data/ignoreRequest' +
            '?token=' + encodeURIComponent(session.token) +
            '&username=' + encodeURIComponent(username)

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showIgnoreError(ServiceError())
                return
            }

            if (response === null) {
                showIgnoreError(CrashError())
                return
            }

            if (response === 'INVALID_TOKEN') {
                invalidSessionListener()
                return
            }

            ignoreListener(response)

        }, () => {
            request = null
            showIgnoreError(ConnectionError())
        })

    })

    const userInfoPanel = UserInfoPanel_Panel(profile)

    const buttonsElement = Div(classPrefix + '-buttons')
    buttonsElement.append(addContactButton)
    buttonsElement.append(ignoreButton)

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(userInfoPanel.element)
    frameElement.append(textElement)
    frameElement.append(buttonsElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        destroy: destroy,
        element: element,
        edit (newProfile) {
            profile = newProfile
            userInfoPanel.edit(profile)
        },
    }

}
