function FormatBytes () {
    const suffixes = ['KB', 'MB', 'GB', 'TB', 'PB']
    return function FormatBytes (size) {

        let suffix = 'B'
        let index = 0
        while (true) {
            if (size < 1024) break
            size = size / 1024
            suffix = suffixes[index]
            index++
            if (index === suffixes.length) break
        }

        return size.toFixed(1).replace(/\.?0+$/, '') + ' ' + suffix

    }
}
