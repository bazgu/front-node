function UserInfoPanel_PhoneItem (profile) {

    let value = profile.phone

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Phone: ')

    const valueNode = TextNode(value)

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit (profile) {
            value = profile.phone
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}
