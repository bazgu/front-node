function UserInfoPanel_FullNameItem (profile) {

    let value = profile.fullName

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Full name: ')

    const valueNode = TextNode(value)

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === '') classList.add('hidden')

    return {
        element: element,
        edit (profile) {
            value = profile.fullName
            if (value === '') classList.add('hidden')
            else classList.remove('hidden')
            valueNode.nodeValue = value
        },
    }

}
