function UserInfoPanel_TimezoneItem (profile) {

    let value = profile.timezone

    const classPrefix = 'UserInfoPanel_Panel-item'

    const labelElement = document.createElement('span')
    labelElement.className = classPrefix + '-label'
    labelElement.append('Timezone: ')

    const valueNode = TextNode('')

    const valueElement = document.createElement('span')
    valueElement.className = classPrefix + '-value'
    valueElement.append(valueNode)

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(valueElement)

    const classList = element.classList
    if (value === null) classList.add('hidden')
    else valueNode.nodeValue = Timezone_Format(value)

    return {
        element: element,
        edit (profile) {
            value = profile.timezone
            if (value === null) {
                classList.add('hidden')
                valueNode.nodeValue = ''
            } else {
                classList.remove('hidden')
                valueNode.nodeValue = Timezone_Format(value)
            }
        },
    }

}
