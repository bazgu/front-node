function UserInfoPanel_Panel (profile) {

    function isEmpty (profile) {
        return profile.fullName === '' && profile.email === '' &&
            profile.phone === '' && profile.timezone === null
    }

    const fullNameItem = UserInfoPanel_FullNameItem(profile)

    const emailItem = UserInfoPanel_EmailItem(profile)

    const phoneItem = UserInfoPanel_PhoneItem(profile)

    const timezoneItem = UserInfoPanel_TimezoneItem(profile)

    const emptyElement = Div('UserInfoPanel_Panel-empty')
    emptyElement.append('No more information available')

    const emptyClassList = emptyElement.classList
    if (!isEmpty(profile)) emptyClassList.add('hidden')

    const element = Div('UserInfoPanel_Panel')
    element.append(fullNameItem.element)
    element.append(emailItem.element)
    element.append(phoneItem.element)
    element.append(timezoneItem.element)
    element.append(emptyElement)

    return {
        element: element,
        edit (profile) {

            fullNameItem.edit(profile)
            emailItem.edit(profile)
            phoneItem.edit(profile)
            timezoneItem.edit(profile)

            if (isEmpty(profile)) emptyClassList.remove('hidden')
            else emptyClassList.add('hidden')

        },
    }

}
