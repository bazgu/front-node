function ServiceError () {

    const element = Div('FormError')
    element.append('Operation failed.')
    element.append(document.createElement('br'))
    element.append('Please, try again.')

    return { element: element }

}
