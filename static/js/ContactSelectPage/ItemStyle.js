function ContactSelectPage_ItemStyle (getResourceUrl) {
    return '.ContactSelectPage_Item-button.selected {' +
            'background-image: url(' + getResourceUrl('img/checked/normal.svg') + ')' +
        '}' +
        '.ContactSelectPage_Item-button.selected:active {' +
            'background-image: url(' + getResourceUrl('img/checked/active.svg') + ')' +
        '}'
}
