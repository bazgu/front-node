function ContactSelectPage_Page (contacts, selectListener, closeListener) {

    function addContact (contact) {

        const username = contact.username

        const item = ContactSelectPage_Item(contact, () => {
            selectedContacts.push(item.contact)
            if (selectedContacts.length === 1) button.disabled = false
        }, () => {
            selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            if (selectedContacts.length === 0) button.disabled = true
        }, () => {

            if (item.isSelected()) {
                selectedContacts.splice(selectedContacts.indexOf(item.contact), 1)
            }

            delete itemsMap[username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            listElement.removeChild(item.element)

        }, close)

        itemsMap[username] = item
        itemsArray.push(item)
        listElement.append(item.element)

    }

    function close () {
        itemsArray.forEach(item => {
            item.destroy()
        })
        closeListener()
    }

    const itemsMap = Object.create(null)
    const itemsArray = []
    const selectedContacts = []

    const classPrefix = 'ContactSelectPage_Page'

    const closeButton = CloseButton(close)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Select Contact')

    const listElement = Div(classPrefix + '-list')
    contacts.forEach(addContact)

    const button = document.createElement('button')
    button.className = classPrefix + '-button GreenButton'
    button.disabled = true
    button.append('Send')
    button.addEventListener('click', () => {
        selectListener(selectedContacts)
    })
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            close()
        }
    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(listElement)
    frameElement.append(button)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) close()
    })

    return {
        addContact: addContact,
        element: element,
        focus () {
            itemsArray[0].focus()
        },
        reorder (contact, index) {

            const item = itemsMap[contact.username]
            itemsArray.splice(itemsArray.indexOf(item), 1)
            itemsArray.splice(index, 0, item)

            const nextItem = itemsArray[index + 1]
            if (nextItem === undefined) {
                listElement.append(item.element)
            } else {
                listElement.insertBefore(item.element, nextItem.element)
            }

            item.update()

        },
    }

}
