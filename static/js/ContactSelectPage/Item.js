function ContactSelectPage_Item (contact, selectListener,
    deselectListener, removeListener, closeListener) {

    function deselect () {
        selected = false
        button.classList.remove('selected')
        clickElement.removeEventListener('click', deselect)
        clickElement.addEventListener('click', select)
        deselectListener()
    }

    function select () {
        selected = true
        button.classList.add('selected')
        clickElement.removeEventListener('click', select)
        clickElement.addEventListener('click', deselect)
        selectListener()
    }

    let selected = false

    const classPrefix = 'ContactSelectPage_Item'

    const button = document.createElement('button')
    button.className = classPrefix + '-button'
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const clickNode = TextNode(contact.getDisplayName())

    const clickElement = Div(classPrefix + '-click')
    clickElement.append(button)
    clickElement.addEventListener('click', select)
    clickElement.append(clickNode)

    const element = Div(classPrefix)
    element.append(clickElement)

    contact.removeEvent.addListener(removeListener)

    return {
        contact: contact,
        element: element,
        destroy () {
            contact.removeEvent.removeListener(removeListener)
        },
        focus () {
            button.focus()
        },
        isSelected () {
            return selected
        },
        update () {
            clickNode.nodeValue = contact.getDisplayName()
        },
    }

}
