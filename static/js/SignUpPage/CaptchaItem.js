function SignUpPage_CaptchaItem (loadListener, backListener) {

    function disable () {
        input.disabled = true
        input.blur()
    }

    function enable () {
        input.disabled = false
    }

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function requestError () {
        request = null
        imageElement.removeChild(loadingNode)
        showCaptchaError()
    }

    function load () {

        disable()

        request = GetJson('data/captcha', () => {

            const status = request.status,
                response = request.response

            request = null
            imageElement.removeChild(loadingNode)

            if (status !== 200) {
                showCaptchaError()
                return
            }

            if (response === null) {
                showCaptchaError()
                return
            }

            enable()
            setCaptcha(response)
            loadListener()

        }, requestError)

    }

    function setCaptcha (captcha) {
        imageElement.style.backgroundImage = 'url(' + captcha.image + ')'
        token = captcha.token
    }

    function showCaptchaError () {

        const reloadButton = document.createElement('button')
        reloadButton.type = 'button'
        reloadButton.className = classPrefix + '-reloadButton GreenButton'
        reloadButton.append('Reload')
        reloadButton.addEventListener('click', () => {
            imageBarElement.removeChild(reloadButton)
            imageElement.removeChild(errorNode)
            imageElement.append(loadingNode)
            imageClassList.remove('error')
            load()
        })

        const errorNode = TextNode('Failed')

        imageBarElement.append(reloadButton)
        imageElement.append(errorNode)
        imageClassList.add('error')

    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null
    let request = null

    let token = null

    const classPrefix = 'SignUpPage_CaptchaItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Verification')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const loadingNode = TextNode('Loading...')

    const imageElement = Div(classPrefix + '-image')
    imageElement.append(loadingNode)

    const imageClassList = imageElement.classList

    const imageBarElement = Div(classPrefix + '-imageBar')
    imageBarElement.append(imageElement)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'text'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(imageBarElement)
    element.append(input)

    load()

    return {
        disable: disable,
        element: element,
        enable: enable,
        showError: showError,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        destroy () {
            if (request === null) return
            request.abort()
            request = null
        },
        getValue () {

            const value = input.value.replace(/\s+/g, '')
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }

            return {
                value: value,
                token: token,
            }

        },
        setNewCaptcha (newCaptcha) {
            setCaptcha(newCaptcha)
            input.value = ''
        },
    }

}
