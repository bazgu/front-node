function SignUpPage_Page (timezoneList, backListener, signUpListener) {

    function back () {
        destroy()
        backListener()
    }

    function destroy () {
        captchaItem.destroy()
        if (request === null) return
        request.abort()
        request = null
    }

    function enableItems () {
        usernameItem.enable()
        passwordItem.enable()
        repeatPasswordItem.enable()
        captchaItem.enable()
        advancedPanel.enable()
        button.disabled = false
        buttonNode.nodeValue = 'Sign Up'
    }

    function requestError () {
        request = null
        showError(ConnectionError())
    }

    function showError (_error) {
        enableItems()
        error = _error
        form.insertBefore(error.element, button)
        button.focus()
    }

    let error = null
    let request = null

    const classPrefix = 'SignUpPage_Page'

    const backButton = BackButton(back)

    const titleElement = document.createElement('h1')
    titleElement.className = classPrefix + '-title'
    titleElement.append('Create an Account')

    const usernameItem = SignUpPage_UsernameItem(back)

    const passwordItem = SignUpPage_PasswordItem(back)

    const repeatPasswordItem = SignUpPage_RepeatPasswordItem(back)

    const captchaItem = SignUpPage_CaptchaItem(() => {
        button.disabled = false
    }, back)

    const advancedPanel = SignUpPage_AdvancedPanel(timezoneList, back)

    const advancedItem = SignUpPage_AdvancedItem(advancedPanel.show, advancedPanel.hide, back)

    const buttonNode = TextNode('Sign Up')

    const button = document.createElement('button')
    button.disabled = true
    button.className = classPrefix + '-button GreenButton'
    button.append(buttonNode)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            back()
        }
    })

    const form = document.createElement('form')
    form.className = classPrefix + '-form'
    form.append(Page_Blocks('x_small', [
        usernameItem.element,
        passwordItem.element,
        repeatPasswordItem.element,
        captchaItem.element,
        advancedItem.element,
        advancedPanel.element,
    ]))
    form.append(button)
    form.addEventListener('submit', e => {

        e.preventDefault()
        usernameItem.clearError()
        passwordItem.clearError()
        repeatPasswordItem.clearError()
        captchaItem.clearError()

        if (error !== null) {
            form.removeChild(error.element)
            error = null
        }

        const username = usernameItem.getValue()
        if (username === null) return

        const password = passwordItem.getValue()
        if (password === null) return

        const repeatPassword = repeatPasswordItem.getValue(password)
        if (repeatPassword === null) return

        const captcha = captchaItem.getValue()
        if (captcha === null) return

        usernameItem.disable()
        passwordItem.disable()
        repeatPasswordItem.disable()
        captchaItem.disable()
        advancedPanel.disable()
        button.disabled = true
        buttonNode.nodeValue = 'Signing up...'

        const url = 'data/signUp?username=' + encodeURIComponent(username) +
            '&password=' + encodeURIComponent(password) +
            '&captcha_token=' + encodeURIComponent(captcha.token) +
            '&captcha_value=' + encodeURIComponent(captcha.value) +
            '&timezone=' + encodeURIComponent(advancedPanel.getTimezoneValue())

        request = GetJson(url, () => {

            const status = request.status,
                response = request.response

            request = null

            if (status !== 200) {
                showError(ServiceError())
                return
            }

            if (response === null) {
                showError(CrashError())
                return
            }

            if (response.error === 'USERNAME_UNAVAILABLE') {
                enableItems()
                usernameItem.showError(errorElement => {
                    errorElement.append('The username is unavailable.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Please, try something else.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('What about ')
                    errorElement.append((() => {
                        const b = document.createElement('b')
                        b.className = classPrefix + '-offerUsername'
                        b.append(response.offerUsername)
                        return b
                    })())
                    errorElement.append('?')
                })
                return
            }

            if (response.error === 'INVALID_CAPTCHA_TOKEN') {
                enableItems()
                captchaItem.setNewCaptcha(response.newCaptcha)
                captchaItem.showError(errorElement => {
                    errorElement.append('The verification has expired.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Please, try again.')
                })
                return
            }

            if (response === 'INVALID_CAPTCHA_VALUE') {
                enableItems()
                captchaItem.showError(errorElement => {
                    errorElement.append('The verification is invalid.')
                })
                return
            }

            signUpListener(username, response)

        }, requestError)

    })

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(backButton.element)
    frameElement.append(titleElement)
    frameElement.append(form)

    const element = Div(classPrefix + ' Page')
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)

    return {
        element: element,
        focus: usernameItem.focus,
    }

}
