function SignUpPage_PasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'SignUpPage_PasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Choose a password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        getValue () {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (Password_IsShort(value)) {
                showError(errorElement => {
                    errorElement.append('The password is too short.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Minimum ' + Password_minLength + ' characters required.')
                })
                return null
            }
            if (Password_ContainsOnlyDigits(value)) {
                showError(errorElement => {
                    errorElement.append('The password is too simple.')
                    errorElement.append(document.createElement('br'))
                    errorElement.append('Use some different symbols.')
                })
                return null
            }
            return value
        },
    }

}
