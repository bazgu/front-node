function SignUpPage_AdvancedItemStyle (getResourceUrl) {
    return '.SignUpPage_AdvancedItem-arrow {' +
            'background-image: url(' + getResourceUrl('img/arrow-collapsed.svg') + ')' +
        '}' +
        '.SignUpPage_AdvancedItem-arrow.expanded {' +
            'background-image: url(' + getResourceUrl('img/arrow-expanded.svg') + ')' +
        '}'
}
