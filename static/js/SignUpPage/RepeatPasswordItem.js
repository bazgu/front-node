function SignUpPage_RepeatPasswordItem (backListener) {

    function hideError () {
        inputClassList.remove('error')
        element.removeChild(errorElement)
        input.removeEventListener('input', inputListener)
    }

    function inputListener () {
        hideError()
        errorElement = null
    }

    function showError (callback) {

        if (errorElement !== null) hideError()
        errorElement = Div(classPrefix + '-error')
        callback(errorElement)

        inputClassList.add('error')
        element.append(errorElement)
        input.addEventListener('input', inputListener)
        input.focus()

    }

    let errorElement = null

    const classPrefix = 'SignUpPage_RepeatPasswordItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Retype the password')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const input = document.createElement('input')
    input.id = label.htmlFor
    input.type = 'password'
    input.className = classPrefix + '-input'
    input.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const inputClassList = input.classList

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(input)

    return {
        element: element,
        clearError () {
            if (errorElement === null) return
            hideError()
            errorElement = null
        },
        disable () {
            input.disabled = true
            input.blur()
        },
        enable () {
            input.disabled = false
        },
        getValue (password) {
            const value = input.value
            if (value === '') {
                showError(EmptyFieldError)
                return null
            }
            if (value !== password) {
                showError(errorElement => {
                    errorElement.append('The passwords doesn\'t match.')
                })
                return null
            }
            return value
        },
    }

}
