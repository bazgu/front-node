function SignUpPage_AdvancedPanel (timezoneList, backListener) {

    const timezoneItem = SignUpPage_TimezoneItem(timezoneList, backListener)

    const element = Div('SignUpPage_AdvancedPanel')
    element.append(timezoneItem.element)

    const classList = element.classList

    return {
        disable: timezoneItem.disable,
        element: element,
        enable: timezoneItem.enable,
        getTimezoneValue: timezoneItem.getValue,
        hide () {
            classList.remove('visible')
        },
        show () {
            classList.add('visible')
        },
    }

}
