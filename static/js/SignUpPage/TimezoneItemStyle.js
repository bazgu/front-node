function SignUpPage_TimezoneItemStyle (getResourceUrl) {
    return '.SignUpPage_TimezoneItem-select {' +
            'background-image: url(' + getResourceUrl('img/arrow.svg') + ')' +
        '}'
}
