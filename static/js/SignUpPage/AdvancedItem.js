function SignUpPage_AdvancedItem (expandListener, collapseListener, backListener) {

    function collapse () {
        button.removeEventListener('click', collapse)
        button.addEventListener('click', expand)
        arrowClassList.remove('expanded')
        collapseListener()
    }

    function expand () {
        button.removeEventListener('click', expand)
        button.addEventListener('click', collapse)
        arrowClassList.add('expanded')
        expandListener()
    }

    const classPrefix = 'SignUpPage_AdvancedItem'

    const arrowElement = Div(classPrefix + '-arrow')

    const arrowClassList = arrowElement.classList

    const button = document.createElement('button')
    button.type = 'button'
    button.className = classPrefix + '-button'
    button.append(arrowElement)
    button.append('Advanced settings')
    button.addEventListener('click', expand)
    button.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const element = Div(classPrefix)
    element.append(button)
    element.append(Div(classPrefix + '-line'))

    return { element: element }

}
