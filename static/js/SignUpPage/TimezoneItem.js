function SignUpPage_TimezoneItem (timezoneList, backListener) {

    const classPrefix = 'SignUpPage_TimezoneItem'

    const label = document.createElement('label')
    label.htmlFor = Math.random()
    label.append('Timezone')

    const labelElement = Div(classPrefix + '-label')
    labelElement.append(label)

    const defaultValue = -(new Date).getTimezoneOffset()

    const defaultOption = document.createElement('option')
    defaultOption.value = defaultValue
    defaultOption.append(Timezone_Format(defaultValue))

    const select = document.createElement('select')
    select.id = label.htmlFor
    select.className = classPrefix + '-select'
    select.append(defaultOption)
    timezoneList.forEach(timezone => {
        const option = document.createElement('option')
        option.value = timezone.value
        option.append(timezone.text)
        select.append(option)
    })
    select.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            backListener()
        }
    })

    const element = Div(classPrefix)
    element.append(labelElement)
    element.append(select)

    return {
        element: element,
        disable () {
            select.disabled = true
            select.blur()
        },
        enable () {
            select.disabled = false
        },
        getValue () {
            return parseInt(select.value, 10)
        },
    }

}
