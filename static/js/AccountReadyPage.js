function AccountReadyPage (closeListener) {

    const classPrefix = 'AccountReadyPage'

    const closeButton = CloseButton(closeListener)
    closeButton.addEventListener('keydown', e => {
        if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) return
        if (e.keyCode === 27) {
            e.preventDefault()
            closeListener()
        }
    })

    const titleElement = Div(classPrefix + '-title')
    titleElement.append('Welcome!')

    const textElement = Div(classPrefix + '-text')
    textElement.append(TextNode(
        'Your account is ready. Add some' +
        ' contacts to start a conversation.'
    ))

    const frameElement = Div(classPrefix + '-frame')
    frameElement.append(closeButton.element)
    frameElement.append(titleElement)
    frameElement.append(textElement)

    const element = Div(classPrefix)
    element.append(Div(classPrefix + '-aligner'))
    element.append(frameElement)
    element.addEventListener('click', e => {
        if (e.button !== 0) return
        if (e.target === element) closeListener()
    })

    return {
        element: element,
        focus () {
            closeButton.focus()
        },
    }

}
