function ConnectionError () {

    const element = Div('FormError')
    element.append('Connection failed.')
    element.append(document.createElement('br'))
    element.append('Please, check your network and try again.')

    return { element: element }

}
