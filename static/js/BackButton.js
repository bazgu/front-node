function BackButton (listener) {

    const element = document.createElement('button')
    element.className = 'BackButton'
    element.append('\u2039 Back')
    element.addEventListener('click', listener)

    return {
        element: element,
        focus () {
            element.focus()
        },
    }

}
