process.chdir(__dirname)

require('./lib/LoadConfig.js')(loadedConfig => {

    var version = process.argv[2]

    var url = require('url')

    var config = require('./config.js'),
        Error404Page = require('./lib/Error404Page.js'),
        IsUsernameValid = require('./lib/IsUsernameValid.js'),
        Log = require('./lib/Log.js'),
        StaticUserPage = require('./lib/StaticUserPage.js')

    var chooseAccountNode = require('./lib/ChooseAccountNode.js')(loadedConfig),
        chooseFileNode = require('./lib/ChooseFileNode.js')(loadedConfig),
        chooseSessionNode = require('./lib/ChooseSessionNode.js')(loadedConfig)

    var pages = Object.create(null)
    pages['/'] = require('./lib/Page/Index.js')
    pages['/debug'] = require('./lib/Page/Debug.js')
    pages['/node'] = require('./lib/Page/Node.js')(version)
    pages['/webappManifest'] = pages['/webapp-manifest'] = require('./lib/Page/WebappManifest.js')
    pages['/data/addContact'] = require('./lib/Page/Data/AddContact.js')(chooseSessionNode)
    pages['/data/captcha'] = require('./lib/Page/Data/Captcha.js')(loadedConfig)
    pages['/data/changePassword'] = require('./lib/Page/Data/ChangePassword.js')(chooseSessionNode)
    pages['/data/editProfile'] = require('./lib/Page/Data/EditProfile.js')(chooseSessionNode)
    pages['/data/feedFile'] = require('./lib/Page/Data/FeedFile.js')(chooseFileNode)
    pages['/data/ignoreRequest'] = require('./lib/Page/Data/IgnoreRequest.js')(chooseSessionNode)
    pages['/data/overrideContactProfile'] = require('./lib/Page/Data/OverrideContactProfile.js')(chooseSessionNode)
    pages['/data/pullMessages'] = require('./lib/Page/Data/PullMessages.js')(chooseSessionNode)
    pages['/data/receiveFile'] = require('./lib/Page/Data/ReceiveFile.js')(chooseFileNode)
    pages['/data/removeContact'] = require('./lib/Page/Data/RemoveContact.js')(chooseSessionNode)
    pages['/data/removeRequest'] = require('./lib/Page/Data/RemoveRequest.js')(chooseSessionNode)
    pages['/data/restoreSession'] = require('./lib/Page/Data/RestoreSession.js')(chooseAccountNode)
    pages['/data/sendFileMessage'] = require('./lib/Page/Data/SendFileMessage.js')(chooseSessionNode)
    pages['/data/sendTextMessage'] = require('./lib/Page/Data/SendTextMessage.js')(chooseSessionNode)
    pages['/data/signIn'] = require('./lib/Page/Data/SignIn.js')(chooseAccountNode)
    pages['/data/signOut'] = require('./lib/Page/Data/SignOut.js')(chooseSessionNode)
    pages['/data/signUp'] = require('./lib/Page/Data/SignUp.js')(chooseAccountNode)
    pages['/data/unwatchPublicProfile'] = require('./lib/Page/Data/UnwatchPublicProfile.js')(chooseSessionNode)
    pages['/data/watchPublicProfile'] = require('./lib/Page/Data/WatchPublicProfile.js')(chooseSessionNode)
    require('./lib/ScanFiles.js')('files', pages)

    require('./lib/Server.js')((req, res) => {
        Log.http(req.method + ' ' + req.url)
        var parsedUrl = url.parse(req.url, true)
        var page = pages[parsedUrl.pathname]
        if (page === undefined) {
            var username = parsedUrl.pathname.substr(1)
            if (IsUsernameValid(username)) {
                StaticUserPage(res, username)
                return
            }
            page = Error404Page
        }
        page(req, res, parsedUrl)
    })

})
