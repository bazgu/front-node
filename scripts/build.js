#!/usr/bin/env node

process.chdir(__dirname)

const fs = require('fs')

const uglifycss = require('uglifycss')
const uglifyjs = require('uglify-js')

process.chdir('..')
const app = require('../lib/App')()
process.chdir('scripts')

const cssContent = app.EchoHtml.CssFiles.map(file => (
    fs.readFileSync('../static/css/' + file + '.css', 'utf8')
)).join('')

const cssLoader =
    "document.head.append((() => {\n" +
    "    const element = document.createElement('style');\n" +
    '    element.innerHTML = ' + JSON.stringify(uglifycss.processString(cssContent, { maxLineLen: 1024 })) + '\n' +
    '    return element\n' +
    "})())\n"

const getResourceUrl =
    'const getResourceUrl = (() => {\n' +
    '    const urls = ' + JSON.stringify((() => {
        const urls = {}
        app.IndexPageSvgFiles.forEach(file => {
            urls[file] = 'data:image/svg+xml;base64,' + fs.readFileSync('../static/' + file).toString('base64')
        })
        return urls
    })()) + '\n' +
    "    return url => urls[url]\n" +
    '})()\n'

const jsContent = '(() => {\n\n' +
    cssLoader + '\n' +
    getResourceUrl + '\n' +
    app.EchoHtml.JsFiles.map(file => (
        fs.readFileSync('../static/js/' + file + '.js', 'utf8')
    )).join('\n') + '\n' +
    '})()\n'

const compressed_js_file = '../static/compressed.js'

fs.writeFileSync('../static/combined.js', jsContent)

const result = uglifyjs.minify({
    'combined.js': jsContent,
}, {
    sourceMap: { url: 'compressed.js.map' },
    output: { max_line_len: 1024 },
})

if (result.code !== fs.readFileSync(compressed_js_file, 'utf8')) {

    fs.writeFileSync(compressed_js_file, result.code)

    const Revisions = app.EchoHtml.Revisions
    Revisions['compressed.js']++

    fs.writeFileSync('../lib/EchoHtml/Revisions.js',
        'module.exports = ' + app.VarExport(Revisions) + '\n')

}
